package shemes.components;

import logic.Execution;
import logic.components.CLK;
import shemes.AbstractSchema;
import util.NameConnector;

public class Memorija extends AbstractSchema {
	CLK clk;
	
	public Memorija() {
		componentName = "Memorija";
		displayName = "Memorija";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new Mem1());
		this.addSubScheme(new Mem2());
		
		clk = new CLK("MEMCLK", 1, 0);
		Execution.addSequentialComponent(clk);

		putComponents();
	}

	public void putComponents() {
		NameConnector.addComponent(componentName, clk.getName(), clk);
	}

}