package shemes.components;

import java.awt.Point;
import java.util.*;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;
import gui.*;

public class Mem2 extends AbstractSchema {

	private OR OR1;
	private NOT NOT1;
	private AND AND1, AND2;
	private REG MEMACC;
	private REG TIME;
	private CMP CMP1;
	private BoolsToInt zeroes;

	public Mem2() {
		componentName = "Mem2";
		displayName = "Upravljacka jedinica";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		OR1 = new OR();
		NOT1 = new NOT();
		AND1 = new AND();
		AND2 = new AND();
		
		MEMACC = new REG(8, "MEMACC");
		MEMACC.getOutPin(0).setIsInt();
		MEMACC.getOutPin(0).setNumOfLines(8);
		MEMACC.initVal(0);

		TIME = new REG(8, "TIME");
		TIME.getOutPin(0).setIsInt();
		TIME.getOutPin(0).setNumOfLines(8);

		CMP1 = new CMP(1);

		zeroes = new BoolsToInt(8, 8);

		putPins();
		putComponents();
	}

	public void initConections() {
		OR1.setInPin(0, NameConnector.getPin("Mem1.rdMEM"));
		OR1.setInPin(1, NameConnector.getPin("Mem1.wrMEM"));
		NOT1.setInPin(0, NameConnector.getPin("Mem2.fcMEM"));
		AND1.setInPin(0, OR1.getOutPin(0));
		AND1.setInPin(1, NOT1.getOutPin(0));

		MEMACC.setInPin(0, new Pin(0, 8, ""));
		MEMACC.setPinLd(new Pin(false, "0"));
		MEMACC.setPinCL(NameConnector.getPin("Mem2.fcMEM"));
		MEMACC.setPinInc(AND1.getOutPin(0));
		MEMACC.setClk((CLK) NameConnector.getComponent("MEMCLK"));

		TIME.initVal(Parameters.memdelay);
		TIME.setClk((CLK) NameConnector.getComponent("MEMCLK"));

		CMP1.setInPin(0, MEMACC.getOutPin(0));
		CMP1.setInPin(1, TIME.getOutPin(0));
		CMP1.setE(OR1.getOutPin(0));
		
		AND2.setInPin(0, NameConnector.getPin("Mem1.rdMEM"));
		AND2.setInPin(1, NameConnector.getPin("Mem2.fcMEM"));

		for (int i = 0; i < 8; i++) {
			zeroes.setInPin(i, new Pin(false, "0"));
		}

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Mem2.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(94, 46));
		points.add(new Point(109, 46));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem1.rdMEM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(94, 57));
		points.add(new Point(109, 57));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem1.wrMEM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 51));
		points.add(new Point(158, 51));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(94, 76));
		points.add(new Point(256, 76));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(139, 76));
		points.add(new Point(139, 63));
		points.add(new Point(153, 63));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem2.fcMEM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(181, 57));
		points.add(new Point(256, 57));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(415, 57));
		points.add(new Point(408, 57));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(332, 29));
		points.add(new Point(332, 48));
		sections.add(points);
		line = new GuiPinLine(sections, zeroes.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(408, 76));
		points.add(new Point(415, 76));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.MEMCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(332, 86));
		points.add(new Point(332, 228));
		points.add(new Point(453, 228));
		sections.add(points);
		line = new GuiPinLine(sections, MEMACC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(604, 56));
		points.add(new Point(597, 56));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(525, 28));
		points.add(new Point(525, 47));
		sections.add(points);
		line = new GuiPinLine(sections, zeroes.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(604, 75));
		points.add(new Point(597, 75));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.MEMCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(525, 85));
		points.add(new Point(525, 133));
		points.add(new Point(402, 133));
		points.add(new Point(402, 192));
		points.add(new Point(453, 192));
		sections.add(points);
		line = new GuiPinLine(sections, TIME.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 191));
		points.add(new Point(555, 191));
		sections.add(points);
		line = new GuiPinLine(sections, CMP1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(94, 136));
		points.add(new Point(105, 136));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem2.fcMEM"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(94, 148));
		points.add(new Point(105, 148));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem1.rdMEM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(128, 142));
		points.add(new Point(136, 142));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(494, 255));
		points.add(new Point(494, 275));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);
		
		// LABELE:
		gui.addLabel(new GuiPinLabel(420, 188, TIME.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(420, 224, MEMACC.getOutPin(0)));

		gui.addLabel(new GuiTextLabel(522, 22, "" + Parameters.memdelay, 12));
	}

	public void putPins() {
		NameConnector.addPin(componentName, "fcMEM", CMP1.getOutPin(1));
		
		NameConnector.addPin(componentName, "MEMout", AND2.getOutPin(0));

	}

	public void putComponents() {
	}

}
