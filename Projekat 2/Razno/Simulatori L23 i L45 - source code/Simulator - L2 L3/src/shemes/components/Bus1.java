package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.AND;
import logic.components.BUS;
import logic.components.BoolsToInt;
import logic.components.CLK;
import logic.components.IntToInt;
import logic.components.MP;
import logic.components.NOT;
import logic.components.OR;
import logic.components.REG;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Bus1 extends AbstractSchema {

	private BUS IBUS1, IBUS2, IBUS3;
	private BUS ABUS, DBUS;
	private BUS NOTRDBUS, NOTWRBUS, NOTFCBUS;
	private TSB TSBMOST1_2, TSBMOST2_1, TSBMDRout1, TSBeMAR, TSBeMDR, TSBrdCPU,
			TSBwrCPU, TSBDWout2, TSB1;
	private TSB TSBMOST1_3, TSBMOST3_2;
	private IntToInt IBUS1low;
	private MP MX1;
	private AND AND1, AND2;
	private OR OR1;
	private REG MAR, MDR, DWH, DWL;
	private NOT NOT1, NOT2, NOT3, NOT4;
	private BoolsToInt DW, DWHint, DWLint;
	
	public Bus1() {
		componentName = "Bus1";
		displayName = "Bus";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {

		IBUS1 = new BUS("IBUS1");
		IBUS1.getOutPin(0).setNumOfLines(16);
		IBUS2 = new BUS("IBUS2");
		IBUS2.getOutPin(0).setNumOfLines(16);
		IBUS3 = new BUS("IBUS3");
		IBUS3.getOutPin(0).setNumOfLines(16);

		ABUS = new BUS("ABUS");
		ABUS.getOutPin(0).setNumOfLines(16);

		DBUS = new BUS("DBUS");
		DBUS.getOutPin(0).setNumOfLines(Parameters.addressableUnit);

		NOTRDBUS = new BUS("NOTRDBUS");
		NOTRDBUS.getOutPin(0).setIsBool();
		NOTWRBUS = new BUS("NOTWRBUS");
		NOTWRBUS.getOutPin(0).setIsBool();
		NOTFCBUS = new BUS("NOTFCBUS");
		NOTFCBUS.getOutPin(0).setIsBool();

		TSBMOST1_2 = new TSB("MOST1_2");
		TSBMOST1_2.getOutPin(0).setNumOfLines(16);

		TSBMOST2_1 = new TSB("MOST2_1");
		TSBMOST2_1.getOutPin(0).setNumOfLines(16);

		TSBMOST1_3 = new TSB("MOST1_3");
		TSBMOST1_3.getOutPin(0).setNumOfLines(16);

		TSBMOST3_2 = new TSB("MOST3_2");
		TSBMOST3_2.getOutPin(0).setNumOfLines(16);

		TSBMDRout1 = new TSB("MDRout1");
		TSBMDRout1.getOutPin(0).setNumOfLines(Parameters.addressableUnit);

		TSBeMAR = new TSB("eMAR");
		TSBeMAR.getOutPin(0).setNumOfLines(16);

		TSBeMDR = new TSB("eMDR");
		TSBeMDR.getOutPin(0).setNumOfLines(Parameters.addressableUnit);

		TSBrdCPU = new TSB("rdCPU");
		TSBrdCPU.getOutPin(0).setNumOfLines(1);
		TSBrdCPU.getOutPin(0).setIsBool();

		TSBwrCPU = new TSB("wrCPU");
		TSBwrCPU.getOutPin(0).setNumOfLines(1);
		TSBwrCPU.getOutPin(0).setIsBool();

		TSBDWout2 = new TSB("DWout2");
		TSBDWout2.getOutPin(0).setNumOfLines(16);

		IBUS1low = new IntToInt(16, 8, false, false);// sa 16 na 8 linija za MDR
														// i DW

		MX1 = new MP(2);
		MX1.getOutPin(0).setIsInt();
		MX1.getOutPin(0).setNumOfLines(Parameters.addressableUnit);

		AND1 = new AND();
		AND2 = new AND();
		OR1 = new OR();

		MAR = new REG(1, "MAR");
		MAR.getOutPin(0).setIsInt();
		MAR.getOutPin(0).setNumOfLines(16);

		MDR = new REG(1, "MDR");
		MDR.getOutPin(0).setIsInt();
		MDR.getOutPin(0).setNumOfLines(Parameters.addressableUnit);

		DWH = new REG(8, "DWH");
		DWL = new REG(8, "DWL");

		TSB1 = new TSB("NOT_FCBUS");
		TSB1.getOutPin(0).setNumOfLines(1);
		TSB1.getOutPin(0).setIsBool();
		
		NOT1 = new NOT();
		NOT2 = new NOT();
		NOT3 = new NOT();
		NOT4 = new NOT();

		DW = new BoolsToInt(16, 16);
		DWHint = new BoolsToInt(8, 8);
		DWLint = new BoolsToInt(8, 8);

		putPins();
		putComponents();
	}

	public void initConections() {

		TSBMOST1_2.setInPin(0, IBUS1.getOutPin(0));
		TSBMOST1_2.setE(NameConnector.getPin("Oper1.MOST1_2"));
		this.addOnIBUS2(TSBMOST1_2.getOutPin(0));

		TSBMOST2_1.setInPin(0, IBUS2.getOutPin(0));
		TSBMOST2_1.setE(NameConnector.getPin("Oper1.MOST2_1"));
		this.addOnIBUS1(TSBMOST2_1.getOutPin(0));

		TSBMOST1_3.setInPin(0, IBUS1.getOutPin(0));
		TSBMOST1_3.setE(NameConnector.getPin("Oper1.MOST1_3"));
		this.addOnIBUS3(TSBMOST1_3.getOutPin(0));

		TSBMOST3_2.setInPin(0, IBUS3.getOutPin(0));
		TSBMOST3_2.setE(NameConnector.getPin("Oper1.MOST3_2"));
		this.addOnIBUS2(TSBMOST3_2.getOutPin(0));

		IBUS1low.setInPin(0, IBUS1.getOutPin(0));

		MX1.setInPin(0, DBUS.getOutPin(0));
		MX1.setInPin(1, IBUS3.getOutPin(0));
		MX1.setCtrl(0, NameConnector.getPin("Oper1.mxMDR"));

		TSB1.setInPin(0, NameConnector.getPin("Bus1.NOTFCBUS"));
		NOT1.setInPin(0, TSB1.getOutPin(0));

		AND1.setInPin(0, NameConnector.getPin("Oper1.rdCPU"));
		AND1.setInPin(1, NOT1.getOutPin(0));

		OR1.setInPin(0, NameConnector.getPin("Oper1.ldMDR"));
		OR1.setInPin(1, AND1.getOutPin(0));

		MDR.setInPin(0, MX1.getOutPin(0));
		MDR.setPinLd(OR1.getOutPin(0));
		MDR.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		
		TSBMDRout1.setInPin(0, MDR.getOutPin(0));
		TSBMDRout1.setE(NameConnector.getPin("Oper1.MDRout1"));
		this.addOnIBUS1(TSBMDRout1.getOutPin(0));

		TSBeMDR.setInPin(0, MDR.getOutPin(0));
		TSBeMDR.setE(NameConnector.getPin("Oper1.eMDR"));
		this.addOnDBUS(TSBeMDR.getOutPin(0));

		MAR.setInPin(0, IBUS2.getOutPin(0));
		MAR.setPinLd(NameConnector.getPin("Oper1.ldMAR"));
		MAR.setPinInc(NameConnector.getPin("Oper1.incMAR"));
		MAR.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		
		TSBeMAR.setInPin(0, MAR.getOutPin(0));
		TSBeMAR.setE(NameConnector.getPin("Oper1.eMAR"));
		this.addOnABUS(TSBeMAR.getOutPin(0));

		TSBrdCPU.setInPin(0, new Pin(true, "1"));
		TSBrdCPU.setE(NameConnector.getPin("Oper1.rdCPU"));
		NOT2.setInPin(0, TSBrdCPU.getOutPin(0));
		this.addOnNOTRDBUS(NOT2.getOutPin(0));

		TSBwrCPU.setInPin(0, new Pin(true, "1"));
		TSBwrCPU.setE(NameConnector.getPin("Oper1.wrCPU"));
		NOT3.setInPin(0, TSBwrCPU.getOutPin(0));
		this.addOnNOTWRBUS(NOT3.getOutPin(0));

		NOT4.setInPin(0, NameConnector.getPin("Oper1.eMAR"));
		// AND2.setInputPin(0, NameConnector.getPin("Oper1.hreq());
		AND2.setInPin(0, new Pin(false, "1"));
		AND2.setInPin(1, NOT4.getOutPin(0));

		DWH.setInPin(0, IBUS1low.getOutPin(0));
		DWH.setPinLd(NameConnector.getPin("Oper1.ldDWH"));
		DWH.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		
		DWL.setInPin(0, IBUS1low.getOutPin(0));
		DWL.setPinLd(NameConnector.getPin("Oper1.ldDWL"));
		DWL.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		
		for (int i = 0; i < 8; i++) {
			DW.setInPin(i, DWL.getOutPin(i));
			DW.setInPin(8 + i, DWH.getOutPin(i));
			DWHint.setInPin(i, DWH.getOutPin(i));
			DWLint.setInPin(i, DWL.getOutPin(i));
		}

		TSBDWout2.setInPin(0, DW.getOutPin(0));
		TSBDWout2.setE(NameConnector.getPin("Oper1.DWout2"));
		this.addOnIBUS2(TSBDWout2.getOutPin(0));
	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Bus1.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(619, 5));
		points.add(new Point(619, 804));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(600, 20));
		points.add(new Point(600, 642));
		points.add(new Point(468, 642));
		points.add(new Point(468, 804));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(581, 35));
		points.add(new Point(581, 623));
		points.add(new Point(318, 623));
		points.add(new Point(318, 804));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(542, 700));
		points.add(new Point(542, 694));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.MOST1_2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(542, 757));
		points.add(new Point(542, 750));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.MOST2_1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(391, 672));
		points.add(new Point(391, 666));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.MOST1_3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(391, 728));
		points.add(new Point(391, 722));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.MOST3_2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(531, 689));
		points.add(new Point(468, 689));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST1_2.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(468, 746));
		points.add(new Point(530, 746));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(552, 746));
		points.add(new Point(619, 746));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST2_1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(553, 689));
		points.add(new Point(619, 689));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(401, 718));
		points.add(new Point(468, 718));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST3_2.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(379, 718));
		points.add(new Point(318, 718));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(380, 661));
		points.add(new Point(318, 661));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMOST1_3.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(402, 661));
		points.add(new Point(619, 661));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(126, 180));
		points.add(new Point(133, 180));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldMAR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(126, 199));
		points.add(new Point(133, 199));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.incMAR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(284, 189));
		points.add(new Point(291, 189));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(208, 208));
		points.add(new Point(208, 312));
		sections.add(points);
		line = new GuiPinLine(sections, MAR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(197, 323));
		points.add(new Point(204, 323));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.eMAR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(208, 334));
		points.add(new Point(208, 351));
		sections.add(points);
		line = new GuiPinLine(sections, TSBeMAR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 85));
		points.add(new Point(457, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.DBUS"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(581, 80));
		points.add(new Point(476, 80));
		points.add(new Point(476, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(495, 123));
		points.add(new Point(501, 123));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.mxMDR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 142));
		points.add(new Point(466, 170));
		sections.add(points);
		line = new GuiPinLine(sections, MX1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(337, 255));
		points.add(new Point(344, 255));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.NOTFCBUS"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(371, 255));
		points.add(new Point(378, 255));
		points.add(new Point(378, 227));
		points.add(new Point(316, 227));
		points.add(new Point(316, 201));
		points.add(new Point(323, 201));
		sections.add(points);
		line = new GuiPinLine(sections, this.NOT1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(152, 416));
		points.add(new Point(152, 409));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.rdCPU"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 195));
		points.add(new Point(363, 195));
		points.add(new Point(353, 195));
		points.add(new Point(353, 202));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(353, 152));
		points.add(new Point(353, 184));
		points.add(new Point(363, 184));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldMDR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(383, 189));
		points.add(new Point(391, 189));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(542, 189));
		points.add(new Point(548, 189));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(316, 152));
		points.add(new Point(316, 189));
		points.add(new Point(323, 189));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.rdCPU"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 208));
		points.add(new Point(466, 312));
		sections.add(points);
		line = new GuiPinLine(sections, MDR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 255));
		points.add(new Point(483, 255));
		sections.add(points);
		line = new GuiPinLine(sections, MDR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(494, 260));
		points.add(new Point(494, 266));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.MDRout1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(505, 255));
		points.add(new Point(619, 255));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMDRout1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(455, 323));
		points.add(new Point(462, 323));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.eMDR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 334));
		points.add(new Point(466, 351));
		sections.add(points);
		line = new GuiPinLine(sections, TSBeMDR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 404));
		points.add(new Point(141, 404));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(168, 404));
		points.add(new Point(174, 404));
		sections.add(points);
		line = new GuiPinLine(sections, NOT2.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(250, 404));
		points.add(new Point(261, 404));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(272, 410));
		points.add(new Point(272, 415));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.wrCPU"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(288, 404));
		points.add(new Point(295, 404));
		sections.add(points);
		line = new GuiPinLine(sections, NOT3.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(401, 399));
		points.add(new Point(412, 399));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(401, 410));
		points.add(new Point(407, 410));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.eMAR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(434, 404));
		points.add(new Point(441, 404));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(619, 468));
		points.add(new Point(252, 468));
		points.add(new Point(252, 487));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1low.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 468));
		points.add(new Point(466, 487));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1low.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(169, 497));
		points.add(new Point(177, 497));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(384, 497));
		points.add(new Point(391, 497));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(169, 516));
		points.add(new Point(177, 516));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldDWH"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(252, 525));
		points.add(new Point(252, 543));
		sections.add(points);
		line = new GuiPinLine(sections, DWHint.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(384, 516));
		points.add(new Point(391, 516));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldDWL"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(466, 525));
		points.add(new Point(466, 544));
		sections.add(points);
		line = new GuiPinLine(sections, DWLint.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 570));
		points.add(new Point(407, 570));
		sections.add(points);
		line = new GuiPinLine(sections, DWHint.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 585));
		points.add(new Point(407, 585));
		sections.add(points);
		line = new GuiPinLine(sections, DWLint.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(445, 578));
		points.add(new Point(483, 578));
		sections.add(points);
		line = new GuiPinLine(sections, DW.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(494, 589));
		points.add(new Point(494, 583));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.DWout2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(505, 578));
		points.add(new Point(600, 578));
		sections.add(points);
		line = new GuiPinLine(sections, TSBDWout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(208, 170));
		points.add(new Point(208, 67));
		points.add(new Point(600, 67));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		// odradi LABELE:
		gui.addLabel(new GuiPinLabel(622, 28, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(602, 46, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(584, 64, NameConnector.getPin("Bus1.IBUS3")));

		gui.addLabel(new GuiPinLabel(212, 145, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(456, 463, IBUS1low.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(255, 463, IBUS1low.getOutPin(0)));

		gui.addLabel(new GuiPinLabel(478, 705, TSBMOST1_2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(580, 761, TSBMOST2_1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(330, 677, TSBMOST1_3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(431, 734, TSBMOST3_2.getOutPin(0)));

		gui.addLabel(new GuiPinLabel(221, 248, MAR.getOutPin(0)));

		gui.addLabel(new GuiPinLabel(471, 362, TSBeMDR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(195, 362, TSBeMAR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(449, 82, DBUS.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(475, 157, MX1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(435, 248, MDR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(535, 271, TSBMDRout1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(244, 555, DWHint.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(461, 555, DWLint.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(452, 592, DW.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(532, 574, TSBDWout2.getOutPin(0)));

		gui.addLabel(new GuiTextLabel(443, 90, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(461, 90, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(446, 168, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(446, 228, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(446, 350, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(511, 272, "" + Parameters.addressableUnit, 14));
		
		gui.addLabel(new GuiTextLabel(426, 100, "" + (Parameters.addressableUnit-1) + "..0", 10));
		gui.addLabel(new GuiTextLabel(515, 100, "" + (Parameters.addressableUnit-1) + "..0", 10));
		gui.addLabel(new GuiTextLabel(503, 231, "" + (Parameters.addressableUnit-1) + "..0", 10));
		gui.addLabel(new GuiTextLabel(547, 252, "" + (Parameters.addressableUnit-1) + "..0", 10));
		gui.addLabel(new GuiTextLabel(506, 354, "" + (Parameters.addressableUnit-1) + "..0", 10));
		
		
	}

	public void addOnIBUS1(Pin pin) {
		IBUS1.setInPin(0, pin);
	}

	public void addOnIBUS2(Pin pin) {
		IBUS2.setInPin(0, pin);
	}

	public void addOnIBUS3(Pin pin) {
		IBUS3.setInPin(0, pin);
	}

	public void addOnDBUS(Pin pin) {
		DBUS.setInPin(0, pin);
	}

	public void addOnABUS(Pin pin) {
		ABUS.setInPin(0, pin);
	}

	public void addOnNOTRDBUS(Pin pin) {
		NOTRDBUS.setInPin(0, pin);
	}

	public void addOnNOTWRBUS(Pin pin) {
		NOTWRBUS.setInPin(0, pin);
	}

	public void addOnNOTFCBUS(Pin pin) {
		NOTFCBUS.setInPin(0, pin);
	}

	public void putPins() {
		NameConnector.addPin(componentName, "IBUS1", IBUS1.getOutPin(0));

		NameConnector.addPin(componentName, "IBUS2", IBUS2.getOutPin(0));

		NameConnector.addPin(componentName, "IBUS3", IBUS3.getOutPin(0));

		NameConnector.addPin(componentName, "ABUS", ABUS.getOutPin(0));

		NameConnector.addPin(componentName, "DBUS", DBUS.getOutPin(0));

		NameConnector.addPin(componentName, "NOTRDBUS", NOTRDBUS.getOutPin(0));

		NameConnector.addPin(componentName, "NOTWRBUS", NOTWRBUS.getOutPin(0));

		NameConnector.addPin(componentName, "NOTFCBUS", NOTFCBUS.getOutPin(0));

		NameConnector.addPin(componentName, "MDR", MDR.getOutPin(0));

		NameConnector.addPin(componentName, "MAR", MAR.getOutPin(0));

		NameConnector.addPin(componentName, "MX1", MX1.getOutPin(0));

		NameConnector.addPin(componentName, "TSBMOST1_2out",
				TSBMOST1_2.getOutPin(0));

		NameConnector.addPin(componentName, "TSBMOST2_1out",
				TSBMOST2_1.getOutPin(0));

		NameConnector.addPin(componentName, "TSBMDRoutToIBUS",
				TSBMDRout1.getOutPin(0));

		NameConnector.addPin(componentName, "TSBeMDRout", TSBeMDR.getOutPin(0));

		NameConnector.addPin(componentName, "TSBMARout", TSBeMAR.getOutPin(0));

		NameConnector.addPin(componentName, "fcCPU", NOT1.getOutPin(0));

		NameConnector.addPin(componentName, "hack", AND2.getOutPin(0));

	}

	public REG RegMAR() {
		return MAR;
	}

	public REG RegMDR() {
		return MDR;
	}

	public REG RegDWH() {
		return DWH;
	}

	public REG RegDWL() {
		return DWL;
	}

	public void putComponents() {
		NameConnector.addComponent(componentName, "MAR", MAR);
		NameConnector.addComponent(componentName, "MDR", MDR);
		NameConnector.addComponent(componentName, "DWH", DWH);
		NameConnector.addComponent(componentName, "DWL", DWL);
	}
}
