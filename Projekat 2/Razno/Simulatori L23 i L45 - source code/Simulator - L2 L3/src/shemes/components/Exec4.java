package shemes.components;

import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.*;

import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;
import gui.GuiPinLabel;

public class Exec4 extends AbstractSchema {

	private NOT NOT1, NOT2, NOT3, NOT4, NOT5, NOT6, NOT7;
	private XOR XOR1;
	private OR OR1, OR2;
	private MP MX7, MX8, MX9;
	private IntToBools IBUS3bits;
	private IntToInt IBUS3low4, IBUS3low3;

	public Exec4() {
		componentName = "Exec4";
		displayName = "Exec 4";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		NOT1 = new NOT();
		NOT2 = new NOT();
		NOT3 = new NOT();
		NOT4 = new NOT();
		NOT5 = new NOT();
		NOT6 = new NOT();
		NOT7 = new NOT();
		XOR1 = new XOR();
		OR1 = new OR();
		OR2 = new OR();
		MX7 = new MP(8);
		MX8 = new MP(8);
		MX9 = new MP(2);
		IBUS3bits = new IntToBools(8, 8);
		IBUS3low4 = new IntToInt(16, 4);
		IBUS3low3 = new IntToInt(16, 3);

		putPins();
		putComponents();
	}

	public void initConections() {

		NOT1.setInPin(0, NameConnector.getPin("Exec2.PSWZ"));
		XOR1.setInPin(0, NameConnector.getPin("Exec2.PSWN"));
		XOR1.setInPin(1, NameConnector.getPin("Exec2.PSWV"));
		NOT2.setInPin(0, XOR1.getOutPin(0));
		OR1.setInPin(0, XOR1.getOutPin(0));
		OR1.setInPin(1, NameConnector.getPin("Exec2.PSWZ"));
		NOT3.setInPin(0, OR1.getOutPin(0));
		OR2.setInPin(0, NameConnector.getPin("Exec2.PSWC"));
		OR2.setInPin(1, NameConnector.getPin("Exec2.PSWZ"));
		NOT4.setInPin(0, OR2.getOutPin(0));
		NOT5.setInPin(0, NameConnector.getPin("Exec2.PSWC"));
		NOT6.setInPin(0, NameConnector.getPin("Exec2.PSWV"));
		NOT7.setInPin(0, NameConnector.getPin("Exec2.PSWN"));

		MX7.setInPin(0, NameConnector.getPin("Exec2.PSWZ"));// eql
		MX7.setInPin(1, NOT1.getOutPin(0));
		MX7.setInPin(2, NameConnector.getPin("Exec2.PSWN"));// neg
		MX7.setInPin(3, NOT7.getOutPin(0));
		MX7.setInPin(4, NameConnector.getPin("Exec2.PSWV"));// ovf
		MX7.setInPin(5, NOT6.getOutPin(0));
		MX7.setInPin(6, NameConnector.getPin("Exec2.PSWC"));// car
		MX7.setInPin(7, NOT5.getOutPin(0));
		MX7.setCtrl(0, IBUS3bits.getOutPin(0));
		MX7.setCtrl(1, IBUS3bits.getOutPin(1));
		MX7.setCtrl(2, IBUS3bits.getOutPin(2));

		MX8.setInPin(0, NOT3.getOutPin(0));
		MX8.setInPin(1, NOT2.getOutPin(0));
		MX8.setInPin(2, XOR1.getOutPin(0));
		MX8.setInPin(3, OR1.getOutPin(0));
		MX8.setInPin(4, NOT4.getOutPin(0));
		MX8.setInPin(5, NOT5.getOutPin(0));
		MX8.setInPin(6, NameConnector.getPin("Exec2.PSWC"));
		MX8.setInPin(7, OR2.getOutPin(0));
		MX8.setCtrl(0, IBUS3bits.getOutPin(0));
		MX8.setCtrl(1, IBUS3bits.getOutPin(1));
		MX8.setCtrl(2, IBUS3bits.getOutPin(2));

		MX9.setInPin(0, MX8.getOutPin(0));
		MX9.setInPin(1, MX7.getOutPin(0));
		MX9.setCtrl(0, IBUS3bits.getOutPin(3));

		IBUS3bits.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
		IBUS3low4.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
		IBUS3low3.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Exec4.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(120, 41));
		points.add(new Point(237, 41));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(196, 41));
		points.add(new Point(196, 58));
		points.add(new Point(203, 58));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(230, 58));
		points.add(new Point(237, 58));
		sections.add(points);
		line = new GuiPinLine(sections, NOT1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(120, 81));
		points.add(new Point(130, 81));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(120, 92));
		points.add(new Point(130, 92));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWV"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(150, 86));
		points.add(new Point(237, 86));
		sections.add(points);
		line = new GuiPinLine(sections, XOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(158, 86));
		points.add(new Point(158, 126));
		points.add(new Point(167, 126));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(196, 87));
		points.add(new Point(196, 103));
		points.add(new Point(203, 103));
		sections.add(points);
		line = new GuiPinLine(sections, XOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(231, 103));
		points.add(new Point(237, 103));
		sections.add(points);
		line = new GuiPinLine(sections, NOT2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(120, 137));
		points.add(new Point(168, 137));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(188, 132));
		points.add(new Point(237, 132));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(196, 132));
		points.add(new Point(196, 149));
		points.add(new Point(203, 149));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(231, 149));
		points.add(new Point(237, 149));
		sections.add(points);
		line = new GuiPinLine(sections, NOT3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(120, 171));
		points.add(new Point(129, 171));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(120, 183));
		points.add(new Point(130, 183));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(150, 177));
		points.add(new Point(237, 177));
		sections.add(points);
		line = new GuiPinLine(sections, OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(196, 177));
		points.add(new Point(196, 194));
		points.add(new Point(203, 194));
		sections.add(points);
		line = new GuiPinLine(sections, OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(231, 194));
		points.add(new Point(237, 194));
		sections.add(points);
		line = new GuiPinLine(sections, NOT4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(374, 41));
		points.add(new Point(487, 41));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(445, 41));
		points.add(new Point(445, 58));
		points.add(new Point(453, 58));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(480, 58));
		points.add(new Point(487, 58));
		sections.add(points);
		line = new GuiPinLine(sections, NOT5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(374, 86));
		points.add(new Point(487, 86));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(445, 86));
		points.add(new Point(445, 103));
		points.add(new Point(453, 103));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWV"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(480, 103));
		points.add(new Point(487, 103));
		sections.add(points);
		line = new GuiPinLine(sections, NOT6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(374, 132));
		points.add(new Point(487, 132));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(445, 133));
		points.add(new Point(445, 149));
		points.add(new Point(453, 149));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(480, 149));
		points.add(new Point(487, 149));
		sections.add(points);
		line = new GuiPinLine(sections, NOT7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(144, 380));
		points.add(new Point(144, 526));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(125, 395));
		points.add(new Point(125, 511));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(106, 410));
		points.add(new Point(106, 496));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(106, 463));
		points.add(new Point(422, 463));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3low4.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(272, 463));
		points.add(new Point(272, 428));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3low3.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(256, 424));
		points.add(new Point(256, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(2));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(272, 427));
		points.add(new Point(272, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(287, 424));
		points.add(new Point(287, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(407, 424));
		points.add(new Point(407, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(2));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 427));
		points.add(new Point(422, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(437, 424));
		points.add(new Point(437, 417));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 463));
		points.add(new Point(511, 463));
		points.add(new Point(511, 351));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3bits.getOutPin(3));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 463));
		points.add(new Point(422, 428));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3low3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 398));
		points.add(new Point(243, 398));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 380));
		points.add(new Point(243, 380));
		sections.add(points);
		line = new GuiPinLine(sections, NOT1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 361));
		points.add(new Point(243, 361));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 342));
		points.add(new Point(243, 342));
		sections.add(points);
		line = new GuiPinLine(sections, NOT7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 323));
		points.add(new Point(243, 323));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWV"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 304));
		points.add(new Point(243, 304));
		sections.add(points);
		line = new GuiPinLine(sections, NOT6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 285));
		points.add(new Point(243, 285));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(236, 267));
		points.add(new Point(243, 267));
		sections.add(points);
		line = new GuiPinLine(sections, NOT5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 398));
		points.add(new Point(394, 398));
		sections.add(points);
		line = new GuiPinLine(sections, NOT3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 380));
		points.add(new Point(394, 380));
		sections.add(points);
		line = new GuiPinLine(sections, NOT2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 361));
		points.add(new Point(394, 361));
		sections.add(points);
		line = new GuiPinLine(sections, XOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 342));
		points.add(new Point(394, 342));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(386, 323));
		points.add(new Point(394, 323));
		sections.add(points);
		line = new GuiPinLine(sections, NOT4.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 304));
		points.add(new Point(394, 304));
		sections.add(points);
		line = new GuiPinLine(sections, NOT5.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 285));
		points.add(new Point(394, 285));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWC"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 267));
		points.add(new Point(394, 267));
		sections.add(points);
		line = new GuiPinLine(sections, OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(300, 332));
		points.add(new Point(319, 332));
		points.add(new Point(319, 238));
		points.add(new Point(469, 238));
		points.add(new Point(469, 314));
		points.add(new Point(488, 314));
		sections.add(points);
		line = new GuiPinLine(sections, MX7.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(450, 332));
		points.add(new Point(488, 332));
		sections.add(points);
		line = new GuiPinLine(sections, MX8.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(533, 323));
		points.add(new Point(541, 323));
		sections.add(points);
		line = new GuiPinLine(sections, MX9.getOutPin(0));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(148, 402, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(129, 432, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(76, 462, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(210, 477, IBUS3low4.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(277, 453, IBUS3low3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(426, 453, IBUS3low3.getOutPin(0)));

	}

	public void putPins() {
		NameConnector.addPin(componentName, "brpom", MX9.getOutPin(0));

	}

	public void putComponents() {

	}
}
