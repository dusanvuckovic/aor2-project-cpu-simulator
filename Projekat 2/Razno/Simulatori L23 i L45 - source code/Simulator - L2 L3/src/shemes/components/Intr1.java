package shemes.components;

import java.awt.Point;
import java.util.*;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;
import gui.*;

public class Intr1 extends AbstractSchema {

	private RSFF PRINS, PRCOD, PRADR, PRINM;
	private RSFF PRINTR1, PRINTR2, PRINTR3, PRINTR4, PRINTR5, PRINTR6, PRINTR7;
	private REG IMR;
	private IntToBools IMRBits;
	private TSB TSBIMRout2;
	private DC DC1;
	private OR ORprekid;
	private AND ANDprekid;
	private NOT NOTprekid;

	public Intr1() {
		componentName = "Intr1";
		displayName = "Intr 1";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {

		PRINS = new RSFF("PRINS");

		PRCOD = new RSFF("PRCOD");

		PRADR = new RSFF("PRADR");

		PRINM = new RSFF("PRINM");

		PRINTR1 = new RSFF("PRINTR1");

		PRINTR2 = new RSFF("PRINTR2");

		PRINTR3 = new RSFF("PRINTR3");

		PRINTR4 = new RSFF("PRINTR4");

		PRINTR5 = new RSFF("PRINTR5");

		PRINTR6 = new RSFF("PRINTR6");

		PRINTR7 = new RSFF("PRINTR7");

		IMR = new REG(1, "IMR");
		IMR.getOutPin(0).setIsInt();
		IMR.getOutPin(0).setNumOfLines(16);
		IMR.initVal(0xff); // DOZVOLI MASKIRAJUCE SVE PREKIDE

		IMRBits = new IntToBools(16, 16);

		TSBIMRout2 = new TSB("IMRout2 ");
		TSBIMRout2.getOutPin(0).setNumOfLines(16);

		ORprekid = new OR(6);
		NOTprekid = new NOT();
		ANDprekid = new AND();

		DC1 = new DC(3);

		putPins();
		putComponents();
	}

	public void initConections() {
		PRINS.setInPin(0, NameConnector.getPin("Oper1.stPRINS"));
		PRINS.setInPin(1, NameConnector.getPin("Oper1.clPRINS"));
		PRINS.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINS.setReset(new Pin(false, "0"));

		PRCOD.setInPin(0, NameConnector.getPin("Oper1.stPRCOD"));
		PRCOD.setInPin(1, NameConnector.getPin("Oper1.clPRCOD"));
		PRCOD.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRCOD.setReset(new Pin(false, "0"));

		PRADR.setInPin(0, NameConnector.getPin("Oper1.stPRADR"));
		PRADR.setInPin(1, NameConnector.getPin("Oper1.clPRADR"));
		PRADR.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRADR.setReset(new Pin(false, "0"));

		PRINM.setInPin(0, new Pin(false, "0"));// OVDE ZAKACITI PIN SA FAULTA
		PRINM.setInPin(1, NameConnector.getPin("Oper1.clPRINM"));
		PRINM.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINM.setReset(new Pin(false, "0"));

		PRINTR1.setInPin(0, new Pin(false, "0"));
		PRINTR1.setInPin(1, DC1.getOutPin(1));
		PRINTR1.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR1.setReset(new Pin(false, "0"));

		PRINTR2.setInPin(0, new Pin(false, "0"));
		PRINTR2.setInPin(1, DC1.getOutPin(2));
		PRINTR2.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR2.setReset(new Pin(false, "0"));

		PRINTR3.setInPin(0, new Pin(false, "0"));
		PRINTR3.setInPin(1, DC1.getOutPin(3));
		PRINTR3.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR3.setReset(new Pin(false, "0"));

		PRINTR4.setInPin(0, new Pin(false, "0"));
		PRINTR4.setInPin(1, DC1.getOutPin(4));
		PRINTR4.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR4.setReset(new Pin(false, "0"));

		PRINTR5.setInPin(0, new Pin(false, "0"));
		PRINTR5.setInPin(1, DC1.getOutPin(5));
		PRINTR5.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR5.setReset(new Pin(false, "0"));

		PRINTR6.setInPin(0, new Pin(false, "0"));
		PRINTR6.setInPin(1, DC1.getOutPin(6));
		PRINTR6.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR6.setReset(new Pin(false, "0"));

		PRINTR7.setInPin(0, new Pin(false, "0"));
		PRINTR7.setInPin(1, DC1.getOutPin(7));
		PRINTR7.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		PRINTR7.setReset(new Pin(false, "0"));

		IMR.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
		IMR.setPinLd(NameConnector.getPin("Oper1.ldIMR"));
		IMR.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		
		IMRBits.setInPin(0, IMR.getOutPin(0));

		TSBIMRout2.setInPin(0, IMR.getOutPin(0));
		TSBIMRout2.setE(NameConnector.getPin("Oper1.IMRout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBIMRout2
				.getOutPin(0));

		NOTprekid.setInPin(0, NameConnector.getPin("Fetch2.RTI"));
		ANDprekid.setInPin(0, NameConnector.getPin("Exec2.PSWT"));
		ANDprekid.setInPin(1, NOTprekid.getOutPin(0));

		ORprekid.setInPin(0, PRINS.getOutPin(0));
		ORprekid.setInPin(1, PRCOD.getOutPin(0));
		ORprekid.setInPin(2, PRADR.getOutPin(0));
		ORprekid.setInPin(3, PRINM.getOutPin(0));
		ORprekid.setInPin(4, NameConnector.getPin("Intr2.printr"));
		ORprekid.setInPin(5, ANDprekid.getOutPin(0));

		DC1.setE(NameConnector.getPin("Oper1.clINTR"));
		DC1.setInPin(0, NameConnector.getPin("Intr2.prl0"));
		DC1.setInPin(1, NameConnector.getPin("Intr2.prl1"));
		DC1.setInPin(2, NameConnector.getPin("Intr2.prl2"));
	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Intr1.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 51));
		points.add(new Point(123, 51));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPRINS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 78));
		points.add(new Point(123, 78));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 104));
		points.add(new Point(123, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPRINS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 51));
		points.add(new Point(187, 51));
		sections.add(points);
		line = new GuiPinLine(sections, PRINS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 104));
		points.add(new Point(187, 104));
		sections.add(points);
		line = new GuiPinLine(sections, PRINS.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 51));
		points.add(new Point(322, 51));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPRCOD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 78));
		points.add(new Point(322, 78));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 104));
		points.add(new Point(322, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPRCOD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 51));
		points.add(new Point(386, 51));
		sections.add(points);
		line = new GuiPinLine(sections, PRCOD.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 104));
		points.add(new Point(386, 104));
		sections.add(points);
		line = new GuiPinLine(sections, PRCOD.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 51));
		points.add(new Point(520, 51));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPRADR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 78));
		points.add(new Point(520, 78));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 104));
		points.add(new Point(520, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPRADR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 51));
		points.add(new Point(584, 51));
		sections.add(points);
		line = new GuiPinLine(sections, PRADR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 104));
		points.add(new Point(584, 104));
		sections.add(points);
		line = new GuiPinLine(sections, PRADR.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 193));
		points.add(new Point(123, 193));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 219));
		points.add(new Point(123, 219));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 246));
		points.add(new Point(123, 246));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPRINM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 193));
		points.add(new Point(187, 193));
		sections.add(points);
		line = new GuiPinLine(sections, PRINM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 246));
		points.add(new Point(187, 246));
		sections.add(points);
		line = new GuiPinLine(sections, PRINM.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(601, 141));
		points.add(new Point(601, 314));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(583, 154));
		points.add(new Point(583, 301));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(565, 167));
		points.add(new Point(565, 288));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(297, 223));
		points.add(new Point(303, 223));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldIMR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(447, 223));
		points.add(new Point(454, 223));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 240));
		points.add(new Point(376, 262));
		points.add(new Point(459, 262));
		sections.add(points);
		line = new GuiPinLine(sections, IMR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(469, 266));
		points.add(new Point(469, 272));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IMRout2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(479, 262));
		points.add(new Point(583, 262));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIMRout2.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 206));
		points.add(new Point(376, 186));
		points.add(new Point(565, 186));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 342));
		points.add(new Point(123, 342));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 369));
		points.add(new Point(123, 369));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 395));
		points.add(new Point(123, 395));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 342));
		points.add(new Point(187, 342));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 395));
		points.add(new Point(187, 395));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR3.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 342));
		points.add(new Point(322, 342));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 369));
		points.add(new Point(322, 369));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 395));
		points.add(new Point(322, 395));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 342));
		points.add(new Point(386, 342));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 395));
		points.add(new Point(386, 395));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR2.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(511, 342));
		points.add(new Point(520, 342));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 369));
		points.add(new Point(520, 369));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 395));
		points.add(new Point(520, 395));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 342));
		points.add(new Point(584, 342));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 395));
		points.add(new Point(584, 395));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 455));
		points.add(new Point(123, 455));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 482));
		points.add(new Point(123, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(116, 508));
		points.add(new Point(123, 508));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 455));
		points.add(new Point(187, 455));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(180, 508));
		points.add(new Point(187, 508));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR6.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 455));
		points.add(new Point(322, 455));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 482));
		points.add(new Point(322, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(314, 508));
		points.add(new Point(322, 508));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 455));
		points.add(new Point(386, 455));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 508));
		points.add(new Point(386, 508));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR5.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(511, 455));
		points.add(new Point(520, 455));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 482));
		points.add(new Point(520, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 508));
		points.add(new Point(520, 508));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 455));
		points.add(new Point(584, 455));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 508));
		points.add(new Point(584, 508));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR4.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 563));
		points.add(new Point(176, 563));
		points.add(new Point(176, 582));
		points.add(new Point(180, 582));
		sections.add(points);
		line = new GuiPinLine(sections, PRINS.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 578));
		points.add(new Point(172, 578));
		points.add(new Point(172, 590));
		points.add(new Point(180, 590));
		sections.add(points);
		line = new GuiPinLine(sections, PRCOD.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 593));
		points.add(new Point(169, 593));
		points.add(new Point(169, 597));
		points.add(new Point(182, 597));
		sections.add(points);
		line = new GuiPinLine(sections, PRADR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 608));
		points.add(new Point(169, 608));
		points.add(new Point(169, 605));
		points.add(new Point(182, 605));
		sections.add(points);
		line = new GuiPinLine(sections, PRINM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 624));
		points.add(new Point(172, 624));
		points.add(new Point(172, 612));
		points.add(new Point(180, 612));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.printr"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(135, 654));
		points.add(new Point(142, 654));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(135, 665));
		points.add(new Point(142, 665));
		sections.add(points);
		line = new GuiPinLine(sections, NOTprekid.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(165, 659));
		points.add(new Point(176, 659));
		points.add(new Point(176, 620));
		points.add(new Point(180, 620));
		sections.add(points);
		line = new GuiPinLine(sections, ANDprekid.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(202, 601));
		points.add(new Point(210, 601));
		sections.add(points);
		line = new GuiPinLine(sections, ORprekid.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(352, 695));
		points.add(new Point(352, 688));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clINTR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(318, 635));
		points.add(new Point(325, 635));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(318, 620));
		points.add(new Point(325, 620));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(318, 605));
		points.add(new Point(325, 605));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 673));
		points.add(new Point(386, 673));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 658));
		points.add(new Point(386, 658));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(377, 642));
		points.add(new Point(386, 642));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 627));
		points.add(new Point(386, 627));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 612));
		points.add(new Point(386, 612));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 597));
		points.add(new Point(386, 597));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 582));
		points.add(new Point(386, 582));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(378, 567));
		points.add(new Point(386, 567));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(511, 569));
		points.add(new Point(520, 569));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 595));
		points.add(new Point(520, 595));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(512, 622));
		points.add(new Point(520, 622));
		sections.add(points);
		line = new GuiPinLine(sections, DC1.getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 569));
		points.add(new Point(584, 569));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(577, 622));
		points.add(new Point(584, 622));
		sections.add(points);
		line = new GuiPinLine(sections, PRINTR7.getOutPin(1));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(605, 176, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(587, 206, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(569, 236, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(381, 279, IMR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(512, 258, TSBIMRout2.getOutPin(0)));

	}

	public void putPins() {
		NameConnector.addPin(componentName, "prekid", ORprekid.getOutPin(0));

		NameConnector.addPin(componentName, "PRINS", PRINS.getOutPin(0));

		NameConnector.addPin(componentName, "PRCOD", PRCOD.getOutPin(0));

		NameConnector.addPin(componentName, "PRADR", PRADR.getOutPin(0));

		NameConnector.addPin(componentName, "PRINM", PRINM.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR1", PRINTR1.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR2", PRINTR2.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR3", PRINTR3.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR4", PRINTR4.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR5", PRINTR5.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR6", PRINTR6.getOutPin(0));

		NameConnector.addPin(componentName, "PRINTR7", PRINTR7.getOutPin(0));

		NameConnector.addPin(componentName, "IMR1", IMRBits.getOutPin(1));

		NameConnector.addPin(componentName, "IMR2", IMRBits.getOutPin(2));

		NameConnector.addPin(componentName, "IMR3", IMRBits.getOutPin(3));

		NameConnector.addPin(componentName, "IMR4", IMRBits.getOutPin(4));

		NameConnector.addPin(componentName, "IMR5", IMRBits.getOutPin(5));

		NameConnector.addPin(componentName, "IMR6", IMRBits.getOutPin(6));

		NameConnector.addPin(componentName, "IMR7", IMRBits.getOutPin(7));

		NameConnector.addPin(componentName, "IMR", IMR.getOutPin(0));

	}

	public void putComponents() {
		NameConnector.addComponent(componentName, "IMR", IMR);
	}

	public REG RegIMR() {
		return IMR;
	}

}
