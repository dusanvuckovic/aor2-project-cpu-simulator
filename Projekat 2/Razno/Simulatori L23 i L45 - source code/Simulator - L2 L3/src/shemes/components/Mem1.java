package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.AND;
import logic.components.IntToBools;
import logic.components.MEM;
import logic.components.NOT;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Mem1 extends AbstractSchema {

	private TSB TSBRDB;
	private NOT NOTRDB;
	private AND ANDrdMEM;
	private IntToBools ABUSBits;
	private AND ANDselect;
	private NOT NOTselect;
	private TSB TSBWRB;
	private NOT NOTWRB;
	private AND ANDwrMEM;
	private MEM RAM;
	private TSB TSBMEMout;
	private TSB TSBFCB;
	private NOT NOTFCB;

	public Mem1() {
		componentName = "Mem1";
		displayName = "Operaciona jedinica";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		TSBRDB = new TSB("RDB");
		TSBRDB.getOutPin(0).setNumOfLines(1);
		TSBRDB.getOutPin(0).setIsBool();
		NOTRDB = new NOT();
		ANDrdMEM = new AND();

		ABUSBits = new IntToBools(16, 16);
		ANDselect = new AND(4);
		NOTselect = new NOT();

		TSBWRB = new TSB("WRB");
		TSBWRB.getOutPin(0).setNumOfLines(1);
		TSBWRB.getOutPin(0).setIsBool();
		NOTWRB = new NOT();
		ANDwrMEM = new AND();

		RAM = new MEM(64 * 1024); // 64 Kb
		RAM.getOutPin(0).setIsInt();
		RAM.getOutPin(0).setNumOfLines(Parameters.addressableUnit);
		
		TSBMEMout = new TSB("MEMout");
		TSBMEMout.getOutPin(0).setNumOfLines(Parameters.addressableUnit);

		TSBFCB = new TSB("FCB");
		TSBFCB.getOutPin(0).setIsBool();
		NOTFCB = new NOT();

		putPins();
		putComponents();
	}

	public void initConections() {
		TSBRDB.setInPin(0, NameConnector.getPin("Bus1.NOTRDBUS"));
		TSBRDB.setE(new Pin(true, "1"));
		NOTRDB.setInPin(0, TSBRDB.getOutPin(0));
		ANDrdMEM.setInPin(0, NOTRDB.getOutPin(0));
		ANDrdMEM.setInPin(1, NOTselect.getOutPin(0));

		ABUSBits.setInPin(0, NameConnector.getPin("Bus1.ABUS"));
		ANDselect.setInPin(0, ABUSBits.getOutPin(12));
		ANDselect.setInPin(1, ABUSBits.getOutPin(13));
		ANDselect.setInPin(2, ABUSBits.getOutPin(14));
		ANDselect.setInPin(3, ABUSBits.getOutPin(15));
		NOTselect.setInPin(0, ANDselect.getOutPin(0));

		TSBWRB.setInPin(0, NameConnector.getPin("Bus1.NOTWRBUS"));
		TSBWRB.setE(new Pin(true, "1"));
		NOTWRB.setInPin(0, TSBWRB.getOutPin(0));
		ANDwrMEM.setInPin(0, NOTselect.getOutPin(0));
		ANDwrMEM.setInPin(1, NOTWRB.getOutPin(0));

		RAM.setRead(ANDrdMEM.getOutPin(0));
		RAM.setWrite(ANDwrMEM.getOutPin(0));
		RAM.setInPin(0, NameConnector.getPin("Bus1.ABUS"));
		RAM.setInPin(1, NameConnector.getPin("Bus1.DBUS"));
		TSBMEMout.setInPin(0, RAM.getOutPin(0));
		TSBMEMout.setE(NameConnector.getPin("Mem2.MEMout"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnDBUS(TSBMEMout
				.getOutPin(0));

		TSBFCB.setInPin(0, new Pin(true, "1"));
		TSBFCB.setE(NameConnector.getPin("Mem2.fcMEM"));
		NOTFCB.setInPin(0, TSBFCB.getOutPin(0));
		((Bus1) NameConnector.getSchema("Bus1")).addOnNOTFCBUS(NOTFCB
				.getOutPin(0));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Mem1.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(84, 142));
		points.add(new Point(103, 142));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.NOTRDBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(132, 142));
		points.add(new Point(162, 142));
		sections.add(points);
		line = new GuiPinLine(sections, NOTRDB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(87, 167));
		points.add(new Point(103, 167));
		sections.add(points);
		line = new GuiPinLine(sections, ABUSBits.getOutPin(12));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(87, 180));
		points.add(new Point(103, 180));
		sections.add(points);
		line = new GuiPinLine(sections, ABUSBits.getOutPin(13));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(87, 195));
		points.add(new Point(103, 195));
		sections.add(points);
		line = new GuiPinLine(sections, ABUSBits.getOutPin(14));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(87, 208));
		points.add(new Point(103, 208));
		sections.add(points);
		line = new GuiPinLine(sections, ABUSBits.getOutPin(15));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(131, 187));
		points.add(new Point(143, 187));
		points.add(new Point(143, 153));
		points.add(new Point(162, 153));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(143, 187));
		points.add(new Point(143, 254));
		points.add(new Point(162, 254));
		sections.add(points);
		line = new GuiPinLine(sections, NOTselect.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(83, 267));
		points.add(new Point(103, 267));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.NOTWRBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 266));
		points.add(new Point(162, 266));
		sections.add(points);
		line = new GuiPinLine(sections, NOTWRB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(184, 147));
		points.add(new Point(254, 147));
		points.add(new Point(254, 176));
		points.add(new Point(342, 176));
		sections.add(points);
		line = new GuiPinLine(sections, ANDrdMEM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(184, 261));
		points.add(new Point(253, 261));
		points.add(new Point(253, 195));
		points.add(new Point(342, 195));
		sections.add(points);
		line = new GuiPinLine(sections, ANDwrMEM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(147, 355));
		points.add(new Point(147, 365));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem2.fcMEM"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(191, 371));
		points.add(new Point(158, 371));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(131, 371));
		points.add(new Point(111, 371));
		sections.add(points);
		line = new GuiPinLine(sections, NOTFCB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(443, 68));
		points.add(new Point(443, 136));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.ABUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(616, 68));
		points.add(new Point(616, 380));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(615, 185));
		points.add(new Point(531, 185));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.DBUS"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(437, 233));
		points.add(new Point(437, 286));
		points.add(new Point(521, 286));
		sections.add(points);
		line = new GuiPinLine(sections, RAM.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(531, 301));
		points.add(new Point(531, 291));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Mem2.MEMout"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(542, 286));
		points.add(new Point(615, 286));
		sections.add(points);
		line = new GuiPinLine(sections, TSBMEMout.getOutPin(0));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(449, 91, NameConnector.getPin("Bus1.ABUS")));
		gui.addLabel(new GuiPinLabel(622, 91, NameConnector.getPin("Bus1.DBUS")));
		gui.addLabel(new GuiPinLabel(447, 248, RAM.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(570, 181, NameConnector.getPin("Bus1.DBUS")));
		gui.addLabel(new GuiPinLabel(570, 282, TSBMEMout.getOutPin(0)));
		
		gui.addLabel(new GuiTextLabel(595, 133, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(548, 201, "" + Parameters.addressableUnit, 14));
		gui.addLabel(new GuiTextLabel(415, 249, "" + Parameters.addressableUnit, 14));
		
		gui.addLabel(new GuiTextLabel(595, 93, "" + (Parameters.addressableUnit-1) + "..0", 10));
		gui.addLabel(new GuiTextLabel(510, 193, "" + (Parameters.addressableUnit-1) + "..0", 10));
		gui.addLabel(new GuiTextLabel(452, 232, "" + (Parameters.addressableUnit-1) + "..0", 10));
	}

	public void putPins() {
		NameConnector.addPin(componentName, "rdMEM", ANDrdMEM.getOutPin(0));

		NameConnector.addPin(componentName, "wrMEM", ANDwrMEM.getOutPin(0));

	}

	public void putComponents() {

	}

	public void writeMEM(int adress, int data) {
		RAM.write(adress, data);
	}

	public int readMEM(int adress) {
		return RAM.read(adress);
	}

	public MEM getMEM() {
		return RAM;
	}

}
