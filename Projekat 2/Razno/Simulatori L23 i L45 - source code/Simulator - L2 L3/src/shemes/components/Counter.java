package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.BoolsToInt;
import logic.components.CLK;
import logic.components.IntToBools;
import logic.components.KMADR;
import logic.components.KMOPR;
import logic.components.MP;
import logic.components.MicroMEM;
import logic.components.NOT;
import logic.components.OR;
import logic.components.REG;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Counter extends AbstractSchema {

	private KMOPR KMOPR1;
	private KMADR KMADR1;
	private MP MP1;
	private REG mPC;
	private OR ORmPC;
	private NOT NOTmPC;
	private MicroMEM mMEM;

	private IntToBools mCWbits0_23, mCWbits24_47, mCWbits48_71, mCWbits72_95,
			mCWbits96_103, mCWbits104_111;

	private BoolsToInt zeroes;

	public Counter() {
		componentName = "Counter";
		displayName = "Upravljacka jedinica procesora";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new Oper1());
		this.addSubScheme(new Uprav1());
	}

	public void initComponent() {

		int[] kmoprCodeAddresses = new int[Parameters.counterCodeAddresses.length];
		for (int i = 0; i < kmoprCodeAddresses.length; i++) {
			kmoprCodeAddresses[i] = Integer
					.parseInt(Parameters.counterCodeAddresses[i][1]);
		}
		KMOPR1 = new KMOPR(kmoprCodeAddresses);
		KMOPR1.getOutPin(0).setNumOfLines(8);

		int[] kmoprAdrModesAddresses = new int[Parameters.counterAdrModesAddresses.length];
		for (int i = 0; i < kmoprAdrModesAddresses.length; i++) {
			kmoprAdrModesAddresses[i] = Integer
					.parseInt(Parameters.counterAdrModesAddresses[i][1]);
		}
		KMADR1 = new KMADR(kmoprAdrModesAddresses);
		KMADR1.getOutPin(0).setNumOfLines(8);

		MP1 = new MP(4);
		MP1.getOutPin(0).setNumOfLines(8);
		MP1.getOutPin(0).setIsInt();

		mPC = new REG(1, "mPC");
		mPC.getOutPin(0).setNumOfLines(8);
		mPC.getOutPin(0).setIsInt();

		ORmPC = new OR(4);
		NOTmPC = new NOT();

		mMEM = new MicroMEM(256);

		mCWbits0_23 = new IntToBools(1, 24);
		mCWbits24_47 = new IntToBools(1, 24);
		mCWbits48_71 = new IntToBools(1, 24);
		mCWbits72_95 = new IntToBools(1, 24);
		mCWbits96_103 = new IntToBools(1, 8);
		mCWbits104_111 = new IntToBools(1, 8);

		zeroes = new BoolsToInt(8, 8);

		putPins();
		putComponents();

		super.initComponent();

	}

	public void initConections() {
		for (int i = 0; i < Parameters.counterCodeAddresses.length; i++) {
			String name = Parameters.counterCodeAddresses[i][0];
			Pin pin = NameConnector.getPin(name);
			KMOPR1.setInPin(i, pin);
		}

		for (int i = 0; i < Parameters.counterAdrModesAddresses.length; i++) {
			String name = Parameters.counterAdrModesAddresses[i][0];
			Pin pin = NameConnector.getPin(name);
			KMADR1.setInPin(i, pin);
		}

		MP1.setInPin(0, mMEM.getOutPin(5));
		MP1.setInPin(1, KMADR1.getOutPin(0));
		MP1.setInPin(2, KMOPR1.getOutPin(0));
		Pin p = new Pin(0, 1, "");
		p.setIsInt();
		MP1.setInPin(3, p);
		MP1.setCtrl(0, NameConnector.getPin("Uprav1.bradr"));
		MP1.setCtrl(1, NameConnector.getPin("Uprav1.bropr"));

		ORmPC.setInPin(0, NameConnector.getPin("Uprav1.bradr"));
		ORmPC.setInPin(1, NameConnector.getPin("Uprav1.bropr"));
		ORmPC.setInPin(2, NameConnector.getPin("Uprav1.branch"));
		ORmPC.setInPin(3, NameConnector.getPin("Uprav1.br"));
		NOTmPC.setInPin(0, ORmPC.getOutPin(0));

		mPC.setInPin(0, MP1.getOutPin(0));
		mPC.setPinLd(ORmPC.getOutPin(0));
		mPC.setPinInc(NOTmPC.getOutPin(0));
		mPC.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		mMEM.setInPin(0, mPC.getOutPin(0));

		mCWbits0_23.setInPin(0, mMEM.getOutPin(0));
		mCWbits24_47.setInPin(0, mMEM.getOutPin(1));
		mCWbits48_71.setInPin(0, mMEM.getOutPin(2));
		mCWbits72_95.setInPin(0, mMEM.getOutPin(3));
		mCWbits96_103.setInPin(0, mMEM.getOutPin(4));
		mCWbits104_111.setInPin(0, mMEM.getOutPin(5));

		for (int i = 0; i < 8; i++) {
			zeroes.setInPin(i, new Pin(false, "0"));
		}

		super.initConections();

	}

	public void initGui() {
		GuiPinLine line;
		gui = new GuiSchema("src/images/Counter.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(125, 81));
		points.add(new Point(125, 90));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getInPin(0));// Fetch2.NOP());
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 81));
		points.add(new Point(140, 90));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getInPin(1));// Fetch2.HALT());
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 81));
		points.add(new Point(219, 90));
		sections.add(points);
		line = new GuiPinLine(sections,
				KMOPR1.getInPin(KMOPR1.getInPins().length - 2));// Fetch2.RTS());
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(276, 81));
		points.add(new Point(276, 90));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getInPin(0));// Fetch2.regdir());
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(291, 81));
		points.add(new Point(291, 90));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getInPin(1));// Fetch2.regind());
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(370, 81));
		points.add(new Point(370, 90));
		sections.add(points);
		line = new GuiPinLine(sections,
				KMADR1.getInPin(KMADR1.getInPins().length - 1));// Fetch2.imm());
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(172, 139));
		points.add(new Point(172, 194));
		sections.add(points);
		line = new GuiPinLine(sections, KMOPR1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(323, 139));
		points.add(new Point(323, 166));
		points.add(new Point(202, 166));
		points.add(new Point(202, 194));
		sections.add(points);
		line = new GuiPinLine(sections, KMADR1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(142, 183));
		points.add(new Point(142, 194));
		sections.add(points);
		line = new GuiPinLine(sections, zeroes.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(360, 487));
		points.add(new Point(360, 505));
		points.add(new Point(477, 505));
		points.add(new Point(477, 175));
		points.add(new Point(233, 175));
		points.add(new Point(233, 194));
		sections.add(points);
		line = new GuiPinLine(sections, mMEM.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(262, 203));
		points.add(new Point(255, 203));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bradr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(262, 222));
		points.add(new Point(255, 222));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bropr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 232));
		points.add(new Point(187, 269));
		sections.add(points);
		line = new GuiPinLine(sections, MP1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(112, 288));
		points.add(new Point(120, 288));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(326, 271));
		points.add(new Point(319, 271));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bradr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(318, 282));
		points.add(new Point(327, 282));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bropr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(326, 305));
		points.add(new Point(319, 305));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.branch"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(326, 294));
		points.add(new Point(317, 294));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.br"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(297, 288));
		points.add(new Point(289, 288));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(260, 297));
		points.add(new Point(289, 297));
		points.add(new Point(289, 279));
		points.add(new Point(255, 279));
		sections.add(points);
		line = new GuiPinLine(sections, ORmPC.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 307));
		points.add(new Point(187, 345));
		sections.add(points);
		line = new GuiPinLine(sections, mPC.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(113, 364));
		points.add(new Point(120, 364));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(422, 428));
		points.add(new Point(415, 428));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(415, 450));
		points.add(new Point(422, 450));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(86, 458));
		points.add(new Point(86, 465));
		sections.add(points);
		line = new GuiPinLine(sections, mCWbits0_23.getOutPin(23));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(225, 458));
		points.add(new Point(225, 465));
		sections.add(points);
		line = new GuiPinLine(sections, mCWbits72_95.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(259, 458));
		points.add(new Point(259, 465));
		sections.add(points);
		line = new GuiPinLine(sections, mCWbits96_103.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(304, 458));
		points.add(new Point(304, 465));
		sections.add(points);
		line = new GuiPinLine(sections, mCWbits96_103.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(338, 458));
		points.add(new Point(338, 465));
		sections.add(points);
		line = new GuiPinLine(sections, mCWbits104_111.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(383, 458));
		points.add(new Point(383, 465));
		sections.add(points);
		line = new GuiPinLine(sections, mCWbits104_111.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(157, 491));
		points.add(new Point(157, 543));
		sections.add(points);
		line = new GuiPinLine(sections, mMEM.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(281, 487));
		points.add(new Point(281, 533));
		points.add(new Point(432, 533));
		points.add(new Point(432, 543));
		sections.add(points);
		line = new GuiPinLine(sections, mMEM.getOutPin(4));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 550));
		points.add(new Point(356, 550));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.START"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 565));
		points.add(new Point(356, 565));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.hack"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 580));
		points.add(new Point(356, 580));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.fcCPU"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 610));
		points.add(new Point(356, 610));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch3.gropr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 641));
		points.add(new Point(355, 641));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch3.L1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(346, 671));
		points.add(new Point(355, 671));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(518, 550));
		points.add(new Point(508, 550));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.regdir"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(518, 580));
		points.add(new Point(508, 580));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.NOP"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(518, 610));
		points.add(new Point(508, 610));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec4.brpom"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(518, 641));
		points.add(new Point(508, 641));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.prekid"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(518, 671));
		points.add(new Point(509, 671));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(387, 693));
		points.add(new Point(387, 678));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bradr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(417, 693));
		points.add(new Point(417, 678));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.bropr"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(447, 692));
		points.add(new Point(447, 678));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.br"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 382));
		points.add(new Point(187, 420));
		sections.add(points);
		line = new GuiPinLine(sections, zeroes.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(477, 678));
		points.add(new Point(477, 693));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Uprav1.branch"));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(154, 155, KMOPR1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(305, 155, KMADR1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(389, 195, mMEM.getOutPin(5)));
		gui.addLabel(new GuiPinLabel(389, 520, mMEM.getOutPin(5)));
		gui.addLabel(new GuiPinLabel(160, 250, MP1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(160, 320, mPC.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(255, 520, mMEM.getOutPin(4)));
		gui.addLabel(new GuiPinLabel(60, 520, mMEM.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(102, 520, mMEM.getOutPin(1)));
		gui.addLabel(new GuiPinLabel(144, 520, mMEM.getOutPin(2)));
		gui.addLabel(new GuiPinLabel(186, 520, mMEM.getOutPin(3)));

		super.initGui();

	}

	public void putPins() {
		NameConnector.addPin(componentName, "KMADR", KMADR1.getOutPin(0));

		NameConnector.addPin(componentName, "KMOPR", KMOPR1.getOutPin(0));

		NameConnector.addPin(componentName, "mPC", mPC.getOutPin(0));

		NameConnector
				.addPin(componentName, "mCW96", mCWbits96_103.getOutPin(7));

		NameConnector
				.addPin(componentName, "mCW97", mCWbits96_103.getOutPin(6));

		NameConnector
				.addPin(componentName, "mCW98", mCWbits96_103.getOutPin(5));

		NameConnector
				.addPin(componentName, "mCW99", mCWbits96_103.getOutPin(4));

		NameConnector.addPin(componentName, "mCW100",
				mCWbits96_103.getOutPin(3));

		NameConnector.addPin(componentName, "mCW101",
				mCWbits96_103.getOutPin(2));

		NameConnector.addPin(componentName, "mCW102",
				mCWbits96_103.getOutPin(1));

		NameConnector.addPin(componentName, "mCW103",
				mCWbits96_103.getOutPin(0));

	}

	public void putComponents() {

	}

	public REG mPCREG() {
		return mPC;
	}

	public IntToBools mCW0_23() {
		return mCWbits0_23;
	}

	public IntToBools mCW24_47() {
		return mCWbits24_47;

	}

	public IntToBools mCW48_71() {
		return mCWbits48_71;

	}

	public IntToBools mCW72_95() {
		return mCWbits72_95;
	}

}
