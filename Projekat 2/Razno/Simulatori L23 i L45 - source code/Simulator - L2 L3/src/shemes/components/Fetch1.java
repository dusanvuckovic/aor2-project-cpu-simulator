package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;
import gui.components.TSBGui;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.BoolsToInt;
import logic.components.CLK;
import logic.components.IntToBools;
import logic.components.IntToInt;
import logic.components.REG;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.IndexExtractor;
import util.NameConnector;
import util.Parameters;

public class Fetch1 extends AbstractSchema {

	private REG PC;
	private TSB TSBPCout1, TSBPCHout3, TSBPCLout3;
	private TSB TSBIRDAout3, TSBIRJAout2, TSBIRPOMout3, TSBIRBRout3;

	private IntToInt PChigh, PClow, IBUS1lowINT;
	private IntToBools IBUS1lowBOOLS;

	private REG IR0, IR1, IR2, IR3;

	private BoolsToInt IR0ToINT, IR1ToINT, IR2ToINT, IR3ToINT;

	public Fetch1() {
		componentName = "Fetch1";
		displayName = "Fetch 1";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {

		PC = new REG(1, "PC");
		PC.getOutPin(0).setIsInt();
		PC.getOutPin(0).setNumOfLines(16);
		PC.initVal(256);// 100h

		TSBPCout1 = new TSB("PCout1");
		TSBPCout1.getOutPin(0).setNumOfLines(16);

		PChigh = new IntToInt(16, 8, true, false);
		PClow = new IntToInt(16, 8, false, false);

		TSBPCHout3 = new TSB("PCHout3");
		TSBPCHout3.getOutPin(0).setNumOfLines(8);

		TSBPCLout3 = new TSB("PCLout3");
		TSBPCLout3.getOutPin(0).setNumOfLines(8);

		int dbusSize = Parameters.addressableUnit;
		IBUS1lowINT = new IntToInt(16, dbusSize);
		IBUS1lowBOOLS = new IntToBools(dbusSize, dbusSize);

		IR0 = new REG(dbusSize, "IR0");
		IR1 = new REG(dbusSize, "IR1");
		IR2 = new REG(dbusSize, "IR2");
		IR3 = new REG(dbusSize, "IR3");

		IR0ToINT = new BoolsToInt(dbusSize, dbusSize);
		IR1ToINT = new BoolsToInt(dbusSize, dbusSize);
		IR2ToINT = new BoolsToInt(dbusSize, dbusSize);
		IR3ToINT = new BoolsToInt(dbusSize, dbusSize);

		TSBIRDAout3 = initTSB("IRAD");

		TSBIRJAout2 = initTSB("IRJA");

		TSBIRPOMout3 = initTSB("IRPOM");

		TSBIRBRout3 = initTSB("IRBR");

		putPins();
		putComponents();
	}

	public void initConections() {

		PC.setInPin(0, NameConnector.getPin("Bus1.IBUS2"));
		PC.setPinInc(NameConnector.getPin("Oper1.incPC"));
		PC.setPinLd(NameConnector.getPin("Oper1.ldPC"));
		PC.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		TSBPCout1.setInPin(0, PC.getOutPin(0));
		TSBPCout1.setE(NameConnector.getPin("Oper1.PCout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBPCout1
				.getOutPin(0));

		PChigh.setInPin(0, PC.getOutPin(0));
		PClow.setInPin(0, PC.getOutPin(0));

		TSBPCHout3.setInPin(0, PChigh.getOutPin(0));
		TSBPCHout3.setE(NameConnector.getPin("Oper1.PCHout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBPCHout3
				.getOutPin(0));

		TSBPCLout3.setInPin(0, PClow.getOutPin(0));
		TSBPCLout3.setE(NameConnector.getPin("Oper1.PCLout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBPCLout3
				.getOutPin(0));

		IBUS1lowINT.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));
		IBUS1lowBOOLS.setInPin(0, IBUS1lowINT.getOutPin(0));

		IR0.setPinLd(NameConnector.getPin("Oper1.ldIR0"));
		IR0.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		IR1.setPinLd(NameConnector.getPin("Oper1.ldIR1"));
		IR1.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		IR2.setPinLd(NameConnector.getPin("Oper1.ldIR2"));
		IR2.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		IR3.setPinLd(NameConnector.getPin("Oper1.ldIR3"));
		IR3.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		int dbusSize = Parameters.addressableUnit;
		for (int i = 0; i < dbusSize; i++) {
			Pin p = IBUS1lowBOOLS.getOutPin(i);
			IR0.setInPin(i, p);
			IR1.setInPin(i, p);
			IR2.setInPin(i, p);
			IR3.setInPin(i, p);
		}

		for (int i = 0; i < dbusSize; i++) {
			IR0ToINT.setInPin(i, IR0.getOutPin(i));
			IR1ToINT.setInPin(i, IR1.getOutPin(i));
			IR2ToINT.setInPin(i, IR2.getOutPin(i));
			IR3ToINT.setInPin(i, IR3.getOutPin(i));
		}

		connectTSB("IRAD", TSBIRDAout3);
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBIRDAout3
				.getOutPin(0));

		connectTSB("IRJA", TSBIRJAout2);
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBIRJAout2
				.getOutPin(0));

		connectTSB("IRPOM", TSBIRPOMout3);
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBIRPOMout3
				.getOutPin(0));

		connectTSB("IRBR", TSBIRBRout3);
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBIRBRout3
				.getOutPin(0));

	}

	private TSB initTSB(String component) {
		String[] data = new String[0];
		for (int i = 0; i < Parameters.fetch1Conections.length; i++) {
			data = Parameters.fetch1Conections[i];
			if (data[1].equals(component))
				break;
		}
		TSB result = TSB.createTSB(data);
		return result;

	}

	private void connectTSB(String component, TSB tsb) {
		try {
			String[] data = new String[0];
			for (int i = 0; i < Parameters.fetch1Conections.length; i++) {
				data = Parameters.fetch1Conections[i];
				if (data[1].equals(component))
					break;
			}

			int size = tsb.getOutPin(0).getNumOfLines();
			BoolsToInt input = new BoolsToInt(size, size);
			StringBuilder longName = new StringBuilder();
			for (int i = 0; i < size; i++) {
				input.setInPin(i, NameConnector.getPin(data[4 + i]));
				longName = longName.append(data[4 + i]).append(",");
			}
			input.getOutPin(0).setName(
					IndexExtractor.extractShortName(longName.toString()));
			tsb.setInPin(0, input.getOutPin(0));
			tsb.setE(NameConnector.getPin(data[3]));
			tsb.getE().setName(data[3]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Fetch1.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 7));
		points.add(new Point(592, 775));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(573, 21));
		points.add(new Point(573, 760));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(554, 37));
		points.add(new Point(554, 748));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(573, 70));
		points.add(new Point(370, 70));
		points.add(new Point(370, 93));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(287, 102));
		points.add(new Point(294, 102));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.incPC"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(287, 121));
		points.add(new Point(294, 121));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldPC"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(445, 112));
		points.add(new Point(452, 112));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(370, 131));
		points.add(new Point(370, 251));
		points.add(new Point(456, 251));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(370, 204));
		points.add(new Point(456, 204));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(370, 157));
		points.add(new Point(456, 157));
		sections.add(points);
		line = new GuiPinLine(sections, PC.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(467, 162));
		points.add(new Point(467, 168));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PCout1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(467, 209));
		points.add(new Point(467, 215));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PCHout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(467, 256));
		points.add(new Point(467, 262));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PCLout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 157));
		points.add(new Point(592, 157));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPCout1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(477, 204));
		points.add(new Point(554, 204));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPCHout3.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(477, 251));
		points.add(new Point(554, 251));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPCLout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 298));
		points.add(new Point(226, 298));
		points.add(new Point(226, 320));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(441, 298));
		points.add(new Point(441, 321));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1lowINT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(144, 331));
		points.add(new Point(151, 331));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(144, 349));
		points.add(new Point(151, 349));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldIR0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(226, 359));
		points.add(new Point(226, 377));
		sections.add(points);
		line = new GuiPinLine(sections, IR0ToINT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(359, 331));
		points.add(new Point(366, 331));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(359, 349));
		points.add(new Point(366, 349));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldIR1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(441, 359));
		points.add(new Point(441, 377));
		sections.add(points);
		line = new GuiPinLine(sections, IR1ToINT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(592, 404));
		points.add(new Point(227, 404));
		points.add(new Point(227, 427));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(441, 404));
		points.add(new Point(441, 427));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1lowINT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(144, 436));
		points.add(new Point(151, 436));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(144, 455));
		points.add(new Point(151, 455));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldIR2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(227, 464));
		points.add(new Point(227, 481));
		sections.add(points);
		line = new GuiPinLine(sections, IR2ToINT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(354, 436));
		points.add(new Point(366, 436));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(355, 455));
		points.add(new Point(366, 455));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldIR3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(441, 464));
		points.add(new Point(441, 481));
		sections.add(points);
		line = new GuiPinLine(sections, IR3ToINT.getOutPin(0));
		gui.addLine(line);

		TSBGui TSBIRDAout3Gui = new TSBGui(TSBIRDAout3, 375, 508, null, true,
				false);
		gui = TSBIRDAout3Gui.convert(gui);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 517));
		points.add(new Point(554, 517));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIRDAout3.getOutPin(0));
		gui.addLine(line);

		TSBGui TSBIRJAout2Gui = new TSBGui(TSBIRJAout2, 375, 565, null, true,
				false);
		gui = TSBIRJAout2Gui.convert(gui);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 574));
		points.add(new Point(573, 574));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIRJAout2.getOutPin(0));
		gui.addLine(line);

		TSBGui TSBIRPOMout3Gui = new TSBGui(TSBIRPOMout3, 375, 621, null, true,
				false);
		gui = TSBIRPOMout3Gui.convert(gui);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 630));
		points.add(new Point(554, 630));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIRPOMout3.getOutPin(0));
		gui.addLine(line);

		TSBGui TSBIRBRout3Gui = new TSBGui(TSBIRBRout3, 375, 678, null, true,
				false);
		gui = TSBIRBRout3Gui.convert(gui);
		gui = TSBIRPOMout3Gui.convert(gui);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 687));
		points.add(new Point(554, 687));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIRBRout3.getOutPin(0));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(595, 33, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(577, 66, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(524, 100, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(400, 67, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(330, 162, PC.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(507, 175, TSBPCout1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(512, 220, TSBPCHout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(512, 265, TSBPCLout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(340, 295, IBUS1lowINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(340, 401, IBUS1lowINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(220, 392, IR0ToINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(435, 392, IR1ToINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(220, 496, IR2ToINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(435, 496, IR3ToINT.getOutPin(0)));

		gui.addLabel(new GuiPinLabel(508, 512, TSBIRDAout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(508, 569, TSBIRJAout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(508, 626, TSBIRPOMout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(508, 703, TSBIRBRout3.getOutPin(0)));

		gui.addLabel(new GuiTextLabel(208, 319, "" + IR0.getInPins().length, 14));
		gui.addLabel(new GuiTextLabel(208, 375, "" + IR0.getOutPins().length,
				14));
		gui.addLabel(new GuiTextLabel(423, 319, "" + IR1.getInPins().length, 14));
		gui.addLabel(new GuiTextLabel(423, 375, "" + IR1.getOutPins().length,
				14));
		gui.addLabel(new GuiTextLabel(208, 425, "" + IR2.getInPins().length, 14));
		gui.addLabel(new GuiTextLabel(208, 480, "" + IR2.getOutPins().length,
				14));
		gui.addLabel(new GuiTextLabel(423, 425, "" + IR3.getInPins().length, 14));
		gui.addLabel(new GuiTextLabel(423, 480, "" + IR3.getOutPins().length,
				14));

		gui.addLabel(new GuiTextLabel(
				246,
				381,
				""
						+ (IR0.getOutPins().length + IR1.getOutPins().length
								+ IR2.getOutPins().length
								+ IR3.getOutPins().length + -1)
						+ ".."
						+ (IR1.getOutPins().length + IR2.getOutPins().length + IR3
								.getOutPins().length), 10));
		gui.addLabel(new GuiTextLabel(463, 381, ""
				+ (IR1.getOutPins().length + IR2.getOutPins().length
						+ IR3.getOutPins().length - 1) + ".."
				+ (IR2.getOutPins().length + IR3.getOutPins().length), 10));
		gui.addLabel(new GuiTextLabel(246, 483, ""
				+ (IR2.getOutPins().length + IR3.getOutPins().length - 1)
				+ ".." + (IR3.getOutPins().length), 10));
		gui.addLabel(new GuiTextLabel(463, 483, ""
				+ (IR3.getOutPins().length - 1) + ".." + (0), 10));

	}

	public void putPins() {
		int dbusSize = Parameters.addressableUnit;

		for (int i = 0; i < dbusSize; i++) {
			NameConnector.addPin(componentName, "IR" + (3 * dbusSize + i),
					IR0.getOutPin(i));
		}
		for (int i = 0; i < dbusSize; i++) {
			NameConnector.addPin(componentName, "IR" + (2 * dbusSize + i),
					IR1.getOutPin(i));
		}
		for (int i = 0; i < dbusSize; i++) {
			NameConnector.addPin(componentName, "IR" + (dbusSize + i),
					IR2.getOutPin(i));
		}
		for (int i = 0; i < dbusSize; i++) {
			NameConnector.addPin(componentName, "IR" + (i), IR3.getOutPin(i));
		}

		NameConnector.addPin(componentName, "PC", PC.getOutPin(0));
	}

	public void putComponents() {
		NameConnector.addComponent(componentName, "PC", PC);
		NameConnector.addComponent(componentName, "IR0", IR0);
		NameConnector.addComponent(componentName, "IR1", IR1);
		NameConnector.addComponent(componentName, "IR2", IR2);
		NameConnector.addComponent(componentName, "IR3", IR3);
	}

	public REG RegPC() {
		return PC;
	}

	public REG RegIR0() {
		return IR0;
	}

	public REG RegIR1() {
		return IR1;
	}

	public REG RegIR2() {
		return IR2;
	}

	public REG RegIR3() {
		return IR3;
	}
}
