package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.BoolsToInt;
import logic.components.CD;
import logic.components.CLK;
import logic.components.IntToBools;
import logic.components.IntToInt;
import logic.components.REG;
import logic.components.TSB;
import shemes.AbstractSchema;
import util.NameConnector;

public class Intr3 extends AbstractSchema {

	private CD CD1, CD2;
	private BoolsToInt UINT, UEXT;
	private TSB TSBUINTout3, TSBUEXTout3;
	private IntToInt IBUS3low;
	private REG BR;
	private IntToBools BRbits;
	private BoolsToInt IVTDSP;
	private TSB TSBIVTDSPout3;
	private REG IVTP;
	private TSB TSBIVTPout1;

	public Intr3() {
		componentName = "Intr3";
		displayName = "Intr 3";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		CD1 = new CD(4);
		CD2 = new CD(8);

		UINT = new BoolsToInt(8, 8);
		UEXT = new BoolsToInt(8, 8);

		TSBUINTout3 = new TSB("UINTout3");
		TSBUINTout3.getOutPin(0).setNumOfLines(16);
		TSBUEXTout3 = new TSB("EXTout3");
		TSBUEXTout3.getOutPin(0).setNumOfLines(16);

		IBUS3low = new IntToInt(16, 8);

		BR = new REG(1, "BR");
		BR.getOutPin(0).setIsInt();
		BR.getOutPin(0).setNumOfLines(8);

		BRbits = new IntToBools(8, 8);
		IVTDSP = new BoolsToInt(16, 16);
		TSBIVTDSPout3 = new TSB("IVTDSPout3");
		TSBIVTDSPout3.getOutPin(0).setNumOfLines(16);

		IVTP = new REG(1, "IVTP");
		IVTP.getOutPin(0).setIsInt();
		IVTP.getOutPin(0).setNumOfLines(16);

		TSBIVTPout1 = new TSB("IVTPout1");
		TSBIVTPout1.getOutPin(0).setNumOfLines(16);

		putPins();
		putComponents();
	}

	public void initConections() {

		CD1.setE(new Pin(true, "1"));
		CD1.setInPin(0, NameConnector.getPin("Exec2.PSWT"));
		CD1.setInPin(1, NameConnector.getPin("Intr1.PRINM"));
		CD1.setInPin(2, NameConnector.getPin("Intr1.PRADR"));
		CD1.setInPin(3, NameConnector.getPin("Intr1.PRCOD"));

		UINT.setInPin(0, CD1.getOutPin(0));
		UINT.setInPin(1, CD1.getOutPin(1));
		for (int i = 2; i < 8; i++) {
			UINT.setInPin(i, new Pin(false, "0"));
		}

		TSBUINTout3.setInPin(0, UINT.getOutPin(0));
		TSBUINTout3.setE(NameConnector.getPin("Oper1.UINTout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBUINTout3
				.getOutPin(0));

		CD2.setE(new Pin(true, "1"));
		CD2.setInPin(0, new Pin(false, "0"));
		CD2.setInPin(1, NameConnector.getPin("Intr2.irm1"));
		CD2.setInPin(2, NameConnector.getPin("Intr2.irm2"));
		CD2.setInPin(3, NameConnector.getPin("Intr2.irm3"));
		CD2.setInPin(4, NameConnector.getPin("Intr2.irm4"));
		CD2.setInPin(5, NameConnector.getPin("Intr2.irm5"));
		CD2.setInPin(6, NameConnector.getPin("Intr2.irm6"));
		CD2.setInPin(7, NameConnector.getPin("Intr2.irm7"));

		UEXT.setInPin(0, CD2.getOutPin(0));
		UEXT.setInPin(1, CD2.getOutPin(1));
		UEXT.setInPin(2, CD2.getOutPin(2));
		UEXT.setInPin(3, new Pin(true, "1"));
		for (int i = 4; i < 8; i++) {
			UEXT.setInPin(i, new Pin(false, "0"));
		}

		TSBUEXTout3.setInPin(0, UEXT.getOutPin(0));
		TSBUEXTout3.setE(NameConnector.getPin("Oper1.UEXTout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBUEXTout3
				.getOutPin(0));

		IBUS3low.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
		BR.setInPin(0, IBUS3low.getOutPin(0));
		BR.setPinLd(NameConnector.getPin("Oper1.ldBR"));
		BR.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		BRbits.setInPin(0, BR.getOutPin(0));

		IVTDSP.setInPin(0, new Pin(false, "0"));
		for (int i = 0; i < 8; i++) {
			IVTDSP.setInPin(1 + i, BRbits.getOutPin(i));
		}
		for (int i = 9; i < 16; i++) {
			IVTDSP.setInPin(i, new Pin(false, "0"));
		}

		TSBIVTDSPout3.setInPin(0, IVTDSP.getOutPin(0));
		TSBIVTDSPout3.setE(NameConnector.getPin("Oper1.IVTDSPout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBIVTDSPout3
				.getOutPin(0));

		IVTP.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
		IVTP.setPinLd(NameConnector.getPin("Oper1.ldIVTP"));
		IVTP.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		TSBIVTPout1.setInPin(0, IVTP.getOutPin(0));
		TSBIVTPout1.setE(NameConnector.getPin("Oper1.IVTPout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBIVTPout1
				.getOutPin(0));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Intr3.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(500, 42));
		points.add(new Point(500, 693));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(481, 57));
		points.add(new Point(481, 678));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(462, 72));
		points.add(new Point(462, 663));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 117));
		points.add(new Point(161, 117));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRCOD"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 132));
		points.add(new Point(161, 132));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRADR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 147));
		points.add(new Point(161, 147));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINM"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 162));
		points.add(new Point(161, 162));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWT"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(229, 72));
		points.add(new Point(236, 72));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(229, 102));
		points.add(new Point(236, 102));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(229, 117));
		points.add(new Point(236, 117));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(206, 132));
		points.add(new Point(236, 132));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(206, 147));
		points.add(new Point(236, 147));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(258, 109));
		points.add(new Point(364, 109));
		sections.add(points);
		line = new GuiPinLine(sections, UINT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 115));
		points.add(new Point(376, 120));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.UINTout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(386, 109));
		points.add(new Point(462, 109));
		sections.add(points);
		line = new GuiPinLine(sections, TSBUINTout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 307));
		points.add(new Point(161, 307));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 292));
		points.add(new Point(161, 292));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 277));
		points.add(new Point(161, 277));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 262));
		points.add(new Point(161, 262));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 247));
		points.add(new Point(161, 247));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm4"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 232));
		points.add(new Point(161, 232));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm5"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 217));
		points.add(new Point(161, 217));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm6"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(154, 202));
		points.add(new Point(161, 202));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.irm7"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(229, 194));
		points.add(new Point(236, 194));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(229, 224));
		points.add(new Point(236, 224));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(true, "1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(206, 240));
		points.add(new Point(236, 240));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(2));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(206, 255));
		points.add(new Point(236, 255));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(206, 270));
		points.add(new Point(236, 270));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(258, 232));
		points.add(new Point(364, 232));
		sections.add(points);
		line = new GuiPinLine(sections, UEXT.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 236));
		points.add(new Point(376, 243));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.UEXTout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(386, 232));
		points.add(new Point(462, 232));
		sections.add(points);
		line = new GuiPinLine(sections, TSBUEXTout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(278, 351));
		points.add(new Point(278, 328));
		points.add(new Point(462, 328));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3low.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 360));
		points.add(new Point(203, 360));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 379));
		points.add(new Point(203, 379));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldBR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(278, 388));
		points.add(new Point(278, 421));
		sections.add(points);
		line = new GuiPinLine(sections, BR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(308, 434));
		points.add(new Point(308, 440));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(218, 434));
		points.add(new Point(218, 440));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(248, 434));
		points.add(new Point(248, 440));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(263, 434));
		points.add(new Point(263, 440));
		sections.add(points);
		line = new GuiPinLine(sections, BRbits.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(293, 434));
		points.add(new Point(293, 440));
		sections.add(points);
		line = new GuiPinLine(sections, BRbits.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(252, 464));
		points.add(new Point(252, 490));
		points.add(new Point(364, 490));
		sections.add(points);
		line = new GuiPinLine(sections, IVTDSP.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 495));
		points.add(new Point(376, 501));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("Oper1.IVTDSPout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(386, 490));
		points.add(new Point(462, 490));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIVTDSPout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(278, 558));
		points.add(new Point(278, 535));
		points.add(new Point(462, 535));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 567));
		points.add(new Point(202, 567));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(195, 586));
		points.add(new Point(202, 586));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldIVTP"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(278, 596));
		points.add(new Point(278, 622));
		points.add(new Point(364, 622));
		sections.add(points);
		line = new GuiPinLine(sections, IVTP.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(376, 627));
		points.add(new Point(376, 633));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.IVTPout1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(386, 622));
		points.add(new Point(500, 622));
		sections.add(points);
		line = new GuiPinLine(sections, TSBIVTPout1.getOutPin(0));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(504, 118, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(485, 138, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(466, 158, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(302, 532, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(302, 324, IBUS3low.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(286, 124, UINT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(423, 124, TSBUINTout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(286, 247, UEXT.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(423, 247, TSBUEXTout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(281, 417, BR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(281, 505, IVTDSP.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(281, 637, IVTP.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(415, 618, TSBIVTPout1.getOutPin(0)));
	}

	public void putPins() {
		NameConnector.addPin(componentName, "UINT", UINT.getOutPin(0));

		NameConnector.addPin(componentName, "UEXT", UEXT.getOutPin(0));

		NameConnector.addPin(componentName, "BR", BR.getOutPin(0));

		NameConnector.addPin(componentName, "IVTP", IVTP.getOutPin(0));

		NameConnector.addPin(componentName, "IVTDSP", IVTDSP.getOutPin(0));

		NameConnector
				.addPin(componentName, "IVTPout", TSBIVTPout1.getOutPin(0));

		NameConnector.addPin(componentName, "IVTDSPout",
				TSBIVTDSPout3.getOutPin(0));

	}

	public void putComponents() {
		NameConnector.addComponent(componentName, "BR", BR);
		NameConnector.addComponent(componentName, "IVTP", IVTP);
	}

	public REG RegBR() {
		return BR;
	}

	public REG RegIVTP() {
		return IVTP;
	}

}
