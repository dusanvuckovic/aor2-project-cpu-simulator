package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Exec1 extends AbstractSchema {

	private TSB TSBALUout1, TSBABout3;
	private IntToInt ABLToAB, IBUS2ToALU, IBUS3ToALU;
	private ALU ALUexec;
	private IntToBools ALUbits, ABbits, BBbits;
	private IntToInt IBUS1ToABandBB;
	private TSB TSBBBout2;
	private REG AB, BB;
	private MP MX3, MX4;
	private CD CD1, CD2;
	private TSB TSBAWHout3, TSBAWout3, TSBBWout2;
	private IntToInt AWToAWH;
	private REG AW, BW;

	public Exec1() {
		componentName = "Exec1";
		displayName = "Exec 1";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {

		TSBALUout1 = new TSB("ALUout1");
		TSBALUout1.getOutPin(0).setNumOfLines(Parameters.dataSize);

		ABLToAB = new IntToInt(Parameters.dataSize, 16, false, false);
		TSBABout3 = new TSB("ABout3");
		TSBABout3.getOutPin(0).setNumOfLines(16);

		ALUexec = new ALU(Parameters.dataSize);
		ALUbits = new IntToBools(Parameters.dataSize, Parameters.dataSize);
		ABbits = new IntToBools(Parameters.dataSize, Parameters.dataSize);
		BBbits = new IntToBools(Parameters.dataSize, Parameters.dataSize);

		IBUS1ToABandBB = new IntToInt(16, Parameters.dataSize, false, false);
		IBUS2ToALU = new IntToInt(16, Parameters.dataSize, false, false);
		IBUS3ToALU = new IntToInt(16, Parameters.dataSize, false, false);
		TSBBBout2 = new TSB("BBout2");
		TSBBBout2.getOutPin(0).setNumOfLines(Parameters.dataSize);

		AB = new REG(1, "AB");
		AB.getOutPin(0).setIsInt();
		AB.getOutPin(0).setNumOfLines(Parameters.dataSize);

		BB = new REG(1, "BB");
		BB.getOutPin(0).setIsInt();
		BB.getOutPin(0).setNumOfLines(Parameters.dataSize);

		MX3 = new MP(4);
		CD1 = new CD(4);
		CD1.setE(new Pin(true, "1"));

		MX4 = new MP(4);
		CD2 = new CD(4);
		CD2.setE(new Pin(true, "1"));

		TSBBWout2 = new TSB("BWout2");
		TSBBWout2.getOutPin(0).setNumOfLines(16);

		AWToAWH = new IntToInt(16, 8, true, false);

		TSBAWHout3 = new TSB("AWHout3");
		TSBAWHout3.getOutPin(0).setNumOfLines(8);

		TSBAWout3 = new TSB("AWout3");
		TSBAWout3.getOutPin(0).setNumOfLines(16);

		AW = new REG(1, "AW");
		AW.getOutPin(0).setIsInt();
		AW.getOutPin(0).setNumOfLines(16);
		BW = new REG(1, "BW");
		BW.getOutPin(0).setIsInt();
		BW.getOutPin(0).setNumOfLines(16);

		putPins();
		putComponents();
	}

	public void initConections() {

		TSBALUout1.setInPin(0, ALUexec.getOutPin(0));
		TSBALUout1.setE(NameConnector.getPin("Oper1.ALUout1"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBALUout1
				.getOutPin(0));

		ABLToAB.setInPin(0, AB.getOutPin(0));
		TSBABout3.setInPin(0, ABLToAB.getOutPin(0));
		TSBABout3.setE(NameConnector.getPin("Oper1.ABout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBABout3
				.getOutPin(0));

		IBUS2ToALU.setInPin(0, NameConnector.getPin("Bus1.IBUS2"));
		IBUS3ToALU.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));

		ALUexec.setPinX(IBUS3ToALU.getOutPin(0));
		ALUexec.setPinY(IBUS2ToALU.getOutPin(0));

		ALUexec.setPinAdd(NameConnector.getPin("Oper1.add"));
		ALUexec.setPinSub(NameConnector.getPin("Oper1.sub"));
		ALUexec.setPinInc(NameConnector.getPin("Oper1.inc"));
		ALUexec.setPinDec(NameConnector.getPin("Oper1.dec"));
		ALUexec.setPinAnd(NameConnector.getPin("Oper1.and"));
		ALUexec.setPinOr(NameConnector.getPin("Oper1.or"));
		ALUexec.setPinXor(NameConnector.getPin("Oper1.xor"));
		ALUexec.setPinNot(NameConnector.getPin("Oper1.not"));
		ALUbits.setInPin(0, ALUexec.getOutPin(0));

		IBUS1ToABandBB.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));

		TSBBBout2.setInPin(0, BB.getOutPin(0));
		TSBBBout2.setE(NameConnector.getPin("Oper1.BBout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBBBout2
				.getOutPin(0));

		AB.setInPin(0, IBUS1ToABandBB.getOutPin(0));
		AB.setPinLd(NameConnector.getPin("Oper1.ldAB"));
		AB.setShr(NameConnector.getPin("Oper1.shr"));
		AB.setIR(MX3.getOutPin(0));
		AB.setShl(NameConnector.getPin("Oper1.shl"));
		AB.setIL(MX4.getOutPin(0));
		AB.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		ABbits.setInPin(0, AB.getOutPin(0));

		BB.setInPin(0, IBUS1ToABandBB.getOutPin(0));
		BB.setPinLd(NameConnector.getPin("Oper1.ldBB"));
		BB.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		BBbits.setInPin(0, BB.getOutPin(0));

		CD1.setInPin(0, NameConnector.getPin("Fetch2.ASR"));
		CD1.setInPin(1, NameConnector.getPin("Fetch2.LSR"));
		CD1.setInPin(2, NameConnector.getPin("Fetch2.ROR"));
		CD1.setInPin(3, NameConnector.getPin("Fetch2.RORC"));
		MX3.setCtrl(0, CD1.getOutPin(0));
		MX3.setCtrl(1, CD1.getOutPin(1));
		MX3.setInPin(0, ABbits.getOutPin(Parameters.dataSize - 1));
		MX3.setInPin(1, new Pin(false, "0"));
		MX3.setInPin(2, ABbits.getOutPin(0));
		MX3.setInPin(3, NameConnector.getPin("Exec2.PSWC"));

		CD2.setInPin(0, NameConnector.getPin("Fetch2.ASL"));
		CD2.setInPin(1, NameConnector.getPin("Fetch2.LSL"));
		CD2.setInPin(2, NameConnector.getPin("Fetch2.ROL"));
		CD2.setInPin(3, NameConnector.getPin("Fetch2.ROLC"));
		MX4.setCtrl(0, CD2.getOutPin(0));
		MX4.setCtrl(1, CD2.getOutPin(1));
		MX4.setInPin(0, new Pin(false, "0"));
		MX4.setInPin(1, new Pin(false, "0"));
		MX4.setInPin(2, ABbits.getOutPin(Parameters.dataSize - 1));
		MX4.setInPin(3, NameConnector.getPin("Exec2.PSWC"));

		TSBBWout2.setInPin(0, BW.getOutPin(0));
		TSBBWout2.setE(NameConnector.getPin("Oper1.BWout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBBWout2
				.getOutPin(0));

		AWToAWH.setInPin(0, AW.getOutPin(0));
		TSBAWHout3.setInPin(0, AWToAWH.getOutPin(0));
		TSBAWHout3.setE(NameConnector.getPin("Oper1.AWHout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBAWHout3
				.getOutPin(0));

		TSBAWout3.setInPin(0, AW.getOutPin(0));
		TSBAWout3.setE(NameConnector.getPin("Oper1.AWout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBAWout3
				.getOutPin(0));

		AW.setInPin(0, NameConnector.getPin("Bus1.IBUS2"));
		AW.setPinLd(NameConnector.getPin("Oper1.ldAW"));
		AW.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		BW.setInPin(0, NameConnector.getPin("Bus1.IBUS2"));
		BW.setPinLd(NameConnector.getPin("Oper1.ldBW"));
		BW.setClk((CLK) NameConnector.getComponent("CPUCLK"));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Exec1.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(627, 16));
		points.add(new Point(627, 832));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(608, 31));
		points.add(new Point(608, 817));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(608, 772));
		points.add(new Point(175, 772));
		points.add(new Point(175, 753));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(401, 772));
		points.add(new Point(401, 753));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(589, 46));
		points.add(new Point(589, 801));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		if (Parameters.dataSize == 8) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(341, 63));
			points.add(new Point(366, 63));
			sections.add(points);
			BoolsToInt zeroes = new BoolsToInt(8, 8);
			for (int i = 0; i < 7; i++) {
				zeroes.setInPin(i, new Pin(false, "0"));
			}
			line = new GuiPinLine(sections, zeroes.getOutPin(0));
			gui.addLine(line);
		}

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(341, 78));
		points.add(new Point(366, 78));
		sections.add(points);
		line = new GuiPinLine(sections, AB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(404, 70));
		points.add(new Point(476, 70));
		sections.add(points);
		line = new GuiPinLine(sections, ABLToAB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 75));
		points.add(new Point(487, 81));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ABout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(498, 70));
		points.add(new Point(589, 70));
		sections.add(points);
		line = new GuiPinLine(sections, TSBABout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 632));
		points.add(new Point(147, 632));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ASR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 614));
		points.add(new Point(147, 614));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.LSR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 595));
		points.add(new Point(147, 595));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ROR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(139, 576));
		points.add(new Point(147, 576));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.RORC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(203, 614));
		points.add(new Point(222, 614));
		points.add(new Point(222, 538));
		points.add(new Point(185, 538));
		points.add(new Point(185, 529));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(203, 595));
		points.add(new Point(213, 595));
		points.add(new Point(213, 548));
		points.add(new Point(166, 548));
		points.add(new Point(166, 529));
		sections.add(points);
		line = new GuiPinLine(sections, CD1.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 510));
		points.add(new Point(147, 510));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(139, 491));
		points.add(new Point(147, 491));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(139, 472));
		points.add(new Point(147, 472));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(140, 453));
		points.add(new Point(147, 453));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(203, 481));
		points.add(new Point(222, 481));
		points.add(new Point(222, 415));
		points.add(new Point(92, 415));
		points.add(new Point(92, 368));
		points.add(new Point(100, 368));
		sections.add(points);
		line = new GuiPinLine(sections, MX3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(300, 632));
		points.add(new Point(307, 632));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ASL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(300, 614));
		points.add(new Point(307, 614));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.LSL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(300, 595));
		points.add(new Point(307, 595));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ROL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(299, 576));
		points.add(new Point(307, 576));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ROLC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 614));
		points.add(new Point(382, 614));
		points.add(new Point(382, 538));
		points.add(new Point(344, 538));
		points.add(new Point(344, 529));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 595));
		points.add(new Point(373, 595));
		points.add(new Point(373, 548));
		points.add(new Point(326, 548));
		points.add(new Point(326, 529));
		sections.add(points);
		line = new GuiPinLine(sections, CD2.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(300, 510));
		points.add(new Point(307, 510));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(299, 491));
		points.add(new Point(307, 491));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(299, 472));
		points.add(new Point(307, 472));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(300, 453));
		points.add(new Point(307, 453));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 481));
		points.add(new Point(382, 481));
		points.add(new Point(382, 415));
		points.add(new Point(258, 415));
		points.add(new Point(258, 368));
		points.add(new Point(250, 368));
		sections.add(points);
		line = new GuiPinLine(sections, MX4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(93, 346));
		points.add(new Point(100, 346));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(92, 357));
		points.add(new Point(100, 357));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.shr"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(250, 357));
		points.add(new Point(257, 357));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.shl"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(250, 346));
		points.add(new Point(257, 346));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldAB"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(627, 397));
		points.add(new Point(175, 397));
		points.add(new Point(175, 378));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1ToABandBB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(175, 336));
		points.add(new Point(175, 323));
		sections.add(points);
		line = new GuiPinLine(sections, AB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(207, 166));
		points.add(new Point(215, 166));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.not"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(208, 181));
		points.add(new Point(215, 181));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.xor"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(208, 197));
		points.add(new Point(215, 197));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.or"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(207, 212));
		points.add(new Point(215, 212));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.and"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(211, 242));
		points.add(new Point(230, 242));
		points.add(new Point(230, 231));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(608, 246));
		points.add(new Point(324, 246));
		points.add(new Point(324, 231));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS2ToALU.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 212));
		points.add(new Point(368, 212));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.add"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 197));
		points.add(new Point(368, 197));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.sub"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 181));
		points.add(new Point(368, 181));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.inc"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(361, 166));
		points.add(new Point(368, 166));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.dec"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(368, 132));
		points.add(new Point(346, 132));
		points.add(new Point(346, 148));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.C_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(288, 148));
		points.add(new Point(288, 117));
		points.add(new Point(476, 117));
		sections.add(points);
		line = new GuiPinLine(sections, ALUexec.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 123));
		points.add(new Point(487, 128));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ALUout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(498, 117));
		points.add(new Point(627, 117));
		sections.add(points);
		line = new GuiPinLine(sections, TSBALUout1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(589, 265));
		points.add(new Point(252, 265));
		points.add(new Point(252, 231));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS3ToALU.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(401, 397));
		points.add(new Point(401, 378));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1ToABandBB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(318, 357));
		points.add(new Point(326, 357));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 357));
		points.add(new Point(483, 357));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldBB"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(401, 337));
		points.add(new Point(401, 293));
		points.add(new Point(476, 293));
		sections.add(points);
		line = new GuiPinLine(sections, BB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 298));
		points.add(new Point(487, 304));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.BBout2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(498, 293));
		points.add(new Point(608, 293));
		sections.add(points);
		line = new GuiPinLine(sections, TSBBBout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(93, 732));
		points.add(new Point(100, 732));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldAW"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(250, 732));
		points.add(new Point(257, 732));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(175, 712));
		points.add(new Point(175, 693));
		sections.add(points);
		line = new GuiPinLine(sections, AW.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(319, 732));
		points.add(new Point(326, 732));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldBW"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 732));
		points.add(new Point(483, 732));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(401, 712));
		points.add(new Point(401, 670));
		points.add(new Point(476, 670));
		sections.add(points);
		line = new GuiPinLine(sections, BW.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 681));
		points.add(new Point(487, 675));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.BWout2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(498, 670));
		points.add(new Point(608, 670));
		sections.add(points);
		line = new GuiPinLine(sections, TSBBWout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(410, 623));
		points.add(new Point(476, 623));
		sections.add(points);
		line = new GuiPinLine(sections, AW.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(429, 623));
		points.add(new Point(429, 576));
		points.add(new Point(476, 576));
		sections.add(points);
		line = new GuiPinLine(sections, AWToAWH.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 581));
		points.add(new Point(487, 587));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.AWHout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(487, 628));
		points.add(new Point(487, 634));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.AWout3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(498, 576));
		points.add(new Point(589, 576));
		sections.add(points);
		line = new GuiPinLine(sections, TSBAWHout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(498, 623));
		points.add(new Point(589, 623));
		sections.add(points);
		line = new GuiPinLine(sections, TSBAWout3.getOutPin(0));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(631, 64, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(613, 80, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(593, 94, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(497, 393, IBUS1ToABandBB.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(405, 316, BB.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(526, 309, TSBBBout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(169, 316, AB.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(413, 241, IBUS2ToALU.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(280, 260, IBUS3ToALU.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(312, 112, ALUexec.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(526, 129, TSBALUout1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(536, 84, TSBABout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(164, 690, AW.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(536, 589, TSBAWHout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(582, 636, TSBAWout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(84, 684, TSBBWout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(500, 767, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(406, 691, BW.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(196, 767, NameConnector.getPin("Bus1.IBUS2")));

		// KONFIGURABILNE LABELE
		gui.addLabel(new GuiTextLabel(569, 118, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(569, 247, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(569, 265, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(569, 292, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(569, 397, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(322, 138, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(250, 231, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(321, 231, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(207, 336, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(425, 336, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(373, 81, (Parameters.dataSize - 1)
				+ "..0", 10));
		gui.addLabel(new GuiTextLabel(320, 87, (Parameters.dataSize - 1)
				+ "..0", 10));

		if (Parameters.dataSize != 16) {
			gui.addLabel(new GuiTextLabel(368, 68, (Parameters.dataSize - 1)
					+ "..0", 10));
			gui.addLabel(new GuiTextLabel(350, 60, ""
					+ (16 - Parameters.dataSize), 10));
		}

		gui.addLabel(new GuiTextLabel(350, 75, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(273, 143, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(380, 143, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(451, 133, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(505, 133, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(347, 165, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(257, 244, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(329, 244, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(162, 333, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(388, 333, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(162, 393, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(388, 393, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(451, 413, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(130, 515, "" + (Parameters.dataSize - 1),
				10));
		gui.addLabel(new GuiTextLabel(290, 480, "" + (Parameters.dataSize - 1),
				10));

	}

	public void putPins() {
		for (int i = 0; i < Parameters.dataSize; i++) {
			NameConnector.addPin(componentName, "AB" + i, ABbits.getOutPin(i));
		}

		NameConnector.addPin(componentName, "AB_Last",
				ABbits.getOutPin(Parameters.dataSize - 1));

		NameConnector.addPin(componentName, "C_Last", ALUexec.getPinC_Last());

		NameConnector.addPin(componentName, "ALU_Last",
				ALUbits.getOutPin(Parameters.dataSize - 1));

		NameConnector.addPin(componentName, "BB_Last",
				BBbits.getOutPin(Parameters.dataSize - 1));

	}

	public void putComponents() {
		NameConnector.addComponent(componentName, "AB", AB);
		NameConnector.addComponent(componentName, "BB", BB);
		NameConnector.addComponent(componentName, "AW", AW);
		NameConnector.addComponent(componentName, "BW", BW);
	}

	public REG RegAB() {
		return AB;
	}

	public REG RegBB() {
		return BB;
	}

	public REG RegAW() {
		return AW;
	}

	public REG RegBW() {
		return BW;
	}
}
