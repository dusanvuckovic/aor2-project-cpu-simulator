package shemes.components;

import java.awt.Dimension;
import java.util.*;

import logic.LogicalComponent;
import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.*;
import gui.*;
import gui.components.*;

public class Fetch3 extends AbstractSchema {

	protected Map<String, LogicalComponent> components;

	public Fetch3() {
		componentName = "Fetch3";
		displayName = "Fetch 3";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {

		components = new HashMap<String, LogicalComponent>();
		for (int i = 0; i < Parameters.fetch3Conections.length; i++) {
			String[] data = Parameters.fetch3Conections[i];
			LogicalComponent component = loadComponent(data);
			component.initArgs(data);
			components.put(data[1], component);
		}

		putComponents();
		putPins();
	}

	private LogicalComponent loadComponent(String[] data) {
		LogicalComponent result = null;
		try {
			String type = data[0];
			String name = data[1];
			int size = Integer.parseInt(data[2]);
			if (type.equalsIgnoreCase("OR")) {
				result = new OR(size, name);
			} else if (type.equalsIgnoreCase("AND")) {
				result = new AND(size, name);
			}
		} catch (Exception e) {
		}
		return result;
	}

	public void initConections() {

		for (String[] data : Parameters.fetch3Conections) {
			LogicalComponent component = getLogicalComponent(data[1]);
			for (int i = 3; i < data.length - 1; i++) {
				Pin x = NameConnector.getPin(data[i]);
				component.setInPin(i - 3, x);
			}
		}
	}

	private final int startX = 50;
	private final int startY = 0;

	public void initGui() {
		gui = new GuiSchema("src/images/Fetch3.png");

		List<LogicalComponent> comp = extractComponents();

		int numColumns = (int) Math.ceil(Math.sqrt(comp.size()));
		int numRows = (int) Math.ceil(comp.size() * 1.0 / numColumns);

		int y = startY;
		int cnt = 0;
		int maxX = 0;

		for (int i = 0; i < numRows && cnt < comp.size(); i++) {
			int max = 0;
			int x = startX;
			for (int j = 0; j < numColumns && cnt < comp.size(); j++) {
				LogicalComponent component = comp.get(cnt);
				GuiComponent compGui = loadGuiComponent(component, x, y);
				gui = compGui.convert(gui);
				x += compGui.getWidth();
				maxX = x;
				if (max < compGui.getHeight()) {
					max = compGui.getHeight();
				}

				cnt++;
			}
			y += max;
		}
		Dimension dim = new Dimension(Math.max(maxX + 50, this.gui.getWidth()),
				Math.max(y + 50, this.gui.getHeight()));
		this.gui.adjustSize(dim);

	}

	private List<LogicalComponent> extractComponents() {
		List<LogicalComponent> result = new LinkedList<LogicalComponent>();
		List<LogicalComponent> comps = new LinkedList<LogicalComponent>(
				components.values());
		List<LogicalComponent> temp = new LinkedList<LogicalComponent>(
				components.values());

		for (LogicalComponent component : comps) {
			if (component instanceof AND) {
				if (component.getInPins().length == 2) {
					LogicalComponent left = getComponent(component.getInPin(0));
					LogicalComponent right = getComponent(component.getInPin(1));
					if ((left != null) && (right != null)
							&& (left instanceof OR) && (right instanceof OR)) {
						TwoOrOneAnd t = new TwoOrOneAnd((OR) left, (OR) right,
								(AND) component);
						result.add(t);
						temp.remove(left);
						temp.remove(right);
					} else {
						result.add(component);
					}
				} else {
					result.add(component);
				}
				temp.remove(component);
			}
		}
		result.addAll(temp);
		return result;
	}

	private LogicalComponent getComponent(Pin out) {
		for (LogicalComponent component : components.values()) {
			for (Pin pin : component.getOutPins()) {
				if (out == pin) {
					return component;
				}
			}
		}
		return null;
	}

	private GuiComponent loadGuiComponent(LogicalComponent component, int x,
			int y) {
		GuiComponent compGui = null;
		if (component instanceof OR) {
			compGui = new OrGui((OR) component, x, y, null, true, true);
		} else if (component instanceof AND) {
			compGui = new AndGui((AND) component, x, y, null, true, true);
		} else if (component instanceof TwoOrOneAnd) {
			TwoOrOneAnd tcmp = (TwoOrOneAnd) component;
			compGui = new TwoOrOneAndGui(tcmp.getOr1(), tcmp.getOr2(),
					tcmp.getAnd(), x, y, null, true);
		}
		return compGui;
	}

	public void putPins() {

		for (LogicalComponent logicalComponent : components.values()) {
			NameConnector.addPins(componentName, logicalComponent.getOutPins());
		}

	}

	protected void putComponents() {

		for (String[] data : Parameters.fetch3Conections) {
			LogicalComponent component = getLogicalComponent(data[1]);
			component.initArgs(data);
		}

	}

	private LogicalComponent getLogicalComponent(String name) {
		return components.get(name);
	}

}
