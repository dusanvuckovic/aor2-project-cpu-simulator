package shemes.components;

import shemes.AbstractSchema;
import util.NameConnector;

public class ProcesorOperaciona extends AbstractSchema {

	public ProcesorOperaciona() {
		componentName = "ProcesorOper";
		displayName = "Operaciona jedinica procesora";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new Bus1());
		this.addSubScheme(new Fetch1());
		this.addSubScheme(new Fetch2());
		this.addSubScheme(new Fetch3());
		this.addSubScheme(new Adr1());
		this.addSubScheme(new Exec1());
		this.addSubScheme(new Exec2());
		this.addSubScheme(new Exec3());
		this.addSubScheme(new Exec4());
		this.addSubScheme(new Intr1());
		this.addSubScheme(new Intr2());
		this.addSubScheme(new Intr3());
	}

}
