package shemes.components;

import logic.Execution;
import logic.components.CLK;
import shemes.AbstractSchema;
import util.NameConnector;

public class Procesor extends AbstractSchema {
	CLK clk;

	public Procesor() {
		componentName = "Procesor";
		displayName = "Procesor";
		NameConnector.addSchema(componentName, this);
		this.addSubScheme(new ProcesorOperaciona());
		this.addSubScheme(new Counter());

		clk = new CLK("CPUCLK", 1, 0);
		Execution.addSequentialComponent(clk);

		putComponents();
	}

	public void putComponents() {
		NameConnector.addComponent(componentName, clk.getName(), clk);
	}

}
