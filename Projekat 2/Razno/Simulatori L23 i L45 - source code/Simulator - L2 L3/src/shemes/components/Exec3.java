package shemes.components;

import gui.GuiImageLabel;
import gui.GuiLableLine;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;
import gui.components.OrGui;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.components.AND;
import logic.components.NOT;
import logic.components.OR;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;

public class Exec3 extends AbstractSchema {

	private OR NZOR;
	private AND NAND1;
	private NOT ZNOT[];
	private AND ZNOR;

	private AND ZAND2;
	private OR COR1, COR2, COR3, COR4, COR5;
	private AND CAND1, CAND2, CAND3, CAND4;
	private NOT CNOT1;
	private NOT VNOT1, VNOT2, VNOT3, VNOT4, VNOT5, VNOT6, VNOT7, VNOT8;
	private AND VAND1, VAND2, VAND3, VAND4, VAND5, VAND6, VAND7, VAND8, VAND9,
			VAND10;
	private OR VOR1, VOR2, VOR3;

	public Exec3() {
		componentName = "Exec3";
		displayName = "Exec 3";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		int nz = Parameters.calcSize(Parameters.exec3Conections, "NZOR");
		NZOR = new OR(nz);
		NAND1 = new AND();

		ZNOT = new NOT[Parameters.dataSize];
		for (int i = 0; i < ZNOT.length; i++) {
			ZNOT[i] = new NOT();
		}
		ZNOR = new AND(Parameters.dataSize);
		ZAND2 = new AND();

		CAND1 = new AND();
		CAND2 = new AND();
		CAND3 = new AND();
		CAND4 = new AND();
		CNOT1 = new NOT();
		COR1 = new OR();
		COR2 = new OR();
		COR3 = new OR(4);
		COR4 = new OR(4);
		COR5 = new OR(4);

		VNOT1 = new NOT();
		VNOT2 = new NOT();
		VNOT3 = new NOT();
		VNOT4 = new NOT();
		VNOT5 = new NOT();
		VNOT6 = new NOT();
		VNOT7 = new NOT();
		VNOT8 = new NOT();
		VAND1 = new AND(3);
		VAND2 = new AND(3);
		VAND3 = new AND();
		VAND4 = new AND(3);
		VAND5 = new AND(3);
		VAND6 = new AND();
		VAND7 = new AND();
		VAND8 = new AND();
		VAND9 = new AND();
		VAND10 = new AND();
		VOR1 = new OR();
		VOR2 = new OR();
		VOR3 = new OR(4);

		putPins();
		putComponents();
	}

	public void initConections() {

		String[] line = Parameters.getLine(Parameters.exec3Conections, "NZOR");
		for (int i = 0; i < NZOR.getInPins().length; i++) {
			NZOR.setInPin(i, NameConnector.getPin(line[i + 3]));
		}
		/*
		 * NZOR.setInputPin(0, NameConnector.getPin("Fetch2.LDB"));
		 * NZOR.setInputPin(1, NameConnector.getPin("Fetch2.POPB"));
		 * NZOR.setInputPin(2, NameConnector.getPin("Fetch2.ADD"));
		 * NZOR.setInputPin(3, NameConnector.getPin("Fetch2.SUB"));
		 * NZOR.setInputPin(4, NameConnector.getPin("Fetch2.INC"));
		 * NZOR.setInputPin(5, NameConnector.getPin("Fetch2.DEC"));
		 * NZOR.setInputPin(6, NameConnector.getPin("Fetch2.AND"));
		 * NZOR.setInputPin(7, NameConnector.getPin("Fetch2.OR"));
		 * NZOR.setInputPin(8, NameConnector.getPin("Fetch2.XOR"));
		 * NZOR.setInputPin(9, NameConnector.getPin("Fetch2.NOT"));
		 * NZOR.setInputPin(10, NameConnector.getPin("Fetch2.ASR"));
		 * NZOR.setInputPin(11, NameConnector.getPin("Fetch2.LSR"));
		 * NZOR.setInputPin(12, NameConnector.getPin("Fetch2.ROR"));
		 * NZOR.setInputPin(13, NameConnector.getPin("Fetch2.RORC"));
		 * NZOR.setInputPin(14, NameConnector.getPin("Fetch2.ASL"));
		 * NZOR.setInputPin(15, NameConnector.getPin("Fetch2.LSL"));
		 * NZOR.setInputPin(16, NameConnector.getPin("Fetch2.ROL"));
		 * NZOR.setInputPin(17, NameConnector.getPin("Fetch2.ROLC"));
		 */
		NAND1.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		NAND1.setInPin(1, NZOR.getOutPin(0));

		for (int i = 0; i < Parameters.dataSize; i++) {
			ZNOT[i].setInPin(0, NameConnector.getPin("Exec1.AB" + i));
		}
		for (int i = 0; i < Parameters.dataSize; i++) {
			ZNOR.setInPin(i, ZNOT[i].getOutPin(0));
		}

		ZAND2.setInPin(0, NZOR.getOutPin(0));
		ZAND2.setInPin(1, ZNOR.getOutPin(0));

		COR1.setInPin(0, NameConnector.getPin("Fetch2.ADD"));
		COR1.setInPin(1, NameConnector.getPin("Fetch2.INC"));
		COR2.setInPin(0, NameConnector.getPin("Fetch2.SUB"));
		COR2.setInPin(1, NameConnector.getPin("Fetch2.DEC"));

		CAND1.setInPin(0, COR1.getOutPin(0));
		CAND1.setInPin(1, NameConnector.getPin("Exec1.C_Last"));

		CNOT1.setInPin(0, NameConnector.getPin("Exec1.C_Last"));
		CAND2.setInPin(0, COR2.getOutPin(0));
		CAND2.setInPin(1, CNOT1.getOutPin(0));

		COR3.setInPin(0, NameConnector.getPin("Fetch2.ASR"));
		COR3.setInPin(1, NameConnector.getPin("Fetch2.LSR"));
		COR3.setInPin(2, NameConnector.getPin("Fetch2.ROR"));
		COR3.setInPin(3, NameConnector.getPin("Fetch2.RORC"));

		CAND3.setInPin(0, COR3.getOutPin(0));
		CAND3.setInPin(1, NameConnector.getPin("Exec1.AB0"));

		COR4.setInPin(0, NameConnector.getPin("Fetch2.ASL"));
		COR4.setInPin(1, NameConnector.getPin("Fetch2.LSL"));
		COR4.setInPin(2, NameConnector.getPin("Fetch2.ROL"));
		COR4.setInPin(3, NameConnector.getPin("Fetch2.ROLC"));

		CAND4.setInPin(0, COR4.getOutPin(0));
		CAND4.setInPin(1, NameConnector.getPin("Exec1.AB_Last"));

		COR5.setInPin(0, CAND1.getOutPin(0));
		COR5.setInPin(1, CAND2.getOutPin(0));
		COR5.setInPin(2, CAND3.getOutPin(0));
		COR5.setInPin(3, CAND4.getOutPin(0));

		VNOT1.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		VNOT2.setInPin(0, NameConnector.getPin("Exec1.BB_Last"));
		VNOT3.setInPin(0, NameConnector.getPin("Exec1.ALU_Last"));
		VAND1.setInPin(0, VNOT1.getOutPin(0));
		VAND1.setInPin(1, VNOT2.getOutPin(0));
		VAND1.setInPin(2, NameConnector.getPin("Exec1.ALU_Last"));
		VAND2.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		VAND2.setInPin(1, NameConnector.getPin("Exec1.BB_Last"));
		VAND2.setInPin(2, VNOT3.getOutPin(0));
		VOR1.setInPin(0, VAND1.getOutPin(0));
		VOR1.setInPin(1, VAND2.getOutPin(0));
		VAND3.setInPin(0, NameConnector.getPin("Fetch2.ADD"));
		VAND3.setInPin(1, VOR1.getOutPin(0));

		VNOT4.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		VNOT5.setInPin(0, NameConnector.getPin("Exec1.BB_Last"));
		VNOT6.setInPin(0, NameConnector.getPin("Exec1.ALU_Last"));
		VAND4.setInPin(0, VNOT4.getOutPin(0));
		VAND4.setInPin(1, NameConnector.getPin("Exec1.BB_Last"));
		VAND4.setInPin(2, NameConnector.getPin("Exec1.ALU_Last"));
		VAND5.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		VAND5.setInPin(1, VNOT5.getOutPin(0));
		VAND5.setInPin(2, VNOT6.getOutPin(0));
		VOR2.setInPin(0, VAND4.getOutPin(0));
		VOR2.setInPin(1, VAND5.getOutPin(0));
		VAND6.setInPin(0, NameConnector.getPin("Fetch2.SUB"));
		VAND6.setInPin(1, VOR2.getOutPin(0));

		VNOT7.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		VAND7.setInPin(0, VNOT7.getOutPin(0));
		VAND7.setInPin(1, NameConnector.getPin("Exec1.ALU_Last"));
		VAND8.setInPin(0, NameConnector.getPin("Fetch2.INC"));
		VAND8.setInPin(1, VAND7.getOutPin(0));

		VNOT8.setInPin(0, NameConnector.getPin("Exec1.ALU_Last"));
		VAND9.setInPin(0, NameConnector.getPin("Exec1.AB_Last"));
		VAND9.setInPin(1, VNOT8.getOutPin(0));
		VAND10.setInPin(0, NameConnector.getPin("Fetch2.DEC"));
		VAND10.setInPin(1, VAND9.getOutPin(0));

		VOR3.setInPin(0, VAND3.getOutPin(0));
		VOR3.setInPin(1, VAND6.getOutPin(0));
		VOR3.setInPin(2, VAND8.getOutPin(0));
		VOR3.setInPin(3, VAND10.getOutPin(0));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Exec3.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(129, 89));
		points.add(new Point(182, 89));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB_Last"));
		gui.addLine(line);

		OrGui orGui = new OrGui(NZOR, 64, 102, null, true, false);
		gui = orGui.convert(gui);

		for (int i = 0; i < Parameters.dataSize; i++) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(129, 407 + 15 * i));
			points.add(new Point(140, 407 + 15 * i));
			sections.add(points);
			String pinName = "Exec1.AB" + (Parameters.dataSize - i - 1);
			line = new GuiPinLine(sections, NameConnector.getPin(pinName));
			gui.addLine(line);
			GuiImageLabel im = new GuiImageLabel(139, 404 + 15 * i,
					"src/images/INV.png");
			gui.addLabel(im);

			gui.addLabel(new GuiTextLabel(104, 411 + 15 * i, "AB", 12));
			gui.addLabel(new GuiTextLabel(120, 413 + 15 * i, ""
					+ (Parameters.dataSize - i - 1), 10));

		}

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(144, 400));
		points.add(new Point(144, 400 + 15 * Parameters.dataSize));
		sections.add(points);
		line = new GuiLableLine(sections);
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		// points.add(new Point(166, 248));
		points.add(new Point(174, 248));
		points.add(new Point(174, 97));
		points.add(new Point(182, 97));
		sections.add(points);
		line = new GuiPinLine(sections, NZOR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(204, 93));
		points.add(new Point(212, 93));
		sections.add(points);
		line = new GuiPinLine(sections, NAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(174, 248));
		points.add(new Point(174, 452));
		points.add(new Point(182, 452));
		sections.add(points);
		line = new GuiPinLine(sections, NZOR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(167, 460));
		points.add(new Point(182, 460));
		sections.add(points);
		line = new GuiPinLine(sections, ZNOR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(204, 456));
		points.add(new Point(212, 456));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 57));
		points.add(new Point(305, 57));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ADD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 68));
		points.add(new Point(305, 68));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.INC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 87));
		points.add(new Point(305, 87));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.SUB"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 98));
		points.add(new Point(305, 98));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.DEC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 115));
		points.add(new Point(333, 115));
		points.add(new Point(333, 74));
		points.add(new Point(341, 74));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(333, 104));
		points.add(new Point(336, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.C_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 62));
		points.add(new Point(341, 62));
		sections.add(points);
		line = new GuiPinLine(sections, COR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 93));
		points.add(new Point(340, 93));
		sections.add(points);
		line = new GuiPinLine(sections, COR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 68));
		points.add(new Point(378, 68));
		points.add(new Point(378, 117));
		points.add(new Point(386, 117));
		sections.add(points);
		line = new GuiPinLine(sections, CAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 98));
		points.add(new Point(371, 98));
		points.add(new Point(371, 125));
		points.add(new Point(388, 125));
		sections.add(points);
		line = new GuiPinLine(sections, CAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 132));
		points.add(new Point(303, 132));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ASR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 148));
		points.add(new Point(304, 148));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.LSR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 163));
		points.add(new Point(304, 163));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ROR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 178));
		points.add(new Point(303, 178));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.RORC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 155));
		points.add(new Point(341, 155));
		sections.add(points);
		line = new GuiPinLine(sections, COR3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 193));
		points.add(new Point(333, 193));
		points.add(new Point(333, 163));
		points.add(new Point(341, 163));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 159));
		points.add(new Point(371, 159));
		points.add(new Point(371, 132));
		points.add(new Point(388, 132));
		sections.add(points);
		line = new GuiPinLine(sections, CAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 208));
		points.add(new Point(303, 208));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ASL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 223));
		points.add(new Point(304, 223));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.LSL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 238));
		points.add(new Point(304, 238));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ROL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 253));
		points.add(new Point(303, 253));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ROLC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 231));
		points.add(new Point(341, 231));
		sections.add(points);
		line = new GuiPinLine(sections, COR4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(295, 269));
		points.add(new Point(333, 269));
		points.add(new Point(333, 238));
		points.add(new Point(341, 238));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 235));
		points.add(new Point(378, 235));
		points.add(new Point(378, 140));
		points.add(new Point(386, 140));
		sections.add(points);
		line = new GuiPinLine(sections, CAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(408, 129));
		points.add(new Point(416, 129));
		sections.add(points);
		line = new GuiPinLine(sections, COR5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 269));
		points.add(new Point(458, 269));
		points.add(new Point(458, 529));
		points.add(new Point(473, 529));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(458, 488));
		points.add(new Point(469, 488));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(458, 443));
		points.add(new Point(473, 443));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(458, 412));
		points.add(new Point(469, 412));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(458, 367));
		points.add(new Point(473, 367));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(458, 337));
		points.add(new Point(469, 337));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.AB_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 284));
		points.add(new Point(443, 284));
		points.add(new Point(443, 450));
		points.add(new Point(469, 450));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(443, 420));
		points.add(new Point(473, 420));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(443, 374));
		points.add(new Point(473, 374));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(443, 344));
		points.add(new Point(469, 344));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.BB_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 299));
		points.add(new Point(427, 299));
		points.add(new Point(427, 537));
		points.add(new Point(469, 537));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.ALU_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(427, 352));
		points.add(new Point(473, 352));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(427, 382));
		points.add(new Point(469, 382));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(427, 427));
		points.add(new Point(473, 427));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(427, 495));
		points.add(new Point(473, 495));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.ALU_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 322));
		points.add(new Point(548, 322));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.ADD"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(496, 344));
		points.add(new Point(503, 344));
		points.add(new Point(503, 356));
		points.add(new Point(513, 356));
		sections.add(points);
		line = new GuiPinLine(sections, VAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(496, 374));
		points.add(new Point(503, 374));
		points.add(new Point(503, 363));
		points.add(new Point(513, 363));
		sections.add(points);
		line = new GuiPinLine(sections, VAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(533, 361));
		points.add(new Point(541, 361));
		points.add(new Point(541, 329));
		points.add(new Point(548, 329));
		sections.add(points);
		line = new GuiPinLine(sections, VOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(571, 325));
		points.add(new Point(586, 325));
		points.add(new Point(586, 427));
		points.add(new Point(594, 427));
		sections.add(points);
		line = new GuiPinLine(sections, VAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 397));
		points.add(new Point(548, 397));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.SUB"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(496, 420));
		points.add(new Point(503, 420));
		points.add(new Point(503, 431));
		points.add(new Point(513, 431));
		sections.add(points);
		line = new GuiPinLine(sections, VAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(496, 450));
		points.add(new Point(503, 450));
		points.add(new Point(503, 439));
		points.add(new Point(513, 439));
		sections.add(points);
		line = new GuiPinLine(sections, VAND5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(533, 437));
		points.add(new Point(541, 437));
		points.add(new Point(541, 405));
		points.add(new Point(548, 405));
		sections.add(points);
		line = new GuiPinLine(sections, VOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(571, 401));
		points.add(new Point(579, 401));
		points.add(new Point(579, 435));
		points.add(new Point(596, 435));
		sections.add(points);
		line = new GuiPinLine(sections, VAND6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 473));
		points.add(new Point(548, 473));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.INC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(427, 458));
		points.add(new Point(469, 458));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec1.ALU_Last"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(496, 492));
		points.add(new Point(541, 492));
		points.add(new Point(541, 480));
		points.add(new Point(548, 480));
		sections.add(points);
		line = new GuiPinLine(sections, VAND7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(571, 476));
		points.add(new Point(579, 476));
		points.add(new Point(579, 443));
		points.add(new Point(596, 443));
		sections.add(points);
		line = new GuiPinLine(sections, VAND8.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(420, 514));
		points.add(new Point(548, 514));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Fetch2.DEC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(496, 533));
		points.add(new Point(541, 533));
		points.add(new Point(541, 522));
		points.add(new Point(548, 522));
		sections.add(points);
		line = new GuiPinLine(sections, VAND9.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(571, 518));
		points.add(new Point(586, 518));
		points.add(new Point(586, 450));
		points.add(new Point(594, 450));
		sections.add(points);
		line = new GuiPinLine(sections, VAND10.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(616, 439));
		points.add(new Point(624, 439));
		sections.add(points);
		line = new GuiPinLine(sections, VOR3.getOutPin(0));
		gui.addLine(line);

		gui.addLabel(new GuiTextLabel(287, 122, "" + Parameters.dataSize, 10));
		gui.addLabel(new GuiTextLabel(287, 275, "" + (Parameters.dataSize - 1),
				10));
		gui.addLabel(new GuiTextLabel(120, 96, "" + (Parameters.dataSize - 1),
				10));
		gui.addLabel(new GuiTextLabel(412, 275, "" + (Parameters.dataSize - 1),
				10));
		gui.addLabel(new GuiTextLabel(412, 290, "" + (Parameters.dataSize - 1),
				10));
		gui.addLabel(new GuiTextLabel(412, 306, "" + (Parameters.dataSize - 1),
				10));

	}

	public void putPins() {
		NameConnector.addPin(componentName, "N", NAND1.getOutPin(0));

		NameConnector.addPin(componentName, "Z", ZAND2.getOutPin(0));

		NameConnector.addPin(componentName, "C", COR5.getOutPin(0));

		NameConnector.addPin(componentName, "V", VOR3.getOutPin(0));

	}

	public void putComponents() {

	}
}
