package shemes.components;

import java.awt.Dimension;
import java.util.*;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.*;
import gui.*;
import gui.components.*;

public class Fetch2 extends AbstractSchema {

	private List<DC> dcs;
	private Map<String, DC> components;
	private Map<DC, String[]> connections;

	public Fetch2() {
		componentName = "Fetch2";
		displayName = "Fetch 2";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		dcs = new LinkedList<DC>();
		components = new HashMap<String, DC>();
		connections = new HashMap<DC, String[]>();
		for (int i = 0; i < Parameters.fetch2Conections.length; i++) {
			String[] data = Parameters.fetch2Conections[i];
			int size = Integer.parseInt(data[2]);
			DC dc = new DC(size);
			dc.initArgs(data);
			dcs.add(dc);
			components.put(data[1], dc);
			connections.put(dc, data);
		}
		putPins();
		putComponents();
	}

	public void putPins() {
		for (DC dc : dcs) {
			NameConnector.addPins(componentName, dc.getOutPins());
		}
	}

	public void putComponents() {
	}

	public void initConections() {

		for (String[] data : Parameters.fetch2Conections) {
			DC dc = components.get(data[1]);
			Pin e = NameConnector.getPin(data[3]);
			dc.setE(e);
			int size = Integer.parseInt(data[2]);
			for (int i = 0; i < size; i++) {
				Pin x = NameConnector.getPin(data[i + 4]);
				dc.setInPin(i, x);
			}
		}
	}

	private final int startX = 50;
	private final int startY = 0;

	public void initGui() {
		gui = new GuiSchema("src/images/Fetch2.png");

		int numColumns = (int) Math.ceil(Math.sqrt(components.size()));
		int numRows = (int) Math.ceil(components.size() * 1.0 / numColumns);

		int y = startY;
		int cnt = 0;
		int maxX = 0;

		for (int i = 0; i < numRows && cnt < components.size(); i++) {
			int maxY = 0;
			int x = startX;
			for (int j = 0; j < numColumns && cnt < components.size(); j++) {
				DC dc = dcs.get(cnt);
				DCGui dcGui = new DCGui(dc, x, y, connections.get(dc), true);
				gui = dcGui.convert(gui);
				x += dcGui.getWidth();
				maxX = x;
				if (maxY < dcGui.getHeight()) {
					maxY = dcGui.getHeight();
				}

				cnt++;
			}
			y += maxY;
		}

		Dimension dim = new Dimension(Math.max(maxX + 50, this.gui.getWidth()),
				Math.max(y + 50, this.gui.getHeight()));
		this.gui.adjustSize(dim);
	}

}
