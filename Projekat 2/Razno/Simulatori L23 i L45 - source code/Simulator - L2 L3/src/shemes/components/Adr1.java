package shemes.components;

import java.awt.Point;
import java.util.*;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;
import util.Parameters;
import gui.*;

public class Adr1 extends AbstractSchema {

	private IntToInt IBUS1ToGPRAR;
	private REG GPRAR;
	private GPR GPRegs;
	private TSB TSBGPRout1;
	private REG SP;
	private TSB TSBSPout2;
	private ADD ADDadr;
	private REG CW;
	private TSB TSBADDout2;
	private TSB TSBCWout3;

	public Adr1() {
		componentName = "Adr1";
		displayName = "Addr";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		IBUS1ToGPRAR = new IntToInt(16, Parameters.GPRNumberOfBits, false,
				false, Parameters.GPRARStartPosition);

		GPRAR = new REG(1, "GPRAR");
		GPRAR.getOutPin(0).setIsInt();
		GPRAR.getOutPin(0).setNumOfLines(Parameters.GPRNumberOfBits);

		GPRegs = new GPR(util.Parameters.numberOfRegisters);
		GPRegs.getOutPin(0).setNumOfLines(16);

		TSBGPRout1 = new TSB("GPRout1");
		TSBGPRout1.getOutPin(0).setNumOfLines(16);

		SP = new REG(1, "SP");
		SP.getOutPin(0).setIsInt();
		SP.getOutPin(0).setNumOfLines(16);
		SP.initVal(4096);// 1000h

		TSBSPout2 = new TSB("SPout2");
		TSBSPout2.getOutPin(0).setNumOfLines(16);

		ADDadr = new ADD();
		ADDadr.getOutPin(0).setIsInt();
		ADDadr.getOutPin(0).setNumOfLines(16);

		CW = new REG(1, "PR");
		CW.getOutPin(0).setIsInt();
		CW.getOutPin(0).setNumOfLines(16);

		TSBCWout3 = new TSB("CWout3");
		TSBCWout3.getOutPin(0).setNumOfLines(16);

		TSBADDout2 = new TSB("ADDout2");
		TSBADDout2.getOutPin(0).setNumOfLines(16);

		putPins();
		putComponents();
	}

	public void initConections() {
		IBUS1ToGPRAR.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));

		GPRAR.setInPin(0, IBUS1ToGPRAR.getOutPin(0));
		GPRAR.setPinLd(NameConnector.getPin("Oper1.ldGPRAR"));
		GPRAR.setPinInc(NameConnector.getPin("Oper1.incGPRAR"));
		GPRAR.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		GPRegs.setAdressPin(GPRAR.getOutPin(0));
		GPRegs.setInputDataPin(NameConnector.getPin("Bus1.IBUS3"));
		GPRegs.setWrite(NameConnector.getPin("Oper1.wrGPR"));
		GPRegs.setRead(new Pin(true, "1"));
		GPRegs.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		TSBGPRout1.setInPin(0, GPRegs.getOutPin(0));
		TSBGPRout1.setE(NameConnector.getPin("Oper1.GPRout1"));

		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS1(TSBGPRout1
				.getOutPin(0));

		SP.setInPin(0, NameConnector.getPin("Bus1.IBUS3"));
		SP.setPinInc(NameConnector.getPin("Oper1.incSP"));
		SP.setPinDec(NameConnector.getPin("Oper1.decSP"));
		SP.setPinLd(NameConnector.getPin("Oper1.ldSP"));
		SP.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		TSBSPout2.setInPin(0, SP.getOutPin(0));
		TSBSPout2.setE(NameConnector.getPin("Oper1.SPout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBSPout2
				.getOutPin(0));

		CW.setInPin(0, NameConnector.getPin("Bus1.IBUS2"));
		CW.setPinLd(NameConnector.getPin("Oper1.ldCW"));
		CW.setClk((CLK) NameConnector.getComponent("CPUCLK"));

		TSBCWout3.setInPin(0, CW.getOutPin(0));
		TSBCWout3.setE(NameConnector.getPin("Oper1.CWout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBCWout3
				.getOutPin(0));

		ADDadr.setPinA(NameConnector.getPin("Bus1.IBUS1"));
		ADDadr.setPinB(NameConnector.getPin("Bus1.IBUS3"));

		TSBADDout2.setInPin(0, ADDadr.getOutPin(0));
		TSBADDout2.setE(NameConnector.getPin("Oper1.ADDout2"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS2(TSBADDout2
				.getOutPin(0));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Adr1.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(589, 7));
		points.add(new Point(589, 736));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(570, 22));
		points.add(new Point(570, 721));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(551, 37));
		points.add(new Point(551, 705));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 93));
		points.add(new Point(242, 74));
		points.add(new Point(588, 74));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1ToGPRAR.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(160, 103));
		points.add(new Point(167, 103));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.incGPRAR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(160, 121));
		points.add(new Point(167, 121));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldGPRAR"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(317, 112));
		points.add(new Point(324, 112));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 131));
		points.add(new Point(242, 213));
		points.add(new Point(268, 213));
		sections.add(points);
		line = new GuiPinLine(sections, GPRAR.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 213));
		points.add(new Point(426, 213));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.wrGPR"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(344, 251));
		points.add(new Point(344, 279));
		points.add(new Point(412, 279));
		sections.add(points);
		line = new GuiPinLine(sections, GPRegs.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(423, 284));
		points.add(new Point(423, 290));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.GPRout1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(435, 279));
		points.add(new Point(589, 279));
		sections.add(points);
		line = new GuiPinLine(sections, TSBGPRout1.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 334));
		points.add(new Point(325, 315));
		points.add(new Point(551, 315));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 343));
		points.add(new Point(250, 343));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.incSP"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 362));
		points.add(new Point(250, 362));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.decSP"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(400, 343));
		points.add(new Point(407, 343));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldSP"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(400, 362));
		points.add(new Point(407, 362));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 371));
		points.add(new Point(325, 400));
		points.add(new Point(412, 400));
		sections.add(points);
		line = new GuiPinLine(sections, SP.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(423, 404));
		points.add(new Point(423, 411));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.SPout2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(434, 400));
		points.add(new Point(570, 400));
		sections.add(points);
		line = new GuiPinLine(sections, TSBSPout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 655));
		points.add(new Point(325, 676));
		points.add(new Point(570, 676));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 646));
		points.add(new Point(250, 646));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldCW"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(242, 627));
		points.add(new Point(250, 627));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 618));
		points.add(new Point(325, 580));
		points.add(new Point(412, 580));
		sections.add(points);
		line = new GuiPinLine(sections, CW.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(325, 486));
		points.add(new Point(325, 447));
		points.add(new Point(412, 447));
		sections.add(points);
		line = new GuiPinLine(sections, ADDadr.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(423, 452));
		points.add(new Point(423, 458));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ADDout2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(433, 447));
		points.add(new Point(570, 447));
		sections.add(points);
		line = new GuiPinLine(sections, TSBADDout2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(433, 580));
		points.add(new Point(551, 580));
		sections.add(points);
		line = new GuiPinLine(sections, TSBCWout3.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(423, 585));
		points.add(new Point(423, 591));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldCW"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(287, 522));
		points.add(new Point(287, 553));
		points.add(new Point(551, 553));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 522));
		points.add(new Point(363, 541));
		points.add(new Point(589, 541));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(344, 176));
		points.add(new Point(344, 150));
		points.add(new Point(551, 150));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(593, 37, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(384, 536, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(572, 54, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(500, 671, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(521, 115, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(441, 146, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(494, 311, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(293, 548, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(338, 71, IBUS1ToGPRAR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(223, 170, GPRAR.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(336, 294, GPRegs.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(487, 275, TSBGPRout1.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(336, 418, SP.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(460, 385, TSBSPout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(460, 435, TSBADDout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(336, 445, ADDadr.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(500, 441, TSBADDout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(500, 395, TSBSPout2.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(500, 576, TSBCWout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(336, 574, CW.getOutPin(0)));

		// KONFIGURABILNE LABELE
		gui.addLabel(new GuiTextLabel(279, 71, (Parameters.GPRARStartPosition
				+ Parameters.GPRNumberOfBits - 1)
				+ ".." + Parameters.GPRARStartPosition, 10));
		gui.addLabel(new GuiTextLabel(231, 88, "" + Parameters.GPRNumberOfBits,
				12));
		gui.addLabel(new GuiTextLabel(294, 154, (Parameters.GPRARStartPosition
				+ Parameters.GPRNumberOfBits - 1)
				+ ".." + Parameters.GPRARStartPosition, 10));
		gui.addLabel(new GuiTextLabel(231, 150,
				"" + Parameters.GPRNumberOfBits, 12));
		gui.addLabel(new GuiTextLabel(281, 221, (Parameters.GPRARStartPosition
				+ Parameters.GPRNumberOfBits - 1)
				+ ".." + Parameters.GPRARStartPosition, 10));

	}

	public void putPins() {
		NameConnector.addPin(componentName, "SP", SP.getOutPin(0));

		NameConnector.addPin(componentName, "CW", CW.getOutPin(0));

		NameConnector.addPin(componentName, "GPRAR", GPRAR.getOutPin(0));

	}

	public void putComponents() {

		for (int i = 0; i < GPRegs.getSize(); i++) {
			NameConnector.addComponent(componentName, "R[" + i + "]",
					GPRegs.getREG(i));
		}
		NameConnector.addComponent(componentName, "GPRAR", GPRAR);
		NameConnector.addComponent(componentName, "SP", SP);
		NameConnector.addComponent(componentName, "CW", CW);

	}

	public int readGPR(int adress) {
		return GPRegs.read(adress);
	}

	public GPR GPR() {
		return GPRegs;
	}

	public REG RegGPRAR() {
		return GPRAR;
	}

	public REG RegSP() {
		return SP;
	}

	public REG RegCW() {
		return CW;
	}

}
