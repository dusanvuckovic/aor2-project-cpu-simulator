package shemes.components;

import gui.GuiPinLabel;
import gui.GuiPinLine;
import gui.GuiSchema;

import java.awt.Point;
import java.util.*;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;

public class Exec2 extends AbstractSchema {

	private IntToBools IBUS1bits;
	private AND IAND1;
	private OR IOR1;
	private AND IAND2;
	private OR IOR2;
	private NOT INOT;
	private RSFF IRSFF;
	private AND TAND1;
	private OR TOR1;
	private AND TAND2;
	private OR TOR2;
	private NOT TNOT;
	private RSFF TRSFF;
	private AND NAND1;
	private AND NAND2;
	private OR NOR1;
	private NOT NNOT1;
	private AND NAND3;
	private NOT NNOT2;
	private AND NAND4;
	private OR NOR2;
	private RSFF NRSFF;
	private AND ZAND1;
	private AND ZAND2;
	private OR ZOR1;
	private NOT ZNOT1;
	private AND ZAND3;
	private NOT ZNOT2;
	private AND ZAND4;
	private OR ZOR2;
	private RSFF ZRSFF;
	private AND CAND1;
	private AND CAND2;
	private OR COR1;
	private NOT CNOT1;
	private AND CAND3;
	private NOT CNOT2;
	private AND CAND4;
	private OR COR2;
	private RSFF CRSFF;
	private AND VAND1;
	private AND VAND2;
	private OR VOR1;
	private NOT VNOT1;
	private AND VAND3;
	private NOT VNOT2;
	private AND VAND4;
	private OR VOR2;
	private RSFF VRSFF;
	private AND L0AND1;
	private AND L0AND2;
	private OR L0OR1;
	private NOT L0NOT1;
	private AND L0AND3;
	private NOT L0NOT2;
	private AND L0AND4;
	private OR L0OR2;
	private RSFF L0RSFF;
	private AND L1AND1;
	private AND L1AND2;
	private OR L1OR1;
	private NOT L1NOT1;
	private AND L1AND3;
	private NOT L1NOT2;
	private AND L1AND4;
	private OR L1OR2;
	private RSFF L1RSFF;
	private AND L2AND1;
	private AND L2AND2;
	private OR L2OR1;
	private NOT L2NOT1;
	private AND L2AND3;
	private NOT L2NOT2;
	private AND L2AND4;
	private OR L2OR2;
	private RSFF L2RSFF;
	private BoolsToInt PSWH;
	private TSB TSBPSWHout3;
	private BoolsToInt PSWL;
	private TSB TSBPSWLout3;
	public RSFF STARTRSFF;

	public Exec2() {
		componentName = "Exec2";
		displayName = "Exec 2";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		IBUS1bits = new IntToBools(16, 16);

		IAND1 = new AND();
		IOR1 = new OR();
		INOT = new NOT();
		IAND2 = new AND();
		IOR2 = new OR();
		IRSFF = new RSFF("I");

		TAND1 = new AND();
		TOR1 = new OR();
		TNOT = new NOT();
		TAND2 = new AND();
		TOR2 = new OR();
		TRSFF = new RSFF("T");

		NAND1 = new AND();
		NAND2 = new AND();
		NOR1 = new OR();
		NNOT1 = new NOT();
		NAND3 = new AND();
		NNOT2 = new NOT();
		NAND4 = new AND();
		NOR2 = new OR();
		NRSFF = new RSFF("N");

		ZAND1 = new AND();
		ZAND2 = new AND();
		ZOR1 = new OR();
		ZNOT1 = new NOT();
		ZAND3 = new AND();
		ZNOT2 = new NOT();
		ZAND4 = new AND();
		ZOR2 = new OR();
		ZRSFF = new RSFF("Z");

		CAND1 = new AND();
		CAND2 = new AND();
		COR1 = new OR();
		CNOT1 = new NOT();
		CAND3 = new AND();
		CNOT2 = new NOT();
		CAND4 = new AND();
		COR2 = new OR();
		CRSFF = new RSFF("C");

		VAND1 = new AND();
		VAND2 = new AND();
		VOR1 = new OR();
		VNOT1 = new NOT();
		VAND3 = new AND();
		VNOT2 = new NOT();
		VAND4 = new AND();
		VOR2 = new OR();
		VRSFF = new RSFF("V");

		L0AND1 = new AND();
		L0AND2 = new AND();
		L0OR1 = new OR();
		L0NOT1 = new NOT();
		L0AND3 = new AND();
		L0NOT2 = new NOT();
		L0AND4 = new AND();
		L0OR2 = new OR();
		L0RSFF = new RSFF("L0");

		L1AND1 = new AND();
		L1AND2 = new AND();
		L1OR1 = new OR();
		L1NOT1 = new NOT();
		L1AND3 = new AND();
		L1NOT2 = new NOT();
		L1AND4 = new AND();
		L1OR2 = new OR();
		L1RSFF = new RSFF("L1");

		L2AND1 = new AND();
		L2AND2 = new AND();
		L2OR1 = new OR();
		L2NOT1 = new NOT();
		L2AND3 = new AND();
		L2NOT2 = new NOT();
		L2AND4 = new AND();
		L2OR2 = new OR();
		L2RSFF = new RSFF("L2");

		PSWH = new BoolsToInt(8, 8);
		TSBPSWHout3 = new TSB("PSWHout3");
		TSBPSWHout3.getOutPin(0).setNumOfLines(8);

		PSWL = new BoolsToInt(8, 8);
		TSBPSWLout3 = new TSB("PSWLout3");
		TSBPSWLout3.getOutPin(0).setNumOfLines(8);

		STARTRSFF = new RSFF("START");

		putPins();
		putComponents();
	}

	public void initConections() {
		IBUS1bits.setInPin(0, NameConnector.getPin("Bus1.IBUS1"));

		IAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWH"));
		IAND1.setInPin(1, IBUS1bits.getOutPin(7));
		IOR1.setInPin(0, NameConnector.getPin("Oper1.stPSWI"));
		IOR1.setInPin(1, IAND1.getOutPin(0));
		INOT.setInPin(0, IBUS1bits.getOutPin(7));
		IAND2.setInPin(0, NameConnector.getPin("Oper1.ldPSWH"));
		IAND2.setInPin(1, INOT.getOutPin(0));
		IOR2.setInPin(0, IAND2.getOutPin(0));
		IOR2.setInPin(1, NameConnector.getPin("Oper1.clPSWI"));
		IRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		IRSFF.setReset(new Pin(false, "0"));
		IRSFF.setPinS(IOR1.getOutPin(0));
		IRSFF.setPinR(IOR2.getOutPin(0));

		TAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWH"));
		TAND1.setInPin(1, IBUS1bits.getOutPin(6));
		TOR1.setInPin(0, NameConnector.getPin("Oper1.stPSWT"));
		TOR1.setInPin(1, TAND1.getOutPin(0));
		TNOT.setInPin(0, IBUS1bits.getOutPin(6));
		TAND2.setInPin(0, NameConnector.getPin("Oper1.ldPSWH"));
		TAND2.setInPin(1, TNOT.getOutPin(0));
		TOR2.setInPin(0, TAND2.getOutPin(0));
		TOR2.setInPin(1, NameConnector.getPin("Oper1.clPSWT"));
		TRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		TRSFF.setReset(new Pin(false, "0"));
		TRSFF.setPinS(TOR1.getOutPin(0));
		TRSFF.setPinR(TOR2.getOutPin(0));

		NAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		NAND1.setInPin(1, IBUS1bits.getOutPin(0));
		NAND2.setInPin(0, NameConnector.getPin("Oper1.ldN"));
		NAND2.setInPin(1, NameConnector.getPin("Exec3.N"));
		NOR1.setInPin(0, NAND2.getOutPin(0));
		NOR1.setInPin(1, NAND1.getOutPin(0));
		NNOT1.setInPin(0, IBUS1bits.getOutPin(0));
		NAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		NAND3.setInPin(1, NNOT1.getOutPin(0));
		NNOT2.setInPin(0, NameConnector.getPin("Exec3.N"));
		NAND4.setInPin(0, NameConnector.getPin("Oper1.ldN"));
		NAND4.setInPin(1, NNOT2.getOutPin(0));
		NOR2.setInPin(0, NAND3.getOutPin(0));
		NOR2.setInPin(1, NAND4.getOutPin(0));
		NRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		NRSFF.setReset(new Pin(false, "0"));
		NRSFF.setPinS(NOR1.getOutPin(0));
		NRSFF.setPinR(NOR2.getOutPin(0));

		ZAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		ZAND1.setInPin(1, IBUS1bits.getOutPin(1));
		ZAND2.setInPin(0, NameConnector.getPin("Oper1.ldZ"));
		ZAND2.setInPin(1, NameConnector.getPin("Exec3.Z"));
		ZOR1.setInPin(0, ZAND2.getOutPin(0));
		ZOR1.setInPin(1, ZAND1.getOutPin(0));
		ZNOT1.setInPin(0, IBUS1bits.getOutPin(1));
		ZAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		ZAND3.setInPin(1, ZNOT1.getOutPin(0));
		ZNOT2.setInPin(0, NameConnector.getPin("Exec3.Z"));
		ZAND4.setInPin(0, NameConnector.getPin("Oper1.ldZ"));
		ZAND4.setInPin(1, ZNOT2.getOutPin(0));
		ZOR2.setInPin(0, ZAND3.getOutPin(0));
		ZOR2.setInPin(1, ZAND4.getOutPin(0));
		ZRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		ZRSFF.setReset(new Pin(false, "0"));
		ZRSFF.setPinS(ZOR1.getOutPin(0));
		ZRSFF.setPinR(ZOR2.getOutPin(0));

		CAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		CAND1.setInPin(1, IBUS1bits.getOutPin(2));
		CAND2.setInPin(0, NameConnector.getPin("Oper1.ldC"));
		CAND2.setInPin(1, NameConnector.getPin("Exec3.C"));
		COR1.setInPin(0, CAND2.getOutPin(0));
		COR1.setInPin(1, CAND1.getOutPin(0));
		CNOT1.setInPin(0, IBUS1bits.getOutPin(2));
		CAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		CAND3.setInPin(1, CNOT1.getOutPin(0));
		CNOT2.setInPin(0, NameConnector.getPin("Exec3.C"));
		CAND4.setInPin(0, NameConnector.getPin("Oper1.ldC"));
		CAND4.setInPin(1, CNOT2.getOutPin(0));
		COR2.setInPin(0, CAND3.getOutPin(0));
		COR2.setInPin(1, CAND4.getOutPin(0));
		CRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		CRSFF.setReset(new Pin(false, "0"));
		CRSFF.setPinS(COR1.getOutPin(0));
		CRSFF.setPinR(COR2.getOutPin(0));

		VAND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		VAND1.setInPin(1, IBUS1bits.getOutPin(3));
		VAND2.setInPin(0, NameConnector.getPin("Oper1.ldV"));
		VAND2.setInPin(1, NameConnector.getPin("Exec3.V"));
		VOR1.setInPin(0, VAND2.getOutPin(0));
		VOR1.setInPin(1, VAND1.getOutPin(0));
		VNOT1.setInPin(0, IBUS1bits.getOutPin(3));
		VAND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		VAND3.setInPin(1, VNOT1.getOutPin(0));
		VNOT2.setInPin(0, NameConnector.getPin("Exec3.V"));
		VAND4.setInPin(0, NameConnector.getPin("Oper1.ldV"));
		VAND4.setInPin(1, VNOT2.getOutPin(0));
		VOR2.setInPin(0, VAND3.getOutPin(0));
		VOR2.setInPin(1, VAND4.getOutPin(0));
		VRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		VRSFF.setReset(new Pin(false, "0"));
		VRSFF.setPinS(VOR1.getOutPin(0));
		VRSFF.setPinR(VOR2.getOutPin(0));

		L0AND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		L0AND1.setInPin(1, IBUS1bits.getOutPin(4));
		L0AND2.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L0AND2.setInPin(1, NameConnector.getPin("Intr2.prl0"));
		L0OR1.setInPin(0, L0AND2.getOutPin(0));
		L0OR1.setInPin(1, L0AND1.getOutPin(0));
		L0NOT1.setInPin(0, IBUS1bits.getOutPin(4));
		L0AND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		L0AND3.setInPin(1, L0NOT1.getOutPin(0));
		L0NOT2.setInPin(0, NameConnector.getPin("Intr2.prl0"));
		L0AND4.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L0AND4.setInPin(1, L0NOT2.getOutPin(0));
		L0OR2.setInPin(0, L0AND3.getOutPin(0));
		L0OR2.setInPin(1, L0AND4.getOutPin(0));
		L0RSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		L0RSFF.setReset(new Pin(false, "0"));
		L0RSFF.setPinS(L0OR1.getOutPin(0));
		L0RSFF.setPinR(L0OR2.getOutPin(0));

		L1AND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		L1AND1.setInPin(1, IBUS1bits.getOutPin(5));
		L1AND2.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L1AND2.setInPin(1, NameConnector.getPin("Intr2.prl1"));
		L1OR1.setInPin(0, L1AND2.getOutPin(0));
		L1OR1.setInPin(1, L1AND1.getOutPin(0));
		L1NOT1.setInPin(0, IBUS1bits.getOutPin(5));
		L1AND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		L1AND3.setInPin(1, L1NOT1.getOutPin(0));
		L1NOT2.setInPin(0, NameConnector.getPin("Intr2.prl1"));
		L1AND4.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L1AND4.setInPin(1, L1NOT2.getOutPin(0));
		L1OR2.setInPin(0, L1AND3.getOutPin(0));
		L1OR2.setInPin(1, L1AND4.getOutPin(0));
		L1RSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		L1RSFF.setReset(new Pin(false, "0"));
		L1RSFF.setPinS(L1OR1.getOutPin(0));
		L1RSFF.setPinR(L1OR2.getOutPin(0));

		L2AND1.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		L2AND1.setInPin(1, IBUS1bits.getOutPin(6));
		L2AND2.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L2AND2.setInPin(1, NameConnector.getPin("Intr2.prl2"));
		L2OR1.setInPin(0, L2AND2.getOutPin(0));
		L2OR1.setInPin(1, L2AND1.getOutPin(0));
		L2NOT1.setInPin(0, IBUS1bits.getOutPin(6));
		L2AND3.setInPin(0, NameConnector.getPin("Oper1.ldPSWL"));
		L2AND3.setInPin(1, L2NOT1.getOutPin(0));
		L2NOT2.setInPin(0, NameConnector.getPin("Intr2.prl2"));
		L2AND4.setInPin(0, NameConnector.getPin("Oper1.ldL"));
		L2AND4.setInPin(1, L2NOT2.getOutPin(0));
		L2OR2.setInPin(0, L2AND3.getOutPin(0));
		L2OR2.setInPin(1, L2AND4.getOutPin(0));

		L2RSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		L2RSFF.setReset(new Pin(false, "0"));
		L2RSFF.setPinS(L2OR1.getOutPin(0));
		L2RSFF.setPinR(L2OR2.getOutPin(0));

		for (int i = 0; i < 6; i++) {
			PSWH.setInPin(i, new Pin(false, "0"));
		}
		PSWH.setInPin(6, TRSFF.getOutPin(0));
		PSWH.setInPin(7, IRSFF.getOutPin(0));
		TSBPSWHout3.setInPin(0, PSWH.getOutPin(0));
		TSBPSWHout3.setE(NameConnector.getPin("Oper1.PSWHout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBPSWHout3
				.getOutPin(0));

		PSWL.setInPin(0, NRSFF.getOutPin(0));
		PSWL.setInPin(1, ZRSFF.getOutPin(0));
		PSWL.setInPin(2, CRSFF.getOutPin(0));
		PSWL.setInPin(3, VRSFF.getOutPin(0));
		PSWL.setInPin(4, L0RSFF.getOutPin(0));
		PSWL.setInPin(5, L1RSFF.getOutPin(0));
		PSWL.setInPin(6, L2RSFF.getOutPin(0));
		PSWL.setInPin(7, new Pin(false, "0"));
		TSBPSWLout3.setInPin(0, PSWL.getOutPin(0));
		TSBPSWLout3.setE(NameConnector.getPin("Oper1.PSWLout3"));
		((Bus1) NameConnector.getSchema("Bus1")).addOnIBUS3(TSBPSWLout3
				.getOutPin(0));

		STARTRSFF.setClk((CLK) NameConnector.getComponent("CPUCLK"));
		STARTRSFF.setPinS(new Pin(false, "0"));
		STARTRSFF.setPinR(NameConnector.getPin("Oper1.clSTART"));
		STARTRSFF.setInit(true);

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Exec2.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(341, 16));
		points.add(new Point(341, 729));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(323, 286));
		points.add(new Point(323, 729));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS2"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(304, 300));
		points.add(new Point(304, 715));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(102, 16));
		points.add(new Point(102, 246));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(341, 230));
		points.add(new Point(102, 230));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Bus1.IBUS1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(152, 35));
		points.add(new Point(152, 193));
		points.add(new Point(157, 193));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(153, 65));
		points.add(new Point(157, 65));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(153, 99));
		points.add(new Point(157, 99));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(153, 159));
		points.add(new Point(157, 159));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldPSWH"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(104, 108));
		points.add(new Point(154, 108));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(146, 108));
		points.add(new Point(146, 74));
		points.add(new Point(157, 74));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(7));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(178, 69));
		points.add(new Point(222, 69));
		sections.add(points);
		line = new GuiPinLine(sections, IAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(218, 60));
		points.add(new Point(222, 60));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPSWI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(240, 87));
		points.add(new Point(245, 87));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(241, 65));
		points.add(new Point(245, 65));
		sections.add(points);
		line = new GuiPinLine(sections, IOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(178, 104));
		points.add(new Point(222, 104));
		sections.add(points);
		line = new GuiPinLine(sections, IAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(218, 113));
		points.add(new Point(222, 113));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPSWI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(241, 109));
		points.add(new Point(245, 109));
		sections.add(points);
		line = new GuiPinLine(sections, IOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 65));
		points.add(new Point(300, 65));
		sections.add(points);
		line = new GuiPinLine(sections, IRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 109));
		points.add(new Point(300, 109));
		sections.add(points);
		line = new GuiPinLine(sections, IRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(104, 203));
		points.add(new Point(154, 203));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(146, 202));
		points.add(new Point(146, 168));
		points.add(new Point(158, 168));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(178, 164));
		points.add(new Point(222, 164));
		sections.add(points);
		line = new GuiPinLine(sections, TAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(218, 154));
		points.add(new Point(223, 154));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.stPSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(241, 159));
		points.add(new Point(245, 159));
		sections.add(points);
		line = new GuiPinLine(sections, TOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(240, 181));
		points.add(new Point(245, 181));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(178, 198));
		points.add(new Point(222, 198));
		sections.add(points);
		line = new GuiPinLine(sections, TAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(218, 208));
		points.add(new Point(222, 208));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clPSWT"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(241, 203));
		points.add(new Point(245, 203));
		sections.add(points);
		line = new GuiPinLine(sections, TOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 159));
		points.add(new Point(300, 159));
		sections.add(points);
		line = new GuiPinLine(sections, TRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(294, 203));
		points.add(new Point(300, 203));
		sections.add(points);
		line = new GuiPinLine(sections, TRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 302));
		points.add(new Point(114, 302));
		sections.add(points);
		line = new GuiPinLine(sections, IRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 316));
		points.add(new Point(114, 316));
		sections.add(points);
		line = new GuiPinLine(sections, TRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 330));
		points.add(new Point(114, 330));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 345));
		points.add(new Point(114, 345));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 359));
		points.add(new Point(114, 359));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 373));
		points.add(new Point(114, 373));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 388));
		points.add(new Point(114, 388));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 402));
		points.add(new Point(114, 402));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(137, 351));
		points.add(new Point(202, 351));
		sections.add(points);
		line = new GuiPinLine(sections, PSWH.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(213, 356));
		points.add(new Point(213, 363));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PSWHout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(223, 351));
		points.add(new Point(304, 351));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPSWHout3.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 436));
		points.add(new Point(114, 436));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(108, 451));
		points.add(new Point(115, 451));
		sections.add(points);
		line = new GuiPinLine(sections, L2RSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 465));
		points.add(new Point(115, 465));
		sections.add(points);
		line = new GuiPinLine(sections, L1RSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 479));
		points.add(new Point(115, 479));
		sections.add(points);
		line = new GuiPinLine(sections, L0RSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 493));
		points.add(new Point(115, 493));
		sections.add(points);
		line = new GuiPinLine(sections, VRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 508));
		points.add(new Point(115, 508));
		sections.add(points);
		line = new GuiPinLine(sections, CRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 523));
		points.add(new Point(115, 523));
		sections.add(points);
		line = new GuiPinLine(sections, ZRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(109, 537));
		points.add(new Point(115, 537));
		sections.add(points);
		line = new GuiPinLine(sections, NRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(137, 487));
		points.add(new Point(202, 487));
		sections.add(points);
		line = new GuiPinLine(sections, PSWL.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(213, 492));
		points.add(new Point(213, 497));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.PSWLout3"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(223, 487));
		points.add(new Point(304, 487));
		sections.add(points);
		line = new GuiPinLine(sections, TSBPSWLout3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(128, 593));
		points.add(new Point(136, 593));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(130, 617));
		points.add(new Point(136, 617));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(128, 643));
		points.add(new Point(136, 643));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.clSTART"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(192, 593));
		points.add(new Point(199, 593));
		sections.add(points);
		line = new GuiPinLine(sections, STARTRSFF.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(192, 643));
		points.add(new Point(199, 643));
		sections.add(points);
		line = new GuiPinLine(sections, STARTRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(394, 35));
		points.add(new Point(394, 665));
		points.add(new Point(399, 665));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 35));
		points.add(new Point(394, 665));
		points.add(new Point(399, 665));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 65));
		points.add(new Point(400, 65));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 99));
		points.add(new Point(400, 99));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 159));
		points.add(new Point(400, 159));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 193));
		points.add(new Point(400, 193));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 253));
		points.add(new Point(400, 253));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 288));
		points.add(new Point(400, 288));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 347));
		points.add(new Point(400, 347));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 382));
		points.add(new Point(400, 382));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 442));
		points.add(new Point(400, 442));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 476));
		points.add(new Point(400, 476));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 536));
		points.add(new Point(399, 536));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 571));
		points.add(new Point(400, 571));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(394, 630));
		points.add(new Point(400, 630));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldPSWL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 52));
		points.add(new Point(438, 52));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 52));
		points.add(new Point(432, 112));
		points.add(new Point(437, 112));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldN"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(342, 109));
		points.add(new Point(397, 109));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 108));
		points.add(new Point(388, 74));
		points.add(new Point(400, 74));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 87));
		points.add(new Point(426, 87));
		points.add(new Point(426, 61));
		points.add(new Point(438, 61));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(426, 87));
		points.add(new Point(426, 121));
		points.add(new Point(434, 121));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.N"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 57));
		points.add(new Point(460, 57));
		points.add(new Point(460, 60));
		points.add(new Point(465, 60));
		sections.add(points);
		line = new GuiPinLine(sections, NAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 69));
		points.add(new Point(465, 69));
		sections.add(points);
		line = new GuiPinLine(sections, NAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 65));
		points.add(new Point(488, 65));
		sections.add(points);
		line = new GuiPinLine(sections, NOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 87));
		points.add(new Point(487, 87));
		sections.add(points);

		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 104));
		points.add(new Point(465, 104));
		sections.add(points);
		line = new GuiPinLine(sections, NAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 116));
		points.add(new Point(460, 116));
		points.add(new Point(460, 113));
		points.add(new Point(465, 113));
		sections.add(points);
		line = new GuiPinLine(sections, NAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 109));
		points.add(new Point(488, 109));
		sections.add(points);
		line = new GuiPinLine(sections, NOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(537, 65));
		points.add(new Point(542, 65));
		sections.add(points);
		line = new GuiPinLine(sections, NRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(537, 109));
		points.add(new Point(542, 109));
		sections.add(points);
		line = new GuiPinLine(sections, NRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 146));
		points.add(new Point(438, 146));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 146));
		points.add(new Point(432, 206));
		points.add(new Point(438, 206));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldZ"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 203));
		points.add(new Point(397, 203));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 203));
		points.add(new Point(388, 168));
		points.add(new Point(400, 168));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 164));
		points.add(new Point(465, 164));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 151));
		points.add(new Point(460, 151));
		points.add(new Point(460, 154));
		points.add(new Point(465, 154));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 159));
		points.add(new Point(488, 159));
		sections.add(points);
		line = new GuiPinLine(sections, ZOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 181));
		points.add(new Point(488, 181));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 198));
		points.add(new Point(465, 198));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 181));
		points.add(new Point(426, 181));
		points.add(new Point(426, 156));
		points.add(new Point(438, 156));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(426, 181));
		points.add(new Point(426, 215));
		points.add(new Point(434, 215));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.Z"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 211));
		points.add(new Point(460, 211));
		points.add(new Point(460, 208));
		points.add(new Point(465, 208));
		sections.add(points);
		line = new GuiPinLine(sections, ZAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 203));
		points.add(new Point(488, 203));
		sections.add(points);
		line = new GuiPinLine(sections, ZOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 159));
		points.add(new Point(542, 159));
		sections.add(points);
		line = new GuiPinLine(sections, ZRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 203));
		points.add(new Point(542, 203));
		sections.add(points);
		line = new GuiPinLine(sections, ZRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 241));
		points.add(new Point(438, 241));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 241));
		points.add(new Point(432, 300));
		points.add(new Point(438, 300));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldC"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 275));
		points.add(new Point(426, 275));
		points.add(new Point(426, 250));
		points.add(new Point(438, 250));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(426, 275));
		points.add(new Point(426, 310));
		points.add(new Point(434, 310));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.C"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 297));
		points.add(new Point(388, 297));
		points.add(new Point(388, 263));
		points.add(new Point(400, 263));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 297));
		points.add(new Point(397, 297));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 245));
		points.add(new Point(460, 245));
		points.add(new Point(460, 248));
		points.add(new Point(465, 248));
		sections.add(points);
		line = new GuiPinLine(sections, CAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 258));
		points.add(new Point(465, 258));
		sections.add(points);
		line = new GuiPinLine(sections, CAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 292));
		points.add(new Point(464, 292));
		sections.add(points);
		line = new GuiPinLine(sections, CAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 305));
		points.add(new Point(460, 305));
		points.add(new Point(460, 302));
		points.add(new Point(465, 302));
		sections.add(points);
		line = new GuiPinLine(sections, CAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 253));
		points.add(new Point(488, 253));
		sections.add(points);
		line = new GuiPinLine(sections, COR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 275));
		points.add(new Point(488, 275));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 297));
		points.add(new Point(488, 297));
		sections.add(points);
		line = new GuiPinLine(sections, COR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 253));
		points.add(new Point(542, 253));
		sections.add(points);
		line = new GuiPinLine(sections, CRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 297));
		points.add(new Point(542, 297));
		sections.add(points);
		line = new GuiPinLine(sections, CRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 335));
		points.add(new Point(438, 335));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 335));
		points.add(new Point(432, 395));
		points.add(new Point(438, 395));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldV"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 370));
		points.add(new Point(426, 370));
		points.add(new Point(426, 344));
		points.add(new Point(438, 344));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.V"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(426, 370));
		points.add(new Point(426, 404));
		points.add(new Point(434, 404));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec3.V"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 392));
		points.add(new Point(397, 392));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 392));
		points.add(new Point(388, 357));
		points.add(new Point(400, 357));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(3));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 352));
		points.add(new Point(465, 352));
		sections.add(points);
		line = new GuiPinLine(sections, VAND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 340));
		points.add(new Point(460, 340));
		points.add(new Point(460, 343));
		points.add(new Point(465, 343));
		sections.add(points);
		line = new GuiPinLine(sections, VAND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 347));
		points.add(new Point(488, 347));
		sections.add(points);
		line = new GuiPinLine(sections, VOR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 370));
		points.add(new Point(488, 370));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 387));
		points.add(new Point(465, 387));
		sections.add(points);
		line = new GuiPinLine(sections, VAND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 399));
		points.add(new Point(460, 399));
		points.add(new Point(460, 396));
		points.add(new Point(465, 396));
		sections.add(points);
		line = new GuiPinLine(sections, VAND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 392));
		points.add(new Point(488, 392));
		sections.add(points);
		line = new GuiPinLine(sections, VOR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 347));
		points.add(new Point(542, 347));
		sections.add(points);
		line = new GuiPinLine(sections, VRSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 392));
		points.add(new Point(542, 392));
		sections.add(points);
		line = new GuiPinLine(sections, VRSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 429));
		points.add(new Point(438, 429));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 429));
		points.add(new Point(432, 678));
		points.add(new Point(438, 678));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 489));
		points.add(new Point(438, 489));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 524));
		points.add(new Point(438, 524));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 583));
		points.add(new Point(438, 583));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(432, 618));
		points.add(new Point(438, 618));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Oper1.ldL"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 464));
		points.add(new Point(426, 464));
		points.add(new Point(426, 439));
		points.add(new Point(438, 439));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(426, 464));
		points.add(new Point(426, 498));
		points.add(new Point(434, 498));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 486));
		points.add(new Point(397, 486));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 486));
		points.add(new Point(388, 451));
		points.add(new Point(400, 451));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 447));
		points.add(new Point(465, 447));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 434));
		points.add(new Point(460, 434));
		points.add(new Point(460, 437));
		points.add(new Point(465, 437));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 442));
		points.add(new Point(488, 442));
		sections.add(points);
		line = new GuiPinLine(sections, L0OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 464));
		points.add(new Point(488, 464));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 481));
		points.add(new Point(465, 481));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 494));
		points.add(new Point(460, 494));
		points.add(new Point(460, 491));
		points.add(new Point(465, 491));
		sections.add(points);
		line = new GuiPinLine(sections, L0AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 486));
		points.add(new Point(488, 486));
		sections.add(points);
		line = new GuiPinLine(sections, L0OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 442));
		points.add(new Point(542, 442));
		sections.add(points);
		line = new GuiPinLine(sections, L0RSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 486));
		points.add(new Point(542, 486));
		sections.add(points);
		line = new GuiPinLine(sections, L0RSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 558));
		points.add(new Point(426, 558));
		points.add(new Point(426, 533));
		points.add(new Point(438, 533));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(426, 558));
		points.add(new Point(426, 593));
		points.add(new Point(434, 593));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 580));
		points.add(new Point(397, 580));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 580));
		points.add(new Point(388, 546));
		points.add(new Point(400, 546));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(5));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 541));
		points.add(new Point(465, 541));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 528));
		points.add(new Point(460, 528));
		points.add(new Point(460, 531));
		points.add(new Point(465, 531));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 575));
		points.add(new Point(465, 575));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 588));
		points.add(new Point(460, 588));
		points.add(new Point(460, 585));
		points.add(new Point(465, 585));
		sections.add(points);
		line = new GuiPinLine(sections, L1AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(483, 536));
		points.add(new Point(488, 536));
		sections.add(points);
		line = new GuiPinLine(sections, L1OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 558));
		points.add(new Point(488, 558));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 580));
		points.add(new Point(488, 580));
		sections.add(points);
		line = new GuiPinLine(sections, L1OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 536));
		points.add(new Point(542, 536));
		sections.add(points);
		line = new GuiPinLine(sections, L1RSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 580));
		points.add(new Point(542, 580));
		sections.add(points);
		line = new GuiPinLine(sections, L1RSFF.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(382, 652));
		points.add(new Point(426, 652));
		points.add(new Point(426, 627));
		points.add(new Point(438, 627));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(382, 652));
		points.add(new Point(426, 652));
		points.add(new Point(426, 627));
		points.add(new Point(438, 627));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(426, 652));
		points.add(new Point(426, 687));
		points.add(new Point(434, 687));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr2.prl2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(343, 674));
		points.add(new Point(397, 674));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(388, 674));
		points.add(new Point(388, 640));
		points.add(new Point(400, 640));
		sections.add(points);
		line = new GuiPinLine(sections, IBUS1bits.getOutPin(6));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 635));
		points.add(new Point(465, 635));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 623));
		points.add(new Point(460, 623));
		points.add(new Point(460, 626));
		points.add(new Point(465, 626));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 630));
		points.add(new Point(488, 630));
		sections.add(points);
		line = new GuiPinLine(sections, L2OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(419, 670));
		points.add(new Point(465, 670));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(457, 682));
		points.add(new Point(460, 682));
		points.add(new Point(460, 679));
		points.add(new Point(465, 679));
		sections.add(points);
		line = new GuiPinLine(sections, L2AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 674));
		points.add(new Point(488, 674));
		sections.add(points);
		line = new GuiPinLine(sections, L2OR2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(482, 652));
		points.add(new Point(488, 652));
		sections.add(points);
		line = new GuiPinLine(sections,
				NameConnector.getPin("LogicalComponent.CPUCLK"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 630));
		points.add(new Point(542, 630));
		sections.add(points);
		line = new GuiPinLine(sections, L2RSFF.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(536, 674));
		points.add(new Point(542, 674));
		sections.add(points);
		line = new GuiPinLine(sections, L2RSFF.getOutPin(1));
		gui.addLine(line);

		// LABELE:
		gui.addLabel(new GuiPinLabel(346, 320, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(107, 62, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(309, 24, NameConnector.getPin("Bus1.IBUS1")));
		gui.addLabel(new GuiPinLabel(327, 365, NameConnector.getPin("Bus1.IBUS2")));
		gui.addLabel(new GuiPinLabel(274, 410, NameConnector.getPin("Bus1.IBUS3")));
		gui.addLabel(new GuiPinLabel(165, 370, PSWH.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(260, 370, TSBPSWHout3.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(165, 506, PSWL.getOutPin(0)));
		gui.addLabel(new GuiPinLabel(260, 506, TSBPSWLout3.getOutPin(0)));

	}

	public void putPins() {
		NameConnector.addPin(componentName, "PSWI", IRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWT", TRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWN", NRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWZ", ZRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWC", CRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWV", VRSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWL0", L0RSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWL1", L1RSFF.getOutPin(0));

		NameConnector.addPin(componentName, "PSWL2", L2RSFF.getOutPin(0));

		NameConnector.addPin(componentName, "START", STARTRSFF.getOutPin(0));

	}

	public void putComponents() {
	}

}
