package shemes.components;

import java.awt.Point;
import java.util.*;

import shemes.AbstractSchema;
import util.NameConnector;
import gui.*;

public class Oper1 extends AbstractSchema {

	public Oper1() {
		componentName = "Oper1";
		displayName = "Signali operacione jedinice";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		putPins();
		putComponents();
	}

	public void initConections() {

	}

	public void initGui() {

		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Oper1.png");
		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 65));
		points.add(new Point(52, 65));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(22));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 89));
		points.add(new Point(52, 89));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(21));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 114));
		points.add(new Point(52, 114));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(20));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(40, 138));
		points.add(new Point(53, 138));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(19));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 163));
		points.add(new Point(52, 163));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(18));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 187));
		points.add(new Point(52, 187));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(17));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 211));
		points.add(new Point(52, 211));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(16));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 235));
		points.add(new Point(52, 235));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(15));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 260));
		points.add(new Point(52, 260));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(14));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 284));
		points.add(new Point(52, 284));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(13));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 309));
		points.add(new Point(52, 309));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(12));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 333));
		points.add(new Point(52, 333));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(11));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 358));
		points.add(new Point(52, 358));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(10));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 382));
		points.add(new Point(52, 382));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(9));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 406));
		points.add(new Point(52, 406));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(8));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 430));
		points.add(new Point(52, 430));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 457));
		points.add(new Point(52, 457));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(6));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 482));
		points.add(new Point(52, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(5));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(39, 506));
		points.add(new Point(52, 506));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(4));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 65));
		points.add(new Point(200, 65));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(3));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 89));
		points.add(new Point(200, 89));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(2));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 114));
		points.add(new Point(200, 114));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 138));
		points.add(new Point(200, 138));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 163));
		points.add(new Point(200, 163));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(23));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 187));
		points.add(new Point(200, 187));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(22));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 211));
		points.add(new Point(200, 211));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(21));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 235));
		points.add(new Point(200, 235));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(20));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 260));
		points.add(new Point(200, 260));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(19));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 284));
		points.add(new Point(200, 284));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(18));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 309));
		points.add(new Point(200, 309));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(17));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 333));
		points.add(new Point(200, 333));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(16));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 358));
		points.add(new Point(200, 358));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(15));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 382));
		points.add(new Point(200, 382));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(14));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 406));
		points.add(new Point(200, 406));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(13));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 430));
		points.add(new Point(200, 430));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(12));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 457));
		points.add(new Point(200, 457));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(11));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 482));
		points.add(new Point(200, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(10));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(187, 506));
		points.add(new Point(200, 506));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(9));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 65));
		points.add(new Point(344, 65));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(8));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 89));
		points.add(new Point(344, 89));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 114));
		points.add(new Point(344, 114));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(6));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 138));
		points.add(new Point(344, 138));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(5));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 163));
		points.add(new Point(344, 163));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(4));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 187));
		points.add(new Point(344, 187));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(3));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 211));
		points.add(new Point(344, 211));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(2));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 235));
		points.add(new Point(344, 235));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 260));
		points.add(new Point(344, 260));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 285));
		points.add(new Point(344, 285));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(23));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 310));
		points.add(new Point(344, 310));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(22));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 333));
		points.add(new Point(344, 333));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(21));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 358));
		points.add(new Point(344, 358));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(20));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 382));
		points.add(new Point(344, 382));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(19));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 406));
		points.add(new Point(344, 406));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(18));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 433));
		points.add(new Point(344, 433));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(17));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 458));
		points.add(new Point(344, 458));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(16));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 482));
		points.add(new Point(344, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(15));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(331, 507));
		points.add(new Point(344, 507));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(14));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 65));
		points.add(new Point(491, 65));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(13));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 89));
		points.add(new Point(491, 89));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(12));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 114));
		points.add(new Point(491, 114));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(11));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 138));
		points.add(new Point(491, 138));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(10));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 163));
		points.add(new Point(491, 163));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(9));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 187));
		points.add(new Point(491, 187));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(8));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 211));
		points.add(new Point(491, 211));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 235));
		points.add(new Point(491, 235));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(6));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 260));
		points.add(new Point(491, 260));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(5));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 284));
		points.add(new Point(491, 284));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(4));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 309));
		points.add(new Point(491, 309));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(3));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 333));
		points.add(new Point(491, 333));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(2));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 358));
		points.add(new Point(491, 358));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(1));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 382));
		points.add(new Point(491, 382));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW48_71()
						.getOutPin(0));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 406));
		points.add(new Point(491, 406));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(23));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 433));
		points.add(new Point(491, 433));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(22));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 457));
		points.add(new Point(491, 457));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(21));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 482));
		points.add(new Point(491, 482));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(20));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(478, 506));
		points.add(new Point(491, 506));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(19));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 65));
		points.add(new Point(642, 65));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(18));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 89));
		points.add(new Point(642, 89));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(17));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 114));
		points.add(new Point(642, 114));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(16));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 138));
		points.add(new Point(642, 138));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(15));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 163));
		points.add(new Point(642, 163));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(14));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 187));
		points.add(new Point(642, 187));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(13));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 211));
		points.add(new Point(642, 211));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(12));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 235));
		points.add(new Point(642, 235));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(11));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 260));
		points.add(new Point(642, 260));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(10));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 284));
		points.add(new Point(642, 284));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(9));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 309));
		points.add(new Point(642, 309));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(8));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 333));
		points.add(new Point(642, 333));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(7));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(628, 358));
		points.add(new Point(642, 358));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(6));
		gui.addLine(line);
		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(629, 382));
		points.add(new Point(642, 382));
		sections.add(points);
		line = new GuiPinLine(sections,
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(5));
		gui.addLine(line);

	}

	public void putPins() {
		NameConnector.addPin(componentName, "MOST1_2", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(22));

		NameConnector.addPin(componentName, "MOST1_3", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(21));

		NameConnector.addPin(componentName, "MOST2_1", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(20));

		NameConnector.addPin(componentName, "MOST3_2", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(19));

		NameConnector.addPin(componentName, "rdCPU", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(18));

		NameConnector.addPin(componentName, "wrCPU", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(17));

		NameConnector.addPin(componentName, "ldMDR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(16));

		NameConnector.addPin(componentName, "mxMDR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(15));

		NameConnector.addPin(componentName, "MDRout1", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(14));

		NameConnector.addPin(componentName, "eMDR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(13));

		NameConnector.addPin(componentName, "ldMAR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(12));

		NameConnector.addPin(componentName, "incMAR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(11));

		NameConnector.addPin(componentName, "eMAR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(10));

		NameConnector.addPin(componentName, "ldDWL", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(9));

		NameConnector.addPin(componentName, "ldDWH", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(8));

		NameConnector.addPin(componentName, "DWout2", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(7));

		NameConnector.addPin(componentName, "wrGPR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(6));

		NameConnector.addPin(componentName, "GPRout1", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(5));

		NameConnector.addPin(componentName, "ldGPRAR", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(4));

		NameConnector.addPin(componentName, "incGPRAR",
				((Counter) NameConnector.getSchema("Counter")).mCW0_23()
						.getOutPin(3));

		NameConnector.addPin(componentName, "ldSP", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(2));

		NameConnector.addPin(componentName, "SPout2", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(1));

		NameConnector.addPin(componentName, "incSP", ((Counter) NameConnector
				.getSchema("Counter")).mCW0_23().getOutPin(0));

		NameConnector.addPin(componentName, "decSP", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(23));

		NameConnector.addPin(componentName, "ldCW", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(22));

		NameConnector.addPin(componentName, "CWout3", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(21));

		NameConnector.addPin(componentName, "ADDout2", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(20));

		NameConnector.addPin(componentName, "ldPC", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(19));

		NameConnector.addPin(componentName, "incPC", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(18));

		NameConnector.addPin(componentName, "PCHout3", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(17));

		NameConnector.addPin(componentName, "PCLout3", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(16));

		NameConnector.addPin(componentName, "PCout1", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(15)); // 32

		NameConnector.addPin(componentName, "ldIR0", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(14)); // 33

		NameConnector.addPin(componentName, "ldIR1", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(13)); // 34

		NameConnector.addPin(componentName, "ldIR2", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(12)); // 35

		NameConnector.addPin(componentName, "ldIR3", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(11)); // 36

		NameConnector.addPin(componentName, "IRPOMout3",
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(10)); // 37

		NameConnector.addPin(componentName, "IRJAout2",
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(9)); // 38

		NameConnector.addPin(componentName, "IRDAout3",
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(8)); // 39

		NameConnector.addPin(componentName, "IRBRout3",
				((Counter) NameConnector.getSchema("Counter")).mCW24_47()
						.getOutPin(7)); // 40

		NameConnector.addPin(componentName, "add", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(6)); // 41

		NameConnector.addPin(componentName, "sub", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(5)); // 42

		NameConnector.addPin(componentName, "inc", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(4)); // 43

		NameConnector.addPin(componentName, "dec", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(3)); // 44

		NameConnector.addPin(componentName, "and", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(2)); // 45

		NameConnector.addPin(componentName, "or", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(1)); // 46

		NameConnector.addPin(componentName, "xor", ((Counter) NameConnector
				.getSchema("Counter")).mCW24_47().getOutPin(0)); // 47

		NameConnector.addPin(componentName, "not", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(23)); // 48

		NameConnector.addPin(componentName, "ALUout1", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(22)); // 49

		NameConnector.addPin(componentName, "ldAB", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(21)); // 50

		NameConnector.addPin(componentName, "ABout3", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(20)); // 51

		NameConnector.addPin(componentName, "shr", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(19)); // 52

		NameConnector.addPin(componentName, "shl", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(18)); // 53

		NameConnector.addPin(componentName, "ldBB", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(17)); // 54

		NameConnector.addPin(componentName, "BBout2", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(16)); // 55

		NameConnector.addPin(componentName, "ldAW", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(15)); // 56

		NameConnector.addPin(componentName, "AWout3", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(14)); // 57

		NameConnector.addPin(componentName, "AWHout3", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(13)); // 58

		NameConnector.addPin(componentName, "ldBW", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(12)); // 59

		NameConnector.addPin(componentName, "BWout2", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(11)); // 60

		NameConnector.addPin(componentName, "ldPSWH", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(10)); // 61

		NameConnector.addPin(componentName, "ldPSWL", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(9)); // 62

		NameConnector.addPin(componentName, "ldN", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(8)); // 63

		NameConnector.addPin(componentName, "ldZ", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(7)); // 64

		NameConnector.addPin(componentName, "ldC", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(6)); // 65

		NameConnector.addPin(componentName, "ldV", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(5)); // 66

		NameConnector.addPin(componentName, "ldL", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(4)); // 67

		NameConnector.addPin(componentName, "stPSWI", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(3)); // 68

		NameConnector.addPin(componentName, "clPSWI", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(2)); // 69

		NameConnector.addPin(componentName, "stPSWT", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(1)); // 70

		NameConnector.addPin(componentName, "clPSWT", ((Counter) NameConnector
				.getSchema("Counter")).mCW48_71().getOutPin(0)); // 71

		NameConnector.addPin(componentName, "PSWHout3",
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(23)); // 72

		NameConnector.addPin(componentName, "PSWLout3",
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(22)); // 73

		NameConnector.addPin(componentName, "clSTART", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(21)); // 74

		NameConnector.addPin(componentName, "ldIMR", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(20)); // 75

		NameConnector.addPin(componentName, "IMRout2", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(19)); // 76

		NameConnector.addPin(componentName, "ldBR", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(18)); // 77

		NameConnector.addPin(componentName, "IVTDSPout3",
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(17)); // 78

		NameConnector.addPin(componentName, "ldIVTP", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(16)); // 79

		NameConnector.addPin(componentName, "IVTPout1",
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(15)); // 80

		NameConnector.addPin(componentName, "UINTout3",
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(14)); // 81

		NameConnector.addPin(componentName, "UEXTout3",
				((Counter) NameConnector.getSchema("Counter")).mCW72_95()
						.getOutPin(13)); // 82

		NameConnector.addPin(componentName, "stPRCOD", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(12)); // 83

		NameConnector.addPin(componentName, "stPRADR", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(11)); // 84

		NameConnector.addPin(componentName, "stPRINS", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(10)); // 85

		NameConnector.addPin(componentName, "clPRCOD", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(9)); // 86

		NameConnector.addPin(componentName, "clPRADR", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(8)); // 87

		NameConnector.addPin(componentName, "clPRINS", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(7)); // 88

		NameConnector.addPin(componentName, "clPRINM", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(6)); // 89

		NameConnector.addPin(componentName, "clINTR", ((Counter) NameConnector
				.getSchema("Counter")).mCW72_95().getOutPin(5)); // 90

	}

	public void putComponents() {

	}

}
