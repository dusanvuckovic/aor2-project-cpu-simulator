package shemes.components;

import java.awt.Point;
import java.util.*;

import logic.Pin;
import logic.components.*;
import shemes.AbstractSchema;
import util.NameConnector;
import gui.*;

public class Intr2 extends AbstractSchema {

	private AND AND1, AND2, AND3, AND4, AND5, AND6, AND7, AND8;
	private OR OR1;
	private CD CDP;
	private CMP AB;

	public Intr2() {
		componentName = "Intr2";
		displayName = "Intr 2";
		NameConnector.addSchema(componentName, this);
	}

	public void initComponent() {
		AND1 = new AND();
		AND2 = new AND();
		AND3 = new AND();
		AND4 = new AND();
		AND5 = new AND();
		AND6 = new AND();
		AND7 = new AND();
		AND8 = new AND(3);
		OR1 = new OR(7);
		CDP = new CD(8);
		AB = new CMP(3);

		putPins();
		putComponents();
	}

	public void initConections() {
		AND1.setInPin(0, NameConnector.getPin("Intr1.IMR7"));
		AND1.setInPin(1, NameConnector.getPin("Intr1.PRINTR7"));

		AND2.setInPin(0, NameConnector.getPin("Intr1.IMR6"));
		AND2.setInPin(1, NameConnector.getPin("Intr1.PRINTR6"));

		AND3.setInPin(0, NameConnector.getPin("Intr1.IMR5"));
		AND3.setInPin(1, NameConnector.getPin("Intr1.PRINTR5"));

		AND4.setInPin(0, NameConnector.getPin("Intr1.IMR4"));
		AND4.setInPin(1, NameConnector.getPin("Intr1.PRINTR4"));

		AND5.setInPin(0, NameConnector.getPin("Intr1.IMR3"));
		AND5.setInPin(1, NameConnector.getPin("Intr1.PRINTR3"));

		AND6.setInPin(0, NameConnector.getPin("Intr1.IMR2"));
		AND6.setInPin(1, NameConnector.getPin("Intr1.PRINTR2"));

		AND7.setInPin(0, NameConnector.getPin("Intr1.IMR1"));
		AND7.setInPin(1, NameConnector.getPin("Intr1.PRINTR1"));

		CDP.setE(new Pin(true, "1"));
		CDP.setInPin(0, new Pin(false, "0"));
		CDP.setInPin(1, AND7.getOutPin(0));
		CDP.setInPin(2, AND6.getOutPin(0));
		CDP.setInPin(3, AND5.getOutPin(0));
		CDP.setInPin(4, AND4.getOutPin(0));
		CDP.setInPin(5, AND3.getOutPin(0));
		CDP.setInPin(6, AND2.getOutPin(0));
		CDP.setInPin(7, AND1.getOutPin(0));

		AB.setE(new Pin(true, "1"));
		AB.setPinA(0, CDP.getOutPin(0));
		AB.setPinA(1, CDP.getOutPin(1));
		AB.setPinA(2, CDP.getOutPin(2));
		AB.setPinB(0, NameConnector.getPin("Exec2.PSWL0"));
		AB.setPinB(1, NameConnector.getPin("Exec2.PSWL1"));
		AB.setPinB(2, NameConnector.getPin("Exec2.PSWL2"));

		OR1.setInPin(0, AND1.getOutPin(0));
		OR1.setInPin(1, AND2.getOutPin(0));
		OR1.setInPin(2, AND3.getOutPin(0));
		OR1.setInPin(3, AND4.getOutPin(0));
		OR1.setInPin(4, AND5.getOutPin(0));
		OR1.setInPin(5, AND6.getOutPin(0));
		OR1.setInPin(6, AND7.getOutPin(0));

		AND8.setInPin(0, OR1.getOutPin(0));
		AND8.setInPin(1, AB.getOutPin(0));
		AND8.setInPin(2, NameConnector.getPin("Exec2.PSWI"));

	}

	public void initGui() {
		GuiPinLine line; // Pomocna promenljiva
		gui = new GuiSchema("src/images/Intr2.png");

		List<List<Point>> sections;
		List<Point> points;

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 150));
		points.add(new Point(445, 150));
		points.add(new Point(445, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 150));
		points.add(new Point(457, 150));
		points.add(new Point(457, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 165));
		points.add(new Point(430, 165));
		points.add(new Point(430, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 180));
		points.add(new Point(404, 180));
		points.add(new Point(404, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 195));
		points.add(new Point(377, 195));
		points.add(new Point(377, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR4"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 210));
		points.add(new Point(351, 210));
		points.add(new Point(351, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR5"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 225));
		points.add(new Point(324, 225));
		points.add(new Point(324, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR6"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 240));
		points.add(new Point(298, 240));
		points.add(new Point(298, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.PRINTR7"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 165));
		points.add(new Point(419, 165));
		points.add(new Point(419, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 180));
		points.add(new Point(393, 180));
		points.add(new Point(393, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR3"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 195));
		points.add(new Point(366, 195));
		points.add(new Point(366, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR4"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 210));
		points.add(new Point(340, 210));
		points.add(new Point(340, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR5"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 225));
		points.add(new Point(313, 225));
		points.add(new Point(313, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR6"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(219, 240));
		points.add(new Point(287, 240));
		points.add(new Point(287, 248));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Intr1.IMR7"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(292, 271));
		points.add(new Point(292, 422));
		points.add(new Point(332, 422));
		points.add(new Point(332, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(319, 271));
		points.add(new Point(319, 414));
		points.add(new Point(347, 414));
		points.add(new Point(347, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(345, 271));
		points.add(new Point(345, 407));
		points.add(new Point(362, 407));
		points.add(new Point(362, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(372, 271));
		points.add(new Point(372, 399));
		points.add(new Point(377, 399));
		points.add(new Point(377, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(398, 271));
		points.add(new Point(398, 399));
		points.add(new Point(393, 399));
		points.add(new Point(393, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(425, 271));
		points.add(new Point(425, 407));
		points.add(new Point(408, 407));
		points.add(new Point(408, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(451, 271));
		points.add(new Point(451, 414));
		points.add(new Point(423, 414));
		points.add(new Point(423, 456));
		sections.add(points);
		line = new GuiPinLine(sections, AND7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(438, 448));
		points.add(new Point(438, 456));
		sections.add(points);
		line = new GuiPinLine(sections, new Pin(false, "0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(254, 329));
		points.add(new Point(264, 329));
		points.add(new Point(264, 301));
		points.add(new Point(476, 301));
		sections.add(points);
		line = new GuiPinLine(sections, AND1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(254, 335));
		points.add(new Point(270, 335));
		points.add(new Point(270, 316));
		points.add(new Point(476, 316));
		sections.add(points);
		line = new GuiPinLine(sections, AND2.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(253, 341));
		points.add(new Point(275, 341));
		points.add(new Point(275, 331));
		points.add(new Point(476, 331));
		sections.add(points);
		line = new GuiPinLine(sections, AND3.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 346));
		points.add(new Point(252, 346));
		sections.add(points);
		line = new GuiPinLine(sections, AND4.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 361));
		points.add(new Point(275, 361));
		points.add(new Point(275, 352));
		points.add(new Point(253, 352));
		sections.add(points);
		line = new GuiPinLine(sections, AND5.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 377));
		points.add(new Point(270, 377));
		points.add(new Point(270, 358));
		points.add(new Point(254, 358));
		sections.add(points);
		line = new GuiPinLine(sections, AND6.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(476, 392));
		points.add(new Point(264, 392));
		points.add(new Point(264, 363));
		points.add(new Point(254, 363));
		sections.add(points);
		line = new GuiPinLine(sections, AND7.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(363, 503));
		points.add(new Point(363, 528));
		points.add(new Point(279, 528));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(363, 528));
		points.add(new Point(453, 528));
		sections.add(points);
		line = new GuiPinLine(sections, CDP.getOutPin(2));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(385, 503));
		points.add(new Point(385, 543));
		points.add(new Point(279, 543));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(385, 543));
		points.add(new Point(453, 543));
		sections.add(points);
		line = new GuiPinLine(sections, CDP.getOutPin(1));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(408, 503));
		points.add(new Point(408, 558));
		points.add(new Point(279, 558));
		sections.add(points);
		points = new ArrayList<Point>();
		points.add(new Point(407, 558));
		points.add(new Point(453, 558));
		sections.add(points);
		line = new GuiPinLine(sections, CDP.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(226, 528));
		points.add(new Point(234, 528));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWL2"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(226, 543));
		points.add(new Point(234, 543));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWL1"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(226, 558));
		points.add(new Point(234, 558));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWL0"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(256, 505));
		points.add(new Point(256, 409));
		points.add(new Point(217, 409));
		sections.add(points);
		line = new GuiPinLine(sections, AB.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(232, 346));
		points.add(new Point(224, 346));
		points.add(new Point(224, 403));
		points.add(new Point(217, 403));
		sections.add(points);
		line = new GuiPinLine(sections, OR1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(224, 422));
		points.add(new Point(224, 414));
		points.add(new Point(217, 414));
		sections.add(points);
		line = new GuiPinLine(sections, NameConnector.getPin("Exec2.PSWI"));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(194, 409));
		points.add(new Point(186, 409));
		sections.add(points);
		line = new GuiPinLine(sections, AND8.getOutPin(0));
		gui.addLine(line);

	}

	public void putPins() {
		NameConnector.addPin(componentName, "irm1", AND7.getOutPin(0));

		NameConnector.addPin(componentName, "irm2", AND6.getOutPin(0));

		NameConnector.addPin(componentName, "irm3", AND5.getOutPin(0));

		NameConnector.addPin(componentName, "irm4", AND4.getOutPin(0));

		NameConnector.addPin(componentName, "irm5", AND3.getOutPin(0));

		NameConnector.addPin(componentName, "irm6", AND2.getOutPin(0));

		NameConnector.addPin(componentName, "irm7", AND1.getOutPin(0));

		NameConnector.addPin(componentName, "prl0", CDP.getOutPin(0));

		NameConnector.addPin(componentName, "prl1", CDP.getOutPin(1));

		NameConnector.addPin(componentName, "prl2", CDP.getOutPin(2));

		NameConnector.addPin(componentName, "printr", AND8.getOutPin(0));

	}

	public void putComponents() {

	}

}
