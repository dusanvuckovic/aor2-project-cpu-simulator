package shemes;

import gui.*;

import java.util.*;

public abstract class AbstractSchema implements Schema {
	protected GuiSchema gui;
	protected List<Schema> subSchemes;
	protected String componentName;
	protected String displayName;

	public AbstractSchema() {
		gui = null;
		subSchemes = new LinkedList<Schema>();
		componentName = null;
		displayName = null;
	}

	@Override
	public void initComponent() {
		for (Schema schema : subSchemes) {
			schema.initComponent();
		}
	}

	@Override
	public void initConections() {
		for (Schema schema : subSchemes) {
			schema.initConections();
		}
	}

	@Override
	public void initGui() {
		for (Schema schema : subSchemes) {
			schema.initGui();
		}
	}

	@Override
	public GuiSchema getGui() {
		return gui;
	}

	@Override
	public List<Schema> getSubSchemes() {
		return subSchemes;
	}

	@Override
	public void addSubScheme(Schema schema) {
		subSchemes.add(schema);
	}

	@Override
	public String getName() {
		return componentName;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}

	public static Schema getSchenaWithGui(Schema schema) {
		if (schema == null) {
			return null;
		}
		if (schema.getGui() != null) {
			return schema;
		}
		for (Schema s : schema.getSubSchemes()) {
			Schema result = getSchenaWithGui(s);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

}
