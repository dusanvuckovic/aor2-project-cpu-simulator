package shemes;

import gui.GuiSchema;

import java.util.List;

public interface Schema {

	public void initComponent();

	public void initConections();

	public void initGui();

	public GuiSchema getGui();

	public List<Schema> getSubSchemes();

	public void addSubScheme(Schema schema);

	public String getName();

	public String getDisplayName();
}
