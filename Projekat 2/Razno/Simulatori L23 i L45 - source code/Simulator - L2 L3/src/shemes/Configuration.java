package shemes;

import shemes.components.Memorija;
import shemes.components.Procesor;

public class Configuration extends AbstractSchema {

	public Configuration() {
		componentName = "System";
		displayName = "System";
		this.addSubScheme(new Procesor());
		this.addSubScheme(new Memorija());

	}

}
