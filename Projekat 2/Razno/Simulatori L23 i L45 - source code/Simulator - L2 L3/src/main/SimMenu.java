package main;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import shemes.components.Mem1;
import util.NameConnector;

public class SimMenu extends JMenuBar {

	private static final long serialVersionUID = 1L;

	private JMenu file = new javax.swing.JMenu();
	private JMenuItem jMenuItem1 = new javax.swing.JMenuItem();
	private JMenu view = new javax.swing.JMenu();
	private JMenuItem jMenuItem2 = new javax.swing.JMenuItem();
	private JMenu help = new javax.swing.JMenu();
	private JMenuItem jMenuItem3 = new javax.swing.JMenuItem();

	public SimMenu() {
		createMenu();
	}

	private void createMenu() {
		file.setText("File");

		jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_N,
				java.awt.event.InputEvent.CTRL_MASK));
		jMenuItem1.setText("Load memory");
		jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenuItem1ActionPerformed(evt);
			}
		});
		file.add(jMenuItem1);

		this.add(file);

		view.setText("View");
		jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_T,
				java.awt.event.InputEvent.CTRL_MASK));
		jMenuItem2.setText(" Time Diagram");
		jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenuItem2ActionPerformed(evt);
			}
		});
		view.add(jMenuItem2);

		this.add(view);

		help.setText("Help");

		jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_B,
				java.awt.event.InputEvent.CTRL_MASK));
		jMenuItem3.setText(" About");
		jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenuItem3ActionPerformed(evt);
			}
		});
		help.add(jMenuItem3);

		this.add(help);

	}

	private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
		JFileChooser jc = new JFileChooser(".//");

		int fStatus = jc.showOpenDialog(this);
		if (fStatus == JFileChooser.APPROVE_OPTION) {
			String path = jc.getSelectedFile().getPath();
			((Mem1) NameConnector.getSchema("Mem1")).getMEM()
					.initFromFile(path);
		}

	}

	private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {
		Main.dialogSignals.setVisible(true);
	}

	private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {

	}

}
