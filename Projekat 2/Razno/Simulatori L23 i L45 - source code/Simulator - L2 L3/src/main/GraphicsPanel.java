package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

import logic.Execution;
import logic.Pin;
import util.NameConnector;
import util.TimeHistory;
import util.TimeHistory.TimeHistoryObserver;

public class GraphicsPanel extends JPanel implements TimeHistoryObserver {
	private static final long serialVersionUID = 1L;

	private final int STEPH = 21;
	private final int STEPV = 21;
	private final int IMPULS_HEIGHT = 15;
	private final int START_X = 86;
	private final int START_Y = 37;
	private final int START_XLAB = 15;
	private final int START_YLAB = 40;
	private final int LAB_HEIGHT = 20;
	private final int STEP_XGRID = 21;
	private final int STEP_YGRID = 10;
	private final int GRID_YOFFSET = 30;

	private final float STORK_SIZE = 3F;

	private final Color GRID_CLR = new Color(230, 230, 230);
	private final Color LINE_CLR = Color.BLUE;

	private final int MAX_CLK = 32;

	List<PanelData> data;
	Set<String> usedNames;

	public TimeHistory history;
	private int clk;

	private int startClk;

	public GraphicsPanel() {
		data = new LinkedList<PanelData>();
		usedNames = new HashSet<String>();

		history = null;
		clk = 0;

		startClk = 0;

		adjustSize(new Dimension(810, 200));
		setBackground(Color.WHITE);

		MouseInputAdapter mouseX = new MoveXListener();
		this.addMouseListener(mouseX);
		this.addMouseMotionListener(mouseX);

		MouseInputAdapter mouseY = new MoveYListener();
		this.addMouseListener(mouseY);
		this.addMouseMotionListener(mouseY);
	}

	public void adjustSize(Dimension size) {
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setSize(size);
		validate();
	}

	class PanelData {
		String name = "";
		String prev = "0";
		int numLines = 1;
	}

	public void update(Graphics g) {
		paint(g);
	}

	public void paint(Graphics g) {
		Color tmp = g.getColor();
		Dimension size = this.getParent().getSize();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, Math.max(this.getWidth(), (int) size.getWidth()),
				Math.max(this.getHeight(), (int) size.getHeight()));
		g.setColor(tmp);

		if (data.size() > 0) {
			Graphics2D graph = ((Graphics2D) g);
			drawDiagram(graph);
			getToolkit().sync();
		} else {
			// g.clearRect(0, 0, this.getWidth(), this.getHeight());
		}
	}

	private void drawGrid(int startTime, int endTime, Graphics2D graph) {
		Color tmp = graph.getColor();

		// graph.clearRect(START_XLAB - 2, 0, getWidth(), getHeight());

		for (int i = 0; i < data.size(); i++) {
			graph.drawString(data.get(i).name, START_XLAB, START_YLAB + i
					* LAB_HEIGHT);
		}

		Font pom = graph.getFont();
		graph.setFont(new Font("Times New Roman", 0, 10));
		graph.setColor(Color.BLACK);
		for (int i = startTime; i < endTime + 1; i++) {
			int position = i - startTime;
			graph.drawString(Integer.toString(i), START_X + position
					* STEP_XGRID, STEP_YGRID);
		}
		graph.setFont(pom);
		graph.setColor(GRID_CLR);
		for (int i = startTime; i < endTime + 1; i++) {
			graph.setStroke(new BasicStroke(1.0F, 2, 2));
			int position = i - startTime;
			graph.drawLine(START_X + position * STEP_XGRID, GRID_YOFFSET,
					START_X + position * STEP_XGRID, getHeight() - STEP_YGRID);
		}
		graph.setColor(tmp);
	}

	private void drawLowLine(int num, int sig, Graphics2D graph) {
		graph.setColor(LINE_CLR);
		graph.setStroke(new BasicStroke(STORK_SIZE));
		graph.drawLine(START_X + num * STEPH, START_Y + sig * STEPV, START_X
				+ (num + 1) * STEPH, START_Y + sig * STEPV);
	}

	private void drawHighLine(int num, int sig, Graphics2D graph) {
		graph.setColor(LINE_CLR);
		graph.setStroke(new BasicStroke(STORK_SIZE));
		graph.drawLine(START_X + num * STEPH, START_Y + sig * STEPV
				- IMPULS_HEIGHT, START_X + (num + 1) * STEPH, START_Y + sig
				* STEPV - IMPULS_HEIGHT);
	}

	private void drawHIZLine(int num, int sig, Graphics2D graph) {
		graph.setColor(LINE_CLR);
		graph.setStroke(new BasicStroke(STORK_SIZE));
		graph.drawLine(START_X + num * STEPH, START_Y + sig * STEPV
				- IMPULS_HEIGHT / 2, START_X + (num + 1) * STEPH, START_Y + sig
				* STEPV - IMPULS_HEIGHT / 2);
	}

	private void drawChange(int num, int sig, int prev, int next,
			Graphics2D graph) {
		graph.setColor(LINE_CLR);
		graph.setStroke(new BasicStroke(STORK_SIZE));
		int y1 = START_Y + sig * STEPV - IMPULS_HEIGHT;
		int y2 = START_Y + sig * STEPV;
		if ((prev == 2 && next == 1) || (prev == 1 && next == 2)) {
			y2 = START_Y + sig * STEPV - IMPULS_HEIGHT / 2;
		}
		if (prev == 2 && next == 0 || (prev == 0 && next == 2)) {
			y1 = START_Y + sig * STEPV - IMPULS_HEIGHT / 2;
		}
		graph.drawLine(START_X + num * STEPH, y1, START_X + num * STEPH, y2);
	}

	private int calcNumverOfVisibleCLKs() {
		Dimension size = this.getParent().getSize();
		int result = ((int) size.getWidth() - START_X) / STEPH - 1;
		result = Math.max(result, MAX_CLK);
		return result;
	}

	private void drawDiagram(Graphics2D graph) {

		int startTime = Math.max(0, startClk);
		startTime = Math.min(startTime, history.getHistorySize());
		startClk = startTime;
		int endTime = Math.min(startTime + calcNumverOfVisibleCLKs(),
				history.getHistorySize());

		drawGrid(startTime, endTime, graph);

		for (int j = 0; j < data.size(); j++) {
			PanelData pData = data.get(j);
			if (pData.numLines == 1) {
				for (int i = startTime; i < endTime; i++) {
					int state = history.getSignalFromHistory(i, pData.name);
					int position = i - startTime;

					if (state == TimeHistory.ONE) {
						drawHighLine(position, j, graph);
					} else if (state == TimeHistory.ZERO) {
						drawLowLine(position, j, graph);
					} else {
						drawHIZLine(position, j, graph);
					}
					int prev = Integer.parseInt(pData.prev);
					if ((position != 0) && (prev != state)) {
						drawChange(position, j, prev, state, graph);
					}
					pData.prev = "" + state;
				}
			} else {
				for (int i = startTime; i < endTime; i++) {
					String state = history.getMultiSignalFromHistory(i,
							pData.name);
					int position = i - startTime;

					if (state.equals("Z")) {
						drawHIZLine(position, j, graph);
					} else {
						drawHighLine(position, j, graph);
						drawLowLine(position, j, graph);
					}
					if ((position != 0) && (!pData.prev.equals(state))) {
						drawChange(position, j, pData.prev, state, graph);
					}
					pData.prev = state;
				}

			}
		}
	}

	private void drawChange(int position, int j, String prev, String state,
			Graphics2D graph) {
		if (!state.equals(TimeHistory.TRISTATE)) {
			int val = Integer.parseInt(state);
			state = Integer.toString(val, 16);
			graph.drawString(state, START_X + position * STEP_XGRID
					+ STORK_SIZE, START_YLAB + j * LAB_HEIGHT - STORK_SIZE);
		}
		graph.setColor(LINE_CLR);
		graph.setStroke(new BasicStroke(3.0F));
		int y1 = START_Y + j * STEPV - IMPULS_HEIGHT;
		int y2 = START_Y + j * STEPV;
		graph.drawLine(START_X + position * STEPH, y1, START_X + position
				* STEPH, y2);
	}

	public void addSignalNames(List<String> names) {
		data.clear();
		usedNames.clear();
		for (String name : names) {
			if (!usedNames.contains(name)) {
				PanelData d = new PanelData();
				d.name = name;
				Pin pin = NameConnector.getPin(name);
				if (pin != null) {
					d.numLines = pin.getNumOfLines();
				}
				data.add(d);
				usedNames.add(name);
			}
		}
		adjustSize(new Dimension(this.getWidth(), START_YLAB
				+ (data.size() + 1) * LAB_HEIGHT));
		this.validate();
		getToolkit().sync();
		this.repaint();
	}

	private class MoveXListener extends MouseInputAdapter {
		int startX;

		public void mousePressed(MouseEvent e) {
			startX = e.getX();
		}

		public void mouseDragged(MouseEvent e) {
			updateSize(e);
		}

		public void mouseReleased(MouseEvent e) {

		}

		void updateSize(MouseEvent e) {
			int x = e.getX();
			int dX = (startX - x) / STEP_XGRID;

			if (dX != 0 && startClk >= 0) {
				startClk += dX;
				if (startClk < 0) {
					startClk = 0;
				}
				startX = x;
				repaint();
			}
		}
	}

	private class MoveYListener extends MouseInputAdapter {
		int startX;
		int startY;
		PanelData selectedLine;
		int startIndex;

		public void mousePressed(MouseEvent e) {
			startX = e.getX();
			startY = e.getY();

			startIndex = (startY - START_YLAB) / LAB_HEIGHT;
			if (startIndex >= 0 && startIndex < data.size()) {
				selectedLine = data.get(startIndex);
			} else {
				startIndex = -1;
			}
		}

		public void mouseDragged(MouseEvent e) {
			updateSize(e);
		}

		public void mouseReleased(MouseEvent e) {

		}

		void updateSize(MouseEvent e) {
			if (startX < START_X) {
				int y = e.getY();
				int currentIndex = (y - START_YLAB) / LAB_HEIGHT;

				if (currentIndex < 0 || currentIndex >= data.size()) {
					currentIndex = -1;
				}

				if (startIndex >= 0 && currentIndex >= 0
						&& startIndex != currentIndex) {
					data.remove(selectedLine);
					if (data.size() > currentIndex) {
						data.add(currentIndex, selectedLine);
					} else {
						data.add(selectedLine);
					}
					startIndex = currentIndex;
					repaint();
				}
			}
		}
	}

	public void setHistory(TimeHistory tdh) {
		history = tdh;
	}

	@Override
	public void update() {
		if (Execution.globalTime > 0) {
			setClk(Execution.globalTime);
		} else {
			this.clear();
		}
		this.repaint();
	}

	public void setClk(int clk) {
		if (this.clk != clk) {
			this.clk = clk;
			startClk = Math.max(0, clk - calcNumverOfVisibleCLKs());
			repaint();
		}
	}

	public void clear() {
		data.clear();
		usedNames.clear();
		clk = 0;
		startClk = 0;
	}
}
