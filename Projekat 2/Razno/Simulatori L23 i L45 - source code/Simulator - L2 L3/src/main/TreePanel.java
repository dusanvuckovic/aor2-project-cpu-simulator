package main;

import gui.GuiSchema;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JPanel;

import shemes.AbstractSchema;
import shemes.Schema;

public class TreePanel extends JPanel {

	private static final String LEVELSEPARATOR = "    ";

	private static final long serialVersionUID = 1L;

	java.awt.List listOfShemes = new java.awt.List();

	Map<String, Schema> names;

	public TreePanel(List<Schema> schemas) {
		names = new LinkedHashMap<String, Schema>();
		for (String name : extractNames(schemas, names)) {
			listOfShemes.add(name);
		}

		listOfShemes.addItemListener(new ListOfSchemesItemListener());

		this.setLayout(new BorderLayout());
		this.add(listOfShemes, BorderLayout.CENTER);

	}

	public class ListOfSchemesItemListener implements ItemListener {
		public void itemStateChanged(ItemEvent arg0) {
			GuiSchema currentScheme = Main.currentScheme;
			if (currentScheme != null) {
				Main.centralPanel.remove(currentScheme);
			} else {
				Main.centralPanel.removeAll();
			}

			String selectedText = listOfShemes.getSelectedItem();
			currentScheme = getGui(selectedText);
			Main.centralPanel.setViewportView(currentScheme);
			Main.currentScheme = currentScheme;

			validate();
			repaint();

		}

	}

	private List<String> extractNames(List<Schema> schemas,
			Map<String, Schema> names) {
		List<String> result = new LinkedList<String>();
		Map<Schema, String> shemaNames = new LinkedHashMap<Schema, String>();
		extractNames(schemas, shemaNames, "");
		for (Entry<Schema, String> schema : shemaNames.entrySet()) {
			names.put(schema.getValue(), schema.getKey());
			result.add(schema.getValue());
		}
		return result;
	}

	private void extractNames(List<Schema> schemas,
			Map<Schema, String> schemasNames, String level) {
		for (Schema schema : schemas) {
			String oldName = schemasNames.get(schema);
			String name = level + schema.getDisplayName();

			if (oldName == null || oldName.length() < name.length()) {
				schemasNames.put(schema, name);
				List<Schema> newList = schema.getSubSchemes();
				if (newList != null && level.length() < 20) {
					extractNames(newList, schemasNames, LEVELSEPARATOR + level);
				}
			}
		}
	}

	private GuiSchema getGui(String name) {
		Schema schema = names.get(name);
		schema = AbstractSchema.getSchenaWithGui(schema);
		if (schema == null)
			return null;
		else {
			return schema.getGui();
		}
	}

}
