package main;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import logic.Execution;

public class MenuPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	JButton memory = new JButton("MEMORY");
	JButton registers = new JButton("REGISTERS");
	JButton flipflops = new JButton("FLIPFLOPS");
	JButton reset = new JButton("RESET");
	JButton resetnomem = new JButton("RESET WITHOUT MEM");

	public MenuPanel() {
		memory.addActionListener(new MemoryActionListener());
		memory.setAlignmentX(CENTER_ALIGNMENT);

		registers.addActionListener(new RegistersActionListener());
		registers.setAlignmentX(CENTER_ALIGNMENT);

		flipflops.addActionListener(new FlipflopsActionListener());
		flipflops.setAlignmentX(CENTER_ALIGNMENT);

		reset.addActionListener(new ResetActionListener());
		reset.setAlignmentX(CENTER_ALIGNMENT);

		resetnomem.addActionListener(new ResetNoMemoryActionListener());
		resetnomem.setAlignmentX(CENTER_ALIGNMENT);

		this.setLayout(new GridLayout(5, 1));
		this.add(memory);
		this.add(registers);
		this.add(flipflops);
		this.add(reset);
		this.add(resetnomem);
	}

	public class MemoryActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			Main.dialogRegs.pack();
			Main.dialogMem.setVisible(true);
			Main.currentScheme.repaint();

		}
	}

	public class RegistersActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			Main.dialogRegs.pack();
			Main.dialogRegs.setVisible(true);
			Main.currentScheme.repaint();

		}
	}

	public class FlipflopsActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			Main.dialogRegs.pack();
			Main.dialogFlipflops.setVisible(true);
			Main.currentScheme.repaint();

		}
	}

	public class ResetNoMemoryActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			Execution.init(false);
			Main.east.reset();
		}
	}

	public class ResetActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {

			Execution.init(true);
			Main.east.reset();

		}
	}
}
