package main;

import java.io.*;

import util.*;

public class StepsLoader {

	private String[] steps;// koji su signali aktivni
	private String[] desc;// sa uzvicnicima
	private String currStep;
	private int currStepNumber;

	// private String currDesc;

	public StepsLoader(String stepsConfFile, PrintStream log) {
		steps = new String[256];
		desc = new String[256];
		currStepNumber = 0;

		if (log == null) {
			log = System.out;
		}

		long begin = System.currentTimeMillis();

		log.println();
		log.println("StepLoader:");
		log.println();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(
					stepsConfFile));
			String line = reader.readLine();

			String commnet = null;
			while (line != null) {
				if (line.length() > 0) {
					line = line.trim();
					if (line.charAt(0) == '!') {
						// desc[currStepNumber] = line;
						commnet = line;
					} else if (line.startsWith("madr")) {

						currStep = line.substring(6, line.length());
						currStepNumber = Integer.parseInt(line.substring(4, 6),
								16);
						steps[currStepNumber] = currStep;
						if (commnet != null) {
							desc[currStepNumber] = commnet;
							commnet = null;
						}
						currStepNumber++;
					} else {

					}
				}
				line = reader.readLine();
			}

			reader.close();
			log.println("time taken: " + (System.currentTimeMillis() - begin)
					+ " ms");
		} catch (FileNotFoundException e) {
			log.println("Conf file not found!");
		} catch (Exception e) {
			log.println("Conf file corrupted!");
		}
	}

	public String[] getSteps() {
		return steps;
	}

	public String[] getDesc() {
		return desc;
	}

	public String getStepsAt(int n) {
		return steps[n];
	}

	public String getDescAt(int n) {
		return desc[n];
	}

	public static void main(String[] args) {
		String[] steps = new StepsLoader("src/main/microProgram.txt", null)
				.getSteps();
		String[] desc = new StepsLoader("src/main/microProgram.txt", null)
				.getDesc();
		for (int i = 0; i < steps.length; i++) {
			if (steps[i] != null) {
				Log.log(Integer.toHexString(i) + " : " + steps[i]);
			}
		}
		for (int i = 0; i < desc.length; i++) {
			if (desc[i] != null) {
				Log.log(Integer.toHexString(i) + " : " + desc[i]);
			}
		}
	}
}
