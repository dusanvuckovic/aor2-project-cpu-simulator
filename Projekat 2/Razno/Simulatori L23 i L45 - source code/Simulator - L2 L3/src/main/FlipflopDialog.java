package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import logic.Pin;
import util.NameConnector;

public class FlipflopDialog extends JDialog {
	private static final long serialVersionUID = 1L;

	Ffdijalog panel;

	public FlipflopDialog() {
		this.panel = new Ffdijalog(this);
		this.setResizable(false);
		this.setTitle("CPU flip-flopovi");
		this.setModal(true);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				setVisible(false);
				Main.currentScheme.repaint();
			}
		});

		this.add(panel);
		this.setSize(400, 400);
		this.setLocation(100, 100);
		this.setVisible(false);

	}

	public void init() {
		panel.init();
	}
}

class Ffdijalog extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JButton izmeni;
	private JButton izadji;
	private JDialog greska;
	private JDialog potvrda;
	private boolean error;
	// private JTextField[] regPolja;
	// private REG[] allRegs;
	private JDialog sadrzalacFlipflopova;
	JTextField[] Exec2 = new JTextField[9];
	Pin[] Exec2pins = new Pin[9];
	JTextField[] Intr1 = new JTextField[7]; // PRINS PRCOD PRADR PRINM PRINTR1-3
	Pin[] Intr1pins = new Pin[7];

	public JDialog getSadrzalacFlipflopova() {
		return sadrzalacFlipflopova;
	}

	public void setSadrzalacFlipflopova(JDialog sadrzalacFlipflopova) {
		this.sadrzalacFlipflopova = sadrzalacFlipflopova;
	}

	public Ffdijalog(FlipflopDialog parent) {
		sadrzalacFlipflopova = parent;

		setLayout(new BorderLayout());
		setBackground(Color.white);

	}

	public void init() {
		Color bgcolor = Color.white;
		JPanel registrisvi = new JPanel(new GridLayout(1, 4));
		registrisvi.setBackground(bgcolor);

		JPanel prvi = new JPanel(new GridLayout(9, 2));
		prvi.setBackground(bgcolor);

		JLabel name;
		JPanel temp;

		name = new JLabel("PSWN: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		/*
		 * JPanel tmp = new JPanel(); tmp.setBackground(bgcolor);
		 */
		Exec2[0] = new JTextField();
		Exec2[0].setColumns(3);
		Exec2pins[0] = NameConnector.getPin("Exec2.PSWN");
		Exec2[0].setText(Integer.toString((Exec2pins[0].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[0]);

		/*
		 * JPanel tmpAll = new JPanel(new GridLayout(1, 2));
		 * tmpAll.setBackground(bgcolor); tmpAll.add(ime); tmp.add(regPolja[i]);
		 * tmpAll.add(tmp);
		 */

		prvi.add(temp); // prvi.add(tmpAll);

		name = new JLabel("PSWZ: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[1] = new JTextField();
		Exec2[1].setColumns(3);
		Exec2pins[1] = NameConnector.getPin("Exec2.PSWZ");
		Exec2[1].setText(Integer.toString((Exec2pins[1].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[1]);
		prvi.add(temp);

		name = new JLabel("PSWC: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[2] = new JTextField();
		Exec2[2].setColumns(3);
		Exec2pins[2] = NameConnector.getPin("Exec2.PSWC");
		Exec2[2].setText(Integer.toString((Exec2pins[2].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[2]);
		prvi.add(temp);

		name = new JLabel("PSWV: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[3] = new JTextField();
		Exec2[3].setColumns(3);
		Exec2pins[3] = NameConnector.getPin("Exec2.PSWV");
		Exec2[3].setText(Integer.toString((Exec2pins[3].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[3]);
		prvi.add(temp);

		name = new JLabel("PSWI: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[4] = new JTextField();
		Exec2[4].setColumns(3);
		Exec2pins[4] = NameConnector.getPin("Exec2.PSWI");
		Exec2[4].setText(Integer.toString((Exec2pins[4].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[4]);
		prvi.add(temp);

		name = new JLabel("PSWT: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[5] = new JTextField();
		Exec2[5].setColumns(3);
		Exec2pins[5] = NameConnector.getPin("Exec2.PSWT");
		Exec2[5].setText(Integer.toString((Exec2pins[5].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[5]);
		prvi.add(temp);

		name = new JLabel("PSWL0: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[6] = new JTextField();
		Exec2[6].setColumns(3);
		Exec2pins[6] = NameConnector.getPin("Exec2.PSWL0");
		Exec2[6].setText(Integer.toString((Exec2pins[6].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[6]);
		prvi.add(temp);

		name = new JLabel("PSWL1: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[7] = new JTextField();
		Exec2[7].setColumns(3);
		Exec2pins[7] = NameConnector.getPin("Exec2.PSWL1");
		Exec2[7].setText(Integer.toString((Exec2pins[7].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[7]);
		prvi.add(temp);

		name = new JLabel("START: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		prvi.add(name);
		Exec2[8] = new JTextField();
		Exec2[8].setColumns(3);
		Exec2pins[8] = NameConnector.getPin("Exec2.START");
		Exec2[8].setText(Integer.toString((Exec2pins[8].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Exec2[8]);
		prvi.add(temp);

		registrisvi.add(prvi);

		JPanel drugi = new JPanel(new GridLayout(9, 2));
		drugi.setBackground(bgcolor);

		name = new JLabel("PRINS: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[0] = new JTextField();
		Intr1[0].setColumns(3);
		Intr1pins[0] = NameConnector.getPin("Intr1.PRINS");
		Intr1[0].setText(Integer.toString((Intr1pins[0].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[0]);
		drugi.add(temp);

		name = new JLabel("PRCOD: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[1] = new JTextField();
		Intr1[1].setColumns(3);
		Intr1pins[1] = NameConnector.getPin("Intr1.PRCOD");
		Intr1[1].setText(Integer.toString((Intr1pins[1].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[1]);
		drugi.add(temp);

		name = new JLabel("PRADR: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[2] = new JTextField();
		Intr1[2].setColumns(3);
		Intr1pins[2] = NameConnector.getPin("Intr1.PRADR");
		Intr1[2].setText(Integer.toString((Intr1pins[2].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[2]);
		drugi.add(temp);

		name = new JLabel("PRINM: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[3] = new JTextField();
		Intr1[3].setColumns(3);
		Intr1pins[3] = NameConnector.getPin("Intr1.PRINM");
		Intr1[3].setText(Integer.toString((Intr1pins[3].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[3]);
		drugi.add(temp);

		name = new JLabel("PRINTR3: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[4] = new JTextField();
		Intr1[4].setColumns(3);
		Intr1pins[4] = NameConnector.getPin("Intr1.PRINTR3");
		Intr1[4].setText(Integer.toString((Intr1pins[4].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[4]);
		drugi.add(temp);

		name = new JLabel("PRINTR2: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[5] = new JTextField();
		Intr1[5].setColumns(3);
		Intr1pins[5] = NameConnector.getPin("Intr1.PRINTR2");
		Intr1[5].setText(Integer.toString((Intr1pins[5].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[5]);
		drugi.add(temp);

		name = new JLabel("PRINTR1: ");
		name.setVerticalAlignment(0);
		name.setHorizontalAlignment(4);
		drugi.add(name);
		Intr1[6] = new JTextField();
		Intr1[6].setColumns(3);
		Intr1pins[6] = NameConnector.getPin("Intr1.PRINTR1");
		Intr1[6].setText(Integer.toString((Intr1pins[6].getBoolVal() ? 1 : 0)));
		temp = new JPanel();
		temp.setBackground(Color.white);
		temp.add(Intr1[6]);
		drugi.add(temp);

		for (int j = 0; j < 2; j++) {
			JLabel tmp1 = new JLabel("  ");
			drugi.add(tmp1);
		}
		registrisvi.add(drugi);

		add(registrisvi, "Center");

		JPanel komande = new JPanel();
		komande.setBackground(bgcolor);

		izmeni = new JButton("Change");
		izmeni.addActionListener(this);

		izadji = new JButton("Cancel ");
		izadji.addActionListener(this);

		komande.add(this.izmeni);
		komande.add(this.izadji);
		add(komande, "South");
	}

	public void actionPerformed(ActionEvent arg0) {

		int[] nizvrednosti = new int[Exec2.length + Intr1.length];
		if (arg0.getActionCommand().equals("Change")) {
			for (int i = 0; i < Exec2.length; i++) {
				int val = proverivred(Exec2[i].getText(), i);
				if (error) {
					break;
				}
				nizvrednosti[i] = val;
			}
			for (int i = 0; i < Intr1.length; i++) {
				int val = proverivred(Intr1[i].getText(), i);
				if (error) {
					break;
				}
				nizvrednosti[i + Exec2.length] = val;
			}

			if (!error) {
				for (int i = 0; i < Exec2.length; i++) {
					Exec2pins[i].setBoolVal(nizvrednosti[i] == 1);
				}
				for (int i = 0; i < Intr1.length; i++) {
					Intr1pins[i]
							.setBoolVal(nizvrednosti[i + Exec2.length] == 1);
				}
				potvrda = new JDialog();
				potvrda.setSize(350, 100);
				potvrda.setLocation(200, 200);
				potvrda.setModal(true);
				potvrda.setTitle("Potvrda");
				JPanel osnovni = new JPanel(new GridLayout(2, 1));
				JLabel lab = new JLabel(
						"Promena sadrzaja flip-flopova je uspesno odradjena!");
				lab.setHorizontalAlignment(0);
				osnovni.add(lab);
				JButton ok = new JButton("U redu");
				ok.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						potvrda.setVisible(false);

					}
				});
				JPanel tmp = new JPanel();
				tmp.add(ok);
				tmp.setBackground(Color.white);
				osnovni.add(tmp);
				osnovni.setBackground(Color.white);
				potvrda.add(osnovni);
				potvrda.setVisible(true);

			} else {
				error = false;
				greska = new JDialog();
				greska.setSize(550, 150);
				greska.setLocation(200, 200);
				greska.setModal(true);
				greska.setTitle("Greska!");
				JPanel osnovni = new JPanel(new GridLayout(3, 1));
				JLabel lab = new JLabel(
						"Neka od unetih vrednosti nije dobra (ili prelazi velicinu registra ili je unet netacan hex. broj).");
				lab.setHorizontalAlignment(0);
				osnovni.add(lab);
				JLabel lab1 = new JLabel(
						"Zato se brisu sve tekuce izmene Flipflopova!");
				lab1.setHorizontalAlignment(0);
				osnovni.add(lab1);
				JButton ok = new JButton("U redu");
				ok.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						greska.setVisible(false);
						sadrzalacFlipflopova.setVisible(true);
						// da se ugasi i pregled Flipflopova jer
						// ako se on ne ugasi onda ostaju da se vide te
						// nevalidne
						// vrednosti koje
						// idu van opsega
					}

				});
				JPanel tmp = new JPanel();
				tmp.add(ok);
				tmp.setBackground(Color.white);
				osnovni.add(tmp);
				osnovni.setBackground(Color.white);
				greska.add(osnovni);
				greska.setVisible(true);
			}
		} else {// ako je izadji
			sadrzalacFlipflopova.setVisible(false);
		}
		repaint();
	}

	private int proverivred(String text, int i) {
		int val;
		try {
			val = Integer.parseInt(text, 16);
			if (val != 0 && val != 1) {
				error = true;
				return 0;
			}
		} catch (Exception e) { // ZBOG NUMBER FORMAT EXCEPTION!
			error = true;
			return 0;
		}

		return val;
	}

	public void paint(Graphics g) {
		super.paint(g);

		Exec2[0].setText(Integer.toString(Exec2pins[0].getBoolVal() ? 1 : 0));
		Exec2[1].setText(Integer.toString(Exec2pins[1].getBoolVal() ? 1 : 0));
		Exec2[2].setText(Integer.toString(Exec2pins[2].getBoolVal() ? 1 : 0));
		Exec2[3].setText(Integer.toString(Exec2pins[3].getBoolVal() ? 1 : 0));
		Exec2[4].setText(Integer.toString(Exec2pins[4].getBoolVal() ? 1 : 0));
		Exec2[5].setText(Integer.toString(Exec2pins[5].getBoolVal() ? 1 : 0));
		Exec2[6].setText(Integer.toString(Exec2pins[6].getBoolVal() ? 1 : 0));
		Exec2[7].setText(Integer.toString(Exec2pins[7].getBoolVal() ? 1 : 0));
		Exec2[8].setText(Integer.toString(Exec2pins[8].getBoolVal() ? 1 : 0));

		Intr1[0].setText(Integer.toString(Intr1pins[0].getBoolVal() ? 1 : 0));
		Intr1[1].setText(Integer.toString(Intr1pins[1].getBoolVal() ? 1 : 0));
		Intr1[2].setText(Integer.toString(Intr1pins[2].getBoolVal() ? 1 : 0));
		Intr1[3].setText(Integer.toString(Intr1pins[3].getBoolVal() ? 1 : 0));
		Intr1[4].setText(Integer.toString(Intr1pins[4].getBoolVal() ? 1 : 0));
		Intr1[5].setText(Integer.toString(Intr1pins[5].getBoolVal() ? 1 : 0));
		Intr1[6].setText(Integer.toString(Intr1pins[6].getBoolVal() ? 1 : 0));
	}

}
