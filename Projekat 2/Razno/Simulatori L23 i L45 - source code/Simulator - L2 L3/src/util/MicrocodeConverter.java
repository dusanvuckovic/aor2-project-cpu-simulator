package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MicrocodeConverter {

	private static int NOJUMP = 0;
	private static final String NOJUMPS = "next";
	private static int UNCOND = 1;
	private static final String UNCONDS = "br";
	private static int BRADR = 22;
	private static final String BRADRS = "bradr";
	private static int BROPR = 23;
	private static final String BROPRS = "bropr";

	public static String[][] codes = { { "most1_2", "1" }, { "most1_3", "2" },
			{ "most2_1", "3" }, { "most3_2", "4" }, { "rdcpu", "5" },
			{ "wrcpu", "6" }, { "ldmdr", "7" }, { "mxmdr", "8" },
			{ "mdrout1", "9" }, { "emdr", "10" }, { "ldmar", "11" },
			{ "incmar", "12" }, { "emar", "13" }, { "lddwl", "14" },
			{ "lddwh", "15" }, { "dwout2", "16" }, { "wrgpr", "17" },
			{ "gprout1", "18" }, { "ldgprar", "19" }, { "incgprar", "20" },
			{ "ldsp", "21" }, { "spout2", "22" }, { "incsp", "23" },
			{ "decsp", "24" }, { "ldcw", "25" }, { "cwout3", "26" },
			{ "addout2", "27" }, { "ldpc", "28" }, { "incpc", "29" },
			{ "pchout3", "30" }, { "pclout3", "31" }, { "pcout1", "32" },
			{ "ldir0", "33" }, { "ldir1", "34" }, { "ldir2", "35" },
			{ "ldir3", "36" }, { "irpomout3", "37" }, { "irjaout2", "38" },
			{ "irdaout3", "39" }, { "irbrout3", "40" }, { "add", "41" },
			{ "sub", "42" }, { "inc", "43" }, { "dec", "44" }, { "and", "45" },
			{ "or", "46" }, { "xor", "47" }, { "not", "48" },
			{ "aluout1", "49" }, { "ldab", "50" }, { "about3", "51" },
			{ "shr", "52" }, { "shl", "53" }, { "ldbb", "54" },
			{ "bbout2", "55" }, { "ldaw", "56" }, { "awout3", "57" },
			{ "awhout3", "58" }, { "ldbw", "59" }, { "bwout2", "60" },
			{ "ldpswh", "61" }, { "ldpswl", "62" }, { "ldn", "63" },
			{ "ldz", "64" }, { "ldc", "65" }, { "ldv", "66" }, { "ldl", "67" },
			{ "stpswi", "68" }, { "clpswi", "69" }, { "stpswt", "70" },
			{ "clpswt", "71" }, { "pswhout3", "72" }, { "pswlout3", "73" },
			{ "clstart", "74" }, { "ldimr", "75" }, { "imrout2", "76" },
			{ "ldbr", "77" }, { "ivtdspout3", "78" }, { "ldivtp", "79" },
			{ "ivtpout1", "80" }, { "uintout3", "81" }, { "uextout3", "82" },
			{ "stprcod", "83" }, { "stpradr", "84" }, { "stprins", "85" },
			{ "clprcod", "86" }, { "clpradr", "87" }, { "clprins", "88" },
			{ "clprinm", "89" }, { "clintr", "90" } };

	public void parseIn(String inFile, String outFile) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			List<String> lines = new LinkedList<String>();
			String s;
			while ((s = in.readLine()) != null) {
				lines.add(s);
			}
			in.close();
			List<MicrocodeData> microcode = toMicrocode(lines);

			PrintWriter out = new PrintWriter(new FileWriter(outFile));
			for (MicrocodeData code : microcode) {
				if (code != null && code.tupe == MicrocodeData.OK) {
					out.println(code.microcode);
				} else {
					out.println(MicrocodeData.EMPTYCODE);
				}
			}
			out.close();

		} catch (Exception e) {
			Log.log(e);
		}
	}

	public void parseOut(String inFile, String outFile) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			List<String> lines = new LinkedList<String>();
			String s;
			while ((s = in.readLine()) != null) {
				lines.add(s);
			}
			in.close();
			List<MicrocodeData> microcode = toMicrocodeFromBin(lines);
			List<String> data = fromMicrocode(microcode);

			PrintWriter out = new PrintWriter(new FileWriter(outFile));
			for (String code : data) {
				if (code != null) {
					out.println(code);
				} else {
					out.println("!-----!");
				}
			}
			out.close();

		} catch (Exception e) {
			Log.log(e);
		}
	}

	private List<MicrocodeData> toMicrocodeFromBin(List<String> lines) {
		List<MicrocodeData> result = new LinkedList<MicrocodeData>();

		int i = 0;
		for (String line : lines) {
			if (line != null && !line.equals("")) {
				line = line.trim();
				int com = line.indexOf(' ');
				if (com > 0) {
					line = line.substring(0, com);
				}
				com = line.indexOf(';');
				if (com > 0) {
					line = line.substring(0, com);
				}
				com = line.indexOf('\t');
				if (com > 0) {
					line = line.substring(0, com);
				}
				MicrocodeData code = new MicrocodeData();
				code.line = i++;
				code.microcode = line;
				code.tupe = MicrocodeData.OK;
				result.add(code);
			}
		}

		return result;
	}

	public List<MicrocodeData> toMicrocode(List<String> operations) {
		List<MicrocodeData> result = new ArrayList<MicrocodeData>();
		for (int i = 0; i < 256; i++) {
			result.add(null);
		}
		int i = 0;
		for (String operation : operations) {
			MicrocodeData data = toMicrocode(operation);
			switch (data.tupe) {
			case MicrocodeData.OK:
				result.set(data.line, data);
				break;
			case MicrocodeData.NODATA:
				break;
			case MicrocodeData.ERR:
				throw new RuntimeException("invalid data in line " + i + ": "
						+ operation);
			}
			i++;
		}
		return result;
	}

	public MicrocodeData toMicrocode(String line) {
		MicrocodeData result = new MicrocodeData();
		char[] code = result.microcode.toCharArray();
		try {
			line = prepareLine(line);
			String microcodeAdr = extractAdderess(line);

			String[] args = extractOperands(line);

			for (String arg : args) {
				if (isCode(arg)) {
					int position = cetPosition(arg);
					int nibl = position / 4;
					int val = position % 4;
					char c = code[nibl];
					int d = Integer.parseInt("" + c, 16);
					d = d | (1 << (3 - val));
					c = Integer.toHexString(d).charAt(0);
					code[nibl] = c;
					result.tupe = MicrocodeData.OK;
					result.line = Integer.parseInt(
							microcodeAdr.substring(4, microcodeAdr.length()),
							16);
				} else if (isConditionsBranch(arg) || isUnonditionsBranch(arg)) {
					int branchCode = extractBranchCode(arg);

					String bCode = "0" + Integer.toHexString(branchCode);
					bCode = bCode.substring(bCode.length() - 2, bCode.length());
					char[] cCode = bCode.toCharArray();
					for (int i = 0; i < 2; i++) {
						code[i + 96 / 4] = cCode[i];
					}

					int branchAdr = extractBranchAdr(arg);
					String bAdr = "0" + Integer.toHexString(branchAdr);
					bAdr = bAdr.substring(bAdr.length() - 2, bAdr.length());
					char[] cAdr = bAdr.toCharArray();
					for (int i = 0; i < 2; i++) {
						code[i + 104 / 4] = cAdr[i];
					}

					result.tupe = MicrocodeData.OK;
					result.line = Integer.parseInt(
							microcodeAdr.substring(4, microcodeAdr.length()),
							16);
				} else if (isAddressBranch(arg) || isOperationBranch(arg)) {
					int branchCode = extractBranchCode(arg);

					String bCode = "0" + Integer.toHexString(branchCode);
					bCode = bCode.substring(bCode.length() - 2, bCode.length());
					char[] cCode = bCode.toCharArray();
					for (int i = 0; i < 2; i++) {
						code[i + 96 / 4] = cCode[i];
					}

					result.tupe = MicrocodeData.OK;
					result.line = Integer.parseInt(
							microcodeAdr.substring(4, microcodeAdr.length()),
							16);
				} else {
					throw new RuntimeException(arg);
				}
			}
		} catch (Exception e) {
			Log.log(e);
			result.tupe = MicrocodeData.ERR;
		}
		result.microcode = new String(code);
		return result;
	}

	private String[] extractOperands(String line) {

		if (line == null || line.equals("")) {
			return new String[0];
		}
		line = line.toLowerCase();
		int index = line.indexOf(' ');
		if (index >= 0) {
			line = line.substring(index);
			line = line.trim();
		}
		String args[] = line.split(",");
		for (int i = 0; i < args.length; i++) {
			args[i] = args[i].trim();
		}
		return args;
	}

	private String extractAdderess(String line) {
		String result = "";
		if (line != null && !line.equals("")) {
			int i = line.indexOf(' ');
			if (i >= 0) {
				result = line.substring(0, i);
				result = result.trim();
			}
		}
		return result;
	}

	protected String prepareLine(String operation) {

		operation = operation.toLowerCase();
		int len = -1;
		while (len != operation.length()) {
			len = operation.length();
			operation = operation.replace("  ", " ");
		}
		operation = operation.replace("if !", "if #");
		int index = operation.indexOf('!');
		if (index >= 0) {
			operation = operation.substring(0, index);
		}
		index = operation.indexOf(';');
		if (index > 0) {
			operation = operation.substring(0, index);
		}
		operation = operation.trim();
		operation = operation.replace('\t', ' ');
		len = -1;
		while (len != operation.length()) {
			len = operation.length();
			operation = operation.replace("  ", " ");
		}
		return operation;
	}

	private int extractBranchAdr(String arg) {
		int result = 0;
		try {
			arg = arg.replace('(', ' ');
			arg = arg.replace(')', ' ');
			String adr = arg.substring(arg.indexOf(" madr") + " madr".length(),
					arg.length());
			adr = adr.trim();
			String[] adrs = adr.split(" ");
			result = Integer.parseInt(adrs[0], 16);
		} catch (Exception e) {
			throw new RuntimeException(arg);
		}
		return result;
	}

	static Map<String, Integer> branches;

	private int extractBranchCode(String arg) {
		int result = 1;
		try {
			if (isConditionsBranch(arg)) {
				String operation = arg.substring(
						arg.indexOf("if ") + "if ".length(), arg.length());
				operation = operation.substring(0, operation.indexOf(' '));
				operation = operation.trim();
				if (branches.containsKey(operation)) {
					result = branches.get(operation);
				} else {
					if (operation.contains(".")) {
						String[] data = operation.split("\\.");
						operation = data[data.length - 1];
						operation = operation.trim();
					}
					result = branches.get(operation);
				}
			} else if (isAddressBranch(arg)) {
				result = BRADR;
			} else if (isOperationBranch(arg)) {
				result = BROPR;
			} else if (isUnonditionsBranch(arg)) {
				result = UNCOND;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(arg);
		}
		return result;
	}

	private boolean isConditionsBranch(String arg) {
		return arg.contains(UNCONDS + " ") && (arg.indexOf("if ") >= 0);
	}

	private boolean isUnonditionsBranch(String arg) {
		return arg.contains(UNCONDS + " ") && (arg.indexOf("if ") < 0);
	}

	private boolean isAddressBranch(String arg) {
		return arg.contains(BRADRS);
	}

	private boolean isOperationBranch(String arg) {
		return arg.contains(BROPRS);
	}

	static Map<String, Integer> codesFields;

	private int cetPosition(String arg) {
		Integer i = codesFields.get(arg);
		if (i == null) {
			return 0;
		} else {
			return i;
		}
	}

	static Map<Integer, String> fieldCodes;

	private String cetCode(Integer arg) {
		String result = fieldCodes.get(arg);
		return result;
	}

	static Map<Integer, String> conditionCodes;

	private String getCondition(Integer arg) {
		String result = conditionCodes.get(arg);
		return result;
	}

	static Set<String> operations;

	private boolean isCode(String arg) {
		return operations.contains(arg);
	}

	public List<String> fromMicrocode(List<MicrocodeData> microcode) {
		List<String> result = new LinkedList<String>();

		for (MicrocodeData code : microcode) {
			String operation = fromMicrocode(code);
			result.add(operation);
		}

		return result;
	}

	public String fromMicrocode(MicrocodeData microcode) {
		String result = "";

		String adr = toAddress(microcode.line);
		result += adr + " ";

		int num = 0;
		for (int i = 0; i < 96 / 4; i++) {
			char c = microcode.microcode.charAt(i);
			int posi = Integer.parseInt("" + c, 16);

			for (int j = 0; j < 4; j++) {
				if ((posi & 1) == 1) {
					int index = i * 4 + 3 - j;
					String opr = cetCode(index);
					if (opr != null && !opr.equals("")) {
						if (num != 0) {
							result += ", ";
						}
						result += opr;
						num++;
					}
				}
				posi = posi >> 1;
			}
		}

		String branchCodeString = microcode.microcode.substring(96 / 4,
				2 + 96 / 4);
		int branchCode = Integer.parseInt(branchCodeString, 16);
		if (branchCode == 1) {
			String adrString = toAddress(microcode.microcode.substring(104 / 4,
					2 + 104 / 4));
			if (num != 0) {
				result += ", ";
			}
			result += "br " + adrString;

		} else if (branchCode == BRADR) {

			if (num != 0) {
				result += ", ";
			}
			result += BRADRS;
		} else if (branchCode == BROPR) {

			if (num != 0) {
				result += ", ";
			}
			result += BROPRS;
		} else {
			String condition = getCondition(branchCode);
			if (condition != null && !condition.equals("")) {
				condition = condition.replace('#', '!');
				String adrString = toAddress(microcode.microcode.substring(
						104 / 4, 2 + 104 / 4));
				if (num != 0) {
					result += ", ";
				}
				result += "br (if " + condition + " then " + adrString + ")";
			}
		}
		result += ";";
		return result;
	}

	public String toAddress(String adr) {
		if (adr == null || adr.equals("")) {
			throw new RuntimeException("Not a valid address");
		}
		String result = adr;
		if (result.length() == 1) {
			result = "0" + result;
		}
		result = "madr" + result.toUpperCase();
		return result;
	}

	public String toAddress(int adr) {
		return toAddress(Integer.toHexString(adr));
	}

	static {
		codesFields = new HashMap<String, Integer>();
		for (String[] lin : codes) {
			codesFields.put(lin[0], Integer.parseInt(lin[1]));
		}

		fieldCodes = new HashMap<Integer, String>();
		for (String[] lin : codes) {
			fieldCodes.put(Integer.parseInt(lin[1]), lin[0]);
		}

		operations = codesFields.keySet();

		branches = new HashMap<String, Integer>();
		String[][] t = { { "#start", "2" }, { "hack", "3" }, { "#fccpu", "4" },
				{ "#gropr", "5" }, { "l1", "6" }, { "#gradr", "7" },
				{ "l2_brnch", "8" }, { "l2_arlog", "9" }, { "l3_jump", "10" },
				{ "l3_arlog", "11" }, { "store", "12" }, { "ldw", "13" },
				{ "regdir", "14" }, { "#brpom", "15" }, { "#prekid", "16" },
				{ "#prins", "17" }, { "#prcod", "18" }, { "#pradr", "19" },
				{ "#prinm", "20" }, { "#printr", "21" } };
		for (String[] lin : t) {
			branches.put(lin[0], Integer.parseInt(lin[1]));
		}

		conditionCodes = new HashMap<Integer, String>();
		for (String[] lin : codes) {
			conditionCodes.put(Integer.parseInt(lin[1]), lin[0]);
		}

	}

	public static void load(String[][] init) {
		loadBranchCodes(init);
		loadBranches(init);
		MicrocodeData.init();
	}

	public static void loadBranchCodes(String[][] init) {
		for (int i = 0; i < init.length; i++) {
			String[] data = init[i];
			int index = Integer.parseInt(data[0]);
			if (data.length > 3) {
				String name = data[3];
				if (NOJUMPS.equals(name)) {
					NOJUMP = index;
				}
				if (UNCONDS.equals(name)) {
					UNCOND = index;
				}
				if (BRADRS.equals(name)) {
					BRADR = index;
				}
				if (BROPRS.equals(name)) {
					BROPR = index;
				}
			}
		}

	}

	public static void loadBranches(String[][] init) {
		for (int i = 0; i < init.length; i++) {
			String[] data = init[i];
			int index = Integer.parseInt(data[0]);
			String name = data[1].toLowerCase().trim();
			int poslition = name.indexOf('.');
			if (poslition > 0) {
				name = name.substring(poslition + 1);
			}
			String cmp = data[2].toLowerCase().trim();

			branches.put(cmp + name, index);

			conditionCodes.put(index, cmp + name);
		}
	}

	public static class MicrocodeData {
		public static String EMPTYCODE = "0000000000000000000000000000";
		public static final int OK = 0;
		public static final int NODATA = 1;
		public static final int ERR = 2;

		String microcode;
		int line;
		int tupe;
		String comment;

		public MicrocodeData() {
			microcode = EMPTYCODE;
			line = 0;
			tupe = NODATA;
			comment = null;
		}

		public static void init() {
			char[] code = EMPTYCODE.toCharArray();
			int branchCode = NOJUMP;

			String bCode = "0" + Integer.toHexString(branchCode);
			bCode = bCode.substring(bCode.length() - 2, bCode.length());
			char[] cCode = bCode.toCharArray();
			for (int i = 0; i < 2; i++) {
				code[i + 96 / 4] = cCode[i];
			}

			EMPTYCODE = new String(code);
		}
	}

	public List<String> parseIn(String inFile) {
		List<String> result = new LinkedList<String>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFile));
			List<String> lines = new LinkedList<String>();
			String s;
			while ((s = in.readLine()) != null) {
				lines.add(s);
			}
			in.close();
			List<MicrocodeData> microcode = toMicrocode(lines);

			for (MicrocodeData code : microcode) {
				if (code != null && code.tupe == MicrocodeData.OK) {
					result.add(code.microcode);
				} else {
					result.add(MicrocodeData.EMPTYCODE);
				}
			}

		} catch (Exception e) {
			Log.log(e);
		}
		return result;
	}

	public static void main1(String args[]) {
		MicrocodeConverter c = new MicrocodeConverter();
		Parameters.init("./Konfiguracija.txt");
		loadBranches(Parameters.controlUnitDecoder);

		c.parseIn("./microProgram-1.txt", "./o1.txt");
		c.parseOut("./microkod.txt", "./o2.txt");
	}
}
