package util;

import java.io.*;
import java.util.*;

public class Parameters {

	public static int numberOfRegisters = 32;

	public static int GPRARStartPosition = 0;

	public static int GPRNumberOfBits = 5;

	public static String[][] fetch1Conections = {
			{ "TRI", "IRAD", "16", "IRADout3", "IR15", "IR14", "IR13", "IR12",
					"IR11", "IR10", "IR9", "IR8", "IR7", "IR6", "IR5", "IR4",
					"IR3", "IR2", "IR1", "IR0", "IRAD" },
			{ "TRI", "IRJA", "16", "IRJAout2", "IR23", "IR22", "IR21", "IR20",
					"IR19", "IR18", "IR17", "IR16", "IR15", "IR14", "IR13",
					"IR12", "IR11", "IR10", "IR9", "IR8", "IRJA" },
			{ "TRI", "IRPOM", "16", "IRPOMout3", "IR23", "IR23", "IR23",
					"IR23", "IR23", "IR23", "IR23", "IR23", "IR23", "IR22",
					"IR21", "IR20", "IR19", "IR18", "IR17", "IR16", "IRPOM" },
			{ "TRI", "IRBR", "8", "IRBRout3", "IR31", "IR30", "IR29", "IR28",
					"IR27", "IR26", "IR25", "IR24", "IRBR" } };

	public static String[][] fetch2Conections = {
			{ "DC", "DC1", "2", "START", "IR30", "IR31", "G0", "G1", "G2", "G3" },
			{ "DC", "DC2", "3", "G0", "IR27", "IR28", "IR29", "G0_PG0",
					"G0_PG1", "G0_PG2", "G0_PG3", "G0_PG4", "G0_PG5", "G0_PG6",
					"G0_PG7" },
			{ "DC", "DC3", "3", "G0_PG0", "IR24", "IR25", "IR26", "NOP",
					"HALT", "G0_PG0_2", "G0_PG0_3", "INTD", "INTE", "TRPD",
					"TRPE" },
			{ "DC", "DC4", "3", "G0_PG1", "IR24", "IR25", "IR26", "G0_PG1_0",
					"JMP", "JSR", "RTS", "INT", "RTI", "G0_PG1_6", "G0_PG1_7" },
			{ "DC", "DC5", "3", "G0_PG2", "IR24", "IR25", "IR26", "BEQL",
					"BNEQL", "BNEG", "BNNEG", "BOVF", "BNOVF", "BCAR", "BNCAR" },
			{ "DC", "DC6", "3", "G0_PG3", "IR24", "IR25", "IR26", "BGRT",
					"BGRTE", "BLSS", "BLSSE", "BGRTU", "BGRTEU", "BLSSU",
					"BLSSEU" },
			{ "DC", "DC7", "3", "G0_PG4", "IR24", "IR25", "IR26", "LDB", "LDW",
					"STB", "STW", "POPB", "POPW", "PUSHB", "PUSHW" },
			{ "DC", "DC8", "3", "G0_PG5", "IR24", "IR25", "IR26", "LDIVTP",
					"STIVTP", "LDIMR", "STIMR", "LDSP", "STSP", "G0_PG5_6",
					"G0_PG5_7" },
			{ "DC", "DC9", "3", "G0_PG6", "IR24", "IR25", "IR26", "ADD", "SUB",
					"INC", "DEC", "AND", "OR", "XOR", "NOT" },
			{ "DC", "DC10", "3", "G0_PG7", "IR24", "IR25", "IR26", "ASR",
					"LSR", "ROR", "RORC", "ASL", "LSL", "ROL", "ROLC" },
			{ "DC", "DC11", "3", "1", "IR21", "IR22", "IR23", "regdir",
					"regind", "memdir", "memind", "regindpom", "bxpom",
					"pcpom", "imm" } };

	public static String[][] fetch3Conections = {
			{ "OR", "ORgropr", "10", "Fetch2.G3", "Fetch2.G2", "Fetch2.G1",
					"Fetch2.G0_PG5_7", "Fetch2.G0_PG5_6", "Fetch2.G0_PG1_7",
					"Fetch2.G0_PG1_6", "Fetch2.G0_PG1_0", "Fetch2.G0_PG0_3",
					"Fetch2.G0_PG0_2", "gropr" },
			{ "OR", "ORgradr", "2", "Fetch2.STB", "Fetch2.STW", "ORgradr_0" },
			{ "OR", "ORgradrMode", "1", "Fetch2.imm", "ORgradrMode_0" },
			{ "OR", "ORL1", "12", "Fetch2.G0_PG7", "Fetch2.NOT", "Fetch2.DEC",
					"Fetch2.INC", "Fetch2.G0_PG5", "Fetch2.PUSHW",
					"Fetch2.PUSHB", "Fetch2.POPW", "Fetch2.POPB", "Fetch2.RTI",
					"Fetch2.RTS", "Fetch2.G0_PG0", "l1" },
			{ "OR", "ORL2_brnch", "3", "Fetch2.G0_PG3", "Fetch2.G0_PG2",
					"Fetch2.INT", "l2_brnch" },
			{ "OR", "ORL2_arlog_codes", "9", "Fetch2.XOR", "Fetch2.OR",
					"Fetch2.AND", "Fetch2.SUB", "Fetch2.ADD", "Fetch2.STW",
					"Fetch2.STB", "Fetch2.LDW", "Fetch2.LDB",
					"ORL2_arlog_codes_0" },
			{ "OR", "ORL2_arlog_addressModes", "2", "Fetch2.regdir",
					"Fetch2.regind", "ORL2_arlog_addressModes_0" },
			{ "OR", "ORL3_jump", "2", "Fetch2.JSR", "Fetch2.JMP", "L3_jump" },
			{ "OR", "ORL3_arlog_codes", "6", "Fetch2.XOR", "Fetch2.OR",
					"Fetch2.AND", "Fetch2.SUB", "Fetch2.ADD", "Fetch2.LDB",
					"ORL3_arlog_codes_0" },
			{ "OR", "ORL3_arlog_addressModes", "1", "Fetch2.imm",
					"ORL3_arlog_addressModes_0" },
			{ "OR", "ORstore", "2", "Fetch2.STB", "Fetch2.STW", "store" },
			{ "AND", "ANDgradr", "2", "ORgradr_0", "ORgradrMode_0", "gradr" },
			{ "AND", "ANDL2_arlog", "2", "ORL2_arlog_codes_0",
					"ORL2_arlog_addressModes_0", "L2_arlog" },
			{ "AND", "ANDL3_arlog", "2", "ORL3_arlog_codes_0",
					"ORL3_arlog_addressModes_0", "L3_arlog" } };

	public static String[][] exec3Conections = { { "OR", "NZOR", "18",
			"Fetch2.LDB", "Fetch2.POPB", "Fetch2.ADD", "Fetch2.SUB",
			"Fetch2.INC", "Fetch2.DEC", "Fetch2.AND", "Fetch2.OR",
			"Fetch2.XOR", "Fetch2.NOT", "Fetch2.ASR", "Fetch2.LSR",
			"Fetch2.ROR", "Fetch2.RORC", "Fetch2.ASL", "Fetch2.LSL",
			"Fetch2.ROL", "Fetch2.ROLC", "NZOR" }

	};

	public static String[][] counterCodeAddresses = { { "Fetch2.NOP", "62" },
			{ "Fetch2.HALT", "63" }, { "Fetch2.INTD", "64" },
			{ "Fetch2.INTE", "65" }, { "Fetch2.TRPD", "66" },
			{ "Fetch2.TRPE", "67" }, { "Fetch2.LDB", "68" },
			{ "Fetch2.LDW", "70" }, { "Fetch2.STB", "71" },
			{ "Fetch2.STW", "77" }, { "Fetch2.POPB", "86" },
			{ "Fetch2.POPW", "91" }, { "Fetch2.PUSHB", "99" },
			{ "Fetch2.PUSHW", "104" }, { "Fetch2.LDIVTP", "113" },
			{ "Fetch2.STIVTP", "114" }, { "Fetch2.LDIMR", "115" },
			{ "Fetch2.STIMR", "116" }, { "Fetch2.LDSP", "117" },
			{ "Fetch2.STSP", "118" }, { "Fetch2.ADD", "119" },
			{ "Fetch2.SUB", "121" }, { "Fetch2.INC", "123" },
			{ "Fetch2.DEC", "125" }, { "Fetch2.AND", "127" },
			{ "Fetch2.OR", "129" }, { "Fetch2.XOR", "131" },
			{ "Fetch2.NOT", "133" }, { "Fetch2.ASR", "135" },
			{ "Fetch2.LSR", "135" }, { "Fetch2.ROR", "135" },
			{ "Fetch2.RORC", "135" }, { "Fetch2.ASL", "137" },
			{ "Fetch2.LSL", "137" }, { "Fetch2.ROL", "137" },
			{ "Fetch2.ROLC", "137" }, { "Fetch2.BEQL", "139" },
			{ "Fetch2.BNEQL", "139" }, { "Fetch2.BNEG", "139" },
			{ "Fetch2.BNNEG", "139" }, { "Fetch2.BOVF", "139" },
			{ "Fetch2.BNOVF", "139" }, { "Fetch2.BCAR", "139" },
			{ "Fetch2.BNCAR", "139" }, { "Fetch2.BGRT", "139" },
			{ "Fetch2.BGRTE", "139" }, { "Fetch2.BLSS", "139" },
			{ "Fetch2.BLSSE", "139" }, { "Fetch2.BGRTU", "139" },
			{ "Fetch2.BGRTEU", "139" }, { "Fetch2.BLSSU", "139" },
			{ "Fetch2.BLSSEU", "139" }, { "Fetch2.JMP", "141" },
			{ "Fetch2.INT", "142" }, { "Fetch2.JSR", "143" },
			{ "Fetch2.RTS", "152" }, { "Fetch2.RTI", "160" } };

	public static String[][] counterAdrModesAddresses = {
			{ "Fetch2.regdir", "26" }, { "Fetch2.regind", "30" },
			{ "Fetch2.memdir", "32" }, { "Fetch2.memind", "34" },
			{ "Fetch2.regindpom", "43" }, { "Fetch2.bxpom", "45" },
			{ "Fetch2.pcpom", "48" }, { "Fetch2.imm", "58" } };

	public static String[][] controlUnitDecoder = {
			{ "2", "Exec2.START", "#" }, { "3", "Bus1.hack", "" },
			{ "4", "Bus1.fcCPU", "#" }, { "5", "Fetch3.gropr", "#" },
			{ "6", "Fetch3.L1", "" }, { "7", "Fetch3.gradr", "#" },
			{ "8", "Fetch3.L2_brnch", "" }, { "9", "Fetch3.L2_arlog", "" },
			{ "10", "Fetch3.L3_jump", "" }, { "11", "Fetch3.L3_arlog", "" },
			{ "12", "Fetch3.store", "" }, { "13", "Fetch2.LDW", "" },
			{ "14", "Fetch2.regdir", "" }, { "15", "Exec4.brpom", "#" },
			{ "16", "Intr1.prekid", "#" }, { "17", "Intr1.PRINS", "#" },
			{ "18", "Intr1.PRCOD", "#" }, { "19", "Intr1.PRADR", "#" },
			{ "20", "Intr1.PRINM", "#" }, { "21", "Intr2.printr", "#" } };

	public static int dataSize = 16;

	public static int addressableUnit = 8;

	public static int memdelay = 5;

	public static void init(String file) {
		BufferedReader in = null;
		Map<String, List<String[]>> data = new HashMap<String, List<String[]>>();

		try {
			in = new BufferedReader(new FileReader(file));
			String line;
			while ((line = in.readLine()) != null) {
				if (!isEmpty(line)) {
					String name = getLineName(line);
					if ("dataSize".equals(name)) {
						int val = getLineInt(line);
						if (val == 8 || val == 16) {
							dataSize = val;
						} else {
							Log.log(":Sirina podatka nije ispravna. Dozvoljene vrednost 8 i 16");
						}
					} else if ("memdelay".equals(name)) {
						int val = getLineInt(line);
						if (val >= 0 && val < 256) {
							memdelay = val;
						} else {
							Log.log(":Kasnjenje memorije nije ispravno. Dozvoljena vrednost >= 0 i < 256");
						}
					} else if ("addressableUnit".equals(name)) {
						int val = getLineInt(line);
						if (val == 8 || val == 16) {
							addressableUnit = val;
						} else {
							Log.log(":Adresibilna jedinica moze biti 8 ili 16 bita");
						}
					} else if ("numberOfRegisters".equals(name)) {
						int val = getLineInt(line);
						if (val == 2 || val == 4 || val == 8 || val == 16
								|| val == 32 || val == 64) {
							numberOfRegisters = val;
						} else {
							Log.log(":Broj registara nije ispravan. Dozvoljene vrednost 2, 4, 8, 16 i 32");
						}
					} else if ("GPRARStartPosition".equals(name)) {
						int val = getLineInt(line);
						GPRARStartPosition = val;
					} else if ("GPRNumberOfBits".equals(name)) {
						int val = getLineInt(line);
						GPRNumberOfBits = val;
					} else {
						List<String[]> row = data.get(name);
						if (row == null) {
							row = new LinkedList<String[]>();
							data.put(name, row);
						}
						String[] s = getLine(line);

						row.add(s);
					}
				}
			}
			for (String key : data.keySet()) {
				List<String[]> param = data.get(key);

				if ("FETCH1".equals(key))
					fetch1Conections = convert(param);
				if ("FETCH2".equals(key))
					fetch2Conections = convert(param);
				if ("FETCH3".equals(key))
					fetch3Conections = convert(param);
				if ("EXEC3".equals(key))
					exec3Conections = convert(param);
				if ("KMOPR1".equals(key))
					counterCodeAddresses = convert(param);
				if ("KMADR1".equals(key))
					counterAdrModesAddresses = convert(param);
				if ("CONTRODC".equals(key))
					controlUnitDecoder = convert(param);
			}

			// redirect("./error.txt");

		} catch (Exception e) {
			Log.log(e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				Log.log(e);
			}
		}
	}

	public static void redirect(String file) {
		try {
			FileOutputStream errF = new FileOutputStream(file);
			PrintStream errP = new PrintStream(errF);
			System.setErr(errP);
			FileOutputStream outF = new FileOutputStream(file);
			PrintStream outP = new PrintStream(outF);
			System.setOut(outP);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	protected static String[] getLine(String line) {
		String[] result = null;
		try {
			line = line.trim();
			line = line.replace("\"", "");
			String[] data = line.split(",");
			result = new String[data.length - 1];
			for (int i = 0; i < result.length; i++) {
				result[i] = data[i + 1].trim();
			}
		} catch (Exception e) {
			Log.log(e);
		}

		return result;

	}

	protected static String getLineName(String line) {
		String result = "";
		try {
			line = line.trim();
			line = line.replace("\"", "");
			String[] data = line.split(",");
			result = data[0];
			result = result.trim();
		} catch (Exception e) {
			Log.log(e);
		}
		return result;
	}

	protected static int getLineInt(String line) {
		int result = -1;
		try {
			line = line.trim();
			line = line.replace("\"", "");
			String[] data = line.split(",");
			result = Integer.parseInt(data[1].trim());
		} catch (Exception e) {
			Log.log(e);
		}
		return result;
	}

	protected static boolean isEmpty(String line) {
		boolean result = true;
		try {
			line = line.trim();
			return line.startsWith("//");
		} catch (Exception e) {
			Log.log(e);
		}
		return result;
	}

	protected static String[][] convert(List<String[]> row) {
		String[][] result = new String[row.size()][];
		for (int i = 0; i < row.size(); i++) {
			result[i] = row.get(i);
		}
		return result;
	}

	public static int calcSize(String[][] data, String name) {
		try {
			String[] line = getLine(data, name);
			return Integer.parseInt(line[2]);
		} catch (Exception e) {
			Log.log(e);
		}
		return 0;
	}

	public static String[] getLine(String[][] data, String name) {
		for (String[] line : data) {
			if (line[1].equalsIgnoreCase(name)) {
				return line;
			}
		}
		return null;
	}

}
