package util;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;

public class DrawUtils {
	public static final int ALIGNRIGHT = 0;
	public static final int ALIGNLEFT = 1;
	public static final int ALIGCENTER = 2;

	public static int moveString(String text, int fontSize, int x) {
		Font f = new Font("Times New Roman", Font.PLAIN, fontSize);
		Rectangle2D r = f.getStringBounds(text, new FontRenderContext(null,
				RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT,
				RenderingHints.VALUE_FRACTIONALMETRICS_DEFAULT));

		int result = (int) (x - r.getWidth());

		return result;
	}

	public static int calcX(int x, String label, int fontSize, int alignment) {
		switch (alignment) {
		case ALIGNRIGHT:
			return x;
		case ALIGNLEFT:
			return DrawUtils.moveString(label, fontSize, x);
		case ALIGCENTER:
			return (DrawUtils.moveString(label, fontSize, x) + x) / 2;
		default:
			return x;
		}
	}
}
