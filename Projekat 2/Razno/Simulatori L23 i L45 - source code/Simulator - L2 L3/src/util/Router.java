package util;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

public class Router {

	public static List<Point[]> rout(List<Point> a, int distanceX, int distanceY) {
		List<Point[]> result = new LinkedList<Point[]>();

		Point start = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
		Point end = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

		for (int i = 0; i < a.size(); i++) {
			Point p = a.get(i);
			findMinMax(start, end, p);
		}

		int n = a.size();
		int max = end.y - start.y;

		// int A1 = max / n;
		int M1 = start.y + max / 2;
		// int S1 = start.y;

		int A2 = distanceY;
		int M2 = M1;
		int S2 = M2 + (A2 * n) / 2;

		int xI = distanceX / (n + 6);

		for (int i = 0; i < a.size(); i++) {
			Point first = a.get(i);
			Point[] points = new Point[11];
			points[0] = new Point(first);
			if (S2 - i * A2 > M2) {
				points[1] = new Point(points[0].x + 3 * xI + i * xI,
						points[0].y);
			} else {
				points[1] = new Point(points[0].x + (3 + n) * xI - i * xI,
						points[0].y);
			}
			points[2] = new Point(points[1].x, S2 - i * A2);
			points[3] = new Point(points[2].x + xI, points[2].y);
			points[4] = new Point(first.x + distanceX - 3 * xI, points[2].y);
			points[5] = new Point(first.x + distanceX, points[2].y);
			points[6] = new Point(first.x + distanceX - 3 * xI,
					points[2].y + 15);
			points[7] = new Point(first.x + distanceX, points[2].y + 15);
			points[8] = new Point(first.x + distanceX - 15, points[2].y);
			points[9] = new Point(first.x + distanceX + 20, points[2].y + 8);
			points[10] = new Point(first.x + distanceX + 60, points[2].y + 8);

			result.add(points);
		}

		return result;
	}

	public static void findMinMax(Point start, Point end, Point p) {
		if (p == null)
			return;
		if (p.x < start.x) {
			start.x = p.x;
		}
		if (p.y < start.y) {
			start.y = p.y;
		}
		if (p.x > end.x) {
			end.x = p.x;
		}
		if (p.y > end.y) {
			end.y = p.y;
		}
	}

}
