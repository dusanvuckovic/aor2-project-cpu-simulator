package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.util.List;

import logic.Pin;

// Klasa koja predstavja jednu liniju
// ( linija inace ima jedan ulazni i vise izlaznih pinova, pa zato postoje niz nizova tacaka )
//
public class GuiPinLine implements Drawable {
	protected List<List<Point>> sections;
	// Sve tacke svih izlomljenih linja

	protected Color color;

	protected Pin pin;

	protected float size;

	public GuiPinLine(List<List<Point>> sections, Pin pin) {
		this(sections, pin, convertToSize(pin));
	}

	public GuiPinLine(List<List<Point>> sections, Pin pin, float size) {
		this.sections = sections;
		this.pin = pin;
		this.size = size;
	}

	public void draw(Graphics g) {
		update();

		Graphics2D g2d = (Graphics2D) g;

		Stroke stroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(size));

		Color col = g2d.getColor();
		g2d.setColor(color);

		for (List<Point> section : sections) {
			Point last = null;
			for (Point p : section) {
				if (last != null) {
					g2d.drawLine(last.x, last.y, p.x, p.y);
					if (last.x == p.x)
						g2d.drawLine(last.x + 1, last.y, p.x + 1, p.y);
					if (last.y == p.y)
						g2d.drawLine(last.x, last.y + 1, p.x, p.y + 1);
				}
				last = p;
			}
		}

		g2d.setStroke(stroke);
		g2d.setColor(col);
	}

	public void update() {
		if (pin.isHighZ()) {
			color = Color.GREEN;
		} else {
			if (pin.isBool()) {
				if (pin.getBoolVal())
					color = Color.RED;
				else
					color = Color.BLUE;
			} else {
				color = Color.GRAY;
			}
		}
	}

	private static float convertToSize(Pin pin) {
		if (pin == null)
			return 1;
		int numberOfLines = pin.getNumOfLines();
		if (numberOfLines <= 1)
			return 1;
		if (numberOfLines <= 4)
			return 1.5f;
		if (numberOfLines <= 8)
			return 2;
		if (numberOfLines <= 16)
			return 2.5f;
		return 3;
	}

	public Pin getPin() {
		return pin;
	}

	public void setPin(Pin pin) {
		this.pin = pin;
	}
}
