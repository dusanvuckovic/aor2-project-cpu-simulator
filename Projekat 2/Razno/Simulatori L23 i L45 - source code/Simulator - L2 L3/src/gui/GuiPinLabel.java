package gui;

import java.awt.*;

import logic.Pin;

// Klasa koja predstavja jednu labelu
// labela ima jedan ulazni pin
public class GuiPinLabel implements Drawable {

	protected String label;
	protected Pin pin;
	protected int x;
	protected int y;

	public GuiPinLabel(int xx, int yy, Pin pinko) {
		x = xx;
		y = yy;
		pin = pinko;
	}

	public void draw(Graphics g) {
		update();

		if (!pin.isHighZ()) {
			g.setColor(Color.BLACK);
			g.setFont(new Font("Arial", Font.BOLD, 12));
			g.drawString(label, x, y);
		}
	}

	public void update() {
		if (!pin.isHighZ()) {
			switch (pin.getNumOfLines()) {
			case 4:
				label = String.format("%01x", pin.getIntVal()).toUpperCase();
				break;
			case 8:
				label = String.format("%02x", pin.getIntVal()).toUpperCase();
				break;
			case 12:
				label = String.format("%03x", pin.getIntVal()).toUpperCase();
				break;
			case 16:
				label = String.format("%04x", pin.getIntVal()).toUpperCase();
				break;
			case 24:
				label = String.format("%06x", pin.getIntVal()).toUpperCase();
				break;
			default:
				label = Integer.toHexString(pin.getIntVal());
				break;
			}

		}
	}

	public Pin getPin() {
		return pin;
	}

	public void setPin(Pin pin) {
		this.pin = pin;
	}
}
