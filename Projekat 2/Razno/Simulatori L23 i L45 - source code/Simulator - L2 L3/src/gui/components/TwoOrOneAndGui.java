package gui.components;

import java.awt.Point;
import java.util.*;

import logic.Pin;
import logic.components.*;
import gui.*;

public class TwoOrOneAndGui implements GuiComponent {

	private static final int WIDTH = 200;

	OrGui orGui1, orGui2;
	AndGui andGui;

	OR or1, or2;
	AND and;
	int x, y;
	String args[];
	boolean printConnections;

	public TwoOrOneAndGui(OR or1, OR or2, AND and, int x, int y, String[] args,
			boolean printConnections) {
		super();
		this.or1 = or1;
		this.or2 = or2;
		this.and = and;
		this.x = x;
		this.y = y;
		this.args = args;
		this.printConnections = printConnections;

		orGui1 = new OrGui(or1, x, y, args, true, false);
		orGui2 = new OrGui(or2, x, y + orGui1.getHeight() + 20, args, true,
				false);
		int centarY = (orGui1.getHeight() + orGui2.getHeight()) / 2;
		andGui = new AndGui(and, orGui1.getOutPortPosition(0).x - 65, y
				+ centarY - 25, args, false, true);
	}

	@Override
	public GuiSchema convert(GuiSchema gui) {

		gui = orGui1.convert(gui);
		gui = orGui2.convert(gui);
		gui = andGui.convert(gui);

		GuiPinLine line; // Pomocna promenljiva

		List<List<Point>> sections;
		List<Point> points;

		Point p1 = orGui1.getOutPortPosition(0);
		Point p2 = orGui2.getOutPortPosition(0);
		Point p3 = andGui.getInPortPosition(0);
		Point p4 = andGui.getInPortPosition(1);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(p1.x, p1.y));
		points.add(new Point((p1.x + p3.x) / 2, p1.y));
		points.add(new Point((p1.x + p3.x) / 2, p3.y));
		points.add(new Point(p3.x, p3.y));
		sections.add(points);
		line = new GuiPinLine(sections, or1.getOutPin(0));
		gui.addLine(line);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(p2.x, p2.y));
		points.add(new Point((p2.x + p4.x) / 2, p2.y));
		points.add(new Point((p2.x + p4.x) / 2, p4.y));
		points.add(new Point(p4.x, p4.y));
		sections.add(points);
		line = new GuiPinLine(sections, or2.getOutPin(0));
		gui.addLine(line);

		return gui;
	}

	@Override
	public int getWidth() {
		return WIDTH;
	}

	@Override
	public int getHeight() {
		int result = orGui1.getHeight() + 20 + orGui2.getHeight();
		return result;
	}

	protected static GuiTextLabel getLabel(Pin in, int x, int y) {
		GuiTextLabel label;
		String name = in.getName();
		if (name == null) {
			name = "";
		}
		label = new GuiTextLabel(moveString(name, 12, x), y, name, 12);
		return label;
	}

	private static int moveString(String text, int fontSize, int x) {
		int numChars = (2 * (text.length() + 1)) / 3;
		int result = x - numChars * (fontSize);

		return result;
	}

	@Override
	public Point getOutPortPosition(int index) {
		return andGui.getOutPortPosition(index);
	}

	@Override
	public Point getInPortPosition(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Point getCorner(int corner) {
		switch (corner) {
		case TOPLEFT:
			return new Point(x, y);
		case TOPRIGHT:
			return new Point(x + getWidth(), y);
		case BOTOMLEFT:
			return new Point(x, y + getHeight());
		case BOTOMRIGHT:
			return new Point(x + getWidth(), y + getHeight());

		}
		return null;
	}

}
