package gui.components;

import gui.GuiImageLabel;
import gui.GuiLableLine;
import gui.GuiPinLine;
import gui.GuiSchema;
import gui.GuiTextLabel;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import logic.LogicalComponent;
import logic.Pin;
import util.DrawUtils;

public class OneOutGui implements GuiComponent {

	protected static final int WIDTH = 150;

	LogicalComponent component;
	int x, y;
	String args[];
	boolean printInPortName, printOutPortName;

	protected String imageURL;

	public OneOutGui(LogicalComponent component, int x, int y, String[] args,
			boolean printInPortName, boolean printOutPortName, String imageURL) {
		super();
		this.component = component;
		this.x = x;
		this.y = y;
		this.args = args;
		this.printInPortName = printInPortName;
		this.printOutPortName = printOutPortName;
		this.imageURL = imageURL;

	}

	@Override
	public GuiSchema convert(GuiSchema gui) {
		GuiPinLine line; 
		GuiTextLabel label;

		List<List<Point>> sections;
		List<Point> points;

		int numberOfInputPins = component.getInPins().length;
		if (numberOfInputPins == 0) {
			return gui;
		}

		if (numberOfInputPins == 1) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(x + 106 - 35, y + 20));
			points.add(new Point(x + 146 - 35, y + 20));
			sections.add(points);
			line = new GuiPinLine(sections, component.getInPin(0));
			gui.addLine(line);
			if (printInPortName) {
				label = getLabel(component.getInPin(0), x + 100 - 35, y + 20);
				gui.addLabel(label);
			}
			if (printOutPortName) {
				String outName = component.getName();
				if ((outName != null) && !("".equals(outName))) {
					label = new GuiTextLabel(x + 152 - 35, y + 20, outName, 12);

					gui.addLabel(label);
				}
			}
			return gui;
		}

		for (int i = 0; i < numberOfInputPins; i++) {
			try {
				sections = new ArrayList<List<Point>>();
				points = new ArrayList<Point>();
				points.add(new Point(x + 106 - 35, y + 20 + 15 * i));
				points.add(new Point(x + 113 - 35, y + 20 + 15 * i));
				sections.add(points);
				line = new GuiPinLine(sections, component.getInPin(i));
				gui.addLine(line);
				if (printInPortName) {
					label = getLabel(component.getInPin(i), x + 100 - 33, y
							+ 20 + 15 * i);
					gui.addLabel(label);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(x + 136 - 35, y + 20 + 15
				* (numberOfInputPins - 1) / 2));
		points.add(new Point(x + 146 - 35, y + 20 + 15
				* (numberOfInputPins - 1) / 2));
		sections.add(points);
		line = new GuiPinLine(sections, component.getOutPin(0));
		gui.addLine(line);
		if (printOutPortName) {
			String outName = component.getOutPin(0).getName();
			if ((outName != null) && !("".equals(outName))) {
				label = new GuiTextLabel(x + 152 - 35, y + 20 + 15
						* numberOfInputPins / 2, outName, 12);

				gui.addLabel(label);
			}
		}
		GuiImageLabel im = new GuiImageLabel(x + 79, y + 3 + 15
				* numberOfInputPins / 2, imageURL);

		gui.addLabel(im);

		if (numberOfInputPins > 2) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(x + 114 - 35, y + 15));
			points.add(new Point(x + 114 - 35, y + 15 * numberOfInputPins / 2));
			sections.add(points);
			GuiLableLine upLine = new GuiLableLine(sections);
			gui.addLine(upLine);

			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(x + 114 - 35, y + 15 * numberOfInputPins / 2
					+ 20));
			points.add(new Point(x + 114 - 35, y + 5 + numberOfInputPins * 15
					+ 5));
			sections.add(points);

			upLine = new GuiLableLine(sections);
			gui.addLine(upLine);
		}
		return gui;
	}

	@Override
	public int getWidth() {
		String outName = component.getName();
		return Math.max(WIDTH, 152 - 35 + outName.length() * 10);
	}

	@Override
	public int getHeight() {
		int numberOfInputPins = component.getInPins().length;
		return 20 + (numberOfInputPins - 1) * 15 + 20;
	}

	protected static GuiTextLabel getLabel(Pin in, int x, int y) {
		GuiTextLabel label;
		String name = in.getName();
		if (name == null) {
			name = "";
		}
		label = new GuiTextLabel(x, y, name, 12, DrawUtils.ALIGNLEFT);
		return label;
	}

	@Override
	public Point getOutPortPosition(int index) {
		int numberOfInputPins = component.getInPins().length;
		Point result = new Point(x + 146 - 35, y + 20 + 15
				* (numberOfInputPins - 1) / 2);

		return result;
	}

	@Override
	public Point getInPortPosition(int index) {
		int numberOfInputPins = component.getInPins().length;
		if (numberOfInputPins == 0) {
			return null;
		}

		if (numberOfInputPins == 1) {
			return new Point(x + 106 - 35, y + 20);
		}

		return new Point(x + 106 - 35, y + 20 + 15 * index);

	}

	@Override
	public Point getCorner(int corner) {
		switch (corner) {
		case TOPLEFT:
			return new Point(x, y);
		case TOPRIGHT:
			return new Point(x + getWidth(), y);
		case BOTOMLEFT:
			return new Point(x, y + getHeight());
		case BOTOMRIGHT:
			return new Point(x + getWidth(), y + getHeight());

		}
		return null;
	}

}
