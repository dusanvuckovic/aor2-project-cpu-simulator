package gui.components;

import logic.components.*;

public class OrGui extends OneOutGui {

	public OrGui(OR or, int x, int y, String[] args, boolean printInPortName,
			boolean printOutPortName) {
		super(or, x, y, args, printInPortName, printOutPortName,
				"src/images/OR.png");
	}

}
