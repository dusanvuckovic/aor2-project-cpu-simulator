package gui.components;

import logic.components.*;

public class AndGui extends OneOutGui {

	public AndGui(AND and, int x, int y, String[] args, boolean printInPortName,
			boolean printOutPortName) {
		super(and, x, y, args, printInPortName, printOutPortName,
				"src/images/AND.png");
	}

}
