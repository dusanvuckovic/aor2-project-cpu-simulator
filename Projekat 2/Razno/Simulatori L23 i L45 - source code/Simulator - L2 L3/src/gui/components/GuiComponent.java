package gui.components;

import java.awt.*;

import gui.*;

public interface GuiComponent {
	public static final int TOPLEFT = 0;
	public static final int TOPRIGHT = 1;
	public static final int BOTOMLEFT = 2;
	public static final int BOTOMRIGHT = 3;

	public GuiSchema convert(GuiSchema gui);

	public int getWidth();

	public int getHeight();

	public Point getOutPortPosition(int index);

	public Point getInPortPosition(int index);

	public Point getCorner(int corner);
}
