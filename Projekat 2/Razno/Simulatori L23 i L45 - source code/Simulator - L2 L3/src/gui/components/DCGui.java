package gui.components;

import java.awt.Point;
import java.util.*;

import logic.components.*;
import util.*;
import gui.*;

public class DCGui implements GuiComponent {

	private static final int DCWIDTH = 180;

	List<GuiTextLabel> labels;
	List<GuiLableLine> lines;

	DC dc;
	int x, y;
	String args[];
	boolean printConnections;
	Point[] inPortPositions, outPortPositions;

	public DCGui(DC dc, int x, int y, String[] args, boolean printConnections) {
		this.dc = dc;
		this.x = x;
		this.y = y;
		this.args = args;
		this.printConnections = printConnections;

		labels = new LinkedList<GuiTextLabel>();
		lines = new LinkedList<GuiLableLine>();

		inPortPositions = new Point[dc.getInPins().length];
		outPortPositions = new Point[dc.getOutPins().length];
	}

	private static final int DISTANCE = 15;

	@Override
	public GuiSchema convert(GuiSchema gui) {
		GuiPinLine line; // Pomocna promenljiva
		List<List<Point>> sections;
		List<Point> points;

		int inNum = dc.getInPins().length;
		int outNum = (int) (Math.pow(2, inNum));
		int heihgt = (outNum + 1) * DISTANCE;

		GuiTextLabel label = new GuiTextLabel(x + 54, y + getYMargin(inNum)
				+ heihgt / 2, args[1], 14);
		labels.add(label);

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(x + 39, y + getYMargin(inNum)));
		points.add(new Point(x + 95, y + getYMargin(inNum)));
		points.add(new Point(x + 95, y + getYMargin(inNum) + heihgt));
		points.add(new Point(x + 39, y + getYMargin(inNum) + heihgt));
		points.add(new Point(x + 39, y + getYMargin(inNum)));
		sections.add(points);
		lines.add(new GuiLableLine(sections));

		int startI0 = getYMargin(inNum) + heihgt / 2 + inNum * DISTANCE / 2;

		for (int i = 0; i < inNum; i++) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(x - 50 + 82, y + startI0));
			points.add(new Point(x - 50 + 89, y + startI0));
			sections.add(points);
			line = new GuiPinLine(sections, dc.getInPin(i));
			gui.addLine(line);
			inPortPositions[i] = new Point(x - 50 + 89, y + startI0);

			label = new GuiTextLabel(x - 50 + 94, y + startI0, "" + i, 10);
			labels.add(label);
			label = new GuiTextLabel(x + 28, y + startI0, args[4 + i], 13,
					DrawUtils.ALIGNLEFT);
			labels.add(label);

			startI0 -= DISTANCE;
		}

		sections = new ArrayList<List<Point>>();
		points = new ArrayList<Point>();
		points.add(new Point(x - 50 + 117, y + getYEnd(inNum)));
		points.add(new Point(x - 50 + 117, y + getYEnd(inNum) + 8));
		sections.add(points);
		line = new GuiPinLine(sections, dc.getE());
		gui.addLine(line);

		label = new GuiTextLabel(x - 50 + 116, y + getYEnd(inNum) - 6, "E", 10);
		labels.add(label);
		label = new GuiTextLabel(x + 68, y + getYEnd(inNum) + 22, args[3], 13, DrawUtils.ALIGCENTER);
		labels.add(label);

		startI0 = getYEnd(inNum) - DISTANCE;

		for (int i = 0; i < outNum; i++) {
			sections = new ArrayList<List<Point>>();
			points = new ArrayList<Point>();
			points.add(new Point(x - 50 + 145, y + startI0));
			points.add(new Point(x - 50 + 151, y + startI0));
			sections.add(points);
			line = new GuiPinLine(sections, dc.getOutPin(i));
			gui.addLine(line);
			outPortPositions[i] = new Point(x - 50 + 151, y + startI0);

			label = new GuiTextLabel(x - 50 + 135, y + startI0 + 2, "" + i, 10);
			labels.add(label);
			if (printConnections) {
				label = new GuiTextLabel(x - 50 + 155, y + startI0 + 2, args[4
						+ inNum + i], 13);
				labels.add(label);
			}
			startI0 -= DISTANCE;
		}

		for (GuiTextLabel data : labels) {
			gui.addLabel(data);
		}
		for (GuiLableLine data : lines) {
			gui.addLine(data);
		}
		return gui;
	}

	@Override
	public int getWidth() {
		return DCWIDTH;
	}

	@Override
	public int getHeight() {
		int max = dc.getOutPins().length;
		if (max <= 8) {
			return 180;
		} else {
			return 180 * max / 8;
		}
	}

	protected int getYMargin(int n) {
		if (n == 2) {
			return 46;
		} else {
			return 18;
		}
	}

	protected int getYEnd(int n) {
		int result = getYMargin(n);
		result += ((int) (Math.pow(2, n)) + 1) * DISTANCE;
		return result;
	}

	@Override
	public Point getOutPortPosition(int index) {
		if (index >= 0 && index < outPortPositions.length) {
			return outPortPositions[index];
		}
		return null;
	}

	@Override
	public Point getInPortPosition(int index) {
		if (index >= 0 && index < inPortPositions.length) {
			return inPortPositions[index];
		}
		return null;
	}

	@Override
	public Point getCorner(int corner) {
		switch (corner) {
		case TOPLEFT:
			return new Point(x, y);
		case TOPRIGHT:
			return new Point(x + getWidth(), y);
		case BOTOMLEFT:
			return new Point(x, y + getHeight());
		case BOTOMRIGHT:
			return new Point(x + getWidth(), y + getHeight());

		}
		return null;
	}

}
