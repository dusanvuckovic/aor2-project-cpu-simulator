package gui.components;

import logic.components.*;

public class NorGui extends OneOutGui {

	public NorGui(OR or, int x, int y, String[] args, boolean printInPortName,
			boolean printOutPortName) {
		super(or, x, y, args, printInPortName, printOutPortName, "src/images/NOR.png");
	}

}
