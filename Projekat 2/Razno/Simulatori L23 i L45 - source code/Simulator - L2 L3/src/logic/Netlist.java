package logic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Netlist {

	public static List<LogicalComponent> extractAllComponent(
			List<LogicalComponent> comps) {
		List<LogicalComponent> result = new LinkedList<LogicalComponent>();

		Set<LogicalComponent> processed = new HashSet<LogicalComponent>();
		List<LogicalComponent> forProcessing = new LinkedList<LogicalComponent>(
				comps);
		while (forProcessing.size() > 0) {
			LogicalComponent comp = forProcessing.remove(0);
			if (!processed.contains(comp)) {
				for (Pin pin : comp.getAllOutPins()) {
					if (pin != null && pin.getChildren() != null) {
						for (LogicalComponent lc : pin.getChildren()) {
							if (!processed.contains(lc)) {
								forProcessing.add(lc);
							}
						}
					}
				}
			}
			processed.add(comp);
		}
		result.addAll(processed);
		//printTypeStat(result);
		return result;
	}

	public static List<LogicalComponent> extractAllCombinationalComponent(
			List<LogicalComponent> comps) {
		List<LogicalComponent> result = new LinkedList<LogicalComponent>();
		for (LogicalComponent comp : comps) {
			if (!comp.isSeq()) {
				result.add(comp);
			}
		}
		return result;
	}

	public static List<LogicalComponent> extractAllSequentialComponent(
			List<LogicalComponent> comps) {
		List<LogicalComponent> result = new LinkedList<LogicalComponent>();
		for (LogicalComponent comp : comps) {
			if (comp.isSeq()) {
				result.add(comp);
			}
		}
		return result;
	}

	public static void printTypeStat(List<LogicalComponent> comps) {
		Map<String, Integer> cnts = new HashMap<String, Integer>();
		for (LogicalComponent comp : comps) {
			if (comp != null) {
				String name = comp.getClass().getSimpleName();
				Integer cnt = cnts.get(name);
				if (cnt == null) {
					cnt = 0;
				}
				cnt++;
				cnts.put(name, cnt);
			}
		}
		String[] names = cnts.keySet().toArray(new String[cnts.size()]);
		Arrays.sort(names);
		for (String name : names) {
			System.out.println(name + ":" + cnts.get(name));
		}
	}
}
