package logic;

import java.util.ArrayList;
import java.util.List;

public class Pin {

	private String name;
	private int numOfLines;
	private boolean isBool;

	private int intVal;
	private boolean boolVal;
	private boolean highZ;

	private List<LogicalComponent> children;

	public Pin(String name) {
		this(name, true, 1, 0, false, false);
	}

	public Pin(boolean val, String name) {
		this(name, true, 1, 0, val, false);
	}

	public Pin(int val, int brlin, String name) {
		this(name, false, brlin, val, false, false);
	}

	public Pin(String name, boolean isBool, int numOfLines, int intVal,
			boolean boolVal, boolean highZ) {
		this.name = name;
		this.isBool = isBool;
		this.numOfLines = numOfLines;

		this.intVal = intVal;
		this.boolVal = boolVal;
		this.highZ = highZ;

		children = new ArrayList<LogicalComponent>();

	}

	public boolean isBool() {
		return isBool;
	}

	public void setIsInt() {
		isBool = false;
	}

	public void setIsBool() {
		isBool = true;
	}

	public void clear() {
		if (boolVal == true || intVal != 0) {
			boolVal = false;
			intVal = 0;
			signalAll();
		}
	}

	public boolean getBoolVal() {
		return boolVal;
	}

	public void setBoolVal(boolean bval) {
		if (isBool) {
			if ((boolVal != bval) || (highZ == true)) {
				this.boolVal = bval;
				this.highZ = false;
				signalAll();
			}
		}
	}

	public int getIntVal() {
		return intVal;
	}

	public void setIntVal(int ival) {
		if (!isBool) {
			if ((intVal != ival) || (highZ == true)) {
				this.intVal = ival;
				this.highZ = false;
				signalAll();
			}
		}
	}

	public boolean isHighZ() {
		return highZ;
	}

	public void setHighZ(boolean highZboolValue) {
		if ((highZ != highZboolValue)) {
			this.highZ = highZboolValue;
			signalAll();
		}
	}

	public void setVal(Pin pin) {
		if ((boolVal != pin.boolVal) || (intVal != pin.intVal)
				|| (highZ == true)) {
			this.boolVal = pin.boolVal;
			this.intVal = pin.intVal;
			this.highZ = pin.highZ;
			signalAll();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumOfLines() {
		return numOfLines;
	}

	public void setNumOfLines(int numOfLines) {
		if (numOfLines >= 0 && numOfLines <= 32) {
			this.numOfLines = numOfLines;
		} else {
			throw new RuntimeException(
					"Not suppotres port size greater then 32:" + numOfLines);
		}
	}

	public void addChild(LogicalComponent logcomp) {
		children.add(logcomp);
	}

	public List<LogicalComponent> getChildren() {
		return children;
	}

	void signalAll() {
		LogicalComponent child = null;
		for (int i = 0; i < children.size(); i++) {
			child = children.get(i);
			if (!child.isSeq())
				child.execute();
		}
	}

	public boolean compare(Pin pin) {
		boolean result = (pin != null) && (this.intVal == pin.intVal)
				&& (this.boolVal == pin.boolVal) && (this.highZ == pin.highZ);
		return result;
	}

	public void copyVal(Pin pin) {
		if (pin == null) {
			return;
		}
		this.intVal = pin.intVal;
		this.boolVal = pin.boolVal;
		this.highZ = pin.highZ;
	}

	public String toString() {
		return name + ":" + this.boolVal + ";" + this.intVal + ";" + this.highZ;
	}
}
