package logic;

import java.util.LinkedList;
import java.util.List;

public abstract class LogicalComponent {

	protected String name;
	protected String[] args;

	protected Pin[] in;
	protected Pin[] out;
	protected List<Pin> allPins;

	protected int period;

	public LogicalComponent(int in, int out, String name, int period) {
		this.name = name;
		this.args = null;

		this.in = new Pin[in];
		this.out = new Pin[out];
		this.allPins = new LinkedList<Pin>();

		this.period = period;

		for (int i = 0; i < out; i++) {
			Pin pin = new Pin("");
			this.out[i] = pin;
			allPins.add(pin);
		}

	}

	public LogicalComponent(int in, int out) {
		this(in, out, "", 0);
	}

	public abstract void func();

	public void execute() {
		func();
	}

	public void flush() {

	}

	public void init(boolean fullInit) {
		func();
	}

	public abstract void initArgs(String[] args);

	public boolean isSeq() {
		return period > 0;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getPeriod() {
		return this.period;
	}

	public void setInPins(Pin[] in) {
		this.in = in;
		for (int i = 0; i < in.length; i++) {
			in[i].addChild(this);
		}
	}

	public void setInPin(int index, Pin pin) {
		this.in[index] = pin;
		pin.addChild(this);
	}

	public void setOutPin(int index, Pin pin) {
		out[index] = pin;
	}

	public Pin[] getOutPins() {
		return this.out;
	}

	public Pin[] getInPins() {
		return this.in;
	}

	public Pin getOutPin(int index) {
		return this.out[index];
	}

	public Pin getInPin(int index) {
		return this.in[index];
	}

	public Pin getPin(String pinName) {
		Pin result = null;
		for (Pin pin : allPins) {
			if (pinName.equalsIgnoreCase(pin.getName())) {
				return pin;
			}
		}
		return result;
	}
	public List<Pin> getAllOutPins() {
		return allPins;
	}
}
