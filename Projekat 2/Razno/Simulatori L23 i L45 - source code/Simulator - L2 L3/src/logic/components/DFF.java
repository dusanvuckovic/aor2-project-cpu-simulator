package logic.components;

import logic.Pin;

public class DFF extends FF {

	public DFF(String name) {
		super(1, name);
	}

	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		if ((reset != null) && (reset.getBoolVal())) {
			state = false;
			return;
		}

		state = in[0].getBoolVal();
	}

	public void setPinD(Pin d) {
		setInPin(0, d);
	}

}
