package logic.components;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;

public abstract class FF extends LogicalComponent {
	protected Pin clk;
	protected Pin reset;
	protected boolean init;
	protected boolean state;
	
	public FF(int inpins, String name) {
		super(inpins, 2, name, 1);
		Execution.addSequentialComponent(this);
	}

	public abstract void func();

	public void setReset(Pin reset) {
		this.reset = reset;
		reset.addChild(this);
	}

	public void setClk(CLK clk) {
		this.clk = clk.getClk();
		this.period = clk.getPeriod();
	}

	public void init(boolean fullInit) {
		state = init;
		setOutput();
	}

	public void setInit(boolean init) {
		this.state = init;
		this.init = init;
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

	public void flush() {
		setOutput();
	}

	public void setOutput() {
		out[0].setBoolVal(state);
		out[1].setBoolVal(!state);
	}
}
