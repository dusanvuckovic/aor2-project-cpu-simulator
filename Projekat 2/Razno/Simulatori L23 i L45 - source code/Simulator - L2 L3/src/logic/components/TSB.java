package logic.components;

import logic.LogicalComponent;
import logic.Pin;

public class TSB extends LogicalComponent {

	private Pin enable;

	private Pin lastIn;
	private Pin lastE;
	private Pin lastOut;

	public TSB(String name) {
		super(1, 1, name, 0);
		out[0].setIsInt();
		out[0].setHighZ(true);
		out[0].addChild(this);
		enable = new Pin("", true, 0, 0, true, false);

		lastIn = new Pin("", true, 0, 1, true, false);
		lastE = new Pin("", true, 0, 1, true, false);
		lastOut = new Pin("", true, 0, 1, true, false);

	}

	public void func() {
		/*if (!lastOut.compare(out[0])
				&& (!enable.getBoolVal() || lastE.getBoolVal())) {
			lastIn.copyVal(in[0]);
			lastE.copyVal(enable);
			lastOut.copyVal(out[0]);
			return;
		}
		*/
		if (enable.getBoolVal()) {
			if (in[0].isHighZ()) {
				if (out[0].isBool()) {
					out[0].setBoolVal(true);
				} else {
					out[0].setIntVal(1);
				}
			} else if (out[0].isBool()) {
				out[0].setBoolVal(in[0].getBoolVal());
			} else {
				out[0].setIntVal(in[0].getIntVal());
			}
		} else {
			out[0].setHighZ(true);
			// out[0].clear();
		}
		
		lastIn.copyVal(in[0]);
		lastE.copyVal(enable);
		lastOut.copyVal(out[0]);

	}

	public Pin getE() {
		return enable;
	}

	public void setE(Pin enable) {
		this.enable = enable;
		enable.addChild(this);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

	public static TSB createTSB(String[] data) {
		TSB result = null;
		try {
			String name = data[1];
			result = new TSB(name);
			int size = Integer.parseInt(data[2]);
			result.getOutPin(0).setNumOfLines(size);
			result.getOutPin(0).setName(data[data.length - 1]);

		} catch (Exception e) {
		}
		return result;

	}
}
