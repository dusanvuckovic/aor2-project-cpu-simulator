package logic.components;

import logic.LogicalComponent;

public class BoolsToInt extends LogicalComponent {

	public BoolsToInt(int in, int numLines) {
		super(in, 1);
		out[0].setIsInt();
		out[0].setNumOfLines(numLines);

	}

	public void func() {
		int val = 0;
		for (int i = 0; i < in.length; i++) {
			if (in[i].getBoolVal())
				val = (int) (val + Math.pow(2.0D, i));

		}

		out[0].setIntVal(val);

	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
