package logic.components;

import logic.LogicalComponent;
import logic.Pin;
import util.NameConnector;

public class CLK extends LogicalComponent {
	private String componentName = "LogicalComponent";
	private Pin clk;

	private int delay;
	
	private int time;
	
	public CLK(String name) {
		this(name, 1, 0);
	}

	public CLK(String name, int period, int delay) {
		super(0, 0, name, period);
		this.clk = new Pin(name);
		this.delay = delay;
		NameConnector.addPin(componentName, name, clk);
	}

	@Override
	public void initArgs(String[] args) {
		// TODO Auto-generated method stub

	}

	public Pin getClk() {
		return clk;
	}

	@Override
	public void func() {
		time++;
		if (time % period == delay) {
			clk.setBoolVal(true);
		} else {
			clk.setBoolVal(false);
		}
	}

	public void setStart(int start) {
		this.delay = start;
	}
}
