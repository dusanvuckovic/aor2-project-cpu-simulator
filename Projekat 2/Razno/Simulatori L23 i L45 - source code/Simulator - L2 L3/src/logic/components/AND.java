package logic.components;

import logic.LogicalComponent;

public class AND extends LogicalComponent {

	public AND() {
		this(2);
	}

	public AND(int in) {
		super(in, 1);
	}

	public AND(int in, String name) {
		super(in, 1);
		this.name = name;
	}

	public void func() {
		boolean result = true;
		for (int i = 0; i < in.length; i++) {
			result = (result) && (in[i].getBoolVal());
		}
		out[0].setBoolVal(result);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
		this.out[0].setName(args[args.length - 1]);
	}

}
