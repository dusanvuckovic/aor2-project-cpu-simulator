package logic.components;

import java.io.*;
import java.util.*;

import logic.LogicalComponent;
import logic.Pin;
import util.*;

public class MicroMEM extends LogicalComponent {

	private String[] memory;

	public MicroMEM(int size) {
		super(1, 6);

		out[0].setIsInt();
		out[0].setNumOfLines(24);
		out[1].setIsInt();
		out[1].setNumOfLines(24);
		out[2].setIsInt();
		out[2].setNumOfLines(24);
		out[3].setIsInt();
		out[3].setNumOfLines(24);
		out[4].setIsInt();
		out[4].setNumOfLines(8);
		out[5].setIsInt();
		out[5].setNumOfLines(8);
		memory = new String[size];

		// initFromFile("./microkod.txt");
		initFromMikroprogramFile("./microProgram.txt");
	}

	protected void initFromFile(String fileName) {
		int counter = 0;
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(fileName));
			String line;
			while ((line = reader.readLine()) != null) {
				memory[counter++] = line;
			}
		} catch (Exception e) {
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
			}
		}
	}

	protected void initFromMikroprogramFile(String fileName) {
		int counter = 0;
		try {

			MicrocodeConverter.load(Parameters.controlUnitDecoder);

			MicrocodeConverter c = new MicrocodeConverter();
			
			List<String> lines = c.parseIn(fileName);

			for (String line : lines) {
				memory[counter++] = line;
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void func() {
		Log.log(memory[in[0].getIntVal()]);

		try {
			out[0].setIntVal(Integer.parseInt(
					memory[in[0].getIntVal()].substring(0, 6), 16));
			out[1].setIntVal(Integer.parseInt(
					memory[in[0].getIntVal()].substring(6, 12), 16));
			out[2].setIntVal(Integer.parseInt(
					memory[in[0].getIntVal()].substring(12, 18), 16));
			out[3].setIntVal(Integer.parseInt(
					memory[in[0].getIntVal()].substring(18, 24), 16));
			out[4].setIntVal(Integer.parseInt(
					memory[in[0].getIntVal()].substring(24, 26), 16));
			out[5].setIntVal(Integer.parseInt(
					memory[in[0].getIntVal()].substring(26, 28), 16));
		} catch (Exception e) {
			System.out.println(in[0].getIntVal());
			e.printStackTrace();
			Log.log(e);
		}
	}

	public String getAllOut() {
		return memory[in[0].getIntVal()];
	}

	public static void main(String[] args) {

		MicroMEM mm = new MicroMEM(1024);

		mm.setInPin(0, new Pin(71, 8, ""));

		mm.func();

		Log.log(Integer.toBinaryString(mm.getOutPin(0).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(1).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(2).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(3).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(4).getIntVal()));
		Log.log(Integer.toBinaryString(mm.getOutPin(5).getIntVal()));
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
