package logic.components;

import logic.LogicalComponent;
import logic.Pin;
import util.Log;

public class IntToBools extends LogicalComponent {

	public IntToBools(int numLines, int out) {
		super(1, out);

	}

	public void func() {
		int mval = in[0].getIntVal();

		if (mval > 0) { // Int>0
			for (int j = 0; j < out.length; j++) {
				int mod = mval % 2;
				mval /= 2;
				out[j].setBoolVal(mod == 1);
				if (in[0].isHighZ()) {
					out[j].setHighZ(true);
				}
			}
		} else { // Int<=0
			mval = (int) Math.pow(2.0D, out.length) + mval;
			for (int j = 0; j < out.length; j++) {
				int mod = mval % 2;
				mval /= 2;
				out[j].setBoolVal(mod == 1);
				if (in[0].isHighZ()) {
					out[j].setHighZ(true);
				}
			}
		}

	}

	public static void main(String args[]) {
		IntToBools i = new IntToBools(1, 8);
		i.setInPin(0, new Pin(117, 8, ""));
		i.func();
		for (int j = 0; j < 8; j++) {
			Log.log(" Izlaz :" + j + " " + i.getOutPin(j).getBoolVal());
		}

	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
