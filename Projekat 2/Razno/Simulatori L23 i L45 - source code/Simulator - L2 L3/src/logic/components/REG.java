package logic.components;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;
import util.*;

public class REG extends LogicalComponent {

	private Pin clk;
	
	private Pin cl;
	private Pin ld;
	private Pin inc;
	private Pin dec;
	private Pin shr;
	private Pin shl;
	private Pin carry;
	private Pin ir;
	private Pin il;

	private int value;
	private boolean c;
	private int init;

	public REG(int in, String name) {
		super(in, in, name, 1);
		value = 0;
		c = false;
		Execution.addSequentialComponent(this);
	}

	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		
		int val = 0;
		if ((cl != null) && (cl.getBoolVal())) {
			val++;
		}
		if ((ld != null) && (ld.getBoolVal())) {
			val++;
		}
		if ((inc != null) && (inc.getBoolVal())) {
			val++;
		}
		if ((dec != null) && (dec.getBoolVal())) {
			val++;
		}
		if ((shr != null) && (shr.getBoolVal())) {
			val++;
		}
		if ((shl != null) && (shl.getBoolVal())) {
			val++;
		}
		if (val > 1) {
			Log.log(":Error!!! Multiple register action...");
			return;
		}
		if ((cl != null) && (cl.getBoolVal())) {
			for (int i = 0; i < out.length; i++) {
				value = 0;
			}
			return;
		}
		if ((ld != null) && (ld.getBoolVal())) {
			value = getInput() & createMask();
			return;
		}
		if ((shl != null) && (shl.getBoolVal())) {
			value <<= 1;
			if ((il != null) && (il.getBoolVal())) {
				value |= 1;
			}
			value = value & createMask();
			return;
		}

		if ((shr != null) && (shr.getBoolVal())) {
			value >>= 1;
			if ((ir != null) && (ir.getBoolVal())) {
				value |= (int) Math.pow(2.0D, in[0].getNumOfLines() - 1);
			} else {
				value &= ((int) Math.pow(2.0D, in[0].getNumOfLines() - 1) ^ 0xFFFFFFFF);
			}
			value = value & createMask();
			return;
		}

		if ((inc != null) && (inc.getBoolVal())) {
			if (out[0].isBool()) {
				value += 1;
				if (value % (int) Math.pow(2.0D, in.length) == 0) {
					value = 0;
					c = true;
				} else {
					c = false;
				}
			} else {
				value += 1;
				if (value % (int) Math.pow(2.0D, in[0].getNumOfLines()) == 0) {
					value = 0;
					c = true;
				} else {
					c = false;
				}
			}
			value = value & createMask();
			return;
		}
		if ((dec != null) && (dec.getBoolVal())) {
			if (value > 0) {
				value -= 1;
				c = false;
			} else {
				value = (int) Math.pow(2.0D, in.length);
				c = true;
			}
			value = value & createMask();
			return;
		}
	}

	public void flush() {
		if (out[0].isBool()) {
			int priv = value;
			for (int j = 0; j < out.length; j++) {
				int mod = priv % 2;
				priv /= 2;
				if (mod == 1) {
					out[j].setBoolVal(true);
				} else {
					out[j].setBoolVal(false);
				}
			}
		} else {
			out[0].setIntVal(value);
		}
		if (carry != null) {
			carry.setBoolVal(c);
		}
	}

	private int getInput() {

		if (in[0].isBool()) {
			int suma = 0;
			for (int j = 0; j < in.length; j++) {
				if (in[j].getBoolVal()) {
					suma = (int) (suma + Math.pow(2.0D, j));
				}
			}
			return suma;
		}
		return in[0].getIntVal();

	}

	public void setPinInc(Pin inc) {
		this.inc = inc;
		inc.addChild(this);
	}

	public void setPinDec(Pin dec) {
		this.dec = dec;
		dec.addChild(this);
	}

	public void setPinCL(Pin CL) {
		this.cl = CL;
		CL.addChild(this);
	}

	public void setPinLd(Pin LD) {
		this.ld = LD;
		LD.addChild(this);
	}

	public int getVal() {
		return value;
	}

	public void initVal(int value) {
		this.value = value;
		init = value;
	}

	public void setVal(int val) {

		if (in[0].isBool()) {
			value = (val % (int) Math.pow(2.0D, in.length));
		} else {
			value = (val % (int) Math.pow(2.0D, in[0].getNumOfLines()));
		}
		flush();
	}

	public void setShr(Pin shr) {
		this.shr = shr;
		shr.addChild(this);
	}

	public void setShl(Pin shl) {
		this.shl = shl;
		shr.addChild(this);
	}

	public Pin getC() {
		return carry;
	}

	public void setIR(Pin ir) {
		this.ir = ir;
		ir.addChild(this);
	}

	public void setIL(Pin il) {
		this.il = il;
		il.addChild(this);
	}

	public void init(boolean fullInit) {
		value = init;
		flush();
	}

	public int createMask() {
		int mask;
		if (getOutPin(0).isBool()) {
			mask = out.length;
		} else {
			mask = getOutPin(0).getNumOfLines();
		}
		switch (mask) { // DEC VAL_hex
		case 6:
			return 0x3f;// jer GPRAR je 6 bita registar
		case 8:
			return 0xff;
		case 16:
			return 0xffff;
		}
		return 0xffff;// za svaki slucaj
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

	public String toString() {
		String result = name + "=" + value;
		return result;
	}
	
	public void setClk(CLK clk) {
		this.clk = clk.getClk();
		this.period = clk.getPeriod();
	}
}
