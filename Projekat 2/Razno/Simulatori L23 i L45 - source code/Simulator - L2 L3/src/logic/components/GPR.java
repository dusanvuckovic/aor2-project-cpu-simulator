package logic.components;

import java.util.ArrayList;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;

public class GPR extends LogicalComponent {

	private Pin clk;

	private int size;// broj registara opste namene

	private ArrayList<REG> regs;
	private ArrayList<AND> ands;

	private IntToBools adressBits;
	private DC dekoder;
	private MP mx;

	public GPR(int size) {
		super(2, 1);
		this.size = size;

		int numberOfPins = (int) (Math.log(size) / Math.log(2));

		adressBits = new IntToBools(numberOfPins, numberOfPins);
		out[0].setIsInt();
		out[0].setNumOfLines(16);
		dekoder = new DC(numberOfPins);
		dekoder.setE(new Pin(true, ""));
		for (int i = 0; i < numberOfPins; i++) {
			dekoder.setInPin(i, adressBits.getOutPin(i));
		}

		mx = new MP(size);
		mx.setOutPin(0, this.out[0]);
		for (int i = 0; i < numberOfPins; i++) {
			mx.setCtrl(i, adressBits.getOutPin(i));
		}

		regs = new ArrayList<REG>(size);
		ands = new ArrayList<AND>(size);
		for (int i = 0; i < size; i++) {

			AND ldreg = new AND();
			ldreg.setInPin(0, dekoder.getOutPin(i));
			ands.add(ldreg);

			String name = "R" + i;
			REG r = new REG(1, name);
			r.getOutPin(0).setIsInt();
			r.getOutPin(0).setNumOfLines(16);
			r.setPinLd(ldreg.getOutPin(0));
			regs.add(r);

			mx.setInPin(i, r.getOutPin(0));
			Execution.addSequentialComponent(r);
		}

		// addSequentialComponent(this);
	}

	public void func() {
		if ((clk != null) && (!clk.getBoolVal())) {
			return;
		}
		mx.func();
	}

	public int getSize() {
		return size;
	}

	public void setRead(Pin read) {
		read.addChild(this);
		mx.setE(read);
	}

	public void setWrite(Pin write) {
		for (int i = 0; i < size; i++) {
			ands.get(i).setInPin(1, write);
			write.addChild(ands.get(i));
		}

	}

	public void setAdressPin(Pin adress) {
		in[0] = adress;
		adressBits.setInPin(0, adress);
		adress.addChild(this);
		adress.addChild(adressBits);

	}

	public void setInputDataPin(Pin datain) {
		in[1] = datain;
		for (int i = 0; i < size; i++) {
			regs.get(i).setInPin(0, datain);
			datain.addChild(regs.get(i));
		}

	}

	public int read(int adress) {
		return regs.get(adress).getVal();
	}

	public REG getREG(int i) {
		return regs.get(i);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

	public void setClk(CLK clk) {
		this.clk = clk.getClk();
		this.period = clk.getPeriod();
		for (REG reg : regs) {
			reg.setClk(clk);
		}
	}

}