package logic.components;

import logic.LogicalComponent;

public class KMOPR extends LogicalComponent { // @Darko

	private int[] stepsDec = { 62, 63, 64, 65, 66, 67, 68, 70, 71, 77, 86, 91,
			99, 104, 113, 114, 115, 116, 117, 118, 119, 121, 123, 125, 127,
			129, 131, 133, 135, 135, 135, 135, 137, 137, 137, 137, 139, 139,
			139, 139, 139, 139, 139, 139, 139, 139, 139, 139, 139, 139, 139,
			139, 141, 142, 143, 152, 160 };

	public KMOPR(int[] stepsDec) {
		super(stepsDec.length, 1);
		out[0].setIsInt();
		this.stepsDec = stepsDec;
	}

	public void func() {
		int i = 0;
		for (; i < in.length; i++) {
			if (in[i].getBoolVal()) {
				break;
			}
		}
		if (i < in.length)
			out[0].setIntVal(stepsDec[i]);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
