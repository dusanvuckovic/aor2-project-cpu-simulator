package logic.components;

import logic.LogicalComponent;

public class NAND extends LogicalComponent {

	public NAND() {
		this(2);
	}

	public NAND(int in) {
		super(in, 1);
	}

	public void func() {

		boolean result = true;
		for (int i = 0; i < in.length; i++)
			result = (result) && (in[i].getBoolVal());

		result = !result;
		out[0].setBoolVal(result);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
