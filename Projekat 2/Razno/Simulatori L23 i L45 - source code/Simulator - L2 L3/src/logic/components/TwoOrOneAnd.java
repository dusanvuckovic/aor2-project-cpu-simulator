package logic.components;

import logic.LogicalComponent;

public class TwoOrOneAnd extends LogicalComponent {
	OR or1;
	OR or2;
	AND and;

	public TwoOrOneAnd(OR or1, OR or2, AND and) {
		super(1, 1);
		this.or1 = or1;
		this.or2 = or2;
		this.and = and;
	}

	public void func() {
		throw new RuntimeException("Not implemented exception");
	}

	@Override
	public void initArgs(String[] args) {

	}

	public OR getOr1() {
		return or1;
	}

	public void setOr1(OR or1) {
		this.or1 = or1;
	}

	public OR getOr2() {
		return or2;
	}

	public void setOr2(OR or2) {
		this.or2 = or2;
	}

	public AND getAnd() {
		return and;
	}

	public void setAnd(AND and) {
		this.and = and;
	}

}
