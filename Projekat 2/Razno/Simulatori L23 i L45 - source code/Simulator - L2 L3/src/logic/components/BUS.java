package logic.components;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import logic.Execution;
import logic.LogicalComponent;
import logic.Pin;
import util.Log;

public class BUS extends LogicalComponent {

	public BUS(String name) {
		super(0, 1, name, 0);
		this.out[0].setIsInt();
		Execution.addSequentialComponent(this);
	}

	public void func() {
		int activeInput = 0;
		Set<Integer> vals = new HashSet<Integer>();
		for (int i = 0; i < in.length; i++) {
			if (!in[i].isHighZ()) {
				activeInput = i;
				if (out[0].isBool()) {
					vals.add(in[i].getBoolVal() ? 0 : 1);
				} else {
					vals.add(in[i].getIntVal());
				}
			}
		}
		if (vals.size() > 1) {
			Log.log(":Warning!!! Bus " + name + " may be burned!");
			return;
			// iako se nekad ovo desi ne treba ga smatrati kao ozbiljno jer
			// provereno jer da se desava zato sto nakon klika ne stignu svi
			// signali da se propagiraju i neki ostanu iz prethodnog takta a
			// neki iz tekuceg takta aktivni signali na TSB-ovima ali na kraju
			// takta se sve normalizuje i smanji broj aktivnih ispod 2
		}
		if (vals.size() == 0) {
			out[0].setHighZ(true);
			// out[0].clear();
		}
		if (vals.size() == 1) {
			out[0].setHighZ(false);
			if (out[0].isBool()) {
				out[0].setBoolVal(in[activeInput].getBoolVal());
			} else {
				out[0].setIntVal(in[activeInput].getIntVal());
			}
/*
			// dodato
			for (int i = 0; i < in.length; i++) {
				in[i].setHighZ(false);
				if (in[i].isBool()) {
					in[i].setBoolVal(in[activeInput].getBoolVal());
				} else {
					in[i].setIntVal(in[activeInput].getIntVal());
				}
			}
*/
		}
	}

	public void setInPin(int index, Pin pin) {
		if (pin == null) {
			return;
		}
		in = Arrays.copyOf(in, in.length + 1);
		this.in[in.length - 1] = pin;
		pin.addChild(this);
	}

	@Override
	public void initArgs(String[] args) {
		this.args = args;
		this.name = args[1];
	}

}
