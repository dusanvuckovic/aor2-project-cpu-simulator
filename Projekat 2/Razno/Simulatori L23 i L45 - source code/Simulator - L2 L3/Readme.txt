Da bi se pokrenuto simulator racunarskog sistema potrebno je da na racunaru bude instalirana java virtuelna masina verzija 1.6 ili novije.
Pokretanje simulatora se postize pomocu ORSim.jar fajla koji je dat u prilogu. 
Ukoliko se prilikom pokretanja simulator ne pokrene potrebno je proveriti da li postoji instalirana java, i da li je java nalazi u sistemskoj promenljivi PATH. Ako se ne nalazi dodati putanju do java.exe programa u promenljivi PATH (http://docs.oracle.com/javase/tutorial/essential/environment/paths.html). 

U ovoj arhivi se nalaze sledeci fajlovi:
1. ORSim.jar - simulator racunarskog sistema
2. konfiguracija.txt - konfiguracioni fajl u kome se nalaze opisi Fetch2, Fetch3, Exec3, KMOPR1 i KMADR1 jedinica, kao i ostalih promenljivih
3. microProgram.txt - nalaze se komentari koje je potrebno ispisivati kada se izvrsava mikroinstrukcija na datoj adresi mikroprogramske memorije. Na osnovu ovog fajla se automatski kreira sadrzaj mikroprogramske memorije.
4. /src/images - direktorijum sa slikama
5. error.txt - izlazni fajl sa greskama prilikom podizanja simulatora. Ocekivani sadrzaj kada nema gresaka je 0000000000000000000000000200.
6. Code.txt - primer sadrzaja operativne memorije

Simulator je realizovana na osnovu resenja: Profesora Jovana Djordjevica <jdjordjevic@etf.bg.ac.rs>
Implementacija u programskom jeziku Java osnovne varijante resenja simulatora: Aleksandar Milic <mrevil1990@yahoo.com>
Transformacija simulatora u konfigurabilnu varijantu: Zaharije Radivojevic <zaki@etf.bg.ac.rs>

Greske u radu simulatora javiti na adresu: zaki@etf.bg.ac.rs