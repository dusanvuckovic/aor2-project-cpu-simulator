package rs.ac.bg.etf.aor2.simulator.asm;

import java.util.*;

public interface Instruction {
	public String getInstructionMnemonic();

	public void setInstructionMnemonic(String menmonic);

	public int calcOperandCount();

	public List<Operand> getOperands();

	public void setOperands(List<Operand> operands);

	public String getLabel();

	public void setLabel();

	public int getInstructionAddress();

	public void setInstructionAddress(int address);

	public String getComment();

	public void setComment(String comment);

	public String getInstructionPrefix();

	public void setInstructionPrefix(String menmonic);

	public int calcInstructionLength();

	public byte[] build();

	public List<String> extractSimbols();

	public void evaluatedSimbol(String simbol, int value);

}
