package rs.ac.bg.etf.aor2.simulator.asm;

import java.util.*;

public interface Operand {

	public String getAddressMode();

	public void setAddressMode(String mode);
	
	public int getOperandSize();
	
	public void setOperandSize(int size);

	public List<String> extractSimbols();

	public void evaluatedSimbol(String simbol, int value);

}
