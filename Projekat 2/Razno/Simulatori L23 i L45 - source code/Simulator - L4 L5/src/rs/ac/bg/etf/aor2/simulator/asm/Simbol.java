package rs.ac.bg.etf.aor2.simulator.asm;

public class Simbol extends Structure implements Comparable<Simbol> {
	
	protected Segment segment;

	public Simbol(String name, int address, boolean defined, Segment segment) {
		super(name, address, defined);
		this.segment = segment;
	}

	public Simbol(String name, int address, boolean defined) {
		this(name, address, defined, null);
	}

	public Simbol(String name, int address) {
		this(name, address, true);
	}

	public Simbol(String name) {
		this(name, 0, false);
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	@Override
	public int compareTo(Simbol o) {
		return name.compareToIgnoreCase(o.name);
	}

}
