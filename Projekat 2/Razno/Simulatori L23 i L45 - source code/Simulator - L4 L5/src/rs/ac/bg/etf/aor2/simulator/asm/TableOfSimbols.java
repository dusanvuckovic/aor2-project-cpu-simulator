package rs.ac.bg.etf.aor2.simulator.asm;

import java.util.*;

public class TableOfSimbols {
	Map<String, Simbol> simbols;

	public TableOfSimbols() {
		this.simbols = new HashMap<String, Simbol>();
	}

	public void add(String name, int address) {
		if (exist(name)) {
			Simbol simbol = simbols.get(name);
			simbol.setAddress(address);
			simbol.setDefined(true);
		} else {
			Simbol simbol = new Simbol(name, address);
			simbols.put(name, simbol);
		}
	}

	public void add(String name) {
		if (exist(name))
			return;
		Simbol simbol = new Simbol(name);
		simbols.put(name, simbol);
	}

	public boolean exist(String name) {
		return simbols.get(name) != null;
	}

	public boolean isDefined(String name) {
		Simbol simbol = simbols.get(name);
		return (simbol != null && simbol.isDefined());
	}

	public Simbol getSimbol(String name) {
		return simbols.get(name);
	}

	public List<Simbol> listUndefined() {
		List<Simbol> result = new LinkedList<Simbol>();
		for (Simbol simbol : simbols.values()) {
			if (!simbol.isDefined()) {
				result.add(simbol);
			}
		}
		return result;
	}
}
