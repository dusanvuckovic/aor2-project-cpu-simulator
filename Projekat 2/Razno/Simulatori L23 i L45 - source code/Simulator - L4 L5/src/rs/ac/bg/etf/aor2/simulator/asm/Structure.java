package rs.ac.bg.etf.aor2.simulator.asm;

public class Structure {

	protected String name;
	protected int address;
	protected boolean defined;

	public Structure(String name, int address, boolean defined) {
		super();
		this.name = name;
		this.address = address;
		this.defined = defined;
	}

	public Structure(String name, int address) {
		this(name, address, true);
	}

	public Structure(String name) {
		this(name, 0, false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAddress() {
		return address;
	}

	public void setAddress(int address) {
		this.address = address;
	}

	public boolean isDefined() {
		return defined;
	}

	public void setDefined(boolean defined) {
		this.defined = defined;
	}

}