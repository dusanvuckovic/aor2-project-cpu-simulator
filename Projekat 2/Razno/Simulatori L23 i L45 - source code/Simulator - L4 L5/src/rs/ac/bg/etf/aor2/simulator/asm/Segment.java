package rs.ac.bg.etf.aor2.simulator.asm;

public class Segment extends Structure {

	public Segment(String name, int address, boolean defined) {
		super(name, address, defined);
	}

	public Segment(String name, int address) {
		super(name, address, true);
	}

	public Segment(String name) {
		super(name, 0, false);
	}

}
