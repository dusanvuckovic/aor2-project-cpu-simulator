package rs.ac.bg.etf.aor2.simulator.asm;

import java.util.*;

public class TableOfSegments {
	Map<String, Segment> segments;

	public TableOfSegments() {
		this.segments = new HashMap<String, Segment>();
	}

	public void add(String name, int address) {
		if (exist(name)) {
			Structure segment = segments.get(name);
			segment.setAddress(address);
			segment.setDefined(true);
		} else {
			Segment segment = new Segment(name, address);
			segments.put(name, segment);
		}
	}

	public void add(String name) {
		if (exist(name))
			return;
		Segment segment = new Segment(name);
		segments.put(name, segment);
	}

	public boolean exist(String name) {
		return segments.get(name) != null;
	}

	public boolean isDefined(String name) {
		Structure segment = segments.get(name);
		return (segment != null && segment.isDefined());
	}

	public Structure getSegment(String name) {
		return segments.get(name);
	}

	public List<Segment> listUndefined() {
		List<Segment> result = new LinkedList<Segment>();
		for (Segment segment : segments.values()) {
			if (!segment.isDefined()) {
				result.add(segment);
			}
		}
		return result;
	}
}
