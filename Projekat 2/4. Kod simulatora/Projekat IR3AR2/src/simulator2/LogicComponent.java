package simulator2;

import java.io.Serializable;
import java.util.*;

public interface LogicComponent extends EventExecutable, Serializable, Loadable {

	public long getId();

	public void setId(long id);

	public String getName();

	public void setName(String name);

	public List<Port> getPorts();

	public void setPorts(List<Port> ports);

	public Port getPort(int index);

	public void setPort(int index, Port port);

	public LogicComponentSimple invertPort(int index);

	public Port findPort(String name);

	public List<Port> findPorts(int portType);

	public List<Port> findUnconnectedPorts(int type);

	public void setSignals(List<Signal> signals);

	public void setSignal(Signal signal, int index);

	public int getOutValue();

	public void setOutValue(int val);

	public int getMemValue(int i);

	public void setMemValue(int i, int x);

	public void resetStateCLK();
}
