package simulator2;

public interface Port extends Signal {
	public static final int IN = 1;
	public static final int OUT = 0;
	public static final int INOUT = 10;

	public static final int PORT = 6;

	public void setType(int type); // ! NOT SAFE !

	public int getType();

	public void setPortNumber(int number); // ! NOT SAFE !

	public int getPortNumber();

	public void setLogicComponent(LogicComponent component);

	public LogicComponent getLogicComponent();

}
