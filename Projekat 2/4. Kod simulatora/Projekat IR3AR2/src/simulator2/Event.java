
package simulator2;

public class Event implements Comparable<Event> {

	long time;
	Value value;
	EventExecutable dst; // destination component or signal
	EventExecutable src; // optional //source component or signal

	public Event(long time, Value value, EventExecutable dst) {
		super();

		this.time = time;
		this.value = value;
		this.dst = dst;
		this.src = null;
	}

	public Event(long time, Value value, EventExecutable dst, EventExecutable src) {
		super();

		this.time = time;
		this.value = value;
		this.dst = dst;
		this.src = src;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	public EventExecutable getDst() {
		return dst;
	}

	public void setDst(EventExecutable dst) {
		this.dst = dst;
	}

	public EventExecutable getSrc() {
		return src;
	}

	public void setSrc(EventExecutable src) {
		this.src = src;
	}

	@Override
	public int compareTo(Event t) {
		return time > t.time ? 1 : time < t.time ? -1 : dst.hashCode() - t.dst.hashCode();
	}

}
