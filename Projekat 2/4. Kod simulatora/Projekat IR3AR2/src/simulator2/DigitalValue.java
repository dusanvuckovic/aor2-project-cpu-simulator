package simulator2;

import java.util.*;

public class DigitalValue implements Value {

	private static final long serialVersionUID = 1L;

	public static final int ZERO = 0;
	public static final int ONE = 1;

	protected int value;
	protected int size = 1;
	protected boolean highZ = false; // If highZ is true the value is always set
										// to ZERO.

	// Some basic constructors:

	public DigitalValue(int value, int size, boolean highZ) {
		this.size = size;
		this.highZ = highZ;
		if (highZ)
			this.value = ZERO;
		else
			if (size != 32)
				this.value = value & ((0x1 << size) - 1);
			else
				this.value = value;
	}

	public DigitalValue(int value, int size) {
		if (size != 32)
			this.value = value & ((0x1 << size) - 1);
		else
			this.value = value;
		this.size = size;
	}

	public DigitalValue(int value) {
		this.value = value & 1;
	}

	public DigitalValue(Value value) {
		this.value = value.getIntValue();
		this.size = value.getSize();
		this.highZ = value.getHighZ();
	}

	public DigitalValue() {
		this.value = ZERO;
	}

	public DigitalValue(DigitalValue d) {
		this.value = d.value;
		this.size = d.size;
		this.highZ = d.highZ;
	}

	public DigitalValue(String val) {
		this();
		this.setStringValue(val);
	}

	@Override
	public void setIntValue(int value) {
		if (!highZ)
			if (size != 32)
				this.value = value & ((0x1 << size) - 1);
			else
				this.value = value;
	}

	@Override
	public int getIntValue() {
		return value;
	}

	@Override
	public void setLongValue(long value) {
		if (!highZ)
			if (size != 32)
				this.value = (int) value & ((0x1 << size) - 1);
			else
				this.value = (int) value;
	}

	@Override
	public long getLongValue() {
		return value;
	}

	@Override
	public void setBooleanValue(boolean val) {
		if (val && !highZ) {
			value = ONE;
		} else {
			value = ZERO;
		}
	}

	@Override
	public boolean getBooleanValue() {
		if (value == ZERO) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean getHighZ() {
		return highZ;
	}

	@Override
	public void setHighZ(boolean highZ) {
		this.highZ = highZ;
		if (highZ)
			value = ZERO;
	}

	@Override
	public void setStringValue(String val) {
		setIntValue(Integer.parseInt(val));
	}

	@Override
	public String getStringValue() {
		return Integer.toString(value);
	}

	@Override
	public void setSize(int size) {
		this.size = size;
		if (size != 32)
			value &= ((0x1 << size) - 1);
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public List<Value> separateValues() {
		List<Value> result = new LinkedList<Value>();
		result.add(this);
		return result;
	}

	@Override
	public Value copy() {
		DigitalValue result = new DigitalValue(value, size, highZ);
		return result;
	}

	@Override
	public boolean load(Value value) {
		this.highZ = value.getHighZ();
		if (this.highZ) {
			this.value = 0;
		} else {
			int newVal = value.getIntValue();
			if (this.size != 32)
				this.value = newVal & ((0x1 << size) - 1);
			else
				this.value = newVal;
		}
		return true;
	}

	@Override
	public int compareTo(Value val) {
		return this.getIntValue() - val.getIntValue();
	}

	@Override
	public boolean isEqual(Value val) { // Equality test.
		if (highZ && val.getHighZ()) {
			// System.out.println("equal on highZ");
			return true;
		}

		if (highZ || val.getHighZ()) {
			// System.out.println("unequal on highZ");
			return false;
		}

		boolean ret = (compareTo(val) == 0);
		if (ret) {
			// System.out.println("equal on value");
		} else {
			// System.out.println("unequal on value");
		}
		return ret;
	}

	@Override
	public String toString() {
		String temp = Integer.toString(value);
		if (temp != null)
			return temp;
		return "";
	}

}
