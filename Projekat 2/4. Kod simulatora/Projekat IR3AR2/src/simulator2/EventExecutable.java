package simulator2;

import java.util.*;

public interface EventExecutable {

	public List<Event> preCalculateEvents();

	public List<Event> execute(Event event); // The main method for all
												// executable components.

	public void resetState();

}
