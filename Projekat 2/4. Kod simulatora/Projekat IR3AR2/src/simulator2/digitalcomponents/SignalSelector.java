package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class SignalSelector extends LogicComponentSimple { // adapts signal size
															// from sizeIn to
															// sizeOut
	private static final long serialVersionUID = 1L;
	protected int sizeIn;
	protected int sizeOut;
	protected int boundUpper;
	protected int boundLower;

	public SignalSelector() {
		super();
	}

	public int getSizeIn() {
		return sizeIn;
	}

	public int getSizeOut() {
		return sizeOut;
	}

	public int getBoundUpper() {
		return boundUpper;
	}

	public int getBoundLower() {
		return boundLower;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		sizeIn = Integer.parseInt(data[1]);
		int bound1 = Integer.parseInt(data[2]);
		int bound2 = Integer.parseInt(data[3]);
		if (bound1 > bound2) {
			boundUpper = bound1;
			boundLower = bound2;
		} else {
			boundUpper = bound2;
			boundLower = bound1;
		}
		sizeOut = boundUpper - boundLower + 1;
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, sizeIn, new DigitalValue(DigitalValue.ZERO, sizeIn), "IN", 0, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, sizeOut, new DigitalValue(DigitalValue.ZERO, sizeOut), "OUT", 1, this);
		ports.add(port);
	}

	public Port getPortIN() {
		return ports.get(0);
	}

	public Port getPortOUT() {
		return ports.get(1);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + sizeIn, "" + sizeOut };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value inVal = ports.get(0).getValue();
		int newValue = inVal.getIntValue() & ((1 << (boundUpper + 1)) - 1);
		newValue >>>= boundLower;
		Value value = new DigitalValue(newValue, sizeOut);

		Port out = ports.get(1);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

}
