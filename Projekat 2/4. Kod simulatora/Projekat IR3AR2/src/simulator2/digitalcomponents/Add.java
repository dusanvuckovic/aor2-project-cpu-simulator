package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class Add extends LogicComponentSimple { // the add circuit with a size

	private static final long serialVersionUID = 1L;

	protected int size;

	public Add() {
		super();
	}

	public int getSize() {
		return size;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "A", 0, this);
		ports.add(port);

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "B", 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO, size), "OUT", 2, this);
		ports.add(port);
	}

	public Port getPortA() {
		return ports.get(0);
	}

	public Port getPortB() {
		return ports.get(1);
	}

	public Port getPortOUT() {
		return ports.get(2);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		int sum = ports.get(0).getIntValue() + ports.get(1).getIntValue();
	
		Port out = ports.get(2);
		Value value = new DigitalValue(sum, size);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);
		return result;
	}

}
