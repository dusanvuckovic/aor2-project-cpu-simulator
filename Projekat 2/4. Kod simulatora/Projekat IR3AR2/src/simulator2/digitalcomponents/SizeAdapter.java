package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class SizeAdapter extends LogicComponentSimple { // adapts signal size
														// from sizeIn to
														// sizeOut
	private static final long serialVersionUID = 1L;
	protected int sizeIn;
	protected int sizeOut;

	public SizeAdapter() {
		super();
	}

	public int getSizeIn() {
		return sizeIn;
	}

	public int getSizeOut() {
		return sizeOut;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		sizeIn = Integer.parseInt(data[1]);
		sizeOut = Integer.parseInt(data[2]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, sizeIn, new DigitalValue(DigitalValue.ZERO, sizeIn), "IN", 0, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, sizeOut, new DigitalValue(DigitalValue.ZERO, sizeOut), "OUT", 1, this);
		ports.add(port);
	}

	public Port getPortIN() {
		return ports.get(0);
	}

	public Port getPortOUT() {
		return ports.get(1);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + sizeIn, "" + sizeOut };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value inVal = ports.get(0).getValue();
		Value value = new DigitalValue(inVal.getIntValue(), sizeOut, inVal.getHighZ());

		Port out = ports.get(1);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

}
