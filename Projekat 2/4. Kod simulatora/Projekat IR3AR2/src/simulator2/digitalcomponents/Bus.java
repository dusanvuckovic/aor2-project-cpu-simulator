package simulator2.digitalcomponents;

import java.util.ArrayList;
import java.util.List;

import simulator2.*;

public class Bus extends SignalSimple {

	private static final long serialVersionUID = 1L;

	protected boolean generated = false;
	protected EventExecutable source = null;
	protected EventExecutable prevSource = null;

	public Bus(String name, int size) {
		super(name, size);
	}

	public Bus(String name) {
		super(name, 32);
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();
		EventExecutable src = event.getSrc();

		if (src == prevSource && event.getValue().getHighZ()) {
	
			if (source == null || source == prevSource) {
				Event tempEvent;
				

				value.setHighZ(true);
				for (Signal signal : connectedSignals) {
					if (signal != src) {
						tempEvent = new Event(event.getTime(), value, signal, this);
						result.add(tempEvent);
					}
				}
			}
		} else if (generated && src != source) {
			// TODO: ERROR, BUS BURNED!
		} else {
			generated = true;
			source = src;

			Value newVal;
			Event tempEvent;

			newVal = new DigitalValue(event.getValue());
			if (!value.equals(newVal)) {
				value.load(newVal);
				for (Signal signal : connectedSignals) {
					if (signal != src) {
						tempEvent = new Event(event.getTime(), value, signal, this);
						result.add(tempEvent);
					}
				}
			}
		}

		return result;
	}

	@Override
	public void resetState() {
		value.setIntValue(DigitalValue.ZERO);
		value.setHighZ(false);
		valueList = new ArrayList<Value>();
		generated = false;
		source = null;
	}

	public void resetBus() {
		generated = false;
		if (source != null)
			prevSource = source;
		source = null;
	}

}
