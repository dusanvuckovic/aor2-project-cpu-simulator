package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class Coder extends LogicComponentSimple { // a coder without an enable
													// signal

	private static final long serialVersionUID = 1L;
	protected int numOutputs;

	public int getNumInputs() {
		return 1 << numOutputs;
	}

	public int getNumOutputs() {
		return numOutputs;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		numOutputs = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;
		int i, j;

		for (i = 0; i < 1 << numOutputs; i++) {
			port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "IN" + i, i, this);
			ports.add(port);
		}

		for (j = 0; j < numOutputs; j++) {
			port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "OUT" + j, i, this);
			ports.add(port);
			i++;
		}
	}

	public Port getPortIN(int i) {
		if (i < 1 << numOutputs)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortOUT(int i) {
		if (i < numOutputs)
			return ports.get((1 << numOutputs) + i);
		else
			return null;
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + numOutputs };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		int select = 0;
		for (int i = (1 << numOutputs) - 1; i >= 0; i--) { // find the active
															// signal with the
															// highest priority
			if (ports.get(i).getValue().getIntValue() == 1) {
				select = i;
				break;
			}
		}

		List<Event> result = events;
		result.clear();
		int newVal;
		Port out;
		Value value;
		int addition = 1 << numOutputs;
		for (int i = 0; i < numOutputs; i++) {
			newVal = select & 1;
			select >>>= 1;
			out = ports.get(i + addition);
			value = new DigitalValue(newVal);
			Event tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
		}

		return result;
	}

}
