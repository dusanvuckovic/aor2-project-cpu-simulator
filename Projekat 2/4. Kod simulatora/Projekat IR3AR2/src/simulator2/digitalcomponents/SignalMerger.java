package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class SignalMerger extends LogicComponentSimple { // merges a given
															// number of 1-bit
															// signals into a
															// wider one

	private static final long serialVersionUID = 1L;

	protected int number;
	protected int size;

	public SignalMerger() {
		super();
	}

	public int getNumber() {
		return number;
	}

	public int getSize() {
		return size;
	}

	public void setSignals(List<Signal> inputs, Signal output) {
		int i;

		for (i = 0; i < size; i++) {
			ports.get(i).connectBoth(inputs.get(i)); // actual wiring
		}

		ports.get(i).connectBoth(output);
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		number = Integer.parseInt(data[2]);
		ports = new LinkedList<Port>();
		Port port;

		for (int i = 0; i < number; i++) {
			port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "IN" + i, i, this);
			ports.add(port);
		}

		port = new PortSimple(Port.OUT, size * number, new DigitalValue(DigitalValue.ZERO, size * number), "OUT",
				number, this);
		ports.add(port);
	}

	public Port getPortIN(int i) {
		if (i < number)
			return ports.get(i);
		else {
			System.out.println(name);
			System.out.println("OVDE ZLO I NAOPAKO!");
			return null;
		}
	}

	public Port getPortOUT() {
		return ports.get(number);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + "" + number + "" + size };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		int newVal = 0;
		boolean high = false;
		Value inVal;

		for (int i = number - 1; i >= 0; i--) { // calculates the merged value
			inVal = ports.get(i).getValue();
			newVal <<= size;
			newVal += inVal.getIntValue();
		}

		Value value = new DigitalValue(newVal, size * number);
		value.setHighZ(high);
		Port out = ports.get(number);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

}
