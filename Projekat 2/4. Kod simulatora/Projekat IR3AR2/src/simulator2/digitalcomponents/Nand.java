package simulator2.digitalcomponents;

import simulator2.*;

public class Nand extends OneOutputGate { // the nand circuit

	private static final long serialVersionUID = 1L;

	public Nand() {
		super();
	}

	@Override
	protected Value calculate() {
		int result = 0xffffffff;

		for (int i = 0; i < numInputs; i++) {
			Port port = ports.get(i);

			if (port.getHighZ())
				return new DigitalValue(DigitalValue.ZERO, size, true);

			result = result & port.getValue().getIntValue();
		}

		return new DigitalValue(~result, size);
	}

}
