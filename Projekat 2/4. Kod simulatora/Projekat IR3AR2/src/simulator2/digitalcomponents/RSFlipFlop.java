package simulator2.digitalcomponents;

import java.util.List;

import simulator2.*;

public class RSFlipFlop extends LogicComponentSequential { // the RS flip-flop

	private static final long serialVersionUID = 1L;

	public RSFlipFlop() {
		super();
	}

	@Override
	public void init(String[] data) {
		super.init(data);
		Port port;

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "S", 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "R", 2, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "Q", 3, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ONE), "QNEG", 4, this);
		ports.add(port);
	}

	public Port getPortS() {
		return ports.get(1);
	}

	public Port getPortR() {
		return ports.get(2);
	}

	public Port getPortQ() {
		return ports.get(3);
	}

	public Port getPortQNEG() {
		return ports.get(4);
	}

	@Override
	public List<Event> setState(Event event) {
		List<Event> result = events;
		result.clear();

		int newVal = event.getValue().getIntValue();
		long time = event.getTime();
		Value value1 = new DigitalValue(newVal & 1);
		Value value2 = new DigitalValue(~(newVal & 1));
		Event tempEvent;

		tempEvent = new Event(time, value1, ports.get(3), this);
		result.add(tempEvent);
		tempEvent = new Event(time, value2, ports.get(4), this);
		result.add(tempEvent);

		return result;
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value value1 = new DigitalValue(DigitalValue.ZERO), value2 = new DigitalValue(DigitalValue.ONE);
		Port out;
		Event tempEvent;
		boolean toChange = false;
		long time = event.getTime();

		if (event.getSrc() == ports.get(0) && ports.get(0).getValue().getIntValue() != 0) { // CLK is active
			if (ports.get(1).getValue().getIntValue() == 1 && ports.get(2).getValue().getIntValue() == 1) {
				System.out.println("RS " + name + " S n R!!!");
			}

			if (ports.get(1).getValue().getIntValue() == 1) { // S is active
				value1 = new DigitalValue(DigitalValue.ONE);
				value2 = new DigitalValue(DigitalValue.ZERO);
				toChange = true;
			} else if (ports.get(2).getValue().getIntValue() == 1) { // R is
																		// active
				toChange = true;
			}

			if (toChange) { // need to change the values
				out = ports.get(3);
				tempEvent = new Event(time, value1, out, this); // time + 1
				result.add(tempEvent);
				out = ports.get(4);
				tempEvent = new Event(time, value2, out, this); // time + 1
				result.add(tempEvent);
			}
		}

		return result;
	}
}
