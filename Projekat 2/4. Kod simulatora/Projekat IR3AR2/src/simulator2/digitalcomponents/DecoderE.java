package simulator2.digitalcomponents;

import java.util.List;

import simulator2.*;

public class DecoderE extends Decoder { // a decoder with an enable signal

	private static final long serialVersionUID = 1L;

	public void setSignals(List<Signal> inputs, List<Port> outputs, Signal E) {
		setSignals(inputs, outputs);

		ports.get(numInputs + 1 << numInputs).connectBoth(E);
	}

	@Override
	public void init(String[] data) {
		super.init(data);

		Port port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "E", numInputs + (1 << numInputs),
				this);
		ports.add(port);
	}

	public Port getPortE() {
		return ports.get(numInputs + (1 << numInputs));
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();
		Port out;
		Value value;
		int select;

		if (getPortE().getIntValue() == 0) { // enable signal is zero, no out
												// port should be active
			select = 1 << numInputs;
		} else {
			select = ports.get(numInputs - 1).getValue().getIntValue();

			for (int i = numInputs - 2; i >= 0; i--) { // compute the input
														// value
				select <<= 1;
				select += ports.get(i).getValue().getIntValue();
			}
		}


		for (int i = 0; i < 1 << numInputs; i++) {
			value = new DigitalValue(DigitalValue.ZERO);
			if (i == select) {
				value.setIntValue(DigitalValue.ONE);
			}
			out = ports.get(numInputs + i);
			Event tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
		}

		return result;
	}

}
