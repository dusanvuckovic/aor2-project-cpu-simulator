package simulator2.digitalcomponents;

import simulator2.*;

public class Or extends OneOutputGate { // the or circuit

	private static final long serialVersionUID = 1L;
	public final int orId;
	private static int orIdLast = 0;

	public Or() {
		super();
		orId = orIdLast++;
	}

	@Override
	protected Value calculate() {
		int result = 0x00000000;

		for (int i = 0; i < numInputs; i++) {
			Port port = ports.get(i);

			if (port.getHighZ())
				return new DigitalValue(DigitalValue.ZERO, size, true);

			result = result | port.getValue().getIntValue();
		}

		return new DigitalValue(result, size);
	}

}
