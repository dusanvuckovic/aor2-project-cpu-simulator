package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;
import simulator2.dynamicinit.Microword;

public class MicroMemory extends LogicComponentSimple {

	private static final long serialVersionUID = 1L;

	protected int wordSize;
	protected int adrSize;
	protected Integer memory0[];
	protected Integer memory1[];
	protected Integer memory2[];
	protected Integer memory3[];

	public int getWordSize() {
		return wordSize;
	}

	public int getAdrSize() {
		return adrSize;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		wordSize = Integer.parseInt(data[1]);
		adrSize = Integer.parseInt(data[2]);
		ports = new LinkedList<Port>();
		memory0 = new Integer[1 << adrSize];
		memory1 = new Integer[1 << adrSize];
		memory2 = new Integer[1 << adrSize];
		memory3 = new Integer[1 << adrSize];
		Port port;
		int i, j;

		for (i = 0; i < 1 << adrSize; i++) {
			memory0[i] = 0;
			memory1[i] = 0;
			memory2[i] = 0;
			memory3[i] = 0;
		}

		for (i = 0; i < 4; i++) {
			port = new PortSimple(Port.IN, 32, new DigitalValue(DigitalValue.ZERO, 32), "IN" + i, i, this);
			ports.add(port);
		}

		for (j = 0; j < 4; j++, i++) {
			port = new PortSimple(Port.OUT, 32, new DigitalValue(DigitalValue.ZERO, 32), "OUT" + j, i, this);
			ports.add(port);
		}

		port = new PortSimple(Port.IN, adrSize, new DigitalValue(DigitalValue.ZERO, adrSize), "ADR", i++, this);
		ports.add(port);

		port = new PortSimple(Port.IN, 1, new DigitalValue(DigitalValue.ZERO), "RD", i++, this);
		ports.add(port);

		port = new PortSimple(Port.IN, 1, new DigitalValue(DigitalValue.ZERO), "WR", i, this);
		ports.add(port);
	}

	public void fillMicroMemory(List<Microword> microwords) {
		int size = microwords.size();
		Integer[] helper;
		Microword microword;

		for (int i = 0; i < size; i++) {
			microword = microwords.get(i);
			helper = microword.getIntegerData();
			memory0[i] = helper[0];
			memory1[i] = helper[1];
			memory2[i] = helper[2];
			memory3[i] = helper[3];
		}
	}

	public Port getPortIN(int i) {
		if (i < 4)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortOUT(int i) {
		if (i < 4)
			return ports.get(4 + i);
		else
			return null;
	}

	public Port getPortADR() {
		return ports.get(8);
	}

	public Port getPortRD() {
		return ports.get(9);
	}

	public Port getPortWR() {
		return ports.get(10);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + wordSize + "" + adrSize };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();
		if (ports.get(10).getIntValue() == 1) { // WR is active
			int adr = ports.get(8).getIntValue();
			Value value;

			value = ports.get(0).getValue();
			memory0[adr] = value.getIntValue();
			value = ports.get(1).getValue();
			memory1[adr] = value.getIntValue();
			value = ports.get(2).getValue();
			memory2[adr] = value.getIntValue();
			value = ports.get(3).getValue();
			memory3[adr] = value.getIntValue();
		} else if (ports.get(9).getIntValue() == 1) { // RD is active
			int adr = ports.get(8).getIntValue();
			Port out;
			Value value;
			Event tempEvent;

			out = ports.get(4);
			value = new DigitalValue(memory0[adr], 32);
			tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
			out = ports.get(5);
			value = new DigitalValue(memory1[adr], 32);
			tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
			out = ports.get(6);
			value = new DigitalValue(memory2[adr], 32);
			tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
			out = ports.get(7);
			value = new DigitalValue(memory3[adr], 32);
			tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
		}

		return result;
	}

}
