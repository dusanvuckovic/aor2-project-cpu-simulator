package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class Comparator extends LogicComponentSimple { // a comparator circuit
														// with a number of
														// inputs

	private static final long serialVersionUID = 1L;
	protected int numInputs;

	public Comparator() {
		super();
	}

	public int getNumInputs() {
		return numInputs;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		numInputs = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;
		int i, j;

		for (i = 0; i < numInputs; i++) {
			port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "A" + i, i, this);
			ports.add(port);
		}

		for (j = 0; j < numInputs; j++) {
			port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "B" + j, i, this);
			ports.add(port);
			i++;
		}

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "EQ", 2 * numInputs, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "GR", 2 * numInputs + 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "LE", 2 * numInputs + 2, this);
		ports.add(port);
	}

	public Port getPortA(int i) {
		if (i < numInputs)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortB(int i) {
		if (i < numInputs)
			return ports.get(numInputs + i);
		else
			return null;
	}

	public Port getPortEQ() {
		return ports.get(2 * numInputs);
	}

	public Port getPortGR() {
		return ports.get(2 * numInputs + 1);
	}

	public Port getPortLE() {
		return ports.get(2 * numInputs + 2);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + numInputs };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		int compare = 0;
		int valA, valB;
		for (int i = numInputs - 1; i >= 0; i--) { // compare the signals
			valA = ports.get(i).getIntValue();
			valB = ports.get(numInputs + i).getIntValue();
			if (valA > valB) {
				compare = 1;
				break;
			}
			if (valA < valB) {
				compare = -1;
				break;
			}
		}

		Port out, out1, out2;
		out = ports.get(2 * numInputs + 0);
		out1 = ports.get(2 * numInputs + 1);
		out2 = ports.get(2 * numInputs + 2);

		switch (compare) { // prepare out ports
		case 1:
			out = ports.get(2 * numInputs + 1);
			out1 = ports.get(2 * numInputs + 0);
			out2 = ports.get(2 * numInputs + 2);
			break;
		case -1:
			out = ports.get(2 * numInputs + 2);
			out1 = ports.get(2 * numInputs + 0);
			out2 = ports.get(2 * numInputs + 1);
			break;
		}

		Value value = new DigitalValue(DigitalValue.ONE);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		value = new DigitalValue(DigitalValue.ZERO);
		tempEvent = new Event(event.getTime(), value, out1, this);
		result.add(tempEvent);

		value = new DigitalValue(DigitalValue.ZERO);
		tempEvent = new Event(event.getTime(), value, out2, this);
		result.add(tempEvent);

		return result;
	}

}
