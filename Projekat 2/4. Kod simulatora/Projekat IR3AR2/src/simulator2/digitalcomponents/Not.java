package simulator2.digitalcomponents;

import java.util.LinkedList;

import simulator2.*;

public class Not extends OneOutputGate { // the not circuit

	private static final long serialVersionUID = 1L;

	public Not() {
		super();
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		numInputs = 1;
		size = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "IN", 0, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ONE, size), "OUT", 1, this);
		ports.add(port);
	}

	public Port getPortIN() {
		return ports.get(0);
	}

	@Override
	public Port getPortIN(int i) {
		return ports.get(0);
	}

	@Override
	public Port getPortOUT() {
		return ports.get(1);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id };
		return state;
	}

	@Override
	protected Value calculate() {

		Port port = ports.get(0);

		if (port.getHighZ()) {
			return new DigitalValue(DigitalValue.ZERO, size);
		}

		int result = port.getIntValue();

		return new DigitalValue(~result, size);
	}

}
