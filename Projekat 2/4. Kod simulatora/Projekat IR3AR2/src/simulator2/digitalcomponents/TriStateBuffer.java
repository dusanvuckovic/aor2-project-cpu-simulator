package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class TriStateBuffer extends LogicComponentSimple { // the tri-state
															// buffer with a
															// size

	private static final long serialVersionUID = 1L;
	protected int size;

	public TriStateBuffer() {
		super();
	}

	public int getSize() {
		return size;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "C", 0, this); // control
																							// warrior
		ports.add(port);

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "IN", 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO, size, true), "OUT", 2, this);
		ports.add(port);
	}

	public Port getPortC() {
		return ports.get(0);
	}

	public Port getPortIN() {
		return ports.get(1);
	}

	public Port getPortOUT() {
		return ports.get(2);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value newVal = event.getValue();
		Port out = ports.get(2);

		Value value = new DigitalValue(DigitalValue.ZERO, size);
		int newIntVal = ports.get(1).getIntValue(); // , mask = 0x1 << (newVal.getSize() - 1);
		
		value.setHighZ(newVal.getHighZ());
		value.setIntValue(newIntVal);

		if (ports.get(0).getValue().getIntValue() == 0) { // C is not active, send a highZ signal
			value.setHighZ(true);
		}

		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}
}
