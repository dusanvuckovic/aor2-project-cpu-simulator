package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class SignalSplitterSimple extends LogicComponentSimple { // splits a
																	// signal
																	// with a
																	// given
																	// size to
																	// 1-bit
																	// signals

	private static final long serialVersionUID = 1L;
	protected int number;

	public SignalSplitterSimple() {
		super();
	}

	public int getSize() {
		return number;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		number = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, number, new DigitalValue(DigitalValue.ZERO, number), "IN", number, this);
		ports.add(port);

		for (int i = 0; i < number; i++) {
			port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "OUT" + i, i + 1, this);
			ports.add(port);
		}
	}

	public Port getPortIN() {
		return ports.get(0);
	}

	public Port getPortOUT(int i) {
		if (i < number)
			return ports.get(1 + i);
		else
			return null;
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + number };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		int newVal;
		Value inVal = ports.get(0).getValue();
		Value value;
		Port out;
		Event tempEvent;

		int in = inVal.getIntValue();

		for (int i = 0; i < number; i++) { // sends a value to each out port
			newVal = in & 1;
			in >>= 1;
			value = new DigitalValue(newVal);
			out = ports.get(i + 1);
			tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
		}

		return result;
	}

}
