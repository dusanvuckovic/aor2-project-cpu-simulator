package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class Decrementer extends LogicComponentSimple {

	private static final long serialVersionUID = 1L;

	protected int size;
	protected int decrement;

	public Decrementer() {
		super();
	}

	public int getSize() {
		return size;
	}

	public int getDecrement() {
		return decrement;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		decrement = Integer.parseInt(data[2]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "IN", 0, this);
		ports.add(port);

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "OUT", 1, this);
		ports.add(port);
	}

	public Port getPortIN() {
		return ports.get(0);
	}

	public Port getPortOUT() {
		return ports.get(1);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size, "" + decrement };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Port out = ports.get(1);
		Value value = new DigitalValue(ports.get(0).getIntValue() - decrement, size);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);
		return result;
	}

}
