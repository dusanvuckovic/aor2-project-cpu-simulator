package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class SignalMergerSimple extends LogicComponentSimple { // merges a given
																// number of
																// 1-bit signals
																// into a wider
																// one

	private static final long serialVersionUID = 1L;
	protected int number;

	public SignalMergerSimple() {
		super();
	}

	public int getSize() {
		return number;
	}

	public void setSignals(List<Signal> inputs, Signal output) {
		int i;

		for (i = 0; i < number; i++) {
			ports.get(i).connectBoth(inputs.get(i)); // actual wiring
		}

		ports.get(i).connectBoth(output);
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		number = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		for (int i = 0; i < number; i++) {
			port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "IN" + i, i, this);
			ports.add(port);
		}

		port = new PortSimple(Port.OUT, number, new DigitalValue(DigitalValue.ZERO, number), "OUT", number, this);
		ports.add(port);
	}

	public Port getPortIN(int i) {
		if (i < number)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortOUT() {
		return ports.get(number);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + number };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		int newVal = 0;
		boolean high = false;
		Value inVal;

		for (int i = 0; i < number; i++) { // calculates the merged value
			inVal = ports.get(i).getValue();
			newVal += inVal.getIntValue();
			newVal <<= 1;
		}

		Value value = new DigitalValue(newVal, number);
		value.setHighZ(high);
		Port out = ports.get(number);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

}
