package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class ComparatorWide extends LogicComponentSimple { // a comparator
															// circuit with a
															// number of inputs

	private static final long serialVersionUID = 1L;
	protected int size;

	public ComparatorWide() {
		super();
	}

	public int getNumInputs() {
		return size;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "A", 0, this);
		ports.add(port);

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "B", 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "EQ", 2, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "GR", 3, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "LE", 4, this);
		ports.add(port);
	}

	public Port getPortA() {
		return ports.get(0);
	}

	public Port getPortB() {
		return ports.get(1);
	}

	public Port getPortEQ() {
		return ports.get(2);
	}

	public Port getPortGR() {
		return ports.get(3);
	}

	public Port getPortLE() {
		return ports.get(4);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		int compare = 0;
		int valA = ports.get(0).getIntValue();
		int valB = ports.get(1).getIntValue();
		if (valA > valB) {
			compare = 1;
		} else if (valA < valB) {
			compare = -1;
		}

		Port out, out1, out2;
		out = ports.get(2);
		out1 = ports.get(3);
		out2 = ports.get(4);

		switch (compare) { // prepare out ports
		case 1:
			out = ports.get(3);
			out1 = ports.get(2);
			out2 = ports.get(4);
			break;
		case -1:
			out = ports.get(4);
			out1 = ports.get(2);
			out2 = ports.get(3);
			break;
		}

		Value value = new DigitalValue(DigitalValue.ONE, size);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		value = new DigitalValue(DigitalValue.ZERO, size);
		tempEvent = new Event(event.getTime(), value, out1, this);
		result.add(tempEvent);

		value = new DigitalValue(DigitalValue.ZERO, size);
		tempEvent = new Event(event.getTime(), value, out2, this);
		result.add(tempEvent);

		return result;
	}

}
