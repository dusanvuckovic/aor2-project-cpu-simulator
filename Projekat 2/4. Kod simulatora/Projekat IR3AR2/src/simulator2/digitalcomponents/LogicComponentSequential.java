package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.DigitalValue;
import simulator2.Event;
import simulator2.LogicComponentSimple;
import simulator2.Port;
import simulator2.PortSimple;

public abstract class LogicComponentSequential extends LogicComponentSimple {

	private static final long serialVersionUID = 1L;

	public LogicComponentSequential() {
		super();
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "CLK", 0, this);
		ports.add(port);
	}

	public Port getPortCLK() {
		return ports.get(0);
	}

	public List<Event> setState(Event event) {
		List<Event> result = events;
		result.clear();

		return result;
	}

}
