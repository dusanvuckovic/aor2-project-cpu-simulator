package simulator2.digitalcomponents;

import java.util.*;

import simulator2.*;

public class OneOutputGate extends LogicComponentSimple { // the abstraction for
															// all the simple
															// logic circuits

	private static final long serialVersionUID = 1L;
	protected int numInputs;
	protected int size;

	public OneOutputGate() {
		super();
	}

	public int getNumInputs() {
		return numInputs;
	}

	public int getSize() {
		return size;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		numInputs = Integer.parseInt(data[1]);
		size = Integer.parseInt(data[2]);
		ports = new LinkedList<Port>();
		Port port;

		for (int i = 0; i < numInputs; i++) {
			port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO), "IN" + i, i, this);
			ports.add(port);
		}

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO), "OUT", numInputs, this);
		ports.add(port);

	}

	public Port getPortIN(int i) {
		if (i < numInputs)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortOUT() {
		return ports.get(numInputs);
	}

	public List<Port> getInPorts() {
		List<Port> inPorts = new LinkedList<Port>();

		for (int i = 0; i < numInputs; i++) {
			inPorts.add(ports.get(i));
		}

		return inPorts;
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + numInputs };
		return state;
	}

	protected Value calculate() { // the function of the specific circuit
		return new DigitalValue(DigitalValue.ZERO, size);
	}

	@Override
	public List<Event> execute(Event event) {
		Value value = calculate();

		List<Event> result = events;
		result.clear();

		Port out = ports.get(numInputs);

		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

}
