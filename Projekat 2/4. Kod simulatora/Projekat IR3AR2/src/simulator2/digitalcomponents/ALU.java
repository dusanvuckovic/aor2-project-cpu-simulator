package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class ALU extends LogicComponentSimple { // the ALU circuit with a size

	private static final long serialVersionUID = 1L;
	private static final int NUMINPUTS = 12;
	protected int size;

	public ALU() {
		super();
	}

	public int getSize() {
		return size;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "A", 0, this);
		ports.add(port);

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "B", 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO, size), "OUT", 2, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "C0", 3, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "CMAX", 4, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "ADD", 5, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "SUB", 6, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "NOT", 7, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "OR", 8, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "AND", 9, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "SHL", 10, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "SHR", 11, this);
		ports.add(port);
	}

	public Port getPortA() {
		return ports.get(0);
	}

	public Port getPortB() {
		return ports.get(1);
	}

	public Port getPortOUT() {
		return ports.get(2);
	}

	public Port getPortC0() {
		return ports.get(3);
	}

	public Port getPortCMAX() {
		return ports.get(4);
	}

	public Port getPortADD() {
		return ports.get(5);
	}

	public Port getPortSUB() {
		return ports.get(6);
	}

	public Port getPortNOT() {
		return ports.get(7);
	}

	public Port getPortOR() {
		return ports.get(8);
	}

	public Port getPortAND() {
		return ports.get(9);
	}

	public Port getPortSHL() {
		return ports.get(10);
	}

	public Port getPortSHR() {
		return ports.get(11);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size };
		return state;
	}

	@Override
	public void setSignals(List<Signal> signals) {
		int min = (signals.size() < NUMINPUTS) ? signals.size() : NUMINPUTS;
		for (int i = 0; i < min; i++) {
			ports.get(i).connect(signals.get(i));
			signals.get(i).connect(ports.get(i));
		}
	}

	@Override
	public List<Event> execute(Event event) {
		int in1 = ports.get(0).getValue().getIntValue();
		int in2 = ports.get(1).getValue().getIntValue();

		int sum = in1; // default
		int c0 = 0, cmax = 0;

		if (ports.get(5).getValue().getBooleanValue()) { // add
			long add = (long) in1 + (long) in2;
			sum = (int) add;
			add >>= size;
			cmax = (int)add & 1;
		} else if (ports.get(6).getValue().getBooleanValue()) { // sub
			int in2inv = in2;
			in2inv = (-in2inv);  // finally getting the negative value of the second operand
			long add = (long) in1 + (long) in2inv;
			sum = (int) add;
			add >>= size;
			cmax = (int) add & 1;
			
		} else if (ports.get(7).getValue().getBooleanValue()) { // not
			sum = ~in1;
		} else if (ports.get(8).getValue().getBooleanValue()) { // or
			sum = in1 | in2;
		} else if (ports.get(9).getValue().getBooleanValue()) { // and
			sum = in1 & in2;
		} else if (ports.get(10).getValue().getBooleanValue()) { // shl
			int high = (in1 >> (size - 1)) & 1;
			sum = in1 << 1;
			cmax = high;
		} else if (ports.get(11).getValue().getBooleanValue()) { // shr
			int low = in1 & 1;
			sum = in1 >> 1;
			c0 = low;
		}

		List<Event> result = events;
		result.clear();

		Port out = ports.get(2);
		Value value = new DigitalValue(sum, size);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		out = ports.get(3);
		value = new DigitalValue(c0);
		tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		out = ports.get(4);
		value = new DigitalValue(cmax);
		tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

}
