package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class Multiplexer extends LogicComponentSimple { // a multiplexer with a
														// size and a number of
														// control signals

	private static final long serialVersionUID = 1L;
	protected int size;
	protected int numInputs;

	public Multiplexer() {
		super();
	}

	public int getSize() {
		return size;
	}

	public int getNumInputs() {
		return numInputs;
	}

	public int getNumSignals() {
		return 1 << numInputs;
	}

	public void setSignals(List<Signal> controls, List<Signal> inputs, Signal output) {
		int i, j;

		for (i = 0; i < numInputs; i++) {
			ports.get(i).connectBoth(controls.get(i));
		}

		for (j = 0; j < 1 << numInputs; j++, i++) {
			ports.get(i).connectBoth(inputs.get(j)); // actual wiring
		}

		ports.get(i).connectBoth(output);
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		numInputs = Integer.parseInt(data[2]);
		ports = new LinkedList<Port>();
		Port port;
		int i, j;

		for (i = 0; i < numInputs; i++) {
			port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "S" + i, i, this);
			ports.add(port);
		}

		for (j = 0; j < 1 << numInputs; j++) {
			port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "IN" + j, i, this);
			ports.add(port);
			i++;
		}

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO, size), "OUT", i, this);
		ports.add(port);
	}

	public Port getPortS(int i) {
		if (i < numInputs)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortIN(int i) {
		if (i < 1 << numInputs)
			return ports.get(numInputs + i);
		else
			return null;
	}

	public Port getPortOUT() {
		return ports.get(numInputs + (1 << numInputs));
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size, "" + numInputs };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		int select = ports.get(numInputs - 1).getValue().getIntValue();
		for (int i = numInputs - 2; i >= 0; i--) { // find the selected in port
			select <<= 1;
			select += ports.get(i).getValue().getIntValue();
		}

		int newVal = ports.get(numInputs + select).getValue().getIntValue();

		List<Event> result = events;
		result.clear();
		Port out = ports.get(numInputs + (1 << numInputs));

		Value value = new DigitalValue(newVal, size);
		Event tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);
		return result;
	}
}
