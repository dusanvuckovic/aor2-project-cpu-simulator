package simulator2.digitalcomponents;

import java.util.LinkedList;

import simulator2.DigitalValue;
import simulator2.Port;
import simulator2.PortSimple;

public class IncrementerSimple extends Incrementer {

	private static final long serialVersionUID = 1L;

	public IncrementerSimple() {
		super();
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		size = Integer.parseInt(data[1]);
		increment = 1;
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "IN", 0, this);
		ports.add(port);

		port = new PortSimple(Port.IN, size, new DigitalValue(DigitalValue.ZERO, size), "OUT", 1, this);
		ports.add(port);
	}

}
