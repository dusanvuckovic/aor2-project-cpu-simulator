package simulator2.digitalcomponents;

import java.util.LinkedList;
import java.util.List;

import simulator2.*;

public class Decoder extends LogicComponentSimple { // a decoder without an
													// enable signal

	private static final long serialVersionUID = 1L;
	protected int numInputs;

	public int getNumInputs() {
		return numInputs;
	}

	public int getNumOutputs() {
		return 1 << numInputs;
	}

	public void setSignals(List<Signal> inputs, List<Port> outputs) {
		int i, j;

		for (i = 0; i < numInputs; i++) {
			ports.get(i).connectBoth(inputs.get(i));
		}

		for (j = 0; j < 1 << numInputs; j++, i++) {
			ports.get(i).connectBoth(outputs.get(j));
		}

	}

	@Override
	public void init(String[] data) {
		name = data[0];
		numInputs = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;
		int i, j;

		for (i = 0; i < numInputs; i++) {
			port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "IN" + i, i, this);
			ports.add(port);
		}

		for (j = 0; j < 1 << numInputs; j++, i++) {
			port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "OUT" + j, i, this);
			ports.add(port);
		}
	}

	public Port getPortIN(int i) {
		if (i < numInputs)
			return ports.get(i);
		else
			return null;
	}

	public Port getPortOUT(int i) {
		if (i < 1 << numInputs)
			return ports.get(numInputs + i);
		else
			return null;
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + numInputs };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();
		Port out;
		Value value;
		int select = ports.get(numInputs - 1).getValue().getIntValue();

		for (int i = numInputs - 2; i >= 0; i--) { // compute the input value
			select <<= 1;
			select += ports.get(i).getValue().getIntValue();
		}

		for (int i = 0; i < 1 << numInputs; i++) {
			value = new DigitalValue(DigitalValue.ZERO);
			if (i == select) {
				value.setIntValue(DigitalValue.ONE);
			}
			out = ports.get(numInputs + i);
			Event tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
		}

		return result;
	}

}
