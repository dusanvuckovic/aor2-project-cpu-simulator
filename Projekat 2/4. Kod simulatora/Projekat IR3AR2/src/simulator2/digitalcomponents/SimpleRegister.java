package simulator2.digitalcomponents;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import simulator2.*;
import simulator2.gui.Window;
import simulator2.staticinit.StaticInitializer;

public class SimpleRegister extends LogicComponentSequential { // a register
																// with the
																// basic
																// functions

	private static final long serialVersionUID = 1L;
	protected int size;
	protected int defaultValue = 0;

	public SimpleRegister() {
		super();
	}

	public int getSize() {
		return size;
	}

	public void setDefaultValue(int value) {
		defaultValue = value;
	}

	@Override
	public void init(String[] data) {
		super.init(data);
		size = Integer.parseInt(data[1]);
		Port port;

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "LD", 1, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "CL", 2, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO, size), "IN", 3, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, size, new DigitalValue(DigitalValue.ZERO, size), "OUT", 4, this);
		ports.add(port);
	}

	public Port getPortLD() {
		return ports.get(1);
	}

	public Port getPortCL() {
		return ports.get(2);
	}

	public Port getPortIN() {
		return ports.get(3);
	}

	public Port getPortOUT() {
		return ports.get(4);
	}

	@Override
	public List<Event> setState(Event event) {
		List<Event> result = events;
		result.clear();

		Value newValue = new DigitalValue(event.getValue().getIntValue() & ((1 << size) - 1), size);
		Event tempEvent = new Event(event.getTime(), newValue, ports.get(4), this);
		result.add(tempEvent);

		return result;
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + size };
		return state;
	}

	@Override
	public List<Event> preCalculateEvents() {
		for (Port port : ports) {
			port.preCalculateEvents();
		}

		Event tempEvent;
		for (Port port : ports) {
			if (port.canGenerate() && port.getPortNumber() != 4) {
				tempEvent = new Event(0, new DigitalValue(DigitalValue.ZERO, port.getSize()), port, this);
				port.execute(tempEvent);
			}
		}

		Port out = ports.get(4);
		Value value = new DigitalValue(defaultValue, size);
		tempEvent = new Event(0, value, out, this);
		return tempEvent.getDst().execute(tempEvent);
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		if (event.getSrc() == ports.get(0) && ports.get(0).getValue().getIntValue() != 0) { // CLK
																							// is
																							// active
			if (ports.get(1).getIntValue() == 1) { // LD is active
				int newVal = ports.get(3).getValue().getIntValue();
				Port out = ports.get(4);
				Value value = new DigitalValue(newVal, size);
				Event tempEvent = new Event(event.getTime(), value, out, this); // event.getTime()
																				// +
																				// 1
				result.add(tempEvent);
			} else if (ports.get(2).getIntValue() == 1) { // CL is active
				Port out = ports.get(4);
				Value value = new DigitalValue(DigitalValue.ZERO, size);
				Event tempEvent = new Event(event.getTime(), value, out, this); // event.getTime()
																				// +
																				// 1
				result.add(tempEvent);
			}
		}

		return result;
	}

	@Override
	public int getOutValue() {
		return ports.get(4).getIntValue();
	}

	@Override
	public void setOutValue(int val) {
		List<Event> eventsList = new LinkedList<Event>();
		Port out = ports.get(4);
		Value value = new DigitalValue(val, size);
		Event tempEvent = new Event(Main.simulator.getCurrentTime(), value, out, this); // event.getTime() + 1


		eventsList.add(tempEvent);
		while (eventsList.size() > 0) { // propagate new value
			tempEvent = eventsList.remove(0);
			eventsList.addAll(tempEvent.getDst().execute(tempEvent));
		}

		Iterator<Signal> signalIterator = StaticInitializer.allSignals.values().iterator(); // save signal values for display
		while (signalIterator.hasNext()) {
			signalIterator.next().saveValueLast();
		}

		Window.tMenu.repaintSchema();
		Window.signalWindow.repaint();
	}
}
