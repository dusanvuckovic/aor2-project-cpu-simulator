package simulator2.digitalcomponents;

import simulator2.*;

public class Xor extends OneOutputGate { // the xor circuit

	private static final long serialVersionUID = 1L;

	public Xor() {
		super();
	}

	@Override
	protected Value calculate() {
		int result = 0x00000000;

		for (int i = 0; i < numInputs; i++) {
			Port port = ports.get(i);

			if (port.getHighZ())
				return new DigitalValue(DigitalValue.ZERO, size, true);

			result = result ^ port.getValue().getIntValue();
		}

		return new DigitalValue(result, size);
	}
}
