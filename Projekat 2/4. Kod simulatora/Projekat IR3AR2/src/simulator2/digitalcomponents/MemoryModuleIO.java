package simulator2.digitalcomponents;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import simulator2.*;
import simulator2.gui.Window;
import simulator2.staticinit.StaticInitializer;
import simulator2.util.*;

public class MemoryModuleIO extends LogicComponentSimple {

	private static final long serialVersionUID = 1L;
	protected int wordSize;
	protected int adrSize;
	protected boolean toWrite;
	protected int writeAdr;
	protected int writeValue;
	protected SparseVectorInt memory;

	public MemoryModuleIO() {
		super();
	}

	public int getWordSize() {
		return wordSize;
	}

	public int getAdrSize() {
		return adrSize;
	}

	@Override
	public int getMemValue(int i) {
		return memory.get(i);
	}

	@Override
	public void setMemValue(int i, int x) {
		memory.put(i, x);

		if (ports.get(2).getIntValue() == i && ports.get(3).getValue().getBooleanValue()) {
			List<Event> eventsList = new LinkedList<Event>();
			Port out = ports.get(1);
			Value value = new DigitalValue(x, wordSize);
			Event tempEvent = new Event(Main.simulator.getCurrentTime(), value, out, this); // event.getTime() + 1

			eventsList.add(tempEvent);
			while (eventsList.size() > 0) { // propagate new value
				tempEvent = eventsList.remove(0);
				eventsList.addAll(tempEvent.getDst().execute(tempEvent));
			}

			Iterator<Signal> signalIterator = StaticInitializer.allSignals.values().iterator(); // save signal values for display
			while (signalIterator.hasNext()) {
				signalIterator.next().saveValueLast();
			}

			Window.tMenu.repaintSchema();
			Window.signalWindow.repaint();
		}
	}

	@Override
	public void resetStateCLK() {
		if (toWrite) {
			memory.put(writeAdr, writeValue);
		}
		toWrite = false;
		writeAdr = 0;
		writeValue = 0;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		wordSize = 32; // might make it configurable at a later date
		adrSize = Integer.parseInt(data[1]);
		ports = new LinkedList<Port>();
		Port port;

		port = new PortSimple(Port.IN, wordSize, new DigitalValue(DigitalValue.ZERO, wordSize), "DI", 0, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, wordSize, new DigitalValue(DigitalValue.ZERO, wordSize), "DO", 1, this);
		ports.add(port);

		port = new PortSimple(Port.IN, adrSize, new DigitalValue(DigitalValue.ZERO, adrSize), "ADDR", 2, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "RD", 3, this);
		ports.add(port);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "WR", 4, this);
		ports.add(port);

		memory = new SparseVectorInt();
	}

	public Port getPortDI() {
		return ports.get(0);
	}

	public Port getPortDO() {
		return ports.get(1);
	}

	public Port getPortADDR() {
		return ports.get(2);
	}

	public Port getPortRD() {
		return ports.get(3);
	}

	public Port getPortWR() {
		return ports.get(4);
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + wordSize, "" + adrSize };
		return state;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();
		Port out = ports.get(1);
		Value value;
		Event tempEvent;

		if (ports.get(3).getValue().getBooleanValue()) { // RD is active
			int data = memory.get(ports.get(2).getIntValue());
			value = new DigitalValue(data, wordSize);
		} else {
			if (ports.get(4).getValue().getBooleanValue()) { // WR is active
				toWrite = true;
				writeAdr = ports.get(2).getIntValue();
				writeValue = ports.get(0).getIntValue();
			}
			value = new DigitalValue(DigitalValue.ZERO, wordSize, true);
		}
		tempEvent = new Event(event.getTime(), value, out, this);
		result.add(tempEvent);

		return result;
	}

	@Override
	public void resetState() { // resets the memory
		super.resetState();
		memory.reset();
	}

}
