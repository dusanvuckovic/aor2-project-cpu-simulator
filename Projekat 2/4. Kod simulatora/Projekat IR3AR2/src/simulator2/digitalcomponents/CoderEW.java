package simulator2.digitalcomponents;

import java.util.List;

import simulator2.*;

public class CoderEW extends Coder { // a coder without E and W signals

	private static final long serialVersionUID = 1L;

	@Override
	public void init(String[] data) {
		super.init(data);

		Port port;
		int i = numOutputs + (1 << numOutputs);

		port = new PortSimple(Port.IN, new DigitalValue(DigitalValue.ZERO), "E", i++, this);
		ports.add(port);

		port = new PortSimple(Port.OUT, new DigitalValue(DigitalValue.ZERO), "W", i, this);
		ports.add(port);
	}

	public Port getPortE() {
		return ports.get(numOutputs + (1 << numOutputs));
	}

	public Port getPortW() {
		return ports.get(numOutputs + (1 << numOutputs) + 1);
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();
		int select = 0;
		int W = 0;

		if (getPortE().getValue().getIntValue() != 0) { // enable signal is
														// zero, no out port
														// should be active
			// select = 0;
			for (int i = (1 << numOutputs) - 1; i >= 0; i--) { // find the
																// active signal
																// with the
																// highest
																// priority
				
				if (ports.get(i).getIntValue() == 1) {
					select = i;
					W = 1;
					break;
				}
			}

			int newVal;
			Port out;
			Value value;
			int addition = 1 << numOutputs;
			for (int i = 0; i < numOutputs; i++) {
				newVal = select & 1;
				select >>>= 1;
				out = ports.get(i + addition);
				value = new DigitalValue(newVal);
				Event tempEvent = new Event(event.getTime(), value, out, this);
				result.add(tempEvent);
			}

			out = getPortW();
			value = new DigitalValue(W);
			Event tempEvent = new Event(event.getTime(), value, out, this);
			result.add(tempEvent);
		}

		return result;
	}

}
