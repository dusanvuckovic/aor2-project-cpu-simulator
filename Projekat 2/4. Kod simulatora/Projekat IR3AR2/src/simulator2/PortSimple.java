package simulator2;

import java.util.*;

public class PortSimple extends SignalSimple implements Port {

	private static final long serialVersionUID = 1L;

	protected LogicComponent component;
	protected int portType;
	protected int portNumber;

	public PortSimple(int portType, Value value, String name, int portNumber, LogicComponent component) {
		super(name);

		this.component = component;
		this.portType = portType;
		this.portNumber = portNumber;

		this.value = value.copy();
	}

	public PortSimple(int portType, int size, Value value, String name, int portNumber, LogicComponent component) {
		super(name, size);

		this.component = component;
		this.portType = portType;
		this.portNumber = portNumber;

		this.value = value.copy();
	}

	@Override
	public void setType(int type) { // ! NOT SAFE !
		this.portType = type;
	}

	@Override
	public int getType() {
		return portType;
	}

	@Override
	public Value getValue() {
		return value;
	}

	@Override
	public void setPortNumber(int number) { // ! NOT SAFE !
		this.portNumber = number;
	}

	@Override
	public int getPortNumber() {
		return portNumber;
	}

	@Override
	public void setLogicComponent(LogicComponent component) {
		this.component = component;
	}

	@Override
	public LogicComponent getLogicComponent() {
		return component;
	}

	@Override
	public boolean canConduct() {
		return portType == IN || portType == INOUT;
	}

	@Override
	public boolean canGenerate() {
		return portType == OUT || portType == INOUT;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value newVal = new DigitalValue(DigitalValue.ZERO, value.getSize());
		newVal.setIntValue(event.getValue().getIntValue());
		newVal.setHighZ(event.getValue().getHighZ());

		if (!value.isEqual(newVal)) {
			value.load(newVal);
			EventExecutable src = event.getSrc();
			
			if (canConduct()) { // Sends the value to its component.
				Event tempEvent = new Event(event.getTime(), value, component, this);
				result.add(tempEvent);
			}

			for (Signal signal : connectedSignals) { // Sends the value to all
														// connected signals.
				if (signal != src) {
					Event tempEvent = new Event(event.getTime(), value, signal, this);
					result.add(tempEvent);
				}
			}
		}
		return result;
	}

}
