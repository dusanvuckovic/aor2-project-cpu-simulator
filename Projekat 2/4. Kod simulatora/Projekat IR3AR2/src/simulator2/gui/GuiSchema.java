package simulator2.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;



public class GuiSchema extends JPanel {

	private static final long serialVersionUID = 1L;

	private String nameOfImage;
	private BufferedImage image;
	private List<GuiSignalLine> lines; 		//lista linija signala za ovu schemu
	private List<GuiSignalLabel> valLabels;	//labele vrednosti
	private List<GuiBigSignalLabel> valBigLabels;
	private List<GuiDynamicComp> dynComps;	//slike dinamickih komponfenti
	private List<GuiTextLabel> textLabels;	//labele sa tekstom
	

	public GuiSchema(String filename) {
		nameOfImage = filename;
		setBackground(Color.WHITE);
		try {
			image = ImageIO.read(new File(nameOfImage));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		lines = new ArrayList<GuiSignalLine>();
		valLabels = new ArrayList<GuiSignalLabel>();
		valBigLabels = new ArrayList<GuiBigSignalLabel>();
		dynComps = new ArrayList<GuiDynamicComp>();
		textLabels= new ArrayList<GuiTextLabel>();
		
		Dimension size = new Dimension(image.getWidth(), image.getHeight());
		adjustSize(size);
		
	}

	public void paint(Graphics g) {
		clearPanel(g);								//brisanje svega

		g.drawImage(image, 0, 0, null);				//crtanje pozadine

		for (GuiDynamicComp gl : dynComps) {		//crtanje dinamickih komponenti
			gl.draw(g);
		}
		for (GuiSignalLine gl : lines) {			//crtanje signala
			if (gl.signal!=null) gl.draw(g);
		}
		for (GuiSignalLabel gl : valLabels) {		//crtanje vrednosnih labela
			gl.draw(g);
		}
		for (GuiBigSignalLabel gl : valBigLabels) {		//crtanje vrednosnih labela
			gl.draw(g);
		}
		for (GuiTextLabel gl : textLabels) {		//crtanje labela sa tekstom
			gl.draw(g);
		}
		
		getToolkit().sync();			//ovo sluzi da se nacrta odmah

	}

	public void clearPanel(Graphics g) {
		Color tmp = g.getColor();
		Dimension size = this.getParent().getSize();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, Math.max(this.getWidth(), (int) size.getWidth()),
				Math.max(this.getHeight(), (int) size.getHeight()));
		g.setColor(tmp);
	}


	

	public void addLine(GuiSignalLine line) {
		lines.add(line);
	}

	public void addTextLabel(GuiTextLabel label) {
		textLabels.add(label);
	}
	
	public void addDynComp(GuiDynamicComp dc) {
		dynComps.add(dc);
	}
	
	public void addValLabel(GuiSignalLabel label) {
		valLabels.add(label);
	}
	
	public void addValBigLabel(GuiBigSignalLabel label) {
		valBigLabels.add(label);
	}

	public List<GuiSignalLine> getLines() {
		return lines;
	}

	public void setLines(List<GuiSignalLine> lines) {
		this.lines = lines;
	}

	public List<GuiSignalLabel> getValLabels() {
		return valLabels;
	}
	
	public List<GuiBigSignalLabel> getValBigLabels() {
		return valBigLabels;
	}
	
	public List<GuiTextLabel> getTextLabels() {
		return textLabels;
	}

	public void setTextLabels(List<GuiTextLabel> labels) {
		this.textLabels = labels;
	}
	
	public void setValLabels(List<GuiSignalLabel> labels) {
		this.valLabels = labels;
	}
	
	public void setValBigLabels(List<GuiBigSignalLabel> labels) {
		this.valBigLabels = labels;
	}
	
	public String getNameOfImage() {
		return nameOfImage;
	}
	
	public void adjustSize(Dimension size) {
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setSize(size);
		validate();
	}

}
