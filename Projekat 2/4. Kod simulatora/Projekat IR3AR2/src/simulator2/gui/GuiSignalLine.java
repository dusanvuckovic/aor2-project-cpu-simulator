package simulator2.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.util.List;

import simulator2.Signal;



public class GuiSignalLine implements Drawable {
	protected List<List<Point>> sections;		//bitne tacke linija

	protected Color color;
	protected Signal signal;

	protected float size;

	public GuiSignalLine(List<List<Point>> lines, Signal signal) {
		this(lines, signal, convertToSize(signal));
	}

	public GuiSignalLine(List<List<Point>> sections, Signal signal, float size) {
		this.sections = sections;
		this.signal = signal;
		this.size = size;
	}

	public void draw(Graphics g) {
		update();

		Graphics2D g2d = (Graphics2D) g;

		Stroke stroke = g2d.getStroke();	//cuva stari stroke
		g2d.setStroke(new BasicStroke(size));

		Color col = g2d.getColor();			//cuva staru boju
		g2d.setColor(color);

		for (List<Point> section : sections) {
			Point last = null;
			for (Point p : section) {
				if (last != null) {
					g2d.drawLine(last.x, last.y, p.x, p.y);
					if (last.x == p.x)
						g2d.drawLine(last.x + 1, last.y, p.x + 1, p.y);
					if (last.y == p.y)
						g2d.drawLine(last.x, last.y + 1, p.x, p.y + 1);
				}
				last = p;
			}
		}

		//vraca stare vrednosti
		g2d.setStroke(stroke);
		g2d.setColor(col);
	}

	public void update() { 				//promeni boju u zavisnosti od trenutne vrednosti
		if (signal.getHighZ()) {
			color = Color.GREEN;
		} else {
			if (signal.getSize()==1) {  // ako je bool signal, tj. samo moze biti 1 ili 0
				if (signal.getValue().getBooleanValue())
					color = Color.RED;
				else
					color = Color.BLUE;
			} else {
				color = Color.GRAY;
			}
		}
	}

	private static float convertToSize(Signal signal) {
		if (signal == null)
			return 1;
		int numberOfLines = signal.getSize();
		if (numberOfLines <= 1)
			return 1;
		if (numberOfLines <= 4)
			return 1.5f;
		if (numberOfLines <= 8)
			return 2;
		if (numberOfLines <= 16)
			return 2.5f;
		return 3;
	}

	
}
