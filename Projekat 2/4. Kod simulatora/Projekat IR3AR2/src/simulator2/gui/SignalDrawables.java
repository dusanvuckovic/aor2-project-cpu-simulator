package simulator2.gui;

import java.util.LinkedList;
import java.util.HashMap;
import java.util.List;

import java.awt.Point;

public class SignalDrawables {

	private HashMap<String, List<List<Point>>> madrSignals = null;
	private HashMap<String, List<List<Point>>> addr1Signals = null;
	private HashMap<String, List<List<Point>>> addr2Signals = null;
	private HashMap<String, List<List<Point>>> arbmodSignals = null;
	private HashMap<String, List<List<Point>>> arbmodIOSignals = null;
	private HashMap<String, List<List<Point>>> arbSignals = null;
	private HashMap<String, List<List<Point>>> exec1Signals = null;
	private HashMap<String, List<List<Point>>> exec2Signals = null;
	private HashMap<String, List<List<Point>>> exec4Signals = null;
	private HashMap<String, List<List<Point>>> fetch1Signals = null;
	private HashMap<String, List<List<Point>>> int1Signals = null;
	private HashMap<String, List<List<Point>>> int2Signals = null;
	private HashMap<String, List<List<Point>>> int3Signals = null;
	private HashMap<String, List<List<Point>>> ioSignals = null;
	private HashMap<String, List<List<Point>>> memOprSignals = null;
	private HashMap<String, List<List<Point>>> memTimeSignals = null;
	private HashMap<String, List<List<Point>>> oprSigSignals = null;
	private HashMap<String, List<List<Point>>> uprSigSignals = null;
	private HashMap<String, List<List<Point>>> upravSignals = null;

	public SignalDrawables() {

	}

	public HashMap<String, List<List<Point>>> getMadrSignals() {

		if (madrSignals != null)
			return madrSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(685, 29));
		points.add(new Point(685, 1004));
		lines.add(points);

		points = new LinkedList<Point>(); // from MDRout
		points.add(new Point(463, 385));
		points.add(new Point(685, 385));
		lines.add(points);

		points = new LinkedList<Point>(); // from MARout
		points.add(new Point(168, 496));
		points.add(new Point(685, 496));
		lines.add(points);

		points = new LinkedList<Point>(); // from NLLBUS1
		points.add(new Point(479, 646));
		points.add(new Point(685, 646));
		lines.add(points);

		points = new LinkedList<Point>(); // to BRG1_2
		points.add(new Point(685, 866));
		points.add(new Point(673, 866));
		lines.add(points);

		points = new LinkedList<Point>(); // to BRG1_3
		points.add(new Point(685, 979));
		points.add(new Point(613, 979));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(629, 29));
		points.add(new Point(629, 1004));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX5
		points.add(new Point(629, 143));
		points.add(new Point(410, 143));
		points.add(new Point(410, 150));
		lines.add(points);

		points = new LinkedList<Point>(); // to BRG2_3
		points.add(new Point(629, 759));
		points.add(new Point(617, 759));
		lines.add(points);

		points = new LinkedList<Point>(); // from BRG1_2
		points.add(new Point(629, 866));
		points.add(new Point(642, 866));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(572, 29));
		points.add(new Point(572, 1004));
		lines.add(points);

		points = new LinkedList<Point>(); // to MAR
		points.add(new Point(572, 83));
		points.add(new Point(101, 83));
		points.add(new Point(101, 243));
		lines.add(points);

		points = new LinkedList<Point>(); // from BRG2_3
		points.add(new Point(586, 759));
		points.add(new Point(572, 759));
		lines.add(points);

		points = new LinkedList<Point>(); // from BRG1_3
		points.add(new Point(582, 979));
		points.add(new Point(572, 979));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // DBUS
		points = new LinkedList<Point>(); // to MX5
		points.add(new Point(382, 109));
		points.add(new Point(382, 150));
		lines.add(points);

		points = new LinkedList<Point>(); // from MDRDout
		points.add(new Point(397, 428));
		points.add(new Point(397, 446));
		lines.add(points);
		signals.put("DBUS", lines);

		lines = new LinkedList<List<Point>>(); // ABUS
		points = new LinkedList<Point>(); // from MARAout
		points.add(new Point(101, 566));
		points.add(new Point(101, 593));
		lines.add(points);
		signals.put("ABUS", lines);

		lines = new LinkedList<List<Point>>(); // MAR out signal
		points = new LinkedList<Point>(); // to MARAout
		points.add(new Point(101, 288));
		points.add(new Point(101, 535));
		lines.add(points);

		points = new LinkedList<Point>(); // to MARout
		points.add(new Point(101, 495));
		points.add(new Point(136, 495));
		lines.add(points);
		signals.put("MAR", lines);

		lines = new LinkedList<List<Point>>(); // MX5 out signal
		points = new LinkedList<Point>(); // from MARAout
		points.add(new Point(397, 203));
		points.add(new Point(397, 243));
		lines.add(points);
		signals.put("MX5", lines);

		lines = new LinkedList<List<Point>>(); // MDR out signal
		points = new LinkedList<Point>(); // to MDRDout
		points.add(new Point(397, 288));
		points.add(new Point(397, 397));
		lines.add(points);

		points = new LinkedList<Point>(); // to MDRout
		points.add(new Point(397, 385));
		points.add(new Point(432, 385));
		lines.add(points);
		signals.put("MDR", lines);

		lines = new LinkedList<List<Point>>(); // ldMAR
		points = new LinkedList<Point>(); // to MAR
		points.add(new Point(197, 264));
		points.add(new Point(177, 264));
		lines.add(points);
		signals.put("ldMAR", lines);

		lines = new LinkedList<List<Point>>(); // ldMDR AND circuit out signal
		points = new LinkedList<Point>(); // from ldMDR AND circuit
		points.add(new Point(251, 337));
		points.add(new Point(320, 337));
		lines.add(points);
		signals.put("ldMDR_AND", lines);

		lines = new LinkedList<List<Point>>(); // ldMDR
		points = new LinkedList<Point>(); // to ldMDR OR circuit
		points.add(new Point(302, 326));
		points.add(new Point(320, 326));
		lines.add(points);
		signals.put("ldMDR", lines);

		lines = new LinkedList<List<Point>>(); // ldMDR OR circuit out signal
		points = new LinkedList<Point>(); // to MDR
		points.add(new Point(348, 332));
		points.add(new Point(486, 332));
		points.add(new Point(486, 266));
		points.add(new Point(473, 266));
		lines.add(points);
		signals.put("ldMDR_OR", lines);

		lines = new LinkedList<List<Point>>(); // mxMDR
		points = new LinkedList<Point>(); // to MX5
		points.add(new Point(459, 177));
		points.add(new Point(432, 177));
		lines.add(points);
		signals.put("mxMDR", lines);

		lines = new LinkedList<List<Point>>(); // MARout
		points = new LinkedList<Point>(); // to MARout buffer
		points.add(new Point(150, 524));
		points.add(new Point(150, 504));
		lines.add(points);
		signals.put("MARout", lines);

		lines = new LinkedList<List<Point>>(); // MARAout
		points = new LinkedList<Point>(); // to MARAout buffer
		points.add(new Point(74, 549));
		points.add(new Point(93, 549));
		lines.add(points);
		signals.put("MARAout", lines);

		lines = new LinkedList<List<Point>>(); // MDRout
		points = new LinkedList<Point>(); // to MDRout buffer
		points.add(new Point(446, 419));
		points.add(new Point(446, 393));
		lines.add(points);
		signals.put("MDRout", lines);

		lines = new LinkedList<List<Point>>(); // MDRDout
		points = new LinkedList<Point>(); // to MDRDout buffer
		points.add(new Point(367, 411));
		points.add(new Point(389, 411));
		lines.add(points);
		signals.put("MDRDout", lines);

		lines = new LinkedList<List<Point>>(); // rdCPU
		points = new LinkedList<Point>(); // to rdCPU buffer
		points.add(new Point(142, 843));
		points.add(new Point(142, 825));
		lines.add(points);

		points = new LinkedList<Point>(); // to MDR AND circuit
		points.add(new Point(212, 332));
		points.add(new Point(219, 332));
		lines.add(points);
		signals.put("rdCPU", lines);

		lines = new LinkedList<List<Point>>(); // wrCPU
		points = new LinkedList<Point>(); // to wrCPU buffer
		points.add(new Point(143, 938));
		points.add(new Point(143, 920));
		lines.add(points);
		signals.put("wrCPU", lines);

		lines = new LinkedList<List<Point>>(); // fcCPU
		points = new LinkedList<Point>(); // from fcCPU negator
		points.add(new Point(175, 745));
		points.add(new Point(191, 745));
		lines.add(points);

		points = new LinkedList<Point>(); // to MDR AND circuit
		points.add(new Point(212, 343));
		points.add(new Point(219, 343));
		lines.add(points);
		signals.put("fcCPU", lines);

		lines = new LinkedList<List<Point>>(); // NLLBUS1
		points = new LinkedList<Point>(); // to NLLBUS1 buffer
		points.add(new Point(462, 672));
		points.add(new Point(462, 654));
		lines.add(points);
		signals.put("NLLBUS1", lines);

		lines = new LinkedList<List<Point>>(); // BRG1_2
		points = new LinkedList<Point>(); // to BRG1_2 buffer
		points.add(new Point(659, 893));
		points.add(new Point(659, 874));
		lines.add(points);
		signals.put("BRG1_2", lines);

		lines = new LinkedList<List<Point>>(); // BRG1_3
		points = new LinkedList<Point>(); // to BRG1_3 buffer
		points.add(new Point(599, 1006));
		points.add(new Point(599, 987));
		lines.add(points);
		signals.put("BRG1_3", lines);

		lines = new LinkedList<List<Point>>(); // BRG2_3
		points = new LinkedList<Point>(); // to BRG2_3 buffer
		points.add(new Point(602, 782));
		points.add(new Point(602, 767));
		lines.add(points);
		signals.put("BRG2_3", lines);

		lines = new LinkedList<List<Point>>(); // !FCBUS
		points = new LinkedList<Point>(); // to fcCPU negator
		points.add(new Point(117, 745));
		points.add(new Point(140, 745));
		lines.add(points);
		signals.put("!FCBUS", lines);

		lines = new LinkedList<List<Point>>(); // !RDBUS
		points = new LinkedList<Point>(); // from rdCPU buffer
		points.add(new Point(165, 817));
		points.add(new Point(181, 817));
		lines.add(points);
		signals.put("!RDBUS", lines);

		lines = new LinkedList<List<Point>>(); // !WRBUS
		points = new LinkedList<Point>(); // from wrCPU buffer
		points.add(new Point(166, 912));
		points.add(new Point(182, 912));
		lines.add(points);
		signals.put("!WRBUS", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to NLLBUS1 buffer
		points.add(new Point(432, 645));
		points.add(new Point(448, 645));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to rdCPU buffer
		points.add(new Point(115, 817));
		points.add(new Point(128, 817));
		lines.add(points);

		points = new LinkedList<Point>(); // to wrCPU buffer
		points.add(new Point(116, 912));
		points.add(new Point(129, 912));
		lines.add(points);
		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to MAR
		points.add(new Point(14, 266));
		points.add(new Point(24, 266));
		lines.add(points);

		points = new LinkedList<Point>(); // to MDR
		points.add(new Point(310, 266));
		points.add(new Point(320, 266));
		lines.add(points);
		signals.put("CLK", lines);

		return madrSignals = signals;
	}

	public HashMap<String, List<List<Point>>> getAddr1Signals() {

		if (addr1Signals != null)
			return addr1Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(1419, 32));
		points.add(new Point(1419, 1033));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(1363, 32));
		points.add(new Point(1363, 1033));
		lines.add(points);

		points = new LinkedList<Point>(); // to BADDR2
		points.add(new Point(1363, 277));
		points.add(new Point(1107, 277));
		points.add(new Point(1107, 346));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(1306, 32));
		points.add(new Point(1306, 1033));
		lines.add(points);

		points = new LinkedList<Point>(); // to BADDR1
		points.add(new Point(1306, 127));
		points.add(new Point(920, 127));
		points.add(new Point(920, 180));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // BADDR1 out signal
		points = new LinkedList<Point>(); // to MX0
		points.add(new Point(920, 224));
		points.add(new Point(920, 541));
		points.add(new Point(962, 541));
		lines.add(points);
		signals.put("BADDR1", lines);

		lines = new LinkedList<List<Point>>(); // ldBADDR1
		points = new LinkedList<Point>(); // to BADDR1
		points.add(new Point(1020, 202));
		points.add(new Point(996, 202));
		lines.add(points);
		signals.put("ldBADDR1", lines);

		lines = new LinkedList<List<Point>>(); // BADDR2 out signal
		points = new LinkedList<Point>(); // from BADDR2
		points.add(new Point(1107, 390));
		points.add(new Point(1107, 410));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX2
		points.add(new Point(890, 791));
		points.add(new Point(912, 791));
		lines.add(points);
		signals.put("BADDR2", lines);

		lines = new LinkedList<List<Point>>(); // ldBADDR2
		points = new LinkedList<Point>(); // to BADDR2
		points.add(new Point(1208, 368));
		points.add(new Point(1183, 368));
		lines.add(points);
		signals.put("ldBADDR2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1 25..22
		points = new LinkedList<Point>(); // to GPRADDR1
		points.add(new Point(85, 552));
		points.add(new Point(171, 552));
		points.add(new Point(171, 585));
		lines.add(points);
		signals.put("IBUS125..22", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1 out signal
		points = new LinkedList<Point>(); // from GPRADDR1
		points.add(new Point(171, 629));
		points.add(new Point(171, 648));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX3
		points.add(new Point(709, 576));
		points.add(new Point(731, 576));
		lines.add(points);
		signals.put("GPRADDR1", lines);

		lines = new LinkedList<List<Point>>(); // ldGPRADDR1
		points = new LinkedList<Point>(); // to GPRADDR1
		points.add(new Point(260, 607));
		points.add(new Point(247, 607));
		lines.add(points);
		signals.put("ldGPRADDR1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1 24..21
		points = new LinkedList<Point>(); // to GPRADDR2
		points.add(new Point(85, 698));
		points.add(new Point(171, 698));
		points.add(new Point(171, 723));
		lines.add(points);
		signals.put("IBUS124..21", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2 out signal
		points = new LinkedList<Point>(); // from GPRADDR2
		points.add(new Point(171, 767));
		points.add(new Point(171, 786));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX3
		points.add(new Point(709, 553));
		points.add(new Point(731, 553));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX1
		points.add(new Point(633, 804));
		points.add(new Point(655, 804));
		lines.add(points);
		signals.put("GPRADDR2", lines);

		lines = new LinkedList<List<Point>>(); // ldGPRADDR2
		points = new LinkedList<Point>(); // to GPRADDR2
		points.add(new Point(260, 744));
		points.add(new Point(247, 744));
		lines.add(points);
		signals.put("ldGPRADDR2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1 21..18
		points = new LinkedList<Point>(); // to GPRADDR3
		points.add(new Point(85, 835));
		points.add(new Point(170, 835));
		points.add(new Point(170, 856));
		lines.add(points);
		signals.put("IBUS121..18", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR3 out signal
		points = new LinkedList<Point>(); // from GPRADDR3
		points.add(new Point(171, 900));
		points.add(new Point(171, 919));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX1
		points.add(new Point(633, 827));
		points.add(new Point(655, 827));
		lines.add(points);
		signals.put("GPRADDR3", lines);

		lines = new LinkedList<List<Point>>(); // ldGPRADDR3
		points = new LinkedList<Point>(); // to GPRADDR3
		points.add(new Point(259, 878));
		points.add(new Point(246, 878));
		lines.add(points);
		signals.put("ldGPRADDR3", lines);

		lines = new LinkedList<List<Point>>(); // MX3 out signal
		points = new LinkedList<Point>(); // to MX0
		points.add(new Point(825, 564));
		points.add(new Point(962, 564));
		lines.add(points);
		signals.put("MX3", lines);

		lines = new LinkedList<List<Point>>(); // MX1 out signal
		points = new LinkedList<Point>(); // to MX2
		points.add(new Point(749, 814));
		points.add(new Point(912, 814));
		lines.add(points);
		signals.put("MX1", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR0
		points = new LinkedList<Point>(); // to MX3
		points.add(new Point(778, 642));
		points.add(new Point(778, 622));
		lines.add(points);
		signals.put("mxADDR0", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR1
		points = new LinkedList<Point>(); // to MX1
		points.add(new Point(702, 893));
		points.add(new Point(702, 873));
		lines.add(points);
		signals.put("mxADDR1", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR2
		points = new LinkedList<Point>(); // to MX0
		points.add(new Point(1009, 631));
		points.add(new Point(1009, 611));
		lines.add(points);
		signals.put("mxADDR2", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR3
		points = new LinkedList<Point>(); // to MX2
		points.add(new Point(959, 880));
		points.add(new Point(959, 860));
		lines.add(points);
		signals.put("mxADDR3", lines);

		lines = new LinkedList<List<Point>>(); // ADDR1
		points = new LinkedList<Point>(); // from MX0
		points.add(new Point(1056, 553));
		points.add(new Point(1079, 553));
		lines.add(points);
		signals.put("MX0", lines);

		lines = new LinkedList<List<Point>>(); // ADDR2
		points = new LinkedList<Point>(); // from MX2
		points.add(new Point(1006, 802));
		points.add(new Point(1024, 802));
		lines.add(points);
		signals.put("MX2", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(123, 463));
		points.add(new Point(123, 444));
		lines.add(points);

		points = new LinkedList<Point>(); // to DC1
		points.add(new Point(538, 467));
		points.add(new Point(538, 448));
		lines.add(points);
		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // ADDR1_0
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(64, 264));
		points.add(new Point(76, 264));
		lines.add(points);
		signals.put("ADDR1_0", lines);

		lines = new LinkedList<List<Point>>(); // ADDR1_1
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(64, 239));
		points.add(new Point(76, 239));
		lines.add(points);
		signals.put("ADDR1_1", lines);

		lines = new LinkedList<List<Point>>(); // ADDR1_2
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(64, 214));
		points.add(new Point(76, 214));
		lines.add(points);
		signals.put("ADDR1_2", lines);

		lines = new LinkedList<List<Point>>(); // ADDR1_3
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(64, 189));
		points.add(new Point(76, 189));
		lines.add(points);
		signals.put("ADDR1_3", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_0
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 425));
		points.add(new Point(182, 425));
		lines.add(points);
		signals.put("GPRADDR1_0", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_1
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 401));
		points.add(new Point(182, 401));
		lines.add(points);
		signals.put("GPRADDR1_1", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_2
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 377));
		points.add(new Point(182, 377));
		lines.add(points);
		signals.put("GPRADDR1_2", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_3
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 354));
		points.add(new Point(182, 354));
		lines.add(points);
		signals.put("GPRADDR1_3", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_4
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 330));
		points.add(new Point(182, 330));
		lines.add(points);
		signals.put("GPRADDR1_4", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_5
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 306));
		points.add(new Point(182, 306));
		lines.add(points);
		signals.put("GPRADDR1_5", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_6
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 282));
		points.add(new Point(182, 282));
		lines.add(points);
		signals.put("GPRADDR1_6", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_7
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 259));
		points.add(new Point(182, 259));
		lines.add(points);
		signals.put("GPRADDR1_7", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_8
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 235));
		points.add(new Point(182, 235));
		lines.add(points);
		signals.put("GPRADDR1_8", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_9
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 212));
		points.add(new Point(182, 212));
		lines.add(points);
		signals.put("GPRADDR1_9", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_10
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 188));
		points.add(new Point(182, 188));
		lines.add(points);
		signals.put("GPRADDR1_10", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_11
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 164));
		points.add(new Point(182, 164));
		lines.add(points);
		signals.put("GPRADDR1_11", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_12
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 140));
		points.add(new Point(182, 140));
		lines.add(points);
		signals.put("GPRADDR1_12", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_13
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 117));
		points.add(new Point(182, 117));
		lines.add(points);
		signals.put("GPRADDR1_13", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_14
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 93));
		points.add(new Point(182, 93));
		lines.add(points);
		signals.put("GPRADDR1_14", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_15
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(170, 69));
		points.add(new Point(182, 69));
		lines.add(points);
		signals.put("GPRADDR1_15", lines);

		lines = new LinkedList<List<Point>>(); // ADDR2_0
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(480, 268));
		points.add(new Point(492, 268));
		lines.add(points);
		signals.put("ADDR2_0", lines);

		lines = new LinkedList<List<Point>>(); // ADDR2_1
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(480, 243));
		points.add(new Point(492, 243));
		lines.add(points);
		signals.put("ADDR2_1", lines);

		lines = new LinkedList<List<Point>>(); // ADDR2_2
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(480, 218));
		points.add(new Point(492, 218));
		lines.add(points);
		signals.put("ADDR2_2", lines);

		lines = new LinkedList<List<Point>>(); // ADDR2_3
		points = new LinkedList<Point>(); // to DC0
		points.add(new Point(480, 193));
		points.add(new Point(492, 193));
		lines.add(points);
		signals.put("ADDR2_3", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_0
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 429));
		points.add(new Point(598, 429));
		lines.add(points);
		signals.put("GPRADDR2_0", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_1
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 405));
		points.add(new Point(598, 405));
		lines.add(points);
		signals.put("GPRADDR2_1", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_2
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 381));
		points.add(new Point(598, 381));
		lines.add(points);
		signals.put("GPRADDR2_2", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_3
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 358));
		points.add(new Point(598, 358));
		lines.add(points);
		signals.put("GPRADDR2_3", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_4
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 334));
		points.add(new Point(598, 334));
		lines.add(points);
		signals.put("GPRADDR2_4", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_5
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 310));
		points.add(new Point(598, 310));
		lines.add(points);
		signals.put("GPRADDR2_5", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_6
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 287));
		points.add(new Point(598, 287));
		lines.add(points);
		signals.put("GPRADDR2_6", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_7
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 263));
		points.add(new Point(598, 263));
		lines.add(points);
		signals.put("GPRADDR2_7", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_8
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 239));
		points.add(new Point(598, 239));
		lines.add(points);
		signals.put("GPRADDR2_8", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_9
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 216));
		points.add(new Point(598, 216));
		lines.add(points);
		signals.put("GPRADDR2_9", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_10
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 192));
		points.add(new Point(598, 192));
		lines.add(points);
		signals.put("GPRADDR2_10", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_11
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 168));
		points.add(new Point(598, 168));
		lines.add(points);
		signals.put("GPRADDR2_11", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_12
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 144));
		points.add(new Point(598, 144));
		lines.add(points);
		signals.put("GPRADDR2_12", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_13
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 121));
		points.add(new Point(598, 121));
		lines.add(points);
		signals.put("GPRADDR2_13", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_14
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 97));
		points.add(new Point(598, 97));
		lines.add(points);
		signals.put("GPRADDR2_14", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_15
		points = new LinkedList<Point>(); // from DC0
		points.add(new Point(586, 73));
		points.add(new Point(598, 73));
		lines.add(points);
		signals.put("GPRADDR2_15", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to GPRADDR1
		points.add(new Point(94, 607));
		points.add(new Point(84, 607));
		lines.add(points);

		points = new LinkedList<Point>(); // to GPRADDR2
		points.add(new Point(94, 746));
		points.add(new Point(84, 746));
		lines.add(points);

		points = new LinkedList<Point>(); // to GPRADDR3
		points.add(new Point(93, 878));
		points.add(new Point(83, 878));
		lines.add(points);
		signals.put("CLK", lines);

		return addr1Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getAddr2Signals() {

		if (addr2Signals != null)
			return addr2Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(670, 30));
		points.add(new Point(670, 2665));
		lines.add(points);

		points = new LinkedList<Point>(); // from R0
		points.add(new Point(332, 327));
		points.add(new Point(332, 349));
		points.add(new Point(670, 349));
		lines.add(points);

		points = new LinkedList<Point>(); // from R1
		points.add(new Point(340, 576));
		points.add(new Point(340, 649));
		points.add(new Point(670, 649));
		lines.add(points);

		points = new LinkedList<Point>(); // from R2
		points.add(new Point(340, 930));
		points.add(new Point(340, 1003));
		points.add(new Point(670, 1003));
		lines.add(points);

		points = new LinkedList<Point>(); // from R3
		points.add(new Point(340, 1152));
		points.add(new Point(340, 1225));
		points.add(new Point(670, 1225));
		lines.add(points);

		points = new LinkedList<Point>(); // from R4
		points.add(new Point(340, 1377));
		points.add(new Point(340, 1451));
		points.add(new Point(670, 1451));
		lines.add(points);

		points = new LinkedList<Point>(); // from R5
		points.add(new Point(340, 1597));
		points.add(new Point(340, 1670));
		points.add(new Point(670, 1670));
		lines.add(points);

		points = new LinkedList<Point>(); // from R6
		points.add(new Point(340, 1816));
		points.add(new Point(340, 1890));
		points.add(new Point(670, 1890));
		lines.add(points);

		points = new LinkedList<Point>(); // from R7
		points.add(new Point(340, 2039));
		points.add(new Point(340, 2112));
		points.add(new Point(670, 2112));
		lines.add(points);

		points = new LinkedList<Point>(); // from R8
		points.add(new Point(983, 339));
		points.add(new Point(983, 409));
		points.add(new Point(670, 409));
		lines.add(points);

		points = new LinkedList<Point>(); // from R9
		points.add(new Point(983, 558));
		points.add(new Point(983, 633));
		points.add(new Point(670, 633));
		lines.add(points);

		points = new LinkedList<Point>(); // from R10
		points.add(new Point(983, 777));
		points.add(new Point(983, 852));
		points.add(new Point(670, 852));
		lines.add(points);

		points = new LinkedList<Point>(); // from R11
		points.add(new Point(983, 999));
		points.add(new Point(983, 1084));
		points.add(new Point(670, 1084));
		lines.add(points);

		points = new LinkedList<Point>(); // from R12
		points.add(new Point(983, 1225));
		points.add(new Point(983, 1305));
		points.add(new Point(670, 1305));
		lines.add(points);

		points = new LinkedList<Point>(); // from R13
		points.add(new Point(983, 1444));
		points.add(new Point(983, 1522));
		points.add(new Point(670, 1522));
		lines.add(points);

		points = new LinkedList<Point>(); // from R14
		points.add(new Point(983, 1664));
		points.add(new Point(983, 1739));
		points.add(new Point(670, 1739));
		lines.add(points);

		points = new LinkedList<Point>(); // from R15
		points.add(new Point(983, 1886));
		points.add(new Point(983, 1965));
		points.add(new Point(670, 1965));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(613, 30));
		points.add(new Point(613, 2665));
		lines.add(points);

		points = new LinkedList<Point>(); // from R0
		points.add(new Point(478, 431));
		points.add(new Point(478, 445));
		points.add(new Point(613, 445));
		lines.add(points);

		points = new LinkedList<Point>(); // from R1
		points.add(new Point(478, 717));
		points.add(new Point(478, 783));
		points.add(new Point(613, 783));
		lines.add(points);

		points = new LinkedList<Point>(); // from R2
		points.add(new Point(524, 958));
		points.add(new Point(524, 986));
		points.add(new Point(613, 986));
		lines.add(points);

		points = new LinkedList<Point>(); // from R3
		points.add(new Point(524, 1181));
		points.add(new Point(524, 1208));
		points.add(new Point(613, 1208));
		lines.add(points);

		points = new LinkedList<Point>(); // from R4
		points.add(new Point(524, 1406));
		points.add(new Point(524, 1434));
		points.add(new Point(613, 1434));
		lines.add(points);

		points = new LinkedList<Point>(); // from R5
		points.add(new Point(524, 1625));
		points.add(new Point(524, 1653));
		points.add(new Point(613, 1653));
		lines.add(points);

		points = new LinkedList<Point>(); // from R6
		points.add(new Point(524, 1845));
		points.add(new Point(524, 1873));
		points.add(new Point(613, 1873));
		lines.add(points);

		points = new LinkedList<Point>(); // from R7
		points.add(new Point(524, 2067));
		points.add(new Point(524, 2095));
		points.add(new Point(613, 2095));
		lines.add(points);

		points = new LinkedList<Point>(); // from R8
		points.add(new Point(799, 367));
		points.add(new Point(799, 381));
		points.add(new Point(613, 381));
		lines.add(points);

		points = new LinkedList<Point>(); // from R9
		points.add(new Point(799, 587));
		points.add(new Point(799, 615));
		points.add(new Point(613, 615));
		lines.add(points);

		points = new LinkedList<Point>(); // from R10
		points.add(new Point(799, 806));
		points.add(new Point(799, 833));
		points.add(new Point(613, 833));
		lines.add(points);

		points = new LinkedList<Point>(); // from R11
		points.add(new Point(799, 1028));
		points.add(new Point(799, 1060));
		points.add(new Point(613, 1060));
		lines.add(points);

		points = new LinkedList<Point>(); // from R12
		points.add(new Point(799, 1254));
		points.add(new Point(799, 1286));
		points.add(new Point(613, 1286));
		lines.add(points);

		points = new LinkedList<Point>(); // from R13
		points.add(new Point(799, 1473));
		points.add(new Point(799, 1503));
		points.add(new Point(613, 1503));
		lines.add(points);

		points = new LinkedList<Point>(); // from R14
		points.add(new Point(798, 1693));
		points.add(new Point(798, 1720));
		points.add(new Point(613, 1720));
		lines.add(points);

		points = new LinkedList<Point>(); // from R15
		points.add(new Point(798, 1915));
		points.add(new Point(798, 1942));
		points.add(new Point(613, 1942));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(557, 30));
		points.add(new Point(557, 2665));
		lines.add(points);

		points = new LinkedList<Point>(); // to R0
		points.add(new Point(557, 185));
		points.add(new Point(335, 185));
		points.add(new Point(335, 237));
		lines.add(points);

		points = new LinkedList<Point>(); // to R1
		points.add(new Point(557, 468));
		points.add(new Point(340, 468));
		points.add(new Point(340, 484));
		lines.add(points);

		points = new LinkedList<Point>(); // to R2
		points.add(new Point(557, 812));
		points.add(new Point(340, 812));
		points.add(new Point(340, 840));
		lines.add(points);

		points = new LinkedList<Point>(); // to R3
		points.add(new Point(557, 1019));
		points.add(new Point(340, 1019));
		points.add(new Point(340, 1062));
		lines.add(points);

		points = new LinkedList<Point>(); // to R4
		points.add(new Point(557, 1246));
		points.add(new Point(340, 1246));
		points.add(new Point(340, 1287));
		lines.add(points);

		points = new LinkedList<Point>(); // to R5
		points.add(new Point(557, 1466));
		points.add(new Point(340, 1466));
		points.add(new Point(340, 1507));
		lines.add(points);

		points = new LinkedList<Point>(); // to R6
		points.add(new Point(557, 1689));
		points.add(new Point(340, 1689));
		points.add(new Point(340, 1726));
		lines.add(points);

		points = new LinkedList<Point>(); // to R7
		points.add(new Point(557, 1904));
		points.add(new Point(340, 1904));
		points.add(new Point(340, 1948));
		lines.add(points);

		points = new LinkedList<Point>(); // to R8
		points.add(new Point(557, 220));
		points.add(new Point(983, 220));
		points.add(new Point(983, 248));
		lines.add(points);

		points = new LinkedList<Point>(); // to R9
		points.add(new Point(557, 440));
		points.add(new Point(983, 440));
		points.add(new Point(983, 468));
		lines.add(points);

		points = new LinkedList<Point>(); // to R10
		points.add(new Point(557, 660));
		points.add(new Point(983, 660));
		points.add(new Point(983, 687));
		lines.add(points);

		points = new LinkedList<Point>(); // to R11
		points.add(new Point(557, 880));
		points.add(new Point(983, 880));
		points.add(new Point(983, 909));
		lines.add(points);

		points = new LinkedList<Point>(); // to R12
		points.add(new Point(557, 1106));
		points.add(new Point(983, 1106));
		points.add(new Point(983, 1135));
		lines.add(points);

		points = new LinkedList<Point>(); // to R13
		points.add(new Point(557, 1326));
		points.add(new Point(983, 1326));
		points.add(new Point(983, 1354));
		lines.add(points);

		points = new LinkedList<Point>(); // to R14
		points.add(new Point(557, 1545));
		points.add(new Point(983, 1545));
		points.add(new Point(983, 1574));
		lines.add(points);

		points = new LinkedList<Point>(); // to R15
		points.add(new Point(557, 1767));
		points.add(new Point(983, 1767));
		points.add(new Point(983, 1796));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // R0/PC out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(333, 281));
		points.add(new Point(333, 297));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(333, 289));
		points.add(new Point(478, 289));
		points.add(new Point(478, 401));
		lines.add(points);
		signals.put("R0", lines);

		lines = new LinkedList<List<Point>>(); // R1/SP out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 529));
		points.add(new Point(340, 545));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 536));
		points.add(new Point(478, 536));
		points.add(new Point(478, 686));
		lines.add(points);
		signals.put("R1", lines);

		lines = new LinkedList<List<Point>>(); // R2 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 884));
		points.add(new Point(340, 899));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 890));
		points.add(new Point(525, 890));
		points.add(new Point(525, 928));
		lines.add(points);
		signals.put("R2", lines);

		lines = new LinkedList<List<Point>>(); // R3 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 1106));
		points.add(new Point(340, 1121));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 1112));
		points.add(new Point(525, 1112));
		points.add(new Point(525, 1150));
		lines.add(points);
		signals.put("R3", lines);

		lines = new LinkedList<List<Point>>(); // R4 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 1331));
		points.add(new Point(340, 1347));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 1337));
		points.add(new Point(525, 1337));
		points.add(new Point(525, 1375));
		lines.add(points);
		signals.put("R4", lines);

		lines = new LinkedList<List<Point>>(); // R5 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 1551));
		points.add(new Point(340, 1566));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 1557));
		points.add(new Point(525, 1557));
		points.add(new Point(525, 1595));
		lines.add(points);
		signals.put("R5", lines);

		lines = new LinkedList<List<Point>>(); // R6 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 1770));
		points.add(new Point(340, 1785));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 1776));
		points.add(new Point(525, 1776));
		points.add(new Point(525, 1814));
		lines.add(points);
		signals.put("R6", lines);

		lines = new LinkedList<List<Point>>(); // R7 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(340, 1993));
		points.add(new Point(340, 2008));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(340, 1998));
		points.add(new Point(525, 1998));
		points.add(new Point(525, 2037));
		lines.add(points);
		signals.put("R7", lines);

		lines = new LinkedList<List<Point>>(); // R8 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 293));
		points.add(new Point(983, 308));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 298));
		points.add(new Point(798, 298));
		points.add(new Point(798, 337));
		lines.add(points);
		signals.put("R8", lines);

		lines = new LinkedList<List<Point>>(); // R9 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 512));
		points.add(new Point(983, 527));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 518));
		points.add(new Point(798, 518));
		points.add(new Point(798, 556));
		lines.add(points);
		signals.put("R9", lines);

		lines = new LinkedList<List<Point>>(); // R10 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 731));
		points.add(new Point(983, 746));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 737));
		points.add(new Point(798, 737));
		points.add(new Point(798, 775));
		lines.add(points);
		signals.put("R10", lines);

		lines = new LinkedList<List<Point>>(); // R11 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 953));
		points.add(new Point(983, 969));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 959));
		points.add(new Point(798, 959));
		points.add(new Point(798, 998));
		lines.add(points);
		signals.put("R11", lines);

		lines = new LinkedList<List<Point>>(); // R12 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 1179));
		points.add(new Point(983, 1194));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 1185));
		points.add(new Point(798, 1185));
		points.add(new Point(798, 1223));
		lines.add(points);
		signals.put("R12", lines);

		lines = new LinkedList<List<Point>>(); // R13 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 1398));
		points.add(new Point(983, 1413));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 1404));
		points.add(new Point(798, 1404));
		points.add(new Point(798, 1442));
		lines.add(points);
		signals.put("R13", lines);

		lines = new LinkedList<List<Point>>(); // R14 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 1618));
		points.add(new Point(983, 1633));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 1624));
		points.add(new Point(798, 1624));
		points.add(new Point(798, 1662));
		lines.add(points);
		signals.put("R14", lines);

		lines = new LinkedList<List<Point>>(); // R15 out signal
		points = new LinkedList<Point>(); // to IBUS3
		points.add(new Point(983, 1840));
		points.add(new Point(983, 1855));
		lines.add(points);

		points = new LinkedList<Point>(); // to IBUS2
		points.add(new Point(983, 1846));
		points.add(new Point(798, 1846));
		points.add(new Point(798, 1884));
		lines.add(points);
		signals.put("R15", lines);

		lines = new LinkedList<List<Point>>(); // wrGPR
		points = new LinkedList<Point>(); // to R0
		points.add(new Point(106, 237));
		points.add(new Point(113, 237));
		lines.add(points);

		points = new LinkedList<Point>(); // to R1
		points.add(new Point(195, 490));
		points.add(new Point(202, 490));
		lines.add(points);

		points = new LinkedList<Point>(); // to R2
		points.add(new Point(200, 845));
		points.add(new Point(207, 845));
		lines.add(points);

		points = new LinkedList<Point>(); // to R3
		points.add(new Point(197, 1067));
		points.add(new Point(204, 1067));
		lines.add(points);

		points = new LinkedList<Point>(); // to R4
		points.add(new Point(197, 1293));
		points.add(new Point(204, 1293));
		lines.add(points);

		points = new LinkedList<Point>(); // to R5
		points.add(new Point(197, 1512));
		points.add(new Point(204, 1512));
		lines.add(points);

		points = new LinkedList<Point>(); // to R6
		points.add(new Point(195, 1730));
		points.add(new Point(202, 1730));
		lines.add(points);

		points = new LinkedList<Point>(); // to R7
		points.add(new Point(195, 1952));
		points.add(new Point(202, 1952));
		lines.add(points);

		points = new LinkedList<Point>(); // to R8
		points.add(new Point(1128, 253));
		points.add(new Point(1121, 253));
		lines.add(points);

		points = new LinkedList<Point>(); // to R9
		points.add(new Point(1128, 472));
		points.add(new Point(1121, 472));
		lines.add(points);

		points = new LinkedList<Point>(); // to R10
		points.add(new Point(1128, 691));
		points.add(new Point(1121, 691));
		lines.add(points);

		points = new LinkedList<Point>(); // to R11
		points.add(new Point(1128, 913));
		points.add(new Point(1121, 913));
		lines.add(points);

		points = new LinkedList<Point>(); // to R12
		points.add(new Point(1128, 1139));
		points.add(new Point(1121, 1139));
		lines.add(points);

		points = new LinkedList<Point>(); // to R13
		points.add(new Point(1128, 1358));
		points.add(new Point(1121, 1358));
		lines.add(points);

		points = new LinkedList<Point>(); // to R14
		points.add(new Point(1128, 1578));
		points.add(new Point(1121, 1578));
		lines.add(points);

		points = new LinkedList<Point>(); // to R15
		points.add(new Point(1128, 1800));
		points.add(new Point(1121, 1800));
		lines.add(points);
		signals.put("wrGPR", lines);

		lines = new LinkedList<List<Point>>(); // rdGPR1
		points = new LinkedList<Point>(); // to R0
		points.add(new Point(160, 322));
		points.add(new Point(166, 322));
		lines.add(points);

		points = new LinkedList<Point>(); // to R1
		points.add(new Point(167, 570));
		points.add(new Point(174, 570));
		lines.add(points);

		points = new LinkedList<Point>(); // to R2
		points.add(new Point(233, 918));
		points.add(new Point(239, 918));
		lines.add(points);

		points = new LinkedList<Point>(); // to R3
		points.add(new Point(233, 1141));
		points.add(new Point(239, 1141));
		lines.add(points);

		points = new LinkedList<Point>(); // to R4
		points.add(new Point(233, 1366));
		points.add(new Point(239, 1366));
		lines.add(points);

		points = new LinkedList<Point>(); // to R5
		points.add(new Point(233, 1585));
		points.add(new Point(239, 1585));
		lines.add(points);

		points = new LinkedList<Point>(); // to R6
		points.add(new Point(233, 1805));
		points.add(new Point(239, 1805));
		lines.add(points);

		points = new LinkedList<Point>(); // to R7
		points.add(new Point(233, 2027));
		points.add(new Point(239, 2027));
		lines.add(points);

		points = new LinkedList<Point>(); // to R8
		points.add(new Point(1090, 327));
		points.add(new Point(1084, 327));
		lines.add(points);

		points = new LinkedList<Point>(); // to R9
		points.add(new Point(1090, 547));
		points.add(new Point(1084, 547));
		lines.add(points);

		points = new LinkedList<Point>(); // to R10
		points.add(new Point(1090, 766));
		points.add(new Point(1084, 766));
		lines.add(points);

		points = new LinkedList<Point>(); // to R11
		points.add(new Point(1090, 988));
		points.add(new Point(1084, 988));
		lines.add(points);

		points = new LinkedList<Point>(); // to R12
		points.add(new Point(1090, 1213));
		points.add(new Point(1084, 1213));
		lines.add(points);

		points = new LinkedList<Point>(); // to R13
		points.add(new Point(1090, 1433));
		points.add(new Point(1084, 1433));
		lines.add(points);

		points = new LinkedList<Point>(); // to R14
		points.add(new Point(1090, 1652));
		points.add(new Point(1084, 1652));
		lines.add(points);

		points = new LinkedList<Point>(); // to R15
		points.add(new Point(1090, 1875));
		points.add(new Point(1084, 1875));
		lines.add(points);
		signals.put("rdGPR1", lines);

		lines = new LinkedList<List<Point>>(); // rdGPR2
		points = new LinkedList<Point>(); // to R0
		points.add(new Point(295, 426));
		points.add(new Point(301, 426));
		lines.add(points);

		points = new LinkedList<Point>(); // to R1
		points.add(new Point(408, 706));
		points.add(new Point(414, 706));
		lines.add(points);

		points = new LinkedList<Point>(); // to R2
		points.add(new Point(454, 947));
		points.add(new Point(460, 947));
		lines.add(points);

		points = new LinkedList<Point>(); // to R3
		points.add(new Point(454, 1169));
		points.add(new Point(460, 1169));
		lines.add(points);

		points = new LinkedList<Point>(); // to R4
		points.add(new Point(454, 1395));
		points.add(new Point(460, 1395));
		lines.add(points);

		points = new LinkedList<Point>(); // to R5
		points.add(new Point(454, 1614));
		points.add(new Point(460, 1614));
		lines.add(points);

		points = new LinkedList<Point>(); // to R6
		points.add(new Point(454, 1834));
		points.add(new Point(460, 1834));
		lines.add(points);

		points = new LinkedList<Point>(); // to R7
		points.add(new Point(454, 2056));
		points.add(new Point(460, 2056));
		lines.add(points);

		points = new LinkedList<Point>(); // to R8
		points.add(new Point(869, 356));
		points.add(new Point(863, 356));
		lines.add(points);

		points = new LinkedList<Point>(); // to R9
		points.add(new Point(869, 575));
		points.add(new Point(863, 575));
		lines.add(points);

		points = new LinkedList<Point>(); // to R10
		points.add(new Point(869, 795));
		points.add(new Point(863, 795));
		lines.add(points);

		points = new LinkedList<Point>(); // to R11
		points.add(new Point(869, 1017));
		points.add(new Point(863, 1017));
		lines.add(points);

		points = new LinkedList<Point>(); // to R12
		points.add(new Point(869, 1242));
		points.add(new Point(863, 1242));
		lines.add(points);

		points = new LinkedList<Point>(); // to R13
		points.add(new Point(869, 1462));
		points.add(new Point(863, 1462));
		lines.add(points);

		points = new LinkedList<Point>(); // to R14
		points.add(new Point(869, 1681));
		points.add(new Point(862, 1681));
		lines.add(points);

		points = new LinkedList<Point>(); // to R15
		points.add(new Point(869, 1904));
		points.add(new Point(862, 1904));
		lines.add(points);
		signals.put("rdGPR2", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_0
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(101, 248));
		points.add(new Point(113, 248));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(106, 248));
		points.add(new Point(106, 310));
		points.add(new Point(166, 310));
		lines.add(points);
		signals.put("GPRADDR1_0", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_1
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(190, 501));
		points.add(new Point(202, 501));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(195, 501));
		points.add(new Point(195, 543));
		points.add(new Point(167, 543));
		points.add(new Point(167, 559));
		points.add(new Point(174, 559));
		lines.add(points);
		signals.put("GPRADDR1_1", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_2
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(195, 856));
		points.add(new Point(207, 856));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(200, 856));
		points.add(new Point(200, 907));
		points.add(new Point(239, 907));
		lines.add(points);
		signals.put("GPRADDR1_2", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_3
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(192, 1079));
		points.add(new Point(204, 1079));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(197, 1079));
		points.add(new Point(197, 1129));
		points.add(new Point(239, 1129));
		lines.add(points);
		signals.put("GPRADDR1_3", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_4
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(192, 1304));
		points.add(new Point(204, 1304));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(197, 1304));
		points.add(new Point(197, 1354));
		points.add(new Point(239, 1354));
		lines.add(points);
		signals.put("GPRADDR1_4", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_5
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(192, 1524));
		points.add(new Point(204, 1524));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(197, 1524));
		points.add(new Point(197, 1574));
		points.add(new Point(239, 1574));
		lines.add(points);
		signals.put("GPRADDR1_5", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_6
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(190, 1741));
		points.add(new Point(202, 1741));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(195, 1741));
		points.add(new Point(195, 1793));
		points.add(new Point(239, 1793));
		lines.add(points);
		signals.put("GPRADDR1_6", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_7
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(190, 1964));
		points.add(new Point(202, 1964));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(195, 1964));
		points.add(new Point(195, 2016));
		points.add(new Point(239, 2016));
		lines.add(points);
		signals.put("GPRADDR1_7", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_8
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 264));
		points.add(new Point(1121, 264));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 264));
		points.add(new Point(1128, 316));
		points.add(new Point(1084, 316));
		lines.add(points);
		signals.put("GPRADDR1_8", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_9
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 483));
		points.add(new Point(1121, 483));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 483));
		points.add(new Point(1128, 535));
		points.add(new Point(1084, 535));
		lines.add(points);
		signals.put("GPRADDR1_9", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_10
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 702));
		points.add(new Point(1121, 702));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 702));
		points.add(new Point(1128, 754));
		points.add(new Point(1084, 754));
		lines.add(points);
		signals.put("GPRADDR1_10", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_11
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 925));
		points.add(new Point(1121, 925));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 925));
		points.add(new Point(1128, 977));
		points.add(new Point(1084, 977));
		lines.add(points);
		signals.put("GPRADDR1_11", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_12
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 1150));
		points.add(new Point(1121, 1150));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 1150));
		points.add(new Point(1128, 1202));
		points.add(new Point(1084, 1202));
		lines.add(points);
		signals.put("GPRADDR1_12", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_13
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 1369));
		points.add(new Point(1121, 1369));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 1369));
		points.add(new Point(1128, 1421));
		points.add(new Point(1084, 1421));
		lines.add(points);
		signals.put("GPRADDR1_13", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_14
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 1589));
		points.add(new Point(1121, 1589));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 1589));
		points.add(new Point(1128, 1641));
		points.add(new Point(1083, 1641));
		lines.add(points);
		signals.put("GPRADDR1_14", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR1_15
		points = new LinkedList<Point>(); // to wr
		points.add(new Point(1133, 1811));
		points.add(new Point(1121, 1811));
		lines.add(points);

		points = new LinkedList<Point>(); // to rd
		points.add(new Point(1128, 1811));
		points.add(new Point(1128, 1863));
		points.add(new Point(1083, 1863));
		lines.add(points);
		signals.put("GPRADDR1_15", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_0
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(295, 414));
		points.add(new Point(301, 414));
		lines.add(points);
		signals.put("GPRADDR2_0", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_1
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(407, 694));
		points.add(new Point(414, 694));
		lines.add(points);
		signals.put("GPRADDR2_1", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_2
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(454, 936));
		points.add(new Point(460, 936));
		lines.add(points);
		signals.put("GPRADDR2_2", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_3
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(454, 1158));
		points.add(new Point(460, 1158));
		lines.add(points);
		signals.put("GPRADDR2_3", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_4
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(454, 1383));
		points.add(new Point(460, 1383));
		lines.add(points);
		signals.put("GPRADDR2_4", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_5
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(454, 1603));
		points.add(new Point(460, 1603));
		lines.add(points);
		signals.put("GPRADDR2_5", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_6
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(454, 1822));
		points.add(new Point(460, 1822));
		lines.add(points);
		signals.put("GPRADDR2_6", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_7
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(454, 2045));
		points.add(new Point(460, 2045));
		lines.add(points);
		signals.put("GPRADDR2_7", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_8
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 345));
		points.add(new Point(863, 345));
		lines.add(points);
		signals.put("GPRADDR2_8", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_9
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 564));
		points.add(new Point(863, 564));
		lines.add(points);
		signals.put("GPRADDR2_9", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_10
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 783));
		points.add(new Point(863, 783));
		lines.add(points);
		signals.put("GPRADDR2_10", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_11
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 1006));
		points.add(new Point(863, 1006));
		lines.add(points);
		signals.put("GPRADDR2_11", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_12
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 1231));
		points.add(new Point(863, 1231));
		lines.add(points);
		signals.put("GPRADDR2_12", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_13
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 1450));
		points.add(new Point(863, 1450));
		lines.add(points);
		signals.put("GPRADDR2_13", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_14
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 1670));
		points.add(new Point(862, 1670));
		lines.add(points);
		signals.put("GPRADDR2_14", lines);

		lines = new LinkedList<List<Point>>(); // GPRADDR2_15
		points = new LinkedList<Point>(); // to rd
		points.add(new Point(869, 1892));
		points.add(new Point(862, 1892));
		lines.add(points);
		signals.put("GPRADDR2_15", lines);

		lines = new LinkedList<List<Point>>(); // wrAND0
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(145, 242));
		points.add(new Point(201, 242));
		lines.add(points);
		signals.put("PCand1", lines);

		lines = new LinkedList<List<Point>>(); // wrAND1
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(233, 495));
		points.add(new Point(264, 495));
		lines.add(points);
		signals.put("SPand1", lines);

		lines = new LinkedList<List<Point>>(); // wrAND2
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(238, 851));
		points.add(new Point(264, 851));
		lines.add(points);
		signals.put("first2", lines);

		lines = new LinkedList<List<Point>>(); // wrAND3
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(235, 1073));
		points.add(new Point(264, 1073));
		lines.add(points);
		signals.put("first3", lines);

		lines = new LinkedList<List<Point>>(); // wrAND4
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(235, 1298));
		points.add(new Point(264, 1298));
		lines.add(points);
		signals.put("first4", lines);

		lines = new LinkedList<List<Point>>(); // wrAND5
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(235, 1518));
		points.add(new Point(264, 1518));
		lines.add(points);
		signals.put("first5", lines);

		lines = new LinkedList<List<Point>>(); // wrAND6
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(233, 1736));
		points.add(new Point(264, 1736));
		lines.add(points);
		signals.put("first6", lines);

		lines = new LinkedList<List<Point>>(); // wrAND7
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(233, 1958));
		points.add(new Point(264, 1958));
		lines.add(points);
		signals.put("first7", lines);

		lines = new LinkedList<List<Point>>(); // wrAND8
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 258));
		points.add(new Point(1059, 258));
		lines.add(points);
		signals.put("first8", lines);

		lines = new LinkedList<List<Point>>(); // wrAND9
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 478));
		points.add(new Point(1059, 478));
		lines.add(points);
		signals.put("first9", lines);

		lines = new LinkedList<List<Point>>(); // wrAND10
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 696));
		points.add(new Point(1059, 696));
		lines.add(points);
		signals.put("first10", lines);

		lines = new LinkedList<List<Point>>(); // wrAND11
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 919));
		points.add(new Point(1059, 919));
		lines.add(points);
		signals.put("first11", lines);

		lines = new LinkedList<List<Point>>(); // wrAND12
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 1144));
		points.add(new Point(1059, 1144));
		lines.add(points);
		signals.put("first12", lines);

		lines = new LinkedList<List<Point>>(); // wrAND13
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 1364));
		points.add(new Point(1059, 1364));
		lines.add(points);
		signals.put("first13", lines);

		lines = new LinkedList<List<Point>>(); // wrAND14
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 1583));
		points.add(new Point(1059, 1583));
		lines.add(points);
		signals.put("first14", lines);

		lines = new LinkedList<List<Point>>(); // wrAND15
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(1090, 1805));
		points.add(new Point(1059, 1805));
		lines.add(points);
		signals.put("first15", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND0
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(198, 316));
		points.add(new Point(271, 316));
		lines.add(points);
		signals.put("PCand2", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND1
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(206, 564));
		points.add(new Point(279, 564));
		lines.add(points);
		signals.put("SPand2", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND2
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(271, 913));
		points.add(new Point(331, 913));
		lines.add(points);
		signals.put("second2", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND3
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(271, 1135));
		points.add(new Point(331, 1135));
		lines.add(points);
		signals.put("second3", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND4
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(271, 1360));
		points.add(new Point(331, 1360));
		lines.add(points);
		signals.put("second4", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND5
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(271, 1580));
		points.add(new Point(331, 1580));
		lines.add(points);
		signals.put("second5", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND6
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(271, 1799));
		points.add(new Point(331, 1799));
		lines.add(points);
		signals.put("second6", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND7
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(271, 2021));
		points.add(new Point(331, 2021));
		lines.add(points);
		signals.put("second7", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND8
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 322));
		points.add(new Point(992, 322));
		lines.add(points);
		signals.put("second8", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND9
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 541));
		points.add(new Point(992, 541));
		lines.add(points);
		signals.put("second9", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND10
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 760));
		points.add(new Point(992, 760));
		lines.add(points);
		signals.put("second10", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND11
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 982));
		points.add(new Point(992, 982));
		lines.add(points);
		signals.put("second11", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND12
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 1208));
		points.add(new Point(992, 1208));
		lines.add(points);
		signals.put("second12", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND13
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 1427));
		points.add(new Point(992, 1427));
		lines.add(points);
		signals.put("second13", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND14
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 1647));
		points.add(new Point(992, 1647));
		lines.add(points);
		signals.put("second14", lines);

		lines = new LinkedList<List<Point>>(); // rd1AND15
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(1052, 1869));
		points.add(new Point(992, 1869));
		lines.add(points);
		signals.put("second15", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND0
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(333, 420));
		points.add(new Point(417, 420));
		lines.add(points);
		signals.put("PCand3", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND1
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(446, 700));
		points.add(new Point(469, 700));
		lines.add(points);
		signals.put("SPand3", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND2
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(492, 942));
		points.add(new Point(516, 942));
		lines.add(points);
		signals.put("third2", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND3
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(492, 1164));
		points.add(new Point(516, 1164));
		lines.add(points);
		signals.put("third3", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND4
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(492, 1389));
		points.add(new Point(516, 1389));
		lines.add(points);
		signals.put("third4", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND5
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(492, 1609));
		points.add(new Point(516, 1609));
		lines.add(points);
		signals.put("third5", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND6
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(492, 1830));
		points.add(new Point(516, 1830));
		lines.add(points);
		signals.put("third6", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND7
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(492, 2050));
		points.add(new Point(516, 2050));
		lines.add(points);
		signals.put("third7", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND8
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 350));
		points.add(new Point(807, 350));
		lines.add(points);
		signals.put("third8", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND9
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 570));
		points.add(new Point(807, 570));
		lines.add(points);
		signals.put("third9", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND10
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 789));
		points.add(new Point(807, 789));
		lines.add(points);
		signals.put("third10", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND11
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 1011));
		points.add(new Point(807, 1011));
		lines.add(points);
		signals.put("third11", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND12
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 1237));
		points.add(new Point(807, 1237));
		lines.add(points);
		signals.put("third12", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND13
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 1456));
		points.add(new Point(807, 1456));
		lines.add(points);
		signals.put("third13", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND14
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 1676));
		points.add(new Point(807, 1676));
		lines.add(points);
		signals.put("third14", lines);

		lines = new LinkedList<List<Point>>(); // rd2AND15
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(831, 1898));
		points.add(new Point(807, 1898));
		lines.add(points);
		signals.put("third15", lines);

		lines = new LinkedList<List<Point>>(); // ldPC
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(192, 254));
		points.add(new Point(201, 254));
		lines.add(points);
		signals.put("ldPC", lines);

		lines = new LinkedList<List<Point>>(); // wrPCOR
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(230, 248));
		points.add(new Point(258, 248));
		lines.add(points);
		signals.put("pcload", lines);

		lines = new LinkedList<List<Point>>(); // incPC
		points = new LinkedList<Point>(); // to INC4
		points.add(new Point(238, 271));
		points.add(new Point(258, 271));
		lines.add(points);
		signals.put("incPC", lines);

		lines = new LinkedList<List<Point>>(); // PCout1
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(262, 305));
		points.add(new Point(271, 305));
		lines.add(points);
		signals.put("PCout1", lines);

		lines = new LinkedList<List<Point>>(); // rdPCOR1
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(300, 310));
		points.add(new Point(324, 310));
		lines.add(points);
		signals.put("pcspout0", lines);

		lines = new LinkedList<List<Point>>(); // PCout2
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(408, 409));
		points.add(new Point(417, 409));
		lines.add(points);
		signals.put("PCout2", lines);

		lines = new LinkedList<List<Point>>(); // rdPCOR2
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(445, 414));
		points.add(new Point(469, 414));
		lines.add(points);
		signals.put("pcout2", lines);

		lines = new LinkedList<List<Point>>(); // incSP
		points = new LinkedList<Point>(); // to INC4
		points.add(new Point(239, 518));
		points.add(new Point(264, 518));
		lines.add(points);
		signals.put("incSP", lines);

		lines = new LinkedList<List<Point>>(); // decSP
		points = new LinkedList<Point>(); // to DEC4
		points.add(new Point(438, 518));
		points.add(new Point(417, 518));
		lines.add(points);
		signals.put("decSP", lines);

		lines = new LinkedList<List<Point>>(); // SPout1
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(269, 553));
		points.add(new Point(279, 553));
		lines.add(points);
		signals.put("SPout1", lines);

		lines = new LinkedList<List<Point>>(); // rd1SPOR
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(307, 559));
		points.add(new Point(331, 559));
		lines.add(points);
		signals.put("pcspout1", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to R0
		points.add(new Point(421, 258));
		points.add(new Point(411, 258));
		lines.add(points);

		points = new LinkedList<Point>(); // to R1
		points.add(new Point(427, 496));
		points.add(new Point(417, 496));
		lines.add(points);

		points = new LinkedList<Point>(); // to R2
		points.add(new Point(427, 860));
		points.add(new Point(417, 860));
		lines.add(points);

		points = new LinkedList<Point>(); // to R3
		points.add(new Point(427, 1083));
		points.add(new Point(417, 1083));
		lines.add(points);

		points = new LinkedList<Point>(); // to R4
		points.add(new Point(427, 1309));
		points.add(new Point(417, 1309));
		lines.add(points);

		points = new LinkedList<Point>(); // to R5
		points.add(new Point(427, 1528));
		points.add(new Point(417, 1528));
		lines.add(points);

		points = new LinkedList<Point>(); // to R6
		points.add(new Point(427, 1747));
		points.add(new Point(417, 1747));
		lines.add(points);

		points = new LinkedList<Point>(); // to R7
		points.add(new Point(427, 1969));
		points.add(new Point(417, 1969));
		lines.add(points);

		points = new LinkedList<Point>(); // to R8
		points.add(new Point(906, 269));
		points.add(new Point(896, 269));
		lines.add(points);

		points = new LinkedList<Point>(); // to R9
		points.add(new Point(906, 489));
		points.add(new Point(896, 489));
		lines.add(points);

		points = new LinkedList<Point>(); // to R10
		points.add(new Point(906, 708));
		points.add(new Point(896, 708));
		lines.add(points);

		points = new LinkedList<Point>(); // to R11
		points.add(new Point(906, 930));
		points.add(new Point(896, 930));
		lines.add(points);

		points = new LinkedList<Point>(); // to R12
		points.add(new Point(906, 1156));
		points.add(new Point(896, 1156));
		lines.add(points);

		points = new LinkedList<Point>(); // to R13
		points.add(new Point(906, 1375));
		points.add(new Point(896, 1375));
		lines.add(points);

		points = new LinkedList<Point>(); // to R14
		points.add(new Point(906, 1595));
		points.add(new Point(896, 1595));
		lines.add(points);

		points = new LinkedList<Point>(); // to R15
		points.add(new Point(906, 1817));
		points.add(new Point(896, 1817));
		lines.add(points);
		signals.put("CLK", lines);

		return addr2Signals = signals;
	}

	public HashMap<String, List<List<Point>>> getArbmodSignals() {

		if (arbmodSignals != null)
			return arbmodSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // BUSYBUS
		points = new LinkedList<Point>(); // main line
		points.add(new Point(381, 42));
		points.add(new Point(740, 42));
		lines.add(points);

		points = new LinkedList<Point>(); // to negator
		points.add(new Point(442, 42));
		points.add(new Point(442, 74));
		points.add(new Point(283, 74));
		points.add(new Point(283, 111));
		points.add(new Point(301, 111));
		lines.add(points);

		points = new LinkedList<Point>(); // from busy buffer
		points.add(new Point(575, 104));
		points.add(new Point(651, 104));
		points.add(new Point(651, 42));
		lines.add(points);
		signals.put("BUSYBUS", lines);

		lines = new LinkedList<List<Point>>(); // BUSYBUS inverted
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(337, 111));
		points.add(new Point(369, 111));
		lines.add(points);
		signals.put("CPUBUSYBUSinv", lines);

		lines = new LinkedList<List<Point>>(); // BUSYBUS inverted inverted
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(405, 111));
		points.add(new Point(411, 111));
		points.add(new Point(411, 183));
		points.add(new Point(367, 183));
		points.add(new Point(367, 196));
		points.add(new Point(381, 196));
		lines.add(points);
		signals.put("CPUBUSYBUSinvinv", lines);

		lines = new LinkedList<List<Point>>(); // BR
		points = new LinkedList<Point>(); // from FF0 to AND
		points.add(new Point(181, 201));
		points.add(new Point(320, 201));
		lines.add(points);

		points = new LinkedList<Point>(); // to outside
		points.add(new Point(206, 201));
		points.add(new Point(206, 341));
		lines.add(points);

		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(206, 271));
		points.add(new Point(214, 271));
		lines.add(points);
		signals.put("CPU_BR", lines);

		lines = new LinkedList<List<Point>>(); // BG_IN
		points = new LinkedList<Point>(); // to FF1 AND
		points.add(new Point(263, 345));
		points.add(new Point(263, 213));
		points.add(new Point(320, 213));
		lines.add(points);

		points = new LinkedList<Point>(); // to BG_OUT AND
		points.add(new Point(263, 282));
		points.add(new Point(306, 282));
		lines.add(points);
		signals.put("CPU_BG_IN", lines);

		lines = new LinkedList<List<Point>>(); // BRneg
		points = new LinkedList<Point>(); // from AND
		points.add(new Point(250, 271));
		points.add(new Point(306, 271));
		lines.add(points);
		signals.put("CPUBRNeg", lines);

		lines = new LinkedList<List<Point>>(); // BG_OUT
		points = new LinkedList<Point>(); // from AND
		points.add(new Point(338, 277));
		points.add(new Point(357, 277));
		points.add(new Point(357, 343));
		lines.add(points);
		signals.put("CPU_BG_OUT", lines);

		lines = new LinkedList<List<Point>>(); // BRandBG_IN
		points = new LinkedList<Point>(); // between ANDs
		points.add(new Point(352, 207));
		points.add(new Point(381, 207));
		lines.add(points);
		signals.put("CPUBRandBG_IN", lines);

		lines = new LinkedList<List<Point>>(); // FF1S
		points = new LinkedList<Point>(); // to S
		points.add(new Point(412, 202));
		points.add(new Point(444, 202));
		lines.add(points);
		signals.put("CPUFF1S", lines);

		lines = new LinkedList<List<Point>>(); // FF1Q
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(541, 203));
		points.add(new Point(553, 203));
		points.add(new Point(553, 112));
		lines.add(points);
		signals.put("CPUFF1Q", lines);

		lines = new LinkedList<List<Point>>(); // hreq0
		points = new LinkedList<Point>(); // to FF0
		points.add(new Point(58, 199));
		points.add(new Point(84, 199));
		lines.add(points);
		signals.put("hreq0", lines);

		lines = new LinkedList<List<Point>>(); // hclr0
		points = new LinkedList<Point>(); // to FF0
		points.add(new Point(57, 273));
		points.add(new Point(84, 273));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF1
		points.add(new Point(418, 276));
		points.add(new Point(444, 276));
		lines.add(points);
		signals.put("hclr0", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to busy buffer
		points.add(new Point(523, 104));
		points.add(new Point(540, 104));
		lines.add(points);
		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to busy buffer
		points.add(new Point(74, 237));
		points.add(new Point(84, 237));
		lines.add(points);

		points = new LinkedList<Point>(); // to busy buffer
		points.add(new Point(434, 240));
		points.add(new Point(444, 240));
		lines.add(points);
		signals.put("CLK", lines);

		return arbmodSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getArbmodIOSignals() {

		if (arbmodIOSignals != null)
			return arbmodIOSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // BUSYBUS
		points = new LinkedList<Point>(); // main line
		points.add(new Point(381, 42));
		points.add(new Point(740, 42));
		lines.add(points);

		points = new LinkedList<Point>(); // to negator
		points.add(new Point(442, 42));
		points.add(new Point(442, 74));
		points.add(new Point(283, 74));
		points.add(new Point(283, 111));
		points.add(new Point(301, 111));
		lines.add(points);

		points = new LinkedList<Point>(); // from busy buffer
		points.add(new Point(575, 104));
		points.add(new Point(651, 104));
		points.add(new Point(651, 42));
		lines.add(points);
		signals.put("BUSYBUS", lines);

		lines = new LinkedList<List<Point>>(); // BUSYBUS inverted
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(337, 111));
		points.add(new Point(369, 111));
		lines.add(points);
		signals.put("IOBUSYBUSinv", lines);

		lines = new LinkedList<List<Point>>(); // BUSYBUS inverted inverted
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(405, 111));
		points.add(new Point(411, 111));
		points.add(new Point(411, 183));
		points.add(new Point(367, 183));
		points.add(new Point(367, 196));
		points.add(new Point(381, 196));
		lines.add(points);
		signals.put("IOBUSYBUSinvinv", lines);

		lines = new LinkedList<List<Point>>(); // BR
		points = new LinkedList<Point>(); // from FF0 to AND
		points.add(new Point(181, 201));
		points.add(new Point(320, 201));
		lines.add(points);

		points = new LinkedList<Point>(); // to outside
		points.add(new Point(206, 201));
		points.add(new Point(206, 341));
		lines.add(points);

		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(206, 271));
		points.add(new Point(214, 271));
		lines.add(points);
		signals.put("IO_BR", lines);

		lines = new LinkedList<List<Point>>(); // BG_IN
		points = new LinkedList<Point>(); // to FF1 AND
		points.add(new Point(263, 345));
		points.add(new Point(263, 213));
		points.add(new Point(320, 213));
		lines.add(points);

		points = new LinkedList<Point>(); // to BG_OUT AND
		points.add(new Point(263, 282));
		points.add(new Point(306, 282));
		lines.add(points);
		signals.put("IO_BG_IN", lines);

		lines = new LinkedList<List<Point>>(); // BRneg
		points = new LinkedList<Point>(); // from AND
		points.add(new Point(250, 271));
		points.add(new Point(306, 271));
		lines.add(points);
		signals.put("IOBRNeg", lines);

		lines = new LinkedList<List<Point>>(); // BG_OUT
		points = new LinkedList<Point>(); // from AND
		points.add(new Point(338, 277));
		points.add(new Point(357, 277));
		points.add(new Point(357, 343));
		lines.add(points);
		signals.put("IO_BG_OUT", lines);

		lines = new LinkedList<List<Point>>(); // BRandBG_IN
		points = new LinkedList<Point>(); // between ANDs
		points.add(new Point(352, 207));
		points.add(new Point(381, 207));
		lines.add(points);
		signals.put("IOBRandBG_IN", lines);

		lines = new LinkedList<List<Point>>(); // FF1S
		points = new LinkedList<Point>(); // to S
		points.add(new Point(412, 202));
		points.add(new Point(444, 202));
		lines.add(points);
		signals.put("IOFF1S", lines);

		lines = new LinkedList<List<Point>>(); // FF1Q
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(541, 203));
		points.add(new Point(553, 203));
		points.add(new Point(553, 112));
		lines.add(points);
		signals.put("IOFF1Q", lines);

		lines = new LinkedList<List<Point>>(); // hreq1
		points = new LinkedList<Point>(); // to FF0
		points.add(new Point(58, 199));
		points.add(new Point(84, 199));
		lines.add(points);
		signals.put("hreq1", lines);

		lines = new LinkedList<List<Point>>(); // hclr1
		points = new LinkedList<Point>(); // to FF0
		points.add(new Point(57, 273));
		points.add(new Point(84, 273));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF1
		points.add(new Point(418, 276));
		points.add(new Point(444, 276));
		lines.add(points);
		signals.put("hclr1", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to busy buffer
		points.add(new Point(523, 104));
		points.add(new Point(540, 104));
		lines.add(points);
		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to busy buffer
		points.add(new Point(74, 237));
		points.add(new Point(84, 237));
		lines.add(points);

		points = new LinkedList<Point>(); // to busy buffer
		points.add(new Point(434, 240));
		points.add(new Point(444, 240));
		lines.add(points);
		signals.put("CLK", lines);

		return arbmodIOSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getArbSignals() {

		if (arbSignals != null)
			return arbSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // BUSYBUS
		points = new LinkedList<Point>(); // main line
		points.add(new Point(72, 31));
		points.add(new Point(722, 31));
		lines.add(points);

		points = new LinkedList<Point>(); // to CPU
		points.add(new Point(313, 31));
		points.add(new Point(313, 77));
		lines.add(points);

		points = new LinkedList<Point>(); // to IO
		points.add(new Point(475, 31));
		points.add(new Point(475, 77));
		lines.add(points);
		signals.put("BUSYBUS", lines);

		lines = new LinkedList<List<Point>>(); // BR
		points = new LinkedList<Point>(); // from CPU
		points.add(new Point(281, 177));
		points.add(new Point(281, 254));
		points.add(new Point(353, 254));
		points.add(new Point(353, 275));
		lines.add(points);
		signals.put("CPU_BR", lines);

		lines = new LinkedList<List<Point>>(); // IO BR
		points = new LinkedList<Point>(); // from IO
		points.add(new Point(443, 177));
		points.add(new Point(443, 275));
		lines.add(points);
		signals.put("IO_BR", lines);

		lines = new LinkedList<List<Point>>(); // CD0 out signal
		points = new LinkedList<Point>(); // to DC2
		points.add(new Point(400, 364));
		points.add(new Point(400, 448));
		lines.add(points);
		signals.put("CD0", lines);

		lines = new LinkedList<List<Point>>(); // WtoE
		points = new LinkedList<Point>(); // from W to E
		points.add(new Point(254, 336));
		points.add(new Point(245, 336));
		points.add(new Point(245, 492));
		points.add(new Point(254, 492));
		lines.add(points);
		signals.put("WtoE", lines);

		lines = new LinkedList<List<Point>>(); // CPU_BG_IN
		points = new LinkedList<Point>(); // to CPU
		points.add(new Point(354, 537));
		points.add(new Point(354, 548));
		points.add(new Point(181, 548));
		points.add(new Point(181, 266));
		points.add(new Point(333, 266));
		points.add(new Point(333, 177));
		lines.add(points);
		signals.put("CPU_BG_IN", lines);

		lines = new LinkedList<List<Point>>(); // IO BG_IN
		points = new LinkedList<Point>(); // to IO
		points.add(new Point(428, 537));
		points.add(new Point(428, 564));
		points.add(new Point(157, 564));
		points.add(new Point(157, 238));
		points.add(new Point(495, 238));
		points.add(new Point(495, 177));
		lines.add(points);
		signals.put("IO_BG_IN", lines);

		lines = new LinkedList<List<Point>>(); // BG_OUT
		points = new LinkedList<Point>(); // from CPU
		points.add(new Point(370, 177));
		points.add(new Point(370, 187));
		lines.add(points);
		signals.put("CPU_BG_OUT", lines);

		lines = new LinkedList<List<Point>>(); // IO BG_OUT
		points = new LinkedList<Point>(); // from IO
		points.add(new Point(532, 177));
		points.add(new Point(532, 187));
		lines.add(points);
		signals.put("IO_BG_OUT", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to DC2
		points.add(new Point(236, 320));
		points.add(new Point(254, 320));
		lines.add(points);
		signals.put("1", lines);

		return arbSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getExec1Signals() {

		if (exec1Signals != null)
			return exec1Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(957, 35));
		points.add(new Point(957, 1393));
		lines.add(points);

		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(957, 317));
		points.add(new Point(457, 317));
		points.add(new Point(457, 835));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(901, 90));
		points.add(new Point(901, 1450));
		lines.add(points);

		points = new LinkedList<Point>(); // to ADD
		points.add(new Point(901, 415));
		points.add(new Point(528, 415));
		points.add(new Point(528, 665));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(844, 140));
		points.add(new Point(844, 1510));
		lines.add(points);

		points = new LinkedList<Point>(); // from ADDout buffer
		points.add(new Point(731, 763));
		points.add(new Point(844, 763));
		lines.add(points);

		points = new LinkedList<Point>(); // from ALUout buffer
		points.add(new Point(670, 1091));
		points.add(new Point(844, 1091));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // IR17_0
		points = new LinkedList<Point>(); // to MX3
		points.add(new Point(600, 507));
		points.add(new Point(600, 521));
		lines.add(points);
		signals.put("IR17..0", lines);

		lines = new LinkedList<List<Point>>(); // MX3 out signal
		points = new LinkedList<Point>(); // to ADD
		points.add(new Point(619, 585));
		points.add(new Point(619, 625));
		points.add(new Point(575, 625));
		points.add(new Point(575, 665));
		lines.add(points);
		signals.put("MX3", lines);

		lines = new LinkedList<List<Point>>(); // ADD out signal
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(552, 714));
		points.add(new Point(552, 835));
		lines.add(points);

		points = new LinkedList<Point>(); // to ADDout buffer
		points.add(new Point(552, 763));
		points.add(new Point(700, 763));
		lines.add(points);
		signals.put("ADDComponent", lines);

		lines = new LinkedList<List<Point>>(); // ALU out signal
		points = new LinkedList<Point>(); // to ALUout buffer
		points.add(new Point(504, 969));
		points.add(new Point(504, 1091));
		points.add(new Point(639, 1091));
		lines.add(points);
		signals.put("ALU", lines);

		lines = new LinkedList<List<Point>>(); // mxSIG
		points = new LinkedList<Point>(); // to MX3
		points.add(new Point(674, 556));
		points.add(new Point(661, 556));
		lines.add(points);
		signals.put("mxSIG", lines);

		lines = new LinkedList<List<Point>>(); // ADDout
		points = new LinkedList<Point>(); // to ADDout buffer
		points.add(new Point(714, 789));
		points.add(new Point(714, 771));
		lines.add(points);
		signals.put("ADDout", lines);

		lines = new LinkedList<List<Point>>(); // ALUout
		points = new LinkedList<Point>(); // to ALUout buffer
		points.add(new Point(653, 1117));
		points.add(new Point(653, 1100));
		lines.add(points);
		signals.put("ALUout", lines);

		lines = new LinkedList<List<Point>>(); // add
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(655, 863));
		points.add(new Point(637, 863));
		lines.add(points);
		signals.put("add", lines);

		lines = new LinkedList<List<Point>>(); // sub
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(655, 889));
		points.add(new Point(637, 889));
		lines.add(points);
		signals.put("sub", lines);
		
		lines = new LinkedList<List<Point>>(); // shl
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(655, 915));
		points.add(new Point(637, 915));
		lines.add(points);
		signals.put("shl", lines);

		lines = new LinkedList<List<Point>>(); // shr
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(655, 943));
		points.add(new Point(637, 943));
		lines.add(points);
		signals.put("shr", lines);

		lines = new LinkedList<List<Point>>(); // not
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(354, 871));
		points.add(new Point(371, 871));
		lines.add(points);
		signals.put("not", lines);

		lines = new LinkedList<List<Point>>(); // or
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(354, 899));
		points.add(new Point(371, 899));
		lines.add(points);
		signals.put("or", lines);

		lines = new LinkedList<List<Point>>(); // and
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(354, 927));
		points.add(new Point(371, 927));
		lines.add(points);
		signals.put("and", lines);
		
		lines = new LinkedList<List<Point>>(); // C0
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(391, 987));
		points.add(new Point(391, 969));
		lines.add(points);
		signals.put("C0", lines);

		lines = new LinkedList<List<Point>>(); // C32
		points = new LinkedList<Point>(); // to ALU
		points.add(new Point(617, 987));
		points.add(new Point(617, 969));
		lines.add(points);
		signals.put("CMAX", lines);

		lines = new LinkedList<List<Point>>(); // MDR17_13
		points = new LinkedList<Point>(); // to CNTSHIFT
		points.add(new Point(74, 637));
		points.add(new Point(74, 683));
		lines.add(points);
		signals.put("MDR17..13", lines);

		lines = new LinkedList<List<Point>>(); // CNTSHIFT out signal
		points = new LinkedList<Point>(); // to CMP0
		points.add(new Point(80, 723));
		points.add(new Point(80, 836));
		lines.add(points);
		signals.put("CNTSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // noSHIFT
		points = new LinkedList<Point>(); // from CMP0
		points.add(new Point(111, 895));
		points.add(new Point(111, 937));
		lines.add(points);
		signals.put("noSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // ldCNTSHIFT
		points = new LinkedList<Point>(); // to CNTSHIFT
		points.add(new Point(171, 695));
		points.add(new Point(144, 695));
		lines.add(points);
		signals.put("ldCNTSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // decCNTSHIFT
		points = new LinkedList<Point>(); // to CNTSHIFT
		points.add(new Point(172, 713));
		points.add(new Point(144, 713));
		lines.add(points);
		signals.put("decCNTSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to CNTSHIFT
		points.add(new Point(636, 506));
		points.add(new Point(636, 521));
		lines.add(points);

		points = new LinkedList<Point>(); // to CMP0
		points.add(new Point(224, 771));
		points.add(new Point(224, 805));
		points.add(new Point(149, 805));
		points.add(new Point(149, 836));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to CNTSHIFT
		points.add(new Point(0, 694));
		points.add(new Point(6, 694));
		lines.add(points);
		signals.put("CLK", lines);

		return exec1Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getExec2Signals() {

		if (exec2Signals != null)
			return exec2Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(638, 47));
		points.add(new Point(638, 2104));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_0
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 175));
		points.add(new Point(818, 175));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(811, 175));
		points.add(new Point(811, 106));
		points.add(new Point(823, 106));
		lines.add(points);
		signals.put("IBUS1_0", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_1
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 364));
		points.add(new Point(818, 364));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(811, 364));
		points.add(new Point(811, 295));
		points.add(new Point(823, 295));
		lines.add(points);
		signals.put("IBUS1_1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_2
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 553));
		points.add(new Point(812, 553));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(807, 553));
		points.add(new Point(807, 484));
		points.add(new Point(823, 484));
		lines.add(points);
		signals.put("IBUS1_2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_3
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 742));
		points.add(new Point(818, 742));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(811, 742));
		points.add(new Point(811, 673));
		points.add(new Point(823, 673));
		lines.add(points);
		signals.put("IBUS1_3", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_4
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 929));
		points.add(new Point(818, 929));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(811, 929));
		points.add(new Point(811, 861));
		points.add(new Point(823, 861));
		lines.add(points);
		signals.put("IBUS1_4", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_5
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 1118));
		points.add(new Point(818, 1118));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(811, 1118));
		points.add(new Point(811, 1049));
		points.add(new Point(823, 1049));
		lines.add(points);
		signals.put("IBUS1_5", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_6
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 1307));
		points.add(new Point(821, 1307));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(814, 1307));
		points.add(new Point(814, 1238));
		points.add(new Point(826, 1238));
		lines.add(points);
		signals.put("IBUS1_6", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_14
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 1505));
		points.add(new Point(821, 1505));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(814, 1505));
		points.add(new Point(814, 1436));
		points.add(new Point(826, 1436));
		lines.add(points);
		signals.put("IBUS1_14", lines);

		lines = new LinkedList<List<Point>>(); // IBUS1_15
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(639, 1684));
		points.add(new Point(821, 1684));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(814, 1684));
		points.add(new Point(814, 1615));
		points.add(new Point(826, 1615));
		lines.add(points);
		signals.put("IBUS1_15", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(592, 93));
		points.add(new Point(592, 2151));
		lines.add(points);

		points = new LinkedList<Point>(); // from PSWout
		points.add(new Point(212, 1048));
		points.add(new Point(592, 1048));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(543, 142));
		points.add(new Point(543, 2152));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // ldPSW
		points = new LinkedList<Point>(); // main line
		points.add(new Point(689, 28));
		points.add(new Point(689, 2161));
		lines.add(points);

		points = new LinkedList<Point>(); // to N, upper AND
		points.add(new Point(689, 95));
		points.add(new Point(823, 95));
		lines.add(points);

		points = new LinkedList<Point>(); // to N, lower AND
		points.add(new Point(789, 95));
		points.add(new Point(789, 164));
		points.add(new Point(823, 164));
		lines.add(points);

		points = new LinkedList<Point>(); // to Z, upper AND
		points.add(new Point(689, 284));
		points.add(new Point(823, 284));
		lines.add(points);

		points = new LinkedList<Point>(); // to Z, lower AND
		points.add(new Point(789, 284));
		points.add(new Point(789, 353));
		points.add(new Point(823, 353));
		lines.add(points);

		points = new LinkedList<Point>(); // to C, upper AND
		points.add(new Point(689, 473));
		points.add(new Point(823, 473));
		lines.add(points);

		points = new LinkedList<Point>(); // to C, lower AND
		points.add(new Point(789, 473));
		points.add(new Point(789, 541));
		points.add(new Point(818, 541));
		lines.add(points);

		points = new LinkedList<Point>(); // to V, upper AND
		points.add(new Point(689, 661));
		points.add(new Point(823, 661));
		lines.add(points);

		points = new LinkedList<Point>(); // to V, lower AND
		points.add(new Point(789, 661));
		points.add(new Point(789, 730));
		points.add(new Point(823, 730));
		lines.add(points);

		points = new LinkedList<Point>(); // to l0, upper AND
		points.add(new Point(689, 849));
		points.add(new Point(823, 849));
		lines.add(points);

		points = new LinkedList<Point>(); // to l0, lower AND
		points.add(new Point(789, 849));
		points.add(new Point(789, 918));
		points.add(new Point(823, 918));
		lines.add(points);

		points = new LinkedList<Point>(); // to l1, upper AND
		points.add(new Point(689, 1038));
		points.add(new Point(823, 1038));
		lines.add(points);

		points = new LinkedList<Point>(); // to l1, lower AND
		points.add(new Point(789, 1038));
		points.add(new Point(789, 1107));
		points.add(new Point(823, 1107));
		lines.add(points);

		points = new LinkedList<Point>(); // to l2, upper AND
		points.add(new Point(689, 1227));
		points.add(new Point(826, 1227));
		lines.add(points);

		points = new LinkedList<Point>(); // to l2, lower AND
		points.add(new Point(792, 1227));
		points.add(new Point(792, 1296));
		points.add(new Point(826, 1296));
		lines.add(points);

		points = new LinkedList<Point>(); // to I, upper AND
		points.add(new Point(689, 1425));
		points.add(new Point(826, 1425));
		lines.add(points);

		points = new LinkedList<Point>(); // to I, lower AND
		points.add(new Point(792, 1425));
		points.add(new Point(792, 1494));
		points.add(new Point(826, 1494));
		lines.add(points);

		points = new LinkedList<Point>(); // to T, upper AND
		points.add(new Point(689, 1604));
		points.add(new Point(826, 1604));
		lines.add(points);

		points = new LinkedList<Point>(); // to T, lower AND
		points.add(new Point(792, 1604));
		points.add(new Point(792, 1673));
		points.add(new Point(826, 1673));
		lines.add(points);
		signals.put("ldPSW", lines);

		lines = new LinkedList<List<Point>>(); // ldN
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(802, 76));
		points.add(new Point(910, 76));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(878, 76));
		points.add(new Point(878, 186));
		points.add(new Point(910, 186));
		lines.add(points);
		signals.put("ldN", lines);

		lines = new LinkedList<List<Point>>(); // N
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(969, 135));
		points.add(new Point(889, 135));
		points.add(new Point(889, 87));
		points.add(new Point(910, 87));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(889, 135));
		points.add(new Point(889, 197));
		points.add(new Point(905, 197));
		lines.add(points);
		signals.put("N", lines);

		lines = new LinkedList<List<Point>>(); // ldZ
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(806, 265));
		points.add(new Point(910, 265));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(880, 265));
		points.add(new Point(880, 374));
		points.add(new Point(910, 374));
		lines.add(points);
		signals.put("ldZ", lines);

		lines = new LinkedList<List<Point>>(); // Z
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(967, 325));
		points.add(new Point(889, 325));
		points.add(new Point(889, 276));
		points.add(new Point(910, 276));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(889, 325));
		points.add(new Point(889, 385));
		points.add(new Point(905, 385));
		lines.add(points);
		signals.put("Z", lines);

		lines = new LinkedList<List<Point>>(); // ldC
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(786, 454));
		points.add(new Point(910, 454));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(878, 454));
		points.add(new Point(878, 563));
		points.add(new Point(910, 563));
		lines.add(points);
		signals.put("ldC", lines);

		lines = new LinkedList<List<Point>>(); // C
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(969, 513));
		points.add(new Point(889, 513));
		points.add(new Point(889, 465));
		points.add(new Point(910, 465));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(889, 513));
		points.add(new Point(889, 574));
		points.add(new Point(905, 574));
		lines.add(points);
		signals.put("C", lines);

		lines = new LinkedList<List<Point>>(); // ldV
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(799, 642));
		points.add(new Point(910, 642));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(878, 642));
		points.add(new Point(878, 752));
		points.add(new Point(910, 752));
		lines.add(points);
		signals.put("ldV", lines);

		lines = new LinkedList<List<Point>>(); // V
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(969, 704));
		points.add(new Point(889, 704));
		points.add(new Point(889, 654));
		points.add(new Point(910, 654));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(889, 704));
		points.add(new Point(889, 763));
		points.add(new Point(905, 763));
		lines.add(points);
		signals.put("V", lines);

		lines = new LinkedList<List<Point>>(); // ldL
		points = new LinkedList<Point>(); // to L0, upper AND
		points.add(new Point(775, 830));
		points.add(new Point(910, 830));
		lines.add(points);

		points = new LinkedList<Point>(); // to L0, lower AND
		points.add(new Point(877, 830));
		points.add(new Point(877, 940));
		points.add(new Point(910, 940));
		lines.add(points);

		points = new LinkedList<Point>(); // to L1, upper AND
		points.add(new Point(779, 830));
		points.add(new Point(779, 1019));
		points.add(new Point(910, 1019));
		lines.add(points);

		points = new LinkedList<Point>(); // to L1, lower AND
		points.add(new Point(879, 1019));
		points.add(new Point(879, 1128));
		points.add(new Point(910, 1128));
		lines.add(points);

		points = new LinkedList<Point>(); // to L2, upper AND
		points.add(new Point(779, 1019));
		points.add(new Point(779, 1208));
		points.add(new Point(913, 1208));
		lines.add(points);

		points = new LinkedList<Point>(); // to L2, lower AND
		points.add(new Point(870, 1208));
		points.add(new Point(870, 1317));
		points.add(new Point(913, 1317));
		lines.add(points);
		signals.put("ldL", lines);

		lines = new LinkedList<List<Point>>(); // L0
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(979, 894));
		points.add(new Point(889, 894));
		points.add(new Point(889, 842));
		points.add(new Point(910, 842));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(889, 894));
		points.add(new Point(889, 951));
		points.add(new Point(905, 951));
		lines.add(points);
		signals.put("prl0", lines);

		lines = new LinkedList<List<Point>>(); // L1
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(969, 1081));
		points.add(new Point(889, 1081));
		points.add(new Point(889, 1031));
		points.add(new Point(910, 1031));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(889, 1081));
		points.add(new Point(889, 1140));
		points.add(new Point(905, 1140));
		lines.add(points);
		signals.put("prl1", lines);

		lines = new LinkedList<List<Point>>(); // L2
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(962, 1269));
		points.add(new Point(892, 1269));
		points.add(new Point(892, 1219));
		points.add(new Point(913, 1219));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(892, 1269));
		points.add(new Point(892, 1329));
		points.add(new Point(908, 1329));
		lines.add(points);
		signals.put("prl2", lines);

		lines = new LinkedList<List<Point>>(); // stPSWI
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(982, 1417));
		points.add(new Point(992, 1417));
		lines.add(points);
		signals.put("stPSWI", lines);

		lines = new LinkedList<List<Point>>(); // clPSWI
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(982, 1515));
		points.add(new Point(992, 1515));
		lines.add(points);
		signals.put("clPSWI", lines);

		lines = new LinkedList<List<Point>>(); // stPSWT
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(982, 1596));
		points.add(new Point(992, 1596));
		lines.add(points);
		signals.put("stPSWT", lines);

		lines = new LinkedList<List<Point>>(); // clPSWT
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(982, 1694));
		points.add(new Point(992, 1694));
		lines.add(points);
		signals.put("clPSWT", lines);

		lines = new LinkedList<List<Point>>(); // ldN upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(942, 82));
		points.add(new Point(963, 82));
		points.add(new Point(963, 87));
		points.add(new Point(988, 87));
		lines.add(points);
		signals.put("ldN_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldN lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(942, 191));
		points.add(new Point(963, 191));
		points.add(new Point(963, 185));
		points.add(new Point(989, 185));
		lines.add(points);
		signals.put("ldN_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSN upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(855, 101));
		points.add(new Point(869, 101));
		points.add(new Point(869, 99));
		points.add(new Point(988, 99));
		lines.add(points);
		signals.put("BUSN_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSN lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(855, 170));
		points.add(new Point(869, 170));
		points.add(new Point(869, 174));
		points.add(new Point(988, 174));
		lines.add(points);
		signals.put("BUSN_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // N upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1017, 93));
		points.add(new Point(1038, 93));
		points.add(new Point(1038, 101));
		points.add(new Point(1050, 101));
		lines.add(points);
		signals.put("N_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // N lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1017, 180));
		points.add(new Point(1038, 180));
		points.add(new Point(1038, 173));
		points.add(new Point(1050, 173));
		lines.add(points);
		signals.put("N_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // ldZ upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(942, 271));
		points.add(new Point(963, 271));
		points.add(new Point(963, 276));
		points.add(new Point(988, 276));
		lines.add(points);
		signals.put("ldZ_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldZ lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(942, 380));
		points.add(new Point(963, 380));
		points.add(new Point(963, 374));
		points.add(new Point(988, 374));
		lines.add(points);
		signals.put("ldZ_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSZ upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(855, 289));
		points.add(new Point(869, 289));
		points.add(new Point(869, 287));
		points.add(new Point(988, 287));
		lines.add(points);
		signals.put("BUSZ_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSZ lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(855, 358));
		points.add(new Point(869, 358));
		points.add(new Point(869, 363));
		points.add(new Point(988, 363));
		lines.add(points);
		signals.put("BUSZ_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // Z upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1017, 282));
		points.add(new Point(1038, 282));
		points.add(new Point(1038, 290));
		points.add(new Point(1050, 290));
		lines.add(points);
		signals.put("Z_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // Z lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1017, 368));
		points.add(new Point(1038, 368));
		points.add(new Point(1038, 361));
		points.add(new Point(1050, 361));
		lines.add(points);
		signals.put("Z_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // ldC upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(942, 459));
		points.add(new Point(963, 459));
		points.add(new Point(963, 465));
		points.add(new Point(988, 465));
		lines.add(points);
		signals.put("ldC_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldC lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(942, 569));
		points.add(new Point(963, 569));
		points.add(new Point(963, 563));
		points.add(new Point(988, 563));
		lines.add(points);
		signals.put("ldC_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSC upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(855, 478));
		points.add(new Point(869, 478));
		points.add(new Point(869, 476));
		points.add(new Point(988, 476));
		lines.add(points);
		signals.put("BUSC_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSC lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(849, 547));
		points.add(new Point(863, 547));
		points.add(new Point(863, 552));
		points.add(new Point(988, 552));
		lines.add(points);
		signals.put("BUSC_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // C upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1017, 471));
		points.add(new Point(1038, 471));
		points.add(new Point(1038, 479));
		points.add(new Point(1050, 479));
		lines.add(points);
		signals.put("C_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // C lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1017, 557));
		points.add(new Point(1038, 557));
		points.add(new Point(1038, 550));
		points.add(new Point(1050, 550));
		lines.add(points);
		signals.put("C_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // ldV upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(942, 648));
		points.add(new Point(963, 648));
		points.add(new Point(963, 654));
		points.add(new Point(988, 654));
		lines.add(points);
		signals.put("ldV_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldV lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(942, 757));
		points.add(new Point(963, 757));
		points.add(new Point(963, 752));
		points.add(new Point(988, 752));
		lines.add(points);
		signals.put("ldV_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSV upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(855, 667));
		points.add(new Point(869, 667));
		points.add(new Point(869, 665));
		points.add(new Point(988, 665));
		lines.add(points);
		signals.put("BUSV_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSV lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(855, 736));
		points.add(new Point(869, 736));
		points.add(new Point(869, 740));
		points.add(new Point(988, 740));
		lines.add(points);
		signals.put("BUSV_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // V upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1017, 659));
		points.add(new Point(1038, 659));
		points.add(new Point(1038, 667));
		points.add(new Point(1050, 667));
		lines.add(points);
		signals.put("V_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // V lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1017, 746));
		points.add(new Point(1038, 746));
		points.add(new Point(1038, 739));
		points.add(new Point(1050, 739));
		lines.add(points);
		signals.put("V_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // ldL0 upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(942, 836));
		points.add(new Point(963, 836));
		points.add(new Point(963, 842));
		points.add(new Point(988, 842));
		lines.add(points);
		signals.put("ldprl0_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldL0 lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(942, 945));
		points.add(new Point(963, 945));
		points.add(new Point(963, 940));
		points.add(new Point(988, 940));
		lines.add(points);
		signals.put("ldprl0_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSL0 upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(855, 855));
		points.add(new Point(869, 855));
		points.add(new Point(869, 853));
		points.add(new Point(988, 853));
		lines.add(points);
		signals.put("BUSprl0_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSL0 lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(855, 924));
		points.add(new Point(869, 924));
		points.add(new Point(869, 928));
		points.add(new Point(988, 928));
		lines.add(points);
		signals.put("BUSprl0_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // L0 upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1017, 847));
		points.add(new Point(1038, 847));
		points.add(new Point(1038, 855));
		points.add(new Point(1050, 855));
		lines.add(points);
		signals.put("prl0_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // L0 lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1017, 934));
		points.add(new Point(1038, 934));
		points.add(new Point(1038, 927));
		points.add(new Point(1050, 927));
		lines.add(points);
		signals.put("prl0_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // ldL1 upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(942, 1025));
		points.add(new Point(963, 1025));
		points.add(new Point(963, 1030));
		points.add(new Point(988, 1030));
		lines.add(points);
		signals.put("ldprl1_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldL1 lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(942, 1134));
		points.add(new Point(963, 1134));
		points.add(new Point(963, 1129));
		points.add(new Point(988, 1129));
		lines.add(points);
		signals.put("ldprl1_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSL1 upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(855, 1044));
		points.add(new Point(869, 1044));
		points.add(new Point(869, 1042));
		points.add(new Point(988, 1042));
		lines.add(points);
		signals.put("BUSprl1_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSL1 lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(855, 1113));
		points.add(new Point(869, 1113));
		points.add(new Point(869, 1117));
		points.add(new Point(988, 1117));
		lines.add(points);
		signals.put("BUSprl1_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // L1 upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1017, 1036));
		points.add(new Point(1038, 1036));
		points.add(new Point(1038, 1044));
		points.add(new Point(1050, 1044));
		lines.add(points);
		signals.put("prl1_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // L1 lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1017, 1123));
		points.add(new Point(1038, 1123));
		points.add(new Point(1038, 1116));
		points.add(new Point(1050, 1116));
		lines.add(points);
		signals.put("prl1_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // ldL2 upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(945, 1214));
		points.add(new Point(966, 1214));
		points.add(new Point(966, 1219));
		points.add(new Point(991, 1219));
		lines.add(points);
		signals.put("ldprl2_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // ldL2 lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(945, 1323));
		points.add(new Point(966, 1323));
		points.add(new Point(966, 1317));
		points.add(new Point(991, 1317));
		lines.add(points);
		signals.put("ldprl2_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSL2 upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(858, 1232));
		points.add(new Point(877, 1232));
		points.add(new Point(877, 1230));
		points.add(new Point(991, 1230));
		lines.add(points);
		signals.put("BUSprl2_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSL2 lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(858, 1301));
		points.add(new Point(872, 1301));
		points.add(new Point(872, 1306));
		points.add(new Point(991, 1306));
		lines.add(points);
		signals.put("BUSprl2_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // L2 upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1020, 1225));
		points.add(new Point(1041, 1225));
		points.add(new Point(1041, 1233));
		points.add(new Point(1053, 1233));
		lines.add(points);
		signals.put("prl2_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // L2 lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1020, 1312));
		points.add(new Point(1041, 1312));
		points.add(new Point(1041, 1305));
		points.add(new Point(1053, 1305));
		lines.add(points);
		signals.put("prl2_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // BUSI upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(858, 1430));
		points.add(new Point(869, 1430));
		points.add(new Point(869, 1428));
		points.add(new Point(991, 1428));
		lines.add(points);
		signals.put("BUSI_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUSI lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(858, 1499));
		points.add(new Point(879, 1499));
		points.add(new Point(879, 1504));
		points.add(new Point(991, 1504));
		lines.add(points);
		signals.put("BUSI_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // I upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1020, 1423));
		points.add(new Point(1041, 1423));
		points.add(new Point(1041, 1431));
		points.add(new Point(1053, 1431));
		lines.add(points);
		signals.put("I_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // I lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1020, 1510));
		points.add(new Point(1041, 1510));
		points.add(new Point(1041, 1503));
		points.add(new Point(1053, 1503));
		lines.add(points);
		signals.put("I_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // BUST upper AND
		points = new LinkedList<Point>(); // to upper OR
		points.add(new Point(858, 1609));
		points.add(new Point(869, 1609));
		points.add(new Point(869, 1607));
		points.add(new Point(991, 1607));
		lines.add(points);
		signals.put("BUST_upperAND", lines);

		lines = new LinkedList<List<Point>>(); // BUST lower AND
		points = new LinkedList<Point>(); // to lower OR
		points.add(new Point(858, 1679));
		points.add(new Point(879, 1679));
		points.add(new Point(879, 1683));
		points.add(new Point(991, 1683));
		lines.add(points);
		signals.put("BUST_lowerAND", lines);

		lines = new LinkedList<List<Point>>(); // T upper OR
		points = new LinkedList<Point>(); // to S
		points.add(new Point(1020, 1602));
		points.add(new Point(1041, 1602));
		points.add(new Point(1041, 1610));
		points.add(new Point(1053, 1610));
		lines.add(points);
		signals.put("T_upperOR", lines);

		lines = new LinkedList<List<Point>>(); // T lower OR
		points = new LinkedList<Point>(); // to R
		points.add(new Point(1020, 1689));
		points.add(new Point(1041, 1689));
		points.add(new Point(1041, 1682));
		points.add(new Point(1053, 1682));
		lines.add(points);
		signals.put("T_lowerOR", lines);

		lines = new LinkedList<List<Point>>(); // stSTART
		points = new LinkedList<Point>(); // to S
		points.add(new Point(67, 214));
		points.add(new Point(89, 214));
		lines.add(points);
		signals.put("TSTART", lines);

		lines = new LinkedList<List<Point>>(); // clSTART
		points = new LinkedList<Point>(); // to R
		points.add(new Point(62, 286));
		points.add(new Point(89, 286));
		lines.add(points);
		signals.put("clSTART", lines);

		lines = new LinkedList<List<Point>>(); // START
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(186, 216));
		points.add(new Point(213, 216));
		lines.add(points);
		signals.put("START", lines);

		lines = new LinkedList<List<Point>>(); // PSWout
		points = new LinkedList<Point>(); // to buffer
		points.add(new Point(195, 1073));
		points.add(new Point(195, 1056));
		lines.add(points);
		signals.put("PSWout", lines);

		lines = new LinkedList<List<Point>>(); // PSWN
		points = new LinkedList<Point>(); // from FF3
		points.add(new Point(1147, 102));
		points.add(new Point(1173, 102));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1333));
		points.add(new Point(133, 1333));
		lines.add(points);
		signals.put("PSWN", lines);

		lines = new LinkedList<List<Point>>(); // PSWZ
		points = new LinkedList<Point>(); // from FF4
		points.add(new Point(1147, 291));
		points.add(new Point(1173, 291));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1314));
		points.add(new Point(139, 1314));
		lines.add(points);
		signals.put("PSWZ", lines);

		lines = new LinkedList<List<Point>>(); // PSWC
		points = new LinkedList<Point>(); // from FF5
		points.add(new Point(1147, 480));
		points.add(new Point(1173, 480));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1296));
		points.add(new Point(142, 1296));
		lines.add(points);
		signals.put("PSWC", lines);

		lines = new LinkedList<List<Point>>(); // PSWV
		points = new LinkedList<Point>(); // from FF6
		points.add(new Point(1147, 669));
		points.add(new Point(1173, 669));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1278));
		points.add(new Point(143, 1278));
		lines.add(points);
		signals.put("PSWV", lines);

		lines = new LinkedList<List<Point>>(); // PSWL0
		points = new LinkedList<Point>(); // from FF7
		points.add(new Point(1147, 857));
		points.add(new Point(1173, 857));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1259));
		points.add(new Point(143, 1259));
		lines.add(points);
		signals.put("PSWL0", lines);

		lines = new LinkedList<List<Point>>(); // PSWL1
		points = new LinkedList<Point>(); // from FF8
		points.add(new Point(1147, 1046));
		points.add(new Point(1173, 1046));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1241));
		points.add(new Point(143, 1241));
		lines.add(points);
		signals.put("PSWL1", lines);

		lines = new LinkedList<List<Point>>(); // PSWL2
		points = new LinkedList<Point>(); // from FF9
		points.add(new Point(1150, 1234));
		points.add(new Point(1176, 1234));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1223));
		points.add(new Point(143, 1223));
		lines.add(points);
		signals.put("PSWL2", lines);

		lines = new LinkedList<List<Point>>(); // PSWT
		points = new LinkedList<Point>(); // from FF10
		points.add(new Point(1150, 1432));
		points.add(new Point(1176, 1432));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1076));
		points.add(new Point(143, 1076));
		lines.add(points);
		signals.put("PSWT", lines);

		lines = new LinkedList<List<Point>>(); // PSWI
		points = new LinkedList<Point>(); // from FF11
		points.add(new Point(1150, 1611));
		points.add(new Point(1176, 1611));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer
		points.add(new Point(123, 1058));
		points.add(new Point(143, 1058));
		lines.add(points);
		signals.put("PSWI", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to PSWout buffer, 7
		points.add(new Point(107, 1204));
		points.add(new Point(143, 1204));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 8
		points.add(new Point(107, 1186));
		points.add(new Point(143, 1186));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 9
		points.add(new Point(107, 1168));
		points.add(new Point(143, 1168));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 10
		points.add(new Point(111, 1149));
		points.add(new Point(143, 1149));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 11
		points.add(new Point(111, 1131));
		points.add(new Point(143, 1131));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 12
		points.add(new Point(111, 1113));
		points.add(new Point(143, 1113));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 13
		points.add(new Point(111, 1094));
		points.add(new Point(143, 1094));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 16
		points.add(new Point(111, 1039));
		points.add(new Point(143, 1039));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 17
		points.add(new Point(111, 1021));
		points.add(new Point(143, 1021));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 18
		points.add(new Point(111, 1003));
		points.add(new Point(143, 1003));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 19
		points.add(new Point(111, 984));
		points.add(new Point(143, 984));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 20
		points.add(new Point(111, 966));
		points.add(new Point(143, 966));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 21
		points.add(new Point(111, 948));
		points.add(new Point(143, 948));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 22
		points.add(new Point(111, 929));
		points.add(new Point(143, 929));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 23
		points.add(new Point(111, 911));
		points.add(new Point(143, 911));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 24
		points.add(new Point(111, 893));
		points.add(new Point(143, 893));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 25
		points.add(new Point(111, 874));
		points.add(new Point(143, 874));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 26
		points.add(new Point(111, 856));
		points.add(new Point(143, 856));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 27
		points.add(new Point(111, 838));
		points.add(new Point(143, 838));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 28
		points.add(new Point(111, 819));
		points.add(new Point(143, 819));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 29
		points.add(new Point(111, 801));
		points.add(new Point(143, 801));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 30
		points.add(new Point(111, 783));
		points.add(new Point(140, 783));
		lines.add(points);

		points = new LinkedList<Point>(); // to PSWout buffer, 31
		points.add(new Point(111, 764));
		points.add(new Point(134, 764));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to FF2
		points.add(new Point(79, 252));
		points.add(new Point(89, 252));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF3
		points.add(new Point(1040, 138));
		points.add(new Point(1050, 138));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF4
		points.add(new Point(1040, 327));
		points.add(new Point(1050, 327));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF5
		points.add(new Point(1040, 516));
		points.add(new Point(1050, 516));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF6
		points.add(new Point(1040, 705));
		points.add(new Point(1050, 705));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF7
		points.add(new Point(1040, 893));
		points.add(new Point(1050, 893));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF8
		points.add(new Point(1040, 1082));
		points.add(new Point(1050, 1082));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF9
		points.add(new Point(1043, 1270));
		points.add(new Point(1053, 1270));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF10
		points.add(new Point(1043, 1468));
		points.add(new Point(1053, 1468));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF11
		points.add(new Point(1043, 1647));
		points.add(new Point(1053, 1647));
		lines.add(points);
		signals.put("CLK", lines);

		return exec2Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getExec4Signals() {

		if (exec4Signals != null)
			return exec4Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // PSWN
		points = new LinkedList<Point>(); // to XOR
		points.add(new Point(128, 42));
		points.add(new Point(171, 42));
		lines.add(points);

		points = new LinkedList<Point>(); // to jp
		points.add(new Point(252, 383));
		points.add(new Point(372, 383));
		lines.add(points);

		points = new LinkedList<Point>(); // to jnp NOT
		points.add(new Point(283, 383));
		points.add(new Point(283, 411));
		points.add(new Point(301, 411));
		lines.add(points);
		signals.put("PSWN", lines);

		lines = new LinkedList<List<Point>>(); // PSWZ
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(211, 127));
		points.add(new Point(220, 127));
		lines.add(points);

		points = new LinkedList<Point>(); // to je
		points.add(new Point(252, 304));
		points.add(new Point(372, 304));
		lines.add(points);

		points = new LinkedList<Point>(); // to jne NOT
		points.add(new Point(283, 304));
		points.add(new Point(283, 331));
		points.add(new Point(301, 331));
		lines.add(points);
		signals.put("PSWZ", lines);

		lines = new LinkedList<List<Point>>(); // PSWV
		points = new LinkedList<Point>(); // to XOR
		points.add(new Point(128, 53));
		points.add(new Point(171, 53));
		lines.add(points);

		points = new LinkedList<Point>(); // to jo
		points.add(new Point(256, 221));
		points.add(new Point(372, 221));
		lines.add(points);

		points = new LinkedList<Point>(); // to jno NOT
		points.add(new Point(287, 221));
		points.add(new Point(287, 248));
		points.add(new Point(304, 248));
		lines.add(points);
		signals.put("PSWV", lines);

		lines = new LinkedList<List<Point>>(); // jl
		points = new LinkedList<Point>(); // from XOR
		points.add(new Point(199, 47));
		points.add(new Point(372, 47));
		lines.add(points);

		points = new LinkedList<Point>(); // to jge NOT
		points.add(new Point(210, 47));
		points.add(new Point(210, 76));
		points.add(new Point(215, 76));
		lines.add(points);

		points = new LinkedList<Point>(); // to jle OR
		points.add(new Point(210, 76));
		points.add(new Point(210, 115));
		points.add(new Point(220, 115));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 740));
		points.add(new Point(610, 740));
		lines.add(points);
		signals.put("jl", lines);

		lines = new LinkedList<List<Point>>(); // jge
		points = new LinkedList<Point>(); // from NOT
		points.add(new Point(251, 77));
		points.add(new Point(372, 77));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 488));
		points.add(new Point(610, 488));
		lines.add(points);
		signals.put("jge", lines);

		lines = new LinkedList<List<Point>>(); // jle
		points = new LinkedList<Point>(); // from OR
		points.add(new Point(249, 121));
		points.add(new Point(372, 121));
		lines.add(points);

		points = new LinkedList<Point>(); // to jg NOT
		points.add(new Point(253, 121));
		points.add(new Point(253, 166));
		points.add(new Point(267, 166));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 450));
		points.add(new Point(610, 450));
		lines.add(points);
		signals.put("jle", lines);

		lines = new LinkedList<List<Point>>(); // jg
		points = new LinkedList<Point>(); // from NOT
		points.add(new Point(303, 165));
		points.add(new Point(370, 165));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 468));
		points.add(new Point(610, 468));
		lines.add(points);
		signals.put("jg", lines);

		lines = new LinkedList<List<Point>>(); // jo
		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 683));
		points.add(new Point(610, 683));
		lines.add(points);
		signals.put("jo", lines);

		lines = new LinkedList<List<Point>>(); // jno
		points = new LinkedList<Point>(); // from NOT
		points.add(new Point(340, 249));
		points.add(new Point(372, 249));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 662));
		points.add(new Point(610, 662));
		lines.add(points);
		signals.put("jno", lines);

		lines = new LinkedList<List<Point>>(); // je
		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 528));
		points.add(new Point(610, 528));
		lines.add(points);
		signals.put("je", lines);

		lines = new LinkedList<List<Point>>(); // jne
		points = new LinkedList<Point>(); // from NOT
		points.add(new Point(336, 332));
		points.add(new Point(373, 332));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 508));
		points.add(new Point(610, 508));
		lines.add(points);
		signals.put("jne", lines);

		lines = new LinkedList<List<Point>>(); // jp
		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 720));
		points.add(new Point(610, 720));
		lines.add(points);
		signals.put("jp", lines);

		lines = new LinkedList<List<Point>>(); // jnp
		points = new LinkedList<Point>(); // from NOT
		points.add(new Point(336, 411));
		points.add(new Point(373, 411));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX4
		points.add(new Point(598, 702));
		points.add(new Point(610, 702));
		lines.add(points);
		signals.put("jnp", lines);

		lines = new LinkedList<List<Point>>(); // JLE
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 18));
		points.add(new Point(648, 18));
		lines.add(points);
		signals.put("JLE", lines);

		lines = new LinkedList<List<Point>>(); // JG
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 55));
		points.add(new Point(648, 55));
		lines.add(points);
		signals.put("JG", lines);

		lines = new LinkedList<List<Point>>(); // JGE
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 93));
		points.add(new Point(648, 93));
		lines.add(points);
		signals.put("JGE", lines);

		lines = new LinkedList<List<Point>>(); // JNE
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 131));
		points.add(new Point(648, 131));
		lines.add(points);
		signals.put("JNE", lines);

		lines = new LinkedList<List<Point>>(); // JE
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 169));
		points.add(new Point(648, 169));
		lines.add(points);
		signals.put("JE", lines);

		lines = new LinkedList<List<Point>>(); // JNO
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 207));
		points.add(new Point(648, 207));
		lines.add(points);
		signals.put("JNO", lines);

		lines = new LinkedList<List<Point>>(); // JO
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 244));
		points.add(new Point(648, 244));
		lines.add(points);
		signals.put("JO", lines);

		lines = new LinkedList<List<Point>>(); // JNP
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 282));
		points.add(new Point(648, 282));
		lines.add(points);
		signals.put("JNP", lines);

		lines = new LinkedList<List<Point>>(); // JP
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 319));
		points.add(new Point(648, 319));
		lines.add(points);
		signals.put("JP", lines);

		lines = new LinkedList<List<Point>>(); // JL
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(637, 357));
		points.add(new Point(648, 357));
		lines.add(points);
		signals.put("JL", lines);

		lines = new LinkedList<List<Point>>(); // jmpOR
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(689, 188));
		points.add(new Point(700, 188));
		points.add(new Point(700, 379));
		points.add(new Point(794, 379));
		lines.add(points);
		signals.put("jmpOR", lines);

		lines = new LinkedList<List<Point>>(); // IR29
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(640, 789));
		points.add(new Point(640, 774));
		lines.add(points);
		signals.put("IR29", lines);

		lines = new LinkedList<List<Point>>(); // IR28
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(667, 789));
		points.add(new Point(667, 774));
		lines.add(points);
		signals.put("IR28", lines);

		lines = new LinkedList<List<Point>>(); // IR27
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(689, 789));
		points.add(new Point(689, 774));
		lines.add(points);
		signals.put("IR27", lines);

		lines = new LinkedList<List<Point>>(); // IR26
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(716, 789));
		points.add(new Point(716, 774));
		lines.add(points);
		signals.put("IR26", lines);

		lines = new LinkedList<List<Point>>(); // IR25
		points = new LinkedList<Point>(); // to JMPREGIND
		points.add(new Point(33, 502));
		points.add(new Point(68, 502));
		lines.add(points);
		signals.put("IR25", lines);

		lines = new LinkedList<List<Point>>(); // MX4 out signal
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(743, 595));
		points.add(new Point(787, 595));
		points.add(new Point(787, 390));
		points.add(new Point(794, 390));
		lines.add(points);
		signals.put("MX4", lines);

		lines = new LinkedList<List<Point>>(); // brjmp
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(826, 385));
		points.add(new Point(832, 385));
		lines.add(points);
		signals.put("brjmp", lines);

		return exec4Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getFetch1Signals() {

		if (fetch1Signals != null)
			return fetch1Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(481, 29));
		points.add(new Point(481, 1024));
		lines.add(points);

		points = new LinkedList<Point>(); // to IR
		points.add(new Point(481, 111));
		points.add(new Point(77, 111));
		points.add(new Point(77, 155));
		lines.add(points);

		points = new LinkedList<Point>(); // from JMPPOMout buffer
		points.add(new Point(153, 335));
		points.add(new Point(481, 335));
		lines.add(points);

		points = new LinkedList<Point>(); // from JPOMout buffer
		points.add(new Point(153, 423));
		points.add(new Point(481, 423));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(424, 29));
		points.add(new Point(424, 1024));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(367, 29));
		points.add(new Point(367, 1024));
		lines.add(points);

		points = new LinkedList<Point>(); // from IRPOMout buffer
		points.add(new Point(153, 560));
		points.add(new Point(367, 560));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // IR 31..0
		points = new LinkedList<Point>(); // main line
		points.add(new Point(77, 200));
		points.add(new Point(77, 220));
		lines.add(points);
		signals.put("IR", lines);

		lines = new LinkedList<List<Point>>(); // IR 24..0
		points = new LinkedList<Point>(); // to JMPPOMout buffer
		points.add(new Point(106, 335));
		points.add(new Point(122, 335));
		lines.add(points);
		signals.put("IR23..0", lines);

		lines = new LinkedList<List<Point>>(); // IR 20..0
		points = new LinkedList<Point>(); // to JPOMout buffer
		points.add(new Point(106, 422));
		points.add(new Point(122, 422));
		lines.add(points);
		signals.put("IR20..0", lines);

		lines = new LinkedList<List<Point>>(); // IR 25..22
		points = new LinkedList<Point>(); // to IRPOMout buffer
		points.add(new Point(106, 560));
		points.add(new Point(122, 560));
		lines.add(points);
		signals.put("IR25..22", lines);

		lines = new LinkedList<List<Point>>(); // ldIR
		points = new LinkedList<Point>(); // to IR
		points.add(new Point(177, 177));
		points.add(new Point(153, 177));
		lines.add(points);
		signals.put("ldIR", lines);

		lines = new LinkedList<List<Point>>(); // JMPPOMout
		points = new LinkedList<Point>(); // to JMPPOMout buffer
		points.add(new Point(136, 360));
		points.add(new Point(136, 344));
		lines.add(points);
		signals.put("JMPPOMout", lines);

		lines = new LinkedList<List<Point>>(); // JPOMout
		points = new LinkedList<Point>(); // to JPOMout buffer
		points.add(new Point(136, 448));
		points.add(new Point(136, 431));
		lines.add(points);
		signals.put("JPOMout", lines);

		lines = new LinkedList<List<Point>>(); // IRPOMout
		points = new LinkedList<Point>(); // to IRPOMout buffer
		points.add(new Point(136, 586));
		points.add(new Point(136, 569));
		lines.add(points);
		signals.put("IRPOMout", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to IR
		points.add(new Point(0, 178));
		points.add(new Point(1, 178));
		lines.add(points);
		signals.put("CLK", lines);

		return fetch1Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getInt1Signals() {

		if (int1Signals != null)
			return int1Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // stPRINS
		points = new LinkedList<Point>(); // to S
		points.add(new Point(60, 20));
		points.add(new Point(87, 20));
		lines.add(points);
		signals.put("stPRINS", lines);

		lines = new LinkedList<List<Point>>(); // clPRINS
		points = new LinkedList<Point>(); // to R
		points.add(new Point(60, 92));
		points.add(new Point(87, 92));
		lines.add(points);
		signals.put("clPRINS", lines);

		lines = new LinkedList<List<Point>>(); // PRINS
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(184, 22));
		points.add(new Point(209, 22));
		lines.add(points);

		points = new LinkedList<Point>(); // to Prekid OR
		points.add(new Point(954, 645));
		points.add(new Point(965, 645));
		lines.add(points);
		signals.put("PRINS", lines);

		lines = new LinkedList<List<Point>>(); // stPRCODE
		points = new LinkedList<Point>(); // to S
		points.add(new Point(430, 20));
		points.add(new Point(457, 20));
		lines.add(points);
		signals.put("stPRCOD", lines);

		lines = new LinkedList<List<Point>>(); // clPRCODE
		points = new LinkedList<Point>(); // to R
		points.add(new Point(430, 92));
		points.add(new Point(457, 92));
		lines.add(points);
		signals.put("clPRCOD", lines);

		lines = new LinkedList<List<Point>>(); // PRCODE
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(554, 22));
		points.add(new Point(579, 22));
		lines.add(points);

		points = new LinkedList<Point>(); // to Prekid OR
		points.add(new Point(954, 683));
		points.add(new Point(965, 683));
		lines.add(points);
		signals.put("PRCOD", lines);

		lines = new LinkedList<List<Point>>(); // inm
		points = new LinkedList<Point>(); // to S
		points.add(new Point(800, 20));
		points.add(new Point(827, 20));
		lines.add(points);
		signals.put("inm", lines);

		lines = new LinkedList<List<Point>>(); // clPRINM
		points = new LinkedList<Point>(); // to R
		points.add(new Point(800, 92));
		points.add(new Point(827, 92));
		lines.add(points);
		signals.put("clPRINM", lines);

		lines = new LinkedList<List<Point>>(); // PRINM
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(924, 22));
		points.add(new Point(949, 22));
		lines.add(points);

		points = new LinkedList<Point>(); // to Prekid OR
		points.add(new Point(954, 721));
		points.add(new Point(965, 721));
		lines.add(points);
		signals.put("PRINM", lines);

		lines = new LinkedList<List<Point>>(); // intr1
		points = new LinkedList<Point>(); // to S
		points.add(new Point(60, 220));
		points.add(new Point(87, 220));
		lines.add(points);
		signals.put("intr1", lines);

		lines = new LinkedList<List<Point>>(); // clINTR1
		points = new LinkedList<Point>(); // to R
		points.add(new Point(60, 290));
		points.add(new Point(87, 290));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 315));
		points.add(new Point(1192, 315));
		lines.add(points);
		signals.put("clINTR1", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR1
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(184, 221));
		points.add(new Point(209, 221));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 1004));
		points.add(new Point(330, 1004));
		lines.add(points);
		signals.put("PRINTR1", lines);

		lines = new LinkedList<List<Point>>(); // intr2
		points = new LinkedList<Point>(); // to S
		points.add(new Point(430, 220));
		points.add(new Point(457, 220));
		lines.add(points);
		signals.put("intr2", lines);

		lines = new LinkedList<List<Point>>(); // clINTR2
		points = new LinkedList<Point>(); // to R
		points.add(new Point(430, 290));
		points.add(new Point(457, 290));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 292));
		points.add(new Point(1192, 292));
		lines.add(points);
		signals.put("clINTR2", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR2
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(554, 221));
		points.add(new Point(579, 221));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 966));
		points.add(new Point(330, 966));
		lines.add(points);
		signals.put("PRINTR2", lines);

		lines = new LinkedList<List<Point>>(); // intr3
		points = new LinkedList<Point>(); // to S
		points.add(new Point(60, 419));
		points.add(new Point(87, 419));
		lines.add(points);
		signals.put("intr3", lines);

		lines = new LinkedList<List<Point>>(); // clINTR3
		points = new LinkedList<Point>(); // to R
		points.add(new Point(60, 491));
		points.add(new Point(87, 491));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 270));
		points.add(new Point(1192, 270));
		lines.add(points);
		signals.put("clINTR3", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR3
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(184, 421));
		points.add(new Point(209, 421));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 928));
		points.add(new Point(330, 928));
		lines.add(points);
		signals.put("PRINTR3", lines);

		lines = new LinkedList<List<Point>>(); // intr4
		points = new LinkedList<Point>(); // to S
		points.add(new Point(430, 419));
		points.add(new Point(457, 419));
		lines.add(points);
		signals.put("intr4", lines);

		lines = new LinkedList<List<Point>>(); // clINTR4
		points = new LinkedList<Point>(); // to R
		points.add(new Point(430, 490));
		points.add(new Point(457, 490));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 247));
		points.add(new Point(1192, 247));
		lines.add(points);
		signals.put("clINTR4", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR4
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(554, 421));
		points.add(new Point(579, 421));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 890));
		points.add(new Point(330, 890));
		lines.add(points);
		signals.put("PRINTR4", lines);

		lines = new LinkedList<List<Point>>(); // intr5
		points = new LinkedList<Point>(); // to S
		points.add(new Point(800, 419));
		points.add(new Point(827, 419));
		lines.add(points);
		signals.put("intr5", lines);

		lines = new LinkedList<List<Point>>(); // clINTR5
		points = new LinkedList<Point>(); // to R
		points.add(new Point(800, 491));
		points.add(new Point(827, 491));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 224));
		points.add(new Point(1192, 224));
		lines.add(points);
		signals.put("clINTR5", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR5
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(924, 421));
		points.add(new Point(949, 421));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 853));
		points.add(new Point(330, 853));
		lines.add(points);
		signals.put("PRINTR5", lines);

		lines = new LinkedList<List<Point>>(); // intr6
		points = new LinkedList<Point>(); // to S
		points.add(new Point(60, 619));
		points.add(new Point(87, 619));
		lines.add(points);
		signals.put("intr6", lines);

		lines = new LinkedList<List<Point>>(); // clINTR6
		points = new LinkedList<Point>(); // to R
		points.add(new Point(60, 690));
		points.add(new Point(87, 690));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 201));
		points.add(new Point(1192, 201));
		lines.add(points);
		signals.put("clINTR6", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR6
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(184, 620));
		points.add(new Point(209, 620));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 815));
		points.add(new Point(330, 815));
		lines.add(points);
		signals.put("PRINTR6", lines);

		lines = new LinkedList<List<Point>>(); // intr7
		points = new LinkedList<Point>(); // to S
		points.add(new Point(429, 619));
		points.add(new Point(457, 619));
		lines.add(points);
		signals.put("intr7", lines);

		lines = new LinkedList<List<Point>>(); // clINTR7
		points = new LinkedList<Point>(); // to R
		points.add(new Point(430, 689));
		points.add(new Point(457, 689));
		lines.add(points);

		points = new LinkedList<Point>(); // from DC3
		points.add(new Point(1153, 179));
		points.add(new Point(1192, 179));
		lines.add(points);
		signals.put("clINTR7", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR7
		points = new LinkedList<Point>(); // from Q
		points.add(new Point(554, 620));
		points.add(new Point(579, 620));
		lines.add(points);

		points = new LinkedList<Point>(); // to printr OR
		points.add(new Point(318, 777));
		points.add(new Point(330, 777));
		lines.add(points);
		signals.put("PRINTR7", lines);

		lines = new LinkedList<List<Point>>(); // prl0
		points = new LinkedList<Point>(); // to DC3
		points.add(new Point(1030, 302));
		points.add(new Point(1057, 302));
		lines.add(points);
		signals.put("prl0", lines);

		lines = new LinkedList<List<Point>>(); // prl1
		points = new LinkedList<Point>(); // to DC3
		points.add(new Point(1030, 273));
		points.add(new Point(1057, 273));
		lines.add(points);
		signals.put("prl1", lines);

		lines = new LinkedList<List<Point>>(); // prl2
		points = new LinkedList<Point>(); // to DC3
		points.add(new Point(1030, 245));
		points.add(new Point(1057, 245));
		lines.add(points);
		signals.put("prl2", lines);

		// lines = new LinkedList<List<Point>>(); // clintr0
		// points = new LinkedList<Point>(); // from DC3
		// points.add(new Point(1153, 338));
		// points.add(new Point(1192, 338));
		// lines.add(points);
		// signals.put("clINTR0", lines);

		lines = new LinkedList<List<Point>>(); // clINTR
		points = new LinkedList<Point>(); // to DC3
		points.add(new Point(1105, 381));
		points.add(new Point(1105, 363));
		lines.add(points);
		signals.put("clINTR", lines);

		lines = new LinkedList<List<Point>>(); // PSWT
		points = new LinkedList<Point>(); // to Prekid AND
		points.add(new Point(821, 854));
		points.add(new Point(828, 854));
		lines.add(points);
		signals.put("PSWT", lines);

		lines = new LinkedList<List<Point>>(); // !IRET
		points = new LinkedList<Point>(); // to Prekid AND
		points.add(new Point(821, 865));
		points.add(new Point(828, 865));
		lines.add(points);
		signals.put("!IRET", lines);

		lines = new LinkedList<List<Point>>(); // Prekid AND
		points = new LinkedList<Point>(); // to Prekid OR
		points.add(new Point(859, 859));
		points.add(new Point(955, 859));
		points.add(new Point(955, 796));
		points.add(new Point(965, 796));
		lines.add(points);
		signals.put("PrekidAND", lines);

		lines = new LinkedList<List<Point>>(); // printr
		points = new LinkedList<Point>(); // from printr OR
		points.add(new Point(371, 890));
		points.add(new Point(381, 890));
		lines.add(points);

		points = new LinkedList<Point>(); // to Prekid AND
		points.add(new Point(954, 758));
		points.add(new Point(965, 758));
		lines.add(points);
		signals.put("printr", lines);

		lines = new LinkedList<List<Point>>(); // Prekid
		points = new LinkedList<Point>(); // from Prekid AND
		points.add(new Point(1006, 721));
		points.add(new Point(1016, 721));
		lines.add(points);
		signals.put("prekid", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to FF12
		points.add(new Point(77, 58));
		points.add(new Point(87, 58));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF13
		points.add(new Point(447, 58));
		points.add(new Point(457, 58));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF14
		points.add(new Point(817, 58));
		points.add(new Point(827, 58));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF15
		points.add(new Point(77, 257));
		points.add(new Point(87, 257));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF16
		points.add(new Point(447, 257));
		points.add(new Point(457, 257));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF17
		points.add(new Point(77, 457));
		points.add(new Point(87, 457));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF18
		points.add(new Point(447, 457));
		points.add(new Point(457, 457));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF19
		points.add(new Point(817, 457));
		points.add(new Point(827, 457));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF20
		points.add(new Point(77, 656));
		points.add(new Point(87, 656));
		lines.add(points);

		points = new LinkedList<Point>(); // to FF21
		points.add(new Point(447, 656));
		points.add(new Point(457, 656));
		lines.add(points);
		signals.put("CLK", lines);

		return int1Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getInt2Signals() {

		if (int2Signals != null)
			return int2Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // PRINTR1
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(312, 137));
		points.add(new Point(312, 198));
		lines.add(points);
		signals.put("PRINTR1", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR2
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(354, 137));
		points.add(new Point(354, 198));
		lines.add(points);
		signals.put("PRINTR2", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR3
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(394, 137));
		points.add(new Point(394, 198));
		lines.add(points);
		signals.put("PRINTR3", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR4
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(436, 137));
		points.add(new Point(436, 198));
		lines.add(points);
		signals.put("PRINTR4", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR5
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(474, 137));
		points.add(new Point(474, 198));
		lines.add(points);
		signals.put("PRINTR5", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR6
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(515, 137));
		points.add(new Point(515, 198));
		lines.add(points);
		signals.put("PRINTR6", lines);

		lines = new LinkedList<List<Point>>(); // PRINTR7
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(554, 137));
		points.add(new Point(554, 198));
		lines.add(points);
		signals.put("PRINTR7", lines);

		lines = new LinkedList<List<Point>>(); // prl0
		points = new LinkedList<Point>(); // to CMP1
		points.add(new Point(598, 476));
		points.add(new Point(203, 476));
		lines.add(points);

		points = new LinkedList<Point>(); // from CD1
		points.add(new Point(364, 299));
		points.add(new Point(364, 476));
		lines.add(points);
		signals.put("prl0", lines);

		lines = new LinkedList<List<Point>>(); // prl1
		points = new LinkedList<Point>(); // to CMP1
		points.add(new Point(598, 456));
		points.add(new Point(203, 456));
		lines.add(points);

		points = new LinkedList<Point>(); // from CD1
		points.add(new Point(405, 299));
		points.add(new Point(405, 456));
		lines.add(points);
		signals.put("prl1", lines);

		lines = new LinkedList<List<Point>>(); // prl2
		points = new LinkedList<Point>(); // to CMP1
		points.add(new Point(598, 433));
		points.add(new Point(203, 433));
		lines.add(points);

		points = new LinkedList<Point>(); // from CD1
		points.add(new Point(444, 299));
		points.add(new Point(444, 433));
		lines.add(points);
		signals.put("prl2", lines);

		lines = new LinkedList<List<Point>>(); // PSWL0
		points = new LinkedList<Point>(); // to CMP1
		points.add(new Point(79, 476));
		points.add(new Point(107, 476));
		lines.add(points);
		signals.put("PSWL0", lines);

		lines = new LinkedList<List<Point>>(); // PSWL1
		points = new LinkedList<Point>(); // to CMP1
		points.add(new Point(79, 456));
		points.add(new Point(107, 456));
		lines.add(points);
		signals.put("PSWL1", lines);

		lines = new LinkedList<List<Point>>(); // PSWL2
		points = new LinkedList<Point>(); // to CMP1
		points.add(new Point(79, 432));
		points.add(new Point(107, 432));
		lines.add(points);
		signals.put("PSWL2", lines);

		lines = new LinkedList<List<Point>>(); // CMP1 out signal
		points = new LinkedList<Point>(); // to int AND
		points.add(new Point(155, 399));
		points.add(new Point(155, 52));
		points.add(new Point(89, 52));
		lines.add(points);
		signals.put("CMP1", lines);

		lines = new LinkedList<List<Point>>(); // PSWI
		points = new LinkedList<Point>(); // to int AND
		points.add(new Point(101, 15));
		points.add(new Point(89, 15));
		lines.add(points);
		signals.put("PSWI", lines);

		lines = new LinkedList<List<Point>>(); // int
		points = new LinkedList<Point>(); // from int AND
		points.add(new Point(43, 34));
		points.add(new Point(11, 34));
		lines.add(points);
		signals.put("int", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(275, 137));
		points.add(new Point(275, 198));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to CD1
		points.add(new Point(628, 249));
		points.add(new Point(606, 249));
		lines.add(points);
		signals.put("1", lines);

		return int2Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getInt3Signals() {

		if (int3Signals != null)
			return int3Signals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // IBUS1
		points = new LinkedList<Point>(); // main line
		points.add(new Point(845, 41)); // +16
		points.add(new Point(845, 1287));
		lines.add(points);

		points = new LinkedList<Point>(); // from IVTPout buffer
		points.add(new Point(485, 1256));
		points.add(new Point(845, 1256));
		lines.add(points);
		signals.put("IBUS1", lines);

		lines = new LinkedList<List<Point>>(); // IBUS2
		points = new LinkedList<Point>(); // main line
		points.add(new Point(769, 115));
		points.add(new Point(769, 1261));
		lines.add(points);

		points = new LinkedList<Point>(); // from IVTDSPout buffer
		points.add(new Point(476, 986));
		points.add(new Point(769, 986));
		lines.add(points);
		signals.put("IBUS2", lines);

		lines = new LinkedList<List<Point>>(); // IBUS3
		points = new LinkedList<Point>(); // main line
		points.add(new Point(694, 192));
		points.add(new Point(694, 1211));
		lines.add(points);
		//
		points = new LinkedList<Point>(); // from UIXTout buffer
		points.add(new Point(458, 262));
		points.add(new Point(694, 262));
		lines.add(points);

		points = new LinkedList<Point>(); // from UEXTout buffer
		points.add(new Point(477, 556));
		points.add(new Point(694, 556));
		lines.add(points);

		points = new LinkedList<Point>(); // to BR
		points.add(new Point(694, 796));
		points.add(new Point(156, 796));
		points.add(new Point(156, 838));
		lines.add(points);

		points = new LinkedList<Point>(); // to IVTP
		points.add(new Point(694, 1131));
		points.add(new Point(157, 1131));
		points.add(new Point(157, 1168));
		lines.add(points);
		signals.put("IBUS3", lines);

		lines = new LinkedList<List<Point>>(); // PSWT
		points = new LinkedList<Point>(); // to CD2
		points.add(new Point(72, 363));
		points.add(new Point(89, 363));
		lines.add(points);
		signals.put("PSWT", lines);

		lines = new LinkedList<List<Point>>(); // PRINM
		points = new LinkedList<Point>(); // to CD2
		points.add(new Point(72, 342));
		points.add(new Point(89, 342));
		lines.add(points);
		signals.put("PRINM", lines);

		lines = new LinkedList<List<Point>>(); // PRCOD
		points = new LinkedList<Point>(); // to CD2
		points.add(new Point(72, 305));
		points.add(new Point(89, 305));
		lines.add(points);
		signals.put("PRCOD", lines);

		lines = new LinkedList<List<Point>>(); // CD2_0
		points = new LinkedList<Point>(); // from CD2
		points.add(new Point(184, 346));
		points.add(new Point(234, 346));
		lines.add(points);
		signals.put("CD2_0", lines);

		lines = new LinkedList<List<Point>>(); // CD2_1
		points = new LinkedList<Point>(); // from CD2
		points.add(new Point(184, 323));
		points.add(new Point(234, 323));
		lines.add(points);
		signals.put("CD2_1", lines);

		lines = new LinkedList<List<Point>>(); // UINT (out7..0)
		points = new LinkedList<Point>(); // to UINTout buffer
		points.add(new Point(280, 262));
		points.add(new Point(427, 262));
		lines.add(points);
		signals.put("UINT", lines);

		lines = new LinkedList<List<Point>>(); // UINT out
		points = new LinkedList<Point>(); // to UINTout buffer
		points.add(new Point(441, 288));
		points.add(new Point(441, 271));
		lines.add(points);
		signals.put("UINTout", lines);

		lines = new LinkedList<List<Point>>(); // irm1
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 659));
		points.add(new Point(94, 659));
		lines.add(points);
		signals.put("PRINTR1", lines);

		lines = new LinkedList<List<Point>>(); // irm2
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 637));
		points.add(new Point(94, 637));
		lines.add(points);
		signals.put("PRINTR2", lines);

		lines = new LinkedList<List<Point>>(); // irm3
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 616));
		points.add(new Point(94, 616));
		lines.add(points);
		signals.put("PRINTR3", lines);

		lines = new LinkedList<List<Point>>(); // irm4
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 594));
		points.add(new Point(94, 594));
		lines.add(points);
		signals.put("PRINTR4", lines);

		lines = new LinkedList<List<Point>>(); // irm5
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 572));
		points.add(new Point(94, 572));
		lines.add(points);
		signals.put("PRINTR5", lines);

		lines = new LinkedList<List<Point>>(); // irm6
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 551));
		points.add(new Point(94, 551));
		lines.add(points);
		signals.put("PRINTR6", lines);

		lines = new LinkedList<List<Point>>(); // irm7
		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(65, 529));
		points.add(new Point(94, 529));
		lines.add(points);
		signals.put("PRINTR7", lines);

		lines = new LinkedList<List<Point>>(); // CD3_0
		points = new LinkedList<Point>(); // from CD3
		points.add(new Point(187, 626));
		points.add(new Point(223, 626));
		lines.add(points);
		signals.put("CD3_0", lines);

		lines = new LinkedList<List<Point>>(); // CD3_1
		points = new LinkedList<Point>(); // from CD3
		points.add(new Point(187, 607));
		points.add(new Point(223, 607));
		lines.add(points);
		signals.put("CD3_1", lines);

		lines = new LinkedList<List<Point>>(); // CD3_2
		points = new LinkedList<Point>(); // from CD3
		points.add(new Point(187, 588));
		points.add(new Point(223, 588));
		lines.add(points);
		signals.put("CD3_2", lines);

		lines = new LinkedList<List<Point>>(); // UEXT (out7..0)
		points = new LinkedList<Point>(); // to UEXTout buffer
		points.add(new Point(280, 556));
		points.add(new Point(446, 556));
		lines.add(points);
		signals.put("UEXT", lines);

		lines = new LinkedList<List<Point>>(); // UEXTout
		points = new LinkedList<Point>(); // to UEXTout buffer
		points.add(new Point(460, 582));
		points.add(new Point(460, 565));
		lines.add(points);
		signals.put("UEXTout", lines);

		lines = new LinkedList<List<Point>>(); // ldBR
		points = new LinkedList<Point>(); // to BR
		points.add(new Point(256, 860));
		points.add(new Point(233, 860));
		lines.add(points);
		signals.put("ldBR", lines);

		lines = new LinkedList<List<Point>>(); // BR out signal
		points = new LinkedList<Point>(); // from BR
		points.add(new Point(156, 882));
		points.add(new Point(156, 947));
		lines.add(points);

		points = new LinkedList<Point>(); // to IVTDSPout buffer
		points.add(new Point(127, 968));
		points.add(new Point(127, 985));
		points.add(new Point(445, 985));
		lines.add(points);
		signals.put("BR", lines);

		lines = new LinkedList<List<Point>>(); // IVTDSPout
		points = new LinkedList<Point>(); // to IVTDSPout buffer
		points.add(new Point(459, 1011));
		points.add(new Point(459, 994));
		lines.add(points);
		signals.put("BRout", lines);

		lines = new LinkedList<List<Point>>(); // ldIVTP
		points = new LinkedList<Point>(); // to IVTP
		points.add(new Point(256, 1190));
		points.add(new Point(233, 1190));
		lines.add(points);
		signals.put("ldIVTP", lines);

		lines = new LinkedList<List<Point>>(); // IVTP
		points = new LinkedList<Point>(); // to IVTPout buffer
		points.add(new Point(159, 1212));
		points.add(new Point(159, 1256));
		points.add(new Point(455, 1256));
		lines.add(points);
		signals.put("IVTP", lines);

		lines = new LinkedList<List<Point>>(); // IVTPout
		points = new LinkedList<Point>(); // to IVTPout buffer
		points.add(new Point(470, 1282));
		points.add(new Point(470, 1265));
		lines.add(points);
		signals.put("IVTPout", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to UIXT2
		points.add(new Point(216, 298));
		points.add(new Point(232, 298));
		lines.add(points);

		points = new LinkedList<Point>(); // to UIXT3
		points.add(new Point(216, 274));
		points.add(new Point(232, 274));
		lines.add(points);

		points = new LinkedList<Point>(); // to UIXT4
		points.add(new Point(216, 250));
		points.add(new Point(232, 250));
		lines.add(points);

		points = new LinkedList<Point>(); // to UIXT5
		points.add(new Point(216, 226));
		points.add(new Point(232, 226));
		lines.add(points);

		points = new LinkedList<Point>(); // to UIXT6
		points.add(new Point(216, 202));
		points.add(new Point(232, 202));
		lines.add(points);

		points = new LinkedList<Point>(); // to UIXT7
		points.add(new Point(216, 180));
		points.add(new Point(232, 180));
		lines.add(points);

		points = new LinkedList<Point>(); // to UEXT4
		points.add(new Point(211, 550));
		points.add(new Point(224, 550));
		lines.add(points);

		points = new LinkedList<Point>(); // to UEXT5
		points.add(new Point(211, 532));
		points.add(new Point(224, 532));
		lines.add(points);

		points = new LinkedList<Point>(); // to UEXT6
		points.add(new Point(211, 513));
		points.add(new Point(224, 513));
		lines.add(points);

		points = new LinkedList<Point>(); // to UEXT7
		points.add(new Point(211, 494));
		points.add(new Point(224, 494));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to UEXT3
		points.add(new Point(211, 567));
		points.add(new Point(224, 567));
		lines.add(points);

		points = new LinkedList<Point>(); // to CD2
		points.add(new Point(135, 390));
		points.add(new Point(135, 410));
		lines.add(points);

		points = new LinkedList<Point>(); // to CD3
		points.add(new Point(139, 696));
		points.add(new Point(139, 716));
		lines.add(points);

		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to BR
		points.add(new Point(80, 860));
		points.add(new Point(70, 860));
		lines.add(points);

		points = new LinkedList<Point>(); // to IVTP
		points.add(new Point(80, 1190));
		points.add(new Point(70, 1190));
		lines.add(points);
		signals.put("CLK", lines);

		return int3Signals = signals;

	}

	public HashMap<String, List<List<Point>>> getIOSignals() {

		if (ioSignals != null)
			return ioSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // DBUS
		points = new LinkedList<Point>(); // main line
		points.add(new Point(27, 29));
		points.add(new Point(27, 755));
		lines.add(points);

		points = new LinkedList<Point>(); // from MEMI/Oout buffer
		points.add(new Point(221, 490));
		points.add(new Point(27, 490));
		lines.add(points);
		signals.put("DBUS", lines);

		lines = new LinkedList<List<Point>>(); // ABUS
		points = new LinkedList<Point>(); // main line
		points.add(new Point(102, 104));
		points.add(new Point(102, 740));
		lines.add(points);

		points = new LinkedList<Point>(); // to MEMI/O
		points.add(new Point(102, 265));
		points.add(new Point(187, 265));
		lines.add(points);
		signals.put("ABUS", lines);

		lines = new LinkedList<List<Point>>(); // RDBUS
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(696, 174));
		points.add(new Point(669, 174));
		lines.add(points);
		signals.put("!RDBUS", lines);

		lines = new LinkedList<List<Point>>(); // RDBUS inverted
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(646, 174));
		points.add(new Point(635, 174));
		points.add(new Point(635, 195));
		points.add(new Point(604, 195));
		lines.add(points);
		signals.put("RDBUSinv", lines);

		lines = new LinkedList<List<Point>>(); // M/IO
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(753, 227));
		points.add(new Point(726, 227));
		lines.add(points);
		signals.put("M/IO", lines);

		lines = new LinkedList<List<Point>>(); // M/IO inverted
		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(702, 227));
		points.add(new Point(626, 227));
		points.add(new Point(626, 259));
		points.add(new Point(604, 259));
		lines.add(points);

		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(692, 227));
		points.add(new Point(692, 206));
		points.add(new Point(604, 206));
		lines.add(points);
		signals.put("MIOinv", lines);

		lines = new LinkedList<List<Point>>(); // WRBUS
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(697, 280));
		points.add(new Point(669, 280));
		lines.add(points);
		signals.put("!WRBUS", lines);

		lines = new LinkedList<List<Point>>(); // WRBUS inverted
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(646, 280));
		points.add(new Point(635, 280));
		points.add(new Point(635, 271));
		points.add(new Point(604, 271));
		lines.add(points);
		signals.put("WRBUSinv", lines);

		lines = new LinkedList<List<Point>>(); // RD
		points = new LinkedList<Point>(); // to MEM I/O (almost)
		points.add(new Point(573, 201));
		points.add(new Point(489, 201));
		points.add(new Point(489, 229));
		lines.add(points);

		points = new LinkedList<Point>(); // to OR
		points.add(new Point(470, 229));
		points.add(new Point(557, 229));
		points.add(new Point(557, 374));
		points.add(new Point(583, 374));
		lines.add(points);
		signals.put("RD", lines);

		lines = new LinkedList<List<Point>>(); // WR
		points = new LinkedList<Point>(); // to MEM I/O (almost)
		points.add(new Point(573, 265));
		points.add(new Point(517, 265));
		points.add(new Point(517, 297));
		lines.add(points);

		points = new LinkedList<Point>(); // to OR
		points.add(new Point(470, 297));
		points.add(new Point(531, 297));
		points.add(new Point(531, 385));
		points.add(new Point(584, 385));
		lines.add(points);
		signals.put("WR", lines);

		lines = new LinkedList<List<Point>>(); // MEM I/O out signal
		points = new LinkedList<Point>(); // to MEMI/Oout buffer
		points.add(new Point(338, 348));
		points.add(new Point(338, 491));
		points.add(new Point(252, 491));
		lines.add(points);
		signals.put("memIO", lines);

		lines = new LinkedList<List<Point>>(); // MEMI/Oout
		points = new LinkedList<Point>(); // to MEMI/Oout buffer
		points.add(new Point(238, 465));
		points.add(new Point(238, 482));
		lines.add(points);
		signals.put("MEMI/Oout", lines);

		lines = new LinkedList<List<Point>>(); // INCOR
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(612, 380));
		points.add(new Point(633, 380));
		points.add(new Point(633, 391));
		points.add(new Point(671, 391));
		lines.add(points);
		signals.put("INCOR", lines);

		lines = new LinkedList<List<Point>>(); // IOfc
		points = new LinkedList<Point>(); // to MEMACC
		points.add(new Point(585, 429));
		points.add(new Point(758, 429));
		lines.add(points);

		points = new LinkedList<Point>(); // to AND
		points.add(new Point(638, 429));
		points.add(new Point(638, 403));
		points.add(new Point(666, 403));
		lines.add(points);

		points = new LinkedList<Point>(); // to CMP2
		points.add(new Point(1192, 548));
		points.add(new Point(1167, 548));
		lines.add(points);
		signals.put("IOfc", lines);

		lines = new LinkedList<List<Point>>(); // INCAND
		points = new LinkedList<Point>(); // to MEMACC
		points.add(new Point(703, 397));
		points.add(new Point(734, 397));
		points.add(new Point(734, 406));
		points.add(new Point(758, 406));
		lines.add(points);
		signals.put("INCAND", lines);

		lines = new LinkedList<List<Point>>(); // MEMACC in signal
		points = new LinkedList<Point>(); // to MEMACC
		points.add(new Point(835, 348));
		points.add(new Point(835, 394));
		lines.add(points);
		signals.put("IOMEMACCin", lines);

		lines = new LinkedList<List<Point>>(); // MEMACC out signal
		points = new LinkedList<Point>(); // to CMP2
		points.add(new Point(835, 438));
		points.add(new Point(835, 525));
		points.add(new Point(1026, 525));
		lines.add(points);
		signals.put("IOMEMACC", lines);

		lines = new LinkedList<List<Point>>(); // TIME in signal
		points = new LinkedList<Point>(); // to TIME
		points.add(new Point(630, 485));
		points.add(new Point(630, 533));
		lines.add(points);
		signals.put("IOTIMEin", lines);

		lines = new LinkedList<List<Point>>(); // TIME out signal
		points = new LinkedList<Point>(); // to CMP2
		points.add(new Point(630, 577));
		points.add(new Point(630, 623));
		points.add(new Point(932, 623));
		points.add(new Point(932, 575));
		points.add(new Point(1026, 575));
		lines.add(points);
		signals.put("IOTIME", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to MEMACC
		points.add(new Point(911, 427));
		points.add(new Point(921, 427));
		lines.add(points);

		points = new LinkedList<Point>(); // to TIME
		points.add(new Point(554, 556));
		points.add(new Point(544, 556));
		lines.add(points);
		signals.put("CLK", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to MEMACCIO
		points.add(new Point(930, 405));
		points.add(new Point(910, 405));
		lines.add(points);
		signals.put("0", lines);

		return ioSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getMemOprSignals() {

		if (memOprSignals != null)
			return memOprSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // DBUS
		points = new LinkedList<Point>(); // "main" line
		points.add(new Point(392, 944));
		points.add(new Point(392, 996));
		lines.add(points);

		points = new LinkedList<Point>(); // from MEMout buffer
		points.add(new Point(422, 972));
		points.add(new Point(392, 972));
		lines.add(points);
		signals.put("DBUS", lines);

		lines = new LinkedList<List<Point>>(); // DBUS31..24
		points = new LinkedList<Point>(); // to M0
		points.add(new Point(547, 26));
		points.add(new Point(547, 43));
		lines.add(points);
		signals.put("DBUS31..24", lines);

		lines = new LinkedList<List<Point>>(); // DBUS31..24
		points = new LinkedList<Point>(); // to M1
		points.add(new Point(547, 261));
		points.add(new Point(547, 278));
		lines.add(points);
		signals.put("DBUS23..16", lines);

		lines = new LinkedList<List<Point>>(); // DBUS31..24
		points = new LinkedList<Point>(); // to M2
		points.add(new Point(547, 496));
		points.add(new Point(547, 513));
		lines.add(points);
		signals.put("DBUS15..8", lines);

		lines = new LinkedList<List<Point>>(); // DBUS31..24
		points = new LinkedList<Point>(); // to M3
		points.add(new Point(547, 731));
		points.add(new Point(547, 748));
		lines.add(points);
		signals.put("DBUS7..0", lines);

		lines = new LinkedList<List<Point>>(); // M0 DO
		points = new LinkedList<Point>(); // to MEMout buffer
		points.add(new Point(547, 178));
		points.add(new Point(547, 231));
		points.add(new Point(741, 231));
		points.add(new Point(741, 940));
		points.add(new Point(491, 940));
		lines.add(points);
		signals.put("DO0", lines);

		lines = new LinkedList<List<Point>>(); // M1 DO
		points = new LinkedList<Point>(); // to MEMout buffer
		points.add(new Point(547, 413));
		points.add(new Point(547, 464));
		points.add(new Point(727, 464));
		points.add(new Point(727, 959));
		points.add(new Point(491, 959));
		lines.add(points);
		signals.put("DO1", lines);

		lines = new LinkedList<List<Point>>(); // M2 DO
		points = new LinkedList<Point>(); // to MEMout buffer
		points.add(new Point(547, 648));
		points.add(new Point(547, 699));
		points.add(new Point(714, 699));
		points.add(new Point(714, 978));
		points.add(new Point(491, 978));
		lines.add(points);
		signals.put("DO2", lines);

		lines = new LinkedList<List<Point>>(); // M3 DO
		points = new LinkedList<Point>(); // to MEMout buffer
		points.add(new Point(547, 883));
		points.add(new Point(547, 996));
		points.add(new Point(491, 996));
		lines.add(points);
		signals.put("DO3", lines);

		lines = new LinkedList<List<Point>>(); // DO
		points = new LinkedList<Point>(); // to MEMout buffer
		points.add(new Point(477, 972));
		points.add(new Point(454, 972));
		lines.add(points);
		signals.put("DO", lines);

		lines = new LinkedList<List<Point>>(); // MEMout
		points = new LinkedList<Point>(); // to MEMout buffer
		points.add(new Point(440, 947));
		points.add(new Point(440, 963));
		lines.add(points);
		signals.put("MEMout", lines);

		lines = new LinkedList<List<Point>>(); // ABUS
		points = new LinkedList<Point>(); // to INC
		points.add(new Point(29, 5));
		points.add(new Point(29, 64));
		points.add(new Point(99, 64));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX6
		points.add(new Point(57, 64));
		points.add(new Point(57, 120));
		points.add(new Point(331, 120));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX7
		points.add(new Point(132, 120));
		points.add(new Point(132, 355));
		points.add(new Point(331, 355));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX8
		points.add(new Point(132, 355));
		points.add(new Point(132, 590));
		points.add(new Point(331, 590));
		lines.add(points);

		points = new LinkedList<Point>(); // to M3
		points.add(new Point(132, 590));
		points.add(new Point(132, 723));
		points.add(new Point(365, 723));
		points.add(new Point(365, 816));
		points.add(new Point(433, 816));
		lines.add(points);
		signals.put("ABUS", lines);

		lines = new LinkedList<List<Point>>(); // ABUS incremented
		points = new LinkedList<Point>(); // to MX6
		points.add(new Point(147, 64));
		points.add(new Point(246, 64));
		points.add(new Point(246, 101));
		points.add(new Point(331, 101));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX7
		points.add(new Point(161, 64));
		points.add(new Point(161, 336));
		points.add(new Point(331, 336));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX8
		points.add(new Point(161, 336));
		points.add(new Point(161, 571));
		points.add(new Point(331, 571));
		lines.add(points);
		signals.put("ABUSinc", lines);

		lines = new LinkedList<List<Point>>(); // ABUS0
		points = new LinkedList<Point>(); // to MI0 OR
		points.add(new Point(236, 191));
		points.add(new Point(263, 191));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI2 AND
		points.add(new Point(294, 655));
		points.add(new Point(318, 655));
		lines.add(points);
		signals.put("ABUS0", lines);

		lines = new LinkedList<List<Point>>(); // ABUS1
		points = new LinkedList<Point>(); // to MI0 OR
		points.add(new Point(236, 180));
		points.add(new Point(263, 180));
		lines.add(points);

		points = new LinkedList<Point>(); // to MX7
		points.add(new Point(369, 403));
		points.add(new Point(369, 392));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI2 AND
		points.add(new Point(294, 644));
		points.add(new Point(318, 644));
		lines.add(points);
		signals.put("ABUS1", lines);

		lines = new LinkedList<List<Point>>(); // MI0 OR
		points = new LinkedList<Point>(); // to MX6
		points.add(new Point(291, 186));
		points.add(new Point(369, 186));
		points.add(new Point(369, 157));
		lines.add(points);
		signals.put("MI0OR", lines);

		lines = new LinkedList<List<Point>>(); // MI2 AND
		points = new LinkedList<Point>(); // to MX8
		points.add(new Point(350, 649));
		points.add(new Point(369, 649));
		points.add(new Point(369, 627));
		lines.add(points);
		signals.put("MI2AND", lines);

		lines = new LinkedList<List<Point>>(); // MX6 out signal
		points = new LinkedList<Point>(); // to MI0
		points.add(new Point(407, 111));
		points.add(new Point(433, 111));
		lines.add(points);
		signals.put("MX6", lines);

		lines = new LinkedList<List<Point>>(); // MX7 out signal
		points = new LinkedList<Point>(); // to MI1
		points.add(new Point(407, 346));
		points.add(new Point(433, 346));
		lines.add(points);
		signals.put("MX7", lines);

		lines = new LinkedList<List<Point>>(); // MX8 out signal
		points = new LinkedList<Point>(); // to MI2
		points.add(new Point(407, 581));
		points.add(new Point(433, 581));
		lines.add(points);
		signals.put("MX8", lines);

		lines = new LinkedList<List<Point>>(); // RDBUS
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(60, 808));
		points.add(new Point(87, 808));
		lines.add(points);
		signals.put("!RDBUS", lines);

		lines = new LinkedList<List<Point>>(); // RDBUS inverted
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(123, 808));
		points.add(new Point(197, 808));
		lines.add(points);
		signals.put("RDBUSINV", lines);

		lines = new LinkedList<List<Point>>(); // WRBUS
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(61, 904));
		points.add(new Point(87, 904));
		lines.add(points);
		signals.put("!WRBUS", lines);

		lines = new LinkedList<List<Point>>(); // WRBUS inverted
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(123, 905));
		points.add(new Point(197, 905));
		lines.add(points);
		signals.put("WRBUSINV", lines);

		lines = new LinkedList<List<Point>>(); // M/IO
		points = new LinkedList<Point>(); // to upper AND
		points.add(new Point(132, 857));
		points.add(new Point(160, 857));
		points.add(new Point(160, 820));
		points.add(new Point(192, 820));
		lines.add(points);

		points = new LinkedList<Point>(); // to lower AND
		points.add(new Point(160, 857));
		points.add(new Point(160, 893));
		points.add(new Point(192, 893));
		lines.add(points);
		signals.put("M/IO", lines);

		lines = new LinkedList<List<Point>>(); // RD
		points = new LinkedList<Point>(); // from AND
		points.add(new Point(229, 814));
		points.add(new Point(236, 814));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI0
		points.add(new Point(677, 83));
		points.add(new Point(661, 83));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI1
		points.add(new Point(677, 318));
		points.add(new Point(661, 318));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI2
		points.add(new Point(677, 554));
		points.add(new Point(661, 554));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI3
		points.add(new Point(677, 790));
		points.add(new Point(661, 790));
		lines.add(points);
		signals.put("RD", lines);

		lines = new LinkedList<List<Point>>(); // WR
		points = new LinkedList<Point>(); // from AND
		points.add(new Point(229, 899));
		points.add(new Point(236, 899));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI0
		points.add(new Point(677, 128));
		points.add(new Point(661, 128));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI1
		points.add(new Point(677, 363));
		points.add(new Point(661, 363));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI2
		points.add(new Point(677, 599));
		points.add(new Point(661, 599));
		lines.add(points);

		points = new LinkedList<Point>(); // to MI3
		points.add(new Point(677, 835));
		points.add(new Point(661, 835));
		lines.add(points);
		signals.put("WR", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to fcMEM buffer
		points.add(new Point(75, 1002));
		points.add(new Point(92, 1002));
		lines.add(points);
		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // fcMEM
		points = new LinkedList<Point>(); // to fcMEM buffer
		points.add(new Point(106, 975));
		points.add(new Point(106, 994));
		lines.add(points);
		signals.put("fcMEM", lines);

		lines = new LinkedList<List<Point>>(); // FCBUS
		points = new LinkedList<Point>(); // from fcMEM buffer (negated)
		points.add(new Point(128, 1002));
		points.add(new Point(155, 1002));
		lines.add(points);
		signals.put("!FCBUS", lines);

		return memOprSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getMemTimeSignals() {

		if (memTimeSignals != null)
			return memTimeSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // RD
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(63, 15));
		points.add(new Point(75, 15));
		lines.add(points);

		points = new LinkedList<Point>(); // to MEMout AND
		points.add(new Point(91, 198));
		points.add(new Point(103, 198));
		lines.add(points);
		signals.put("RD", lines);

		lines = new LinkedList<List<Point>>(); // WR
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(63, 52));
		points.add(new Point(75, 52));
		lines.add(points);
		signals.put("WR", lines);

		lines = new LinkedList<List<Point>>(); // opr
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(116, 33));
		points.add(new Point(167, 33));
		lines.add(points);

		points = new LinkedList<Point>(); // to CMP E
		points.add(new Point(584, 277));
		points.add(new Point(564, 277));
		lines.add(points);
		signals.put("opr", lines);

		lines = new LinkedList<List<Point>>(); // fcMEM
		points = new LinkedList<Point>(); // to NOT
		points.add(new Point(79, 71));
		points.add(new Point(117, 71));
		lines.add(points);

		points = new LinkedList<Point>(); // to CL
		points.add(new Point(101, 71));
		points.add(new Point(101, 99));
		points.add(new Point(224, 99));
		points.add(new Point(224, 75));
		points.add(new Point(295, 75));
		lines.add(points);

		points = new LinkedList<Point>(); // to MEMout AND
		points.add(new Point(91, 160));
		points.add(new Point(103, 160));
		lines.add(points);

		points = new LinkedList<Point>(); // from CMP EQL
		points.add(new Point(492, 328));
		points.add(new Point(492, 348));
		lines.add(points);
		signals.put("fcMEM", lines);

		lines = new LinkedList<List<Point>>(); // fcMEM negated
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(153, 71));
		points.add(new Point(167, 71));
		lines.add(points);
		signals.put("fcMEMneg", lines);

		lines = new LinkedList<List<Point>>(); // MEMACC INC AND
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(213, 52));
		points.add(new Point(295, 52));
		lines.add(points);
		signals.put("MEMACC_INC_AND", lines);

		lines = new LinkedList<List<Point>>(); // MEMout
		points = new LinkedList<Point>(); // from MEMout AND
		points.add(new Point(149, 179));
		points.add(new Point(160, 179));
		lines.add(points);
		signals.put("MEMout", lines);

		lines = new LinkedList<List<Point>>(); // MEMACC
		points = new LinkedList<Point>(); // from MEMACC
		points.add(new Point(371, 84));
		points.add(new Point(371, 203));
		points.add(new Point(441, 203));
		points.add(new Point(441, 223));
		lines.add(points);
		signals.put("MEMACC", lines);

		lines = new LinkedList<List<Point>>(); // TSTART
		points = new LinkedList<Point>(); // to TIME LD
		points.add(new Point(721, 62));
		points.add(new Point(701, 62));
		lines.add(points);
		signals.put("TSTART", lines);

		lines = new LinkedList<List<Point>>(); // TIME
		points = new LinkedList<Point>(); // from TIME
		points.add(new Point(625, 84));
		points.add(new Point(625, 203));
		points.add(new Point(545, 203));
		points.add(new Point(545, 223));
		lines.add(points);
		signals.put("TIME", lines);

		lines = new LinkedList<List<Point>>(); // Generator 0
		points = new LinkedList<Point>(); // to MEMACC LD
		points.add(new Point(467, 52));
		points.add(new Point(447, 52));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // CLK
		points = new LinkedList<Point>(); // to MEMACC CLK
		points.add(new Point(467, 71));
		points.add(new Point(447, 71));
		lines.add(points);

		points = new LinkedList<Point>(); // to TIME CLK
		points.add(new Point(529, 62));
		points.add(new Point(548, 62));
		lines.add(points);
		signals.put("CLK", lines);

		return signals;

	}

	public HashMap<String, List<List<Point>>> getUprSigSignals() {

		if (uprSigSignals != null)
			return uprSigSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // mCW89
		points = new LinkedList<Point>(); // to DC
		points.add(new Point(62, 614));
		points.add(new Point(85, 614));
		lines.add(points);

		points = new LinkedList<Point>(); // to DC
		points.add(new Point(58, 1525));
		points.add(new Point(81, 1525));
		lines.add(points);

		signals.put("CW88", lines);

		lines = new LinkedList<List<Point>>(); // mCW90
		points = new LinkedList<Point>(); // to DC
		points.add(new Point(62, 581));
		points.add(new Point(85, 581));
		lines.add(points);

		points = new LinkedList<Point>(); // to DC
		points.add(new Point(58, 1492));
		points.add(new Point(81, 1492));
		lines.add(points);

		signals.put("CW89", lines);

		lines = new LinkedList<List<Point>>(); // mCW91
		points = new LinkedList<Point>(); // to DC
		points.add(new Point(62, 547));
		points.add(new Point(85, 547));
		lines.add(points);

		points = new LinkedList<Point>(); // to DC
		points.add(new Point(58, 1458));
		points.add(new Point(81, 1458));
		lines.add(points);

		signals.put("CW90", lines);

		lines = new LinkedList<List<Point>>(); // mCW92
		points = new LinkedList<Point>(); // to DC
		points.add(new Point(62, 513));
		points.add(new Point(85, 513));
		lines.add(points);

		points = new LinkedList<Point>(); // to DC
		points.add(new Point(58, 1424));
		points.add(new Point(81, 1424));
		lines.add(points);

		signals.put("CW91", lines);

		lines = new LinkedList<List<Point>>(); // mCW93
		points = new LinkedList<Point>(); // to DC
		points.add(new Point(170, 770));
		points.add(new Point(170, 753));
		lines.add(points);

		points = new LinkedList<Point>(); // to DC
		points.add(new Point(166, 1681));
		points.add(new Point(166, 1664));
		lines.add(points);

		signals.put("1", lines);

		lines = new LinkedList<List<Point>>(); // next
		points = new LinkedList<Point>(); // from DC
		points.add(new Point(243, 737)); // -15
		points.add(new Point(259, 737));
		points.add(new Point(259, 985));
		points.add(new Point(701, 985));
		lines.add(points);
		signals.put("next", lines);

		lines = new LinkedList<List<Point>>(); // br
		points = new LinkedList<Point>(); // from DC
		points.add(new Point(243, 714));
		points.add(new Point(304, 714));
		points.add(new Point(304, 957));
		points.add(new Point(701, 957));
		lines.add(points);
		signals.put("br", lines);

		lines = new LinkedList<List<Point>>(); // bropr
		points = new LinkedList<Point>(); // from DC
		points.add(new Point(243, 690));
		points.add(new Point(351, 690));
		points.add(new Point(351, 927));
		points.add(new Point(701, 927));
		lines.add(points);
		signals.put("bropr", lines);

		lines = new LinkedList<List<Point>>(); // DC_START
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 667));
		points.add(new Point(414, 667));
		points.add(new Point(414, 814));
		points.add(new Point(682, 814));
		lines.add(points);
		signals.put("DC_START", lines);

		lines = new LinkedList<List<Point>>(); // START
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 852));
		points.add(new Point(640, 852));// 665
		lines.add(points);
		signals.put("START", lines);

		lines = new LinkedList<List<Point>>(); // DC_hack
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 644));
		points.add(new Point(453, 644));
		points.add(new Point(453, 748));
		points.add(new Point(682, 748));
		lines.add(points);
		signals.put("DC_hack0", lines);

		lines = new LinkedList<List<Point>>(); // hack
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 784));
		points.add(new Point(640, 784));
		lines.add(points);
		signals.put("hack0", lines);

		lines = new LinkedList<List<Point>>(); // DC_fcCPU
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 621));
		points.add(new Point(480, 621));
		points.add(new Point(480, 678));
		points.add(new Point(682, 678));
		lines.add(points);
		signals.put("DC_fcCPU", lines);

		lines = new LinkedList<List<Point>>(); // fcCPU
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 716));
		points.add(new Point(640, 716));
		lines.add(points);
		signals.put("fcCPU", lines);

		lines = new LinkedList<List<Point>>(); // DC_gropr
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 598));
		points.add(new Point(513, 598));
		points.add(new Point(513, 610));
		points.add(new Point(682, 610));
		lines.add(points);
		signals.put("DC_gropr", lines);

		lines = new LinkedList<List<Point>>(); // gropr
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 648));
		points.add(new Point(640, 648));
		lines.add(points);
		signals.put("gropr", lines);

		lines = new LinkedList<List<Point>>(); // DC_!noSHIFT
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 575));
		points.add(new Point(512, 575));
		points.add(new Point(512, 542));
		points.add(new Point(682, 542));
		lines.add(points);
		signals.put("DC_!noSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // DC_noSHIFT
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 552));
		points.add(new Point(485, 552));
		points.add(new Point(485, 477));
		points.add(new Point(682, 477));
		lines.add(points);
		signals.put("DC_noSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // noSHIFT
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 580));
		points.add(new Point(640, 580));
		lines.add(points);

		points = new LinkedList<Point>(); // to AND
		points.add(new Point(633, 514));
		points.add(new Point(682, 514));
		lines.add(points);
		signals.put("noSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // DC_brjmp
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 529));
		points.add(new Point(456, 529));
		points.add(new Point(456, 408));
		points.add(new Point(682, 408));
		lines.add(points);
		signals.put("DC_brjmp", lines);

		lines = new LinkedList<List<Point>>(); // brjmp
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 446));
		points.add(new Point(640, 446));
		lines.add(points);
		signals.put("brjmp", lines);

		lines = new LinkedList<List<Point>>(); // DC_JMPREGIND
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 506));
		points.add(new Point(422, 506));
		points.add(new Point(422, 342));
		points.add(new Point(682, 342));
		lines.add(points);
		signals.put("DC_JMPREGIND", lines);

		lines = new LinkedList<List<Point>>(); // JMPREGIND
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(633, 380));
		points.add(new Point(682, 380));
		lines.add(points);
		signals.put("JMPREGIND", lines);

		lines = new LinkedList<List<Point>>(); // DC_prekid
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 483));
		points.add(new Point(399, 483));
		points.add(new Point(399, 274));
		points.add(new Point(682, 274));
		lines.add(points);
		signals.put("DC_prekid", lines);

		lines = new LinkedList<List<Point>>(); // prekid
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 312));
		points.add(new Point(640, 312));
		lines.add(points);
		signals.put("prekid", lines);

		lines = new LinkedList<List<Point>>(); // DC_PRINS
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 459));
		points.add(new Point(375, 459));
		points.add(new Point(375, 206));
		points.add(new Point(682, 206));
		lines.add(points);
		signals.put("DC_PRINS", lines);

		lines = new LinkedList<List<Point>>(); // PRINS
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 244));
		points.add(new Point(640, 244));
		lines.add(points);
		signals.put("PRINS", lines);

		lines = new LinkedList<List<Point>>(); // DC_PRCOD
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 437));
		points.add(new Point(349, 437));
		points.add(new Point(349, 138));
		points.add(new Point(682, 138));
		lines.add(points);
		signals.put("DC_PRCOD", lines);

		lines = new LinkedList<List<Point>>(); // PRCOD
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 175));
		points.add(new Point(640, 175));
		lines.add(points);
		signals.put("PRCOD", lines);

		lines = new LinkedList<List<Point>>(); // DC_PRADR
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 413));
		points.add(new Point(325, 413));
		points.add(new Point(325, 70));
		points.add(new Point(682, 70));
		lines.add(points);
		signals.put("DC_PRADR", lines);

		lines = new LinkedList<List<Point>>(); // PRADR
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 107));
		points.add(new Point(640, 107));
		lines.add(points);
		signals.put("PRADR", lines);

		lines = new LinkedList<List<Point>>(); // DC_printr
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(243, 390));
		points.add(new Point(287, 390));
		points.add(new Point(287, 1));
		points.add(new Point(682, 1));
		lines.add(points);
		signals.put("DC_printr", lines);

		lines = new LinkedList<List<Point>>(); // printr
		points = new LinkedList<Point>(); // to AND
		points.add(new Point(628, 39));
		points.add(new Point(638, 39));
		lines.add(points);
		signals.put("printr", lines);

		lines = new LinkedList<List<Point>>(); // START_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 833));
		points.add(new Point(909, 833));
		points.add(new Point(909, 663));
		points.add(new Point(952, 663));
		lines.add(points);
		signals.put("AND_START", lines);

		lines = new LinkedList<List<Point>>(); // hack_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 765));
		points.add(new Point(890, 765));
		points.add(new Point(890, 625));
		points.add(new Point(952, 625));
		lines.add(points);
		signals.put("AND_hack0", lines);

		lines = new LinkedList<List<Point>>(); // fcCPU_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 697));
		points.add(new Point(871, 697));
		points.add(new Point(871, 587));
		points.add(new Point(952, 587));
		lines.add(points);
		signals.put("AND_fcCPU", lines);

		lines = new LinkedList<List<Point>>(); // gropr_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 628));
		points.add(new Point(852, 628));
		points.add(new Point(852, 549));
		points.add(new Point(952, 549));
		lines.add(points);
		signals.put("AND_gropr", lines);

		lines = new LinkedList<List<Point>>(); // !noSHIFT_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 560));
		points.add(new Point(833, 560));
		points.add(new Point(833, 514));
		points.add(new Point(952, 514));
		lines.add(points);
		signals.put("AND_!noSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // noSHIFT_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 494));
		points.add(new Point(814, 494));
		points.add(new Point(814, 476));
		points.add(new Point(952, 476));
		lines.add(points);
		signals.put("AND_noSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // brjmp_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 426));
		points.add(new Point(814, 426));
		points.add(new Point(814, 438));
		points.add(new Point(952, 438));
		lines.add(points);
		signals.put("AND_brjmp", lines);

		lines = new LinkedList<List<Point>>(); // JMPREGIND_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 360));
		points.add(new Point(814, 360));
		points.add(new Point(814, 400));
		points.add(new Point(952, 400));
		lines.add(points);
		signals.put("AND_JMPREGIND", lines);

		lines = new LinkedList<List<Point>>(); // prekid_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 292));
		points.add(new Point(833, 292));
		points.add(new Point(833, 363));
		points.add(new Point(952, 363));
		lines.add(points);
		signals.put("AND_prekid", lines);

		lines = new LinkedList<List<Point>>(); // PRINS_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 224));
		points.add(new Point(852, 224));
		points.add(new Point(852, 325));
		points.add(new Point(952, 325));
		lines.add(points);
		signals.put("AND_PRINS", lines);

		lines = new LinkedList<List<Point>>(); // PRCOD_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 156));
		points.add(new Point(871, 156));
		points.add(new Point(871, 287));
		points.add(new Point(952, 287));
		lines.add(points);
		signals.put("AND_PRCOD", lines);

		lines = new LinkedList<List<Point>>(); // PRADR_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 88));
		points.add(new Point(890, 88));
		points.add(new Point(890, 249));
		points.add(new Point(952, 249));
		lines.add(points);
		signals.put("AND_PRADR", lines);

		lines = new LinkedList<List<Point>>(); // printr_AND
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(729, 19));
		points.add(new Point(928, 19));
		points.add(new Point(928, 211));
		points.add(new Point(952, 211));
		lines.add(points);
		signals.put("AND_printr", lines);

		lines = new LinkedList<List<Point>>(); // branch1
		points = new LinkedList<Point>(); // from OR
		points.add(new Point(993, 438));
		points.add(new Point(1003, 438));
		lines.add(points);
		signals.put("branch1", lines);

		return uprSigSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getOprSigSignals() {

		if (oprSigSignals != null)
			return oprSigSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // ldBADDR1
		points = new LinkedList<Point>(); // from CW1
		points.add(new Point(43, 14));
		points.add(new Point(80, 14));
		lines.add(points);
		signals.put("ldBADDR1", lines);

		lines = new LinkedList<List<Point>>(); // ldBADDR2
		points = new LinkedList<Point>(); // from CW2
		points.add(new Point(245, 14));
		points.add(new Point(281, 14));
		lines.add(points);
		signals.put("ldBADDR2", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR1
		points = new LinkedList<Point>(); // from CW3
		points.add(new Point(446, 14));
		points.add(new Point(483, 14));
		lines.add(points);
		signals.put("mxADDR1", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR2
		points = new LinkedList<Point>(); // from CW4
		points.add(new Point(648, 14));
		points.add(new Point(685, 14));
		lines.add(points);
		signals.put("mxADDR2", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR3
		points = new LinkedList<Point>(); // from CW5
		points.add(new Point(849, 14));
		points.add(new Point(886, 14));
		lines.add(points);
		signals.put("mxADDR3", lines);

		lines = new LinkedList<List<Point>>(); // ldGPRADDR1
		points = new LinkedList<Point>(); // from CW6
		points.add(new Point(1051, 14));
		points.add(new Point(1088, 14));
		lines.add(points);
		signals.put("ldGPRADDR1", lines);

		lines = new LinkedList<List<Point>>(); // ldGPRADDR2
		points = new LinkedList<Point>(); // from CW7
		points.add(new Point(1253, 14));
		points.add(new Point(1289, 14));
		lines.add(points);
		signals.put("ldGPRADDR2", lines);

		lines = new LinkedList<List<Point>>(); // ldGPRADDR3
		points = new LinkedList<Point>(); // from CW8
		points.add(new Point(43, 69));
		points.add(new Point(80, 69));
		lines.add(points);
		signals.put("ldGPRADDR3", lines);

		lines = new LinkedList<List<Point>>(); // ldPC
		points = new LinkedList<Point>(); // from CW9
		points.add(new Point(245, 69));
		points.add(new Point(281, 69));
		lines.add(points);
		signals.put("ldPC", lines);

		lines = new LinkedList<List<Point>>(); // incPC
		points = new LinkedList<Point>(); // from CW10
		points.add(new Point(446, 69));
		points.add(new Point(483, 69));
		lines.add(points);
		signals.put("incPC", lines);

		lines = new LinkedList<List<Point>>(); // PCout1
		points = new LinkedList<Point>(); // from CW11
		points.add(new Point(648, 69));
		points.add(new Point(685, 69));
		lines.add(points);
		signals.put("PCout1", lines);

		lines = new LinkedList<List<Point>>(); // PCout2
		points = new LinkedList<Point>(); // from CW12
		points.add(new Point(849, 69));
		points.add(new Point(886, 69));
		lines.add(points);
		signals.put("PCout2", lines);

		lines = new LinkedList<List<Point>>(); // incSP
		points = new LinkedList<Point>(); // from CW13
		points.add(new Point(1051, 69));
		points.add(new Point(1088, 69));
		lines.add(points);
		signals.put("incSP", lines);

		lines = new LinkedList<List<Point>>(); // decSP
		points = new LinkedList<Point>(); // from CW14
		points.add(new Point(1253, 69));
		points.add(new Point(1289, 69));
		lines.add(points);
		signals.put("decSP", lines);

		lines = new LinkedList<List<Point>>(); // SPout1
		points = new LinkedList<Point>(); // from CW15
		points.add(new Point(43, 123));
		points.add(new Point(80, 123));
		lines.add(points);
		signals.put("SPout1", lines);

		lines = new LinkedList<List<Point>>(); // wrGPR
		points = new LinkedList<Point>(); // from CW16
		points.add(new Point(245, 123));
		points.add(new Point(281, 123));
		lines.add(points);
		signals.put("wrGPR", lines);

		lines = new LinkedList<List<Point>>(); // rdGPR1
		points = new LinkedList<Point>(); // from CW17
		points.add(new Point(446, 123));
		points.add(new Point(483, 123));
		lines.add(points);
		signals.put("rdGPR1", lines);

		lines = new LinkedList<List<Point>>(); // rdGPR2
		points = new LinkedList<Point>(); // from CW18
		points.add(new Point(648, 123));
		points.add(new Point(685, 123));
		lines.add(points);
		signals.put("rdGPR2", lines);

		lines = new LinkedList<List<Point>>(); // mxSIg
		points = new LinkedList<Point>(); // from CW19
		points.add(new Point(849, 123));
		points.add(new Point(886, 123));
		lines.add(points);
		signals.put("mxSIG", lines);

		lines = new LinkedList<List<Point>>(); // ADDout
		points = new LinkedList<Point>(); // from CW20
		points.add(new Point(1051, 123));
		points.add(new Point(1088, 123));
		lines.add(points);
		signals.put("ADDout", lines);

		lines = new LinkedList<List<Point>>(); // decCNTSHIFT
		points = new LinkedList<Point>(); // from CW21
		points.add(new Point(1253, 123));
		points.add(new Point(1289, 123));
		lines.add(points);
		signals.put("decCNTSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // not
		points = new LinkedList<Point>(); // from CW22
		points.add(new Point(43, 177));
		points.add(new Point(80, 177));
		lines.add(points);
		signals.put("not", lines);

		lines = new LinkedList<List<Point>>(); // or
		points = new LinkedList<Point>(); // from CW23
		points.add(new Point(245, 177));
		points.add(new Point(281, 177));
		lines.add(points);
		signals.put("or", lines);

		lines = new LinkedList<List<Point>>(); // and
		points = new LinkedList<Point>(); // from CW24
		points.add(new Point(446, 177));
		points.add(new Point(483, 177));
		lines.add(points);
		signals.put("and", lines);

		lines = new LinkedList<List<Point>>(); // add
		points = new LinkedList<Point>(); // from CW25
		points.add(new Point(648, 177));
		points.add(new Point(685, 177));
		lines.add(points);
		signals.put("add", lines);

		lines = new LinkedList<List<Point>>(); // sub
		points = new LinkedList<Point>(); // from CW26
		points.add(new Point(849, 177));
		points.add(new Point(886, 177));
		lines.add(points);
		signals.put("sub", lines);

		lines = new LinkedList<List<Point>>(); // shl
		points = new LinkedList<Point>(); // from CW27
		points.add(new Point(1051, 177));
		points.add(new Point(1088, 177));
		lines.add(points);
		signals.put("shl", lines);

		lines = new LinkedList<List<Point>>(); // shr
		points = new LinkedList<Point>(); // from CW28
		points.add(new Point(1253, 177));
		points.add(new Point(1289, 177));
		lines.add(points);
		signals.put("shr", lines);

		lines = new LinkedList<List<Point>>(); // ALUout
		points = new LinkedList<Point>(); // from CW29
		points.add(new Point(43, 231));
		points.add(new Point(80, 231));
		lines.add(points);
		signals.put("ALUout", lines);

		lines = new LinkedList<List<Point>>(); // TSTART
		points = new LinkedList<Point>(); // from CW30
		points.add(new Point(245, 231));
		points.add(new Point(281, 231));
		lines.add(points);
		signals.put("TSTART", lines);

		lines = new LinkedList<List<Point>>(); // clSTART
		points = new LinkedList<Point>(); // from CW31
		points.add(new Point(446, 231));
		points.add(new Point(483, 231));
		lines.add(points);
		signals.put("clSTART", lines);

		lines = new LinkedList<List<Point>>(); // ldN
		points = new LinkedList<Point>(); // from CW32
		points.add(new Point(648, 231));
		points.add(new Point(685, 231));
		lines.add(points);
		signals.put("ldN", lines);

		lines = new LinkedList<List<Point>>(); // ldZ
		points = new LinkedList<Point>(); // from CW33
		points.add(new Point(849, 231));
		points.add(new Point(886, 231));
		lines.add(points);
		signals.put("ldZ", lines);

		lines = new LinkedList<List<Point>>(); // ldC
		points = new LinkedList<Point>(); // from CW34
		points.add(new Point(1051, 231));
		points.add(new Point(1088, 231));
		lines.add(points);
		signals.put("ldC", lines);

		lines = new LinkedList<List<Point>>(); // ldV
		points = new LinkedList<Point>(); // from CW35
		points.add(new Point(1253, 231));
		points.add(new Point(1289, 231));
		lines.add(points);
		signals.put("ldV", lines);

		lines = new LinkedList<List<Point>>(); // ldL
		points = new LinkedList<Point>(); // from CW36
		points.add(new Point(43, 285));
		points.add(new Point(80, 285));
		lines.add(points);
		signals.put("ldL", lines);

		lines = new LinkedList<List<Point>>(); // PSWout
		points = new LinkedList<Point>(); // from CW37
		points.add(new Point(245, 285));
		points.add(new Point(281, 285));
		lines.add(points);
		signals.put("PSWout", lines);

		lines = new LinkedList<List<Point>>(); // stPSWI
		points = new LinkedList<Point>(); // from CW38
		points.add(new Point(446, 285));
		points.add(new Point(483, 285));
		lines.add(points);
		signals.put("stPSWI", lines);

		lines = new LinkedList<List<Point>>(); // clPSWI
		points = new LinkedList<Point>(); // from CW39
		points.add(new Point(648, 285));
		points.add(new Point(685, 285));
		lines.add(points);
		signals.put("clPSWI", lines);

		lines = new LinkedList<List<Point>>(); // stPSWT
		points = new LinkedList<Point>(); // from CW40
		points.add(new Point(849, 285));
		points.add(new Point(886, 285));
		lines.add(points);
		signals.put("stPSWT", lines);

		lines = new LinkedList<List<Point>>(); // clPSWT
		points = new LinkedList<Point>(); // from CW41
		points.add(new Point(1051, 285));
		points.add(new Point(1088, 285));
		lines.add(points);
		signals.put("clPSWT", lines);

		lines = new LinkedList<List<Point>>(); // JMPPOMout
		points = new LinkedList<Point>(); // from CW42
		points.add(new Point(1253, 285));
		points.add(new Point(1289, 285));
		lines.add(points);
		signals.put("JMPPOMout", lines);

		lines = new LinkedList<List<Point>>(); // JPOMout
		points = new LinkedList<Point>(); // from CW43
		points.add(new Point(43, 339));
		points.add(new Point(80, 339));
		lines.add(points);
		signals.put("JPOMout", lines);

		lines = new LinkedList<List<Point>>(); // IRPOMout
		points = new LinkedList<Point>(); // from CW44
		points.add(new Point(245, 339));
		points.add(new Point(281, 339));
		lines.add(points);
		signals.put("IRPOMout", lines);

		lines = new LinkedList<List<Point>>(); // stPRINS
		points = new LinkedList<Point>(); // from CW45
		points.add(new Point(446, 339));
		points.add(new Point(483, 339));
		lines.add(points);
		signals.put("stPRINS", lines);

		lines = new LinkedList<List<Point>>(); // clPRINS
		points = new LinkedList<Point>(); // from CW46
		points.add(new Point(648, 339));
		points.add(new Point(685, 339));
		lines.add(points);
		signals.put("clPRINS", lines);

		lines = new LinkedList<List<Point>>(); // stPRCOD
		points = new LinkedList<Point>(); // from CW47
		points.add(new Point(849, 339));
		points.add(new Point(886, 339));
		lines.add(points);
		signals.put("stPRCOD", lines);

		lines = new LinkedList<List<Point>>(); // clPRCOD
		points = new LinkedList<Point>(); // from CW48
		points.add(new Point(1051, 339));
		points.add(new Point(1088, 339));
		lines.add(points);
		signals.put("clPRCOD", lines);

		lines = new LinkedList<List<Point>>(); // clPRINM
		points = new LinkedList<Point>(); // from CW49
		points.add(new Point(1253, 339));
		points.add(new Point(1289, 339));
		lines.add(points);
		signals.put("clPRINM", lines);

		lines = new LinkedList<List<Point>>(); // clINTR1
		points = new LinkedList<Point>(); // from CW50
		points.add(new Point(43, 393));
		points.add(new Point(80, 393));
		lines.add(points);
		signals.put("clINTR", lines);

		lines = new LinkedList<List<Point>>(); // clINTR2
		points = new LinkedList<Point>(); // from CW51
		points.add(new Point(245, 393));
		points.add(new Point(281, 393));
		lines.add(points);
		signals.put("UINTout", lines);

		lines = new LinkedList<List<Point>>(); // clINTR3
		points = new LinkedList<Point>(); // from CW52
		points.add(new Point(446, 393));
		points.add(new Point(483, 393));
		lines.add(points);
		signals.put("UEXTout", lines);

		lines = new LinkedList<List<Point>>(); // clINTR4
		points = new LinkedList<Point>(); // from CW53
		points.add(new Point(648, 393));
		points.add(new Point(685, 393));
		lines.add(points);
		signals.put("BRout", lines);

		lines = new LinkedList<List<Point>>(); // clINTR5
		points = new LinkedList<Point>(); // from CW54
		points.add(new Point(849, 393));
		points.add(new Point(886, 393));
		lines.add(points);
		signals.put("IVTPout", lines);

		lines = new LinkedList<List<Point>>(); // clINTR6
		points = new LinkedList<Point>(); // from CW55
		points.add(new Point(1051, 393));
		points.add(new Point(1088, 393));
		lines.add(points);
		signals.put("MEMI/Oout", lines);

		lines = new LinkedList<List<Point>>(); // clINTR7
		points = new LinkedList<Point>(); // from CW56
		points.add(new Point(1253, 393));
		points.add(new Point(1289, 393));
		lines.add(points);
		signals.put("hreq0", lines);

		lines = new LinkedList<List<Point>>(); // UIXTout
		points = new LinkedList<Point>(); // from CW57
		points.add(new Point(43, 447));
		points.add(new Point(80, 447));
		lines.add(points);
		signals.put("SPout2", lines);

		lines = new LinkedList<List<Point>>(); // UEXTout
		points = new LinkedList<Point>(); // from CW58
		points.add(new Point(245, 447));
		points.add(new Point(281, 447));
		lines.add(points);
		signals.put("ldMAR", lines);

		lines = new LinkedList<List<Point>>(); // BRout
		points = new LinkedList<Point>(); // from CW59
		points.add(new Point(446, 447));
		points.add(new Point(483, 447));
		lines.add(points);
		signals.put("MARout", lines);

		lines = new LinkedList<List<Point>>(); // IVTPout
		points = new LinkedList<Point>(); // from CW60
		points.add(new Point(648, 447));
		points.add(new Point(685, 447));
		lines.add(points);
		signals.put("MARAout", lines);

		lines = new LinkedList<List<Point>>(); // MEM/IOout
		points = new LinkedList<Point>(); // from CW61
		points.add(new Point(849, 447));
		points.add(new Point(886, 447));
		lines.add(points);
		signals.put("mxMDR", lines);

		lines = new LinkedList<List<Point>>(); // doMEM
		points = new LinkedList<Point>(); // from CW62
		points.add(new Point(1051, 447));
		points.add(new Point(1088, 447));
		lines.add(points);
		signals.put("ldMDR", lines);

		lines = new LinkedList<List<Point>>(); // ldBADDR1
		points = new LinkedList<Point>(); // from CW63
		points.add(new Point(1253, 447));
		points.add(new Point(1289, 447));
		lines.add(points);
		signals.put("MDRout", lines);

		lines = new LinkedList<List<Point>>(); // ldMAR
		points = new LinkedList<Point>(); // from CW64
		points.add(new Point(43, 501));
		points.add(new Point(80, 501));
		lines.add(points);
		signals.put("rdCPU", lines);

		lines = new LinkedList<List<Point>>(); // MARout
		points = new LinkedList<Point>(); // from CW65
		points.add(new Point(245, 501));
		points.add(new Point(281, 501));
		lines.add(points);
		signals.put("wrCPU", lines);

		lines = new LinkedList<List<Point>>(); // MARAout
		points = new LinkedList<Point>(); // from CW66
		points.add(new Point(446, 501));
		points.add(new Point(483, 501));
		lines.add(points);
		signals.put("NULLBUS1", lines);

		lines = new LinkedList<List<Point>>(); // mxMDR
		points = new LinkedList<Point>(); // from CW67
		points.add(new Point(648, 501));
		points.add(new Point(685, 501));
		lines.add(points);
		signals.put("BRG2_3", lines);

		lines = new LinkedList<List<Point>>(); // ldMDR
		points = new LinkedList<Point>(); // from CW68
		points.add(new Point(849, 501));
		points.add(new Point(886, 501));
		lines.add(points);
		signals.put("BRG1_2", lines);

		lines = new LinkedList<List<Point>>(); // MDRout
		points = new LinkedList<Point>(); // from CW69
		points.add(new Point(1051, 501));
		points.add(new Point(1088, 501));
		lines.add(points);
		signals.put("BRG1_3", lines);

		lines = new LinkedList<List<Point>>(); // rdCPU
		points = new LinkedList<Point>(); // from CW70
		points.add(new Point(1253, 501));
		points.add(new Point(1289, 501));
		lines.add(points);
		signals.put("ldCNTSHIFT", lines);

		lines = new LinkedList<List<Point>>(); // wrCPU
		points = new LinkedList<Point>(); // from CW71
		points.add(new Point(43, 554));
		points.add(new Point(80, 554));
		lines.add(points);
		signals.put("inm", lines);

		lines = new LinkedList<List<Point>>(); // NULLBUS1
		points = new LinkedList<Point>(); // from CW72
		points.add(new Point(245, 554));
		points.add(new Point(281, 554));
		lines.add(points);
		signals.put("ldIR", lines);

		lines = new LinkedList<List<Point>>(); // BRG2_3
		points = new LinkedList<Point>(); // from CW73
		points.add(new Point(446, 554));
		points.add(new Point(483, 554));
		lines.add(points);
		signals.put("stERROR", lines);

		lines = new LinkedList<List<Point>>(); // BRG1_2
		points = new LinkedList<Point>(); // from CW74
		points.add(new Point(648, 554));
		points.add(new Point(685, 554));
		lines.add(points);
		signals.put("ldBR", lines);

		lines = new LinkedList<List<Point>>(); // fcMEM
		points = new LinkedList<Point>(); // from CW75
		points.add(new Point(849, 554));
		points.add(new Point(886, 554));
		lines.add(points);
		signals.put("ldPSW", lines);

		lines = new LinkedList<List<Point>>(); // MEMout
		points = new LinkedList<Point>(); // from CW76
		points.add(new Point(1051, 554));
		points.add(new Point(1088, 554));
		lines.add(points);
		signals.put("M/IO", lines);

		lines = new LinkedList<List<Point>>(); // mxADDR0
		points = new LinkedList<Point>(); // from CW76
		points.add(new Point(1253, 554));
		points.add(new Point(1289, 554));
		lines.add(points);
		signals.put("mxADDR0", lines);

		lines = new LinkedList<List<Point>>(); // hclr0
		points = new LinkedList<Point>(); // from CW71
		points.add(new Point(43, 611));
		points.add(new Point(80, 611));
		lines.add(points);
		signals.put("hclr0", lines);

		lines = new LinkedList<List<Point>>(); // hreq1
		points = new LinkedList<Point>(); // from CW72
		points.add(new Point(245, 611));
		points.add(new Point(281, 611));
		lines.add(points);
		signals.put("hreq1", lines);

		lines = new LinkedList<List<Point>>(); // hclr1
		points = new LinkedList<Point>(); // from CW73
		points.add(new Point(446, 611));
		points.add(new Point(483, 611));
		lines.add(points);
		signals.put("hclr1", lines);

		lines = new LinkedList<List<Point>>(); // ldIVTP
		points = new LinkedList<Point>(); // from CW74
		points.add(new Point(648, 611));
		points.add(new Point(685, 611));
		lines.add(points);
		signals.put("ldIVTP", lines);

		lines = new LinkedList<List<Point>>(); // MDRDout
		points = new LinkedList<Point>(); // from CW75
		points.add(new Point(849, 611));
		points.add(new Point(886, 611));
		lines.add(points);
		signals.put("MDRDout", lines);

		// lines = new LinkedList<List<Point>>(); // MEMout
		// points = new LinkedList<Point>(); // from CW76
		// points.add(new Point(1051, 611));
		// points.add(new Point(1088, 611));
		// lines.add(points);
		// signals.put("M/IO", lines);

		// lines = new LinkedList<List<Point>>(); // MEMout
		// points = new LinkedList<Point>(); // from CW76
		// points.add(new Point(1253, 611));
		// points.add(new Point(1289, 611));
		// lines.add(points);
		// signals.put("mxADDR0", lines);

		return oprSigSignals = signals;

	}

	public HashMap<String, List<List<Point>>> getUpravSignals() {

		if (upravSignals != null)
			return upravSignals;

		HashMap<String, List<List<Point>>> signals = new HashMap<String, List<List<Point>>>();
		List<List<Point>> lines;
		List<Point> points;

		lines = new LinkedList<List<Point>>(); // ADD
		points = new LinkedList<Point>(); // to KMOPR
		points.add(new Point(72, 40));
		points.add(new Point(72, 73));
		lines.add(points);
		signals.put("ADD", lines);

		lines = new LinkedList<List<Point>>(); // SUB
		points = new LinkedList<Point>(); // to KMOPR
		points.add(new Point(110, 40));
		points.add(new Point(110, 73));
		lines.add(points);
		signals.put("SUB", lines);

		lines = new LinkedList<List<Point>>(); // IRET
		points = new LinkedList<Point>(); // to KMOPR
		points.add(new Point(220, 40));
		points.add(new Point(220, 73));
		lines.add(points);
		signals.put("IRET", lines);

		lines = new LinkedList<List<Point>>(); // KMOPR
		points = new LinkedList<Point>(); // to MP
		points.add(new Point(145, 143));
		points.add(new Point(145, 187));
		points.add(new Point(96, 187));
		points.add(new Point(96, 224));
		lines.add(points);
		signals.put("KMOPR", lines);

		lines = new LinkedList<List<Point>>(); // bropr
		points = new LinkedList<Point>(); // to KMOPR
		points.add(new Point(270, 259));
		points.add(new Point(252, 259));
		lines.add(points);

		points = new LinkedList<Point>(); // to OR
		points.add(new Point(451, 369));
		points.add(new Point(440, 369));
		lines.add(points);
		signals.put("bropr", lines);

		lines = new LinkedList<List<Point>>(); // MP out signal
		points = new LinkedList<Point>(); // to mPC
		points.add(new Point(142, 294));
		points.add(new Point(142, 375));
		lines.add(points);
		signals.put("MP", lines);

		lines = new LinkedList<List<Point>>(); // branch
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(451, 406));
		points.add(new Point(440, 406));
		lines.add(points);
		signals.put("branch", lines);

		lines = new LinkedList<List<Point>>(); // br
		points = new LinkedList<Point>(); // to OR
		points.add(new Point(451, 444));
		points.add(new Point(440, 444));
		lines.add(points);
		signals.put("br", lines);

		lines = new LinkedList<List<Point>>(); // LD OR
		points = new LinkedList<Point>(); // to LD
		points.add(new Point(399, 406));
		points.add(new Point(285, 406));
		points.add(new Point(285, 390));
		points.add(new Point(252, 390));
		lines.add(points);

		points = new LinkedList<Point>(); // to INC
		points.add(new Point(285, 406));
		points.add(new Point(285, 429));
		points.add(new Point(257, 429));
		lines.add(points);
		signals.put("LD_OR", lines);

		lines = new LinkedList<List<Point>>(); // CLKmPC
		points = new LinkedList<Point>(); // to mPC
		points.add(new Point(17, 410));
		points.add(new Point(35, 410));
		lines.add(points);
		signals.put("CLK", lines);

		points = new LinkedList<Point>(); // to CW
		points.add(new Point(35, 745));
		points.add(new Point(16, 745));
		lines.add(points);
		signals.put("CLK", lines);

		lines = new LinkedList<List<Point>>(); // mPC out signal
		points = new LinkedList<Point>(); // to mMEM
		points.add(new Point(145, 445));
		points.add(new Point(145, 526));
		lines.add(points);
		signals.put("mPC", lines);

		lines = new LinkedList<List<Point>>(); // Generator0
		points = new LinkedList<Point>(); // to mMEM
		points.add(new Point(17, 561));
		points.add(new Point(35, 561));
		lines.add(points);
		signals.put("0", lines);

		lines = new LinkedList<List<Point>>(); // mMEM out signal
		points = new LinkedList<Point>(); // to CW
		points.add(new Point(147, 596));
		points.add(new Point(147, 661));
		points.add(new Point(364, 661));
		points.add(new Point(364, 707));
		lines.add(points);
		signals.put("mMEM", lines);

		lines = new LinkedList<List<Point>>(); // CW87..0
		points = new LinkedList<Point>(); // to signals block
		points.add(new Point(232, 814));
		points.add(new Point(232, 906));
		points.add(new Point(204, 906));
		points.add(new Point(204, 953));
		lines.add(points);
		signals.put("CW0..87", lines);

		lines = new LinkedList<List<Point>>(); // CW95..88
		points = new LinkedList<Point>(); // to signals block
		points.add(new Point(475, 814));
		points.add(new Point(475, 916));
		points.add(new Point(742, 916));
		points.add(new Point(742, 953));
		lines.add(points);
		signals.put("CW88..95", lines);

		lines = new LinkedList<List<Point>>(); // CW103..96
		points = new LinkedList<Point>(); // to MP
		points.add(new Point(616, 814));
		points.add(new Point(616, 840));
		points.add(new Point(931, 840));
		points.add(new Point(931, 184));
		points.add(new Point(202, 184));
		points.add(new Point(202, 224));
		lines.add(points);
		signals.put("CW96..103", lines);

		lines = new LinkedList<List<Point>>(); // Generator1
		points = new LinkedList<Point>(); // to CW
		points.add(new Point(704, 742));
		points.add(new Point(686, 742));
		lines.add(points);
		signals.put("1", lines);

		points = new LinkedList<Point>(); // to mMEM
		points.add(new Point(269, 560));
		points.add(new Point(252, 560));
		lines.add(points);
		signals.put("1", lines);

		return upravSignals = signals;

	}

	public void clear() {
		madrSignals = null;
		addr1Signals = null;
		addr2Signals = null;
		arbmodSignals = null;
		arbmodIOSignals = null;
		arbSignals = null;
		exec1Signals = null;
		exec2Signals = null;
		exec4Signals = null;
		fetch1Signals = null;
		int1Signals = null;
		int2Signals = null;
		int3Signals = null;
		ioSignals = null;
		memOprSignals = null;
		memTimeSignals = null;
		oprSigSignals = null;
		uprSigSignals = null;
		upravSignals = null;
	}

}
