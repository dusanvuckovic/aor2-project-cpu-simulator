package simulator2.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import simulator2.Signal;

// Klasa koja predstavja jednu testualnu labelu
public class GuiTextLabel implements Drawable {

	int fontSize = 14;	// podrazumevano 14;

	protected String label;	
	protected Signal signal;
	protected int x;
	protected int y;

	public GuiTextLabel(int xx, int yy, Signal s) { //konstruktor sa signalom
		x = xx;
		y = yy;
		signal = s;
		label = s.getName();
	}
	
	public GuiTextLabel(int xx, int yy, String l) { //konstruktor sa tekstom
		x = xx;
		y = yy;
		label = l;
	}

	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.setFont(new Font("Calibri", Font.PLAIN, fontSize));
		g.drawString(label, x, y);
	}

	@Override
	public void update() {
		//ne menja se
	}

	
}