package simulator2.gui;

import java.util.List;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import simulator2.Port;
import simulator2.Signal;
import simulator2.digitalcomponents.OneOutputGate;
import simulator2.digitalcomponents.DecoderE;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.staticinit.StaticInitializer;
import simulator2.dynamicinit.GraphingContainer;

public class DynamicGuiUtility {
	
	public void setDC2(ArrayList<GraphingContainer.JumpData> list, GuiSchema schema) {
		
		
		//ako nema dodatnih skokova:
		if (GraphingContainer.howManyDynamicJumps == 0) {
			List<List<Point>> lines;
			List<Point> points;
			
			lines = new LinkedList<List<Point>>();
			
			points = new LinkedList<Point>();
			points.add(new Point(600, 1100)); 
			points.add(new Point(650, 1100));
			lines.add(points);
			
			schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get("branch1")));
			schema.addTextLabel(new GuiTextLabel(550, 1105, "branch1"));
			schema.addTextLabel(new GuiTextLabel(660, 1105, "branch"));
			return;
		} 
		
		int startX = 238;	//x koordinata izlaza 0 dekodera
		int startY = 1647;	//y koordinata izlaza 0 dekodera
		int YdecDC = 23; 	//razmak izmedju izlaza dekodera
		int YdecAND = 40;	//razmak izmedju y koordinata I kola
		
		//razmak od dekodera do pregiba
		int spaceX =  30;
		//pocetno dodavanje Y koordinate
		int spaceY = 400;
		
		//koordinate nultog I kola 
		int andX = 600;
		//int andY = 550;
		
		ArrayList<Point> andExitCoords = new ArrayList<Point>();	//koordinate izlaza I kola
		
		BufferedImage imageAND = null, imageNOT = null, imageOR = null;

		
		//slika za I kola
		File imagefileAND = new File("slike/AND2.png");
		try {
			imageAND = ImageIO.read(imagefileAND); 
		} catch (IOException e) {
			//nista za sad
		}
	
		
		//slika za ILI kolo
		String filename = null;
		
		switch (list.size()) {
		case 1: filename = "slike/OR2.png"; break;
		case 2: filename = "slike/OR2.png"; break;
		case 3: filename = "slike/OR3.png"; break;
		case 4: filename = "slike/OR4.png"; break;
		case 5: filename = "slike/OR5.png"; break;
		case 6: filename = "slike/OR6.png"; break;
		case 7: filename = "slike/OR7.png"; break;
		case 8: filename = "slike/OR8.png"; break;
		case 9: filename = "slike/OR9.png"; break;
		case 10: filename = "slike/OR10.png"; break;
		case 11: filename = "slike/OR11.png"; break;
		case 12: filename = "slike/OR12.png"; break;
		case 13: filename = "slike/OR13.png"; break;
		case 14: filename = "slike/OR14.png"; break;
		case 15: filename = "slike/OR15.png"; break;
		default: filename = "slike/OR16.png"; break;
		}
		
		File imagefileOR = new File(filename);
		try {
			imageOR = ImageIO.read(imagefileOR); 
		} catch (IOException e) {
			//nista za sad
		}
		
		//slika za NOT kola
		File imagefileNOT = new File("slike/NOT.png");
		try {
			imageNOT = ImageIO.read(imagefileNOT); 
		} catch (IOException e) {
			//nista za sad
		}		
				
		//generisanje koordinata za kola I, linije signala i izlaze I kola
		for (int i = 0; i < list.size(); i++) {
			
			List<List<Point>> lines;
			List<Point> points;
			
			lines = new LinkedList<List<Point>>();
			
			points = new LinkedList<Point>();
			points.add(new Point(startX, startY)); 
			points.add(new Point(startX + spaceX, startY));
			lines.add(points);
			
			points = new LinkedList<Point>();
			points.add(new Point(startX + spaceX, startY));
			points.add(new Point(startX + spaceX, startY + spaceY)); 
			lines.add(points);
			
			points = new LinkedList<Point>();
			points.add(new Point(startX + spaceX, startY + spaceY));
			points.add(new Point(andX, startY + spaceY)); 
			lines.add(points);
			
			andExitCoords.add(new Point(andX + imageAND.getWidth(), startY + spaceY + imageAND.getHeight()/2));
			
			schema.addDynComp(new GuiDynamicComp(imageAND, andX, startY + spaceY));
			schema.addTextLabel(new GuiTextLabel(andX - 100, startY + spaceY + 40, list.get(i).jd.signalName));
			schema.addLine(new GuiSignalLine(lines, list.get(i).dcSignal));
			
			if (list.get(i).jd.isInverted) {	//ako treba NOT
				//koordinate NOT kola
				int xx = andX - 50;
				int yy = startY + spaceY + imageAND.getHeight() - imageNOT.getHeight()/2;
				schema.addDynComp(new GuiDynamicComp(imageNOT, xx, yy));
				
				//linije signala uslova
				lines = new LinkedList<List<Point>>();
				
				points = new LinkedList<Point>();
				points.add(new Point(xx - 10, startY + spaceY + imageAND.getHeight())); 
				points.add(new Point(xx, startY + spaceY + imageAND.getHeight()));
				lines.add(points);
				
				schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get(list.get(i).jd.signalName)));
				
				//linije signala invertovanog uslova
				lines = new LinkedList<List<Point>>();
				
				points = new LinkedList<Point>();
				points.add(new Point(xx + imageNOT.getWidth(), startY + spaceY + imageAND.getHeight())); 
				points.add(new Point(andX, startY + spaceY + imageAND.getHeight()));
				lines.add(points);
				
				schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get("!" + list.get(i).jd.signalName)));
				
			}
			else {
				//linija signala uslova
				
				lines = new LinkedList<List<Point>>();
				
				points = new LinkedList<Point>();
				points.add(new Point(andX - 60, startY + spaceY + imageAND.getHeight())); 
				points.add(new Point(andX, startY + spaceY + imageAND.getHeight()));
				lines.add(points);
				
				schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get(list.get(i).jd.signalName)));
			}
			
			
			spaceY-=YdecAND;
			spaceX+=10;
			
			
			startY -=YdecDC;
		}
		
		
		//ILI kolo
		int x = 1000;
		int y = 1350 - imageOR.getHeight()/2;
		schema.addDynComp(new GuiDynamicComp(imageOR, x,y));
		
		//branch2 
		List<List<Point>> lines;
		List<Point> points;
		
		lines = new LinkedList<List<Point>>();
		
		points = new LinkedList<Point>();
		points.add(new Point(x + imageOR.getWidth(), y + imageOR.getHeight()/2)); 
		points.add(new Point(x + imageOR.getWidth() + 20, y + imageOR.getHeight()/2)); 
		lines.add(points);
		schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get("branch2")));
		schema.addTextLabel(new GuiTextLabel(x + imageOR.getWidth() + 25,  y + imageOR.getHeight()/2 , "branch2"));
		
		int orSpaceY = imageOR.getHeight() / list.size();
		if (list.size() > 1)
			orSpaceY = imageOR.getHeight() / (list.size() - 1);	//razmak izmedju ulaza ILI kola
		int orSpaceX = 250;	//osnovno rastojanje od izlaza ILI kola do pregiba
		
		//izlazi I kola se dovode na ILI kolo
		for (int i = 0; i < list.size(); i++) {		
			lines = new LinkedList<List<Point>>();
			
			points = new LinkedList<Point>();
			points.add(andExitCoords.get(i)); 
			points.add(new Point(andExitCoords.get(i).x + orSpaceX, andExitCoords.get(i).y)); 
			lines.add(points);
			
			points = new LinkedList<Point>();
			points.add(new Point(andExitCoords.get(i).x + orSpaceX, andExitCoords.get(i).y)); 
			points.add(new Point(andExitCoords.get(i).x + orSpaceX, y + imageOR.getHeight() - i*orSpaceY));
			lines.add(points);
			
			points = new LinkedList<Point>();
			points.add(new Point(andExitCoords.get(i).x + orSpaceX, y + imageOR.getHeight() - i*orSpaceY));
			points.add(new Point(x, y + imageOR.getHeight() - i*orSpaceY));
			lines.add(points);
			
			schema.addLine(new GuiSignalLine(lines, list.get(i).andOut));
			
			orSpaceX-=15;
			
			
		}
		
		
		//dodavanje ili kola za spoj branch1 i branch2
		
		BufferedImage imageOR2 = null;
		File imagefileOR2 = new File("slike/OR2.png");
		try {
			imageOR2 = ImageIO.read(imagefileOR2); 
		} catch (IOException e) {
			//nista za sad
		}
		
		int xOr=900;
		int yOr=1000;
		
		
		schema.addDynComp(new GuiDynamicComp(imageOR2, xOr, yOr));
		
		//
		lines = new LinkedList<List<Point>>();
		
		points = new LinkedList<Point>();
		points.add(new Point(xOr - 10, yOr)); 
		points.add(new Point(xOr, yOr)); 
		lines.add(points);
		schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get("branch1")));
		schema.addTextLabel(new GuiTextLabel( xOr - 65, yOr + 5,"branch1"));
		//
		lines = new LinkedList<List<Point>>();
		
		points = new LinkedList<Point>();
		points.add(new Point(xOr - 10, yOr + imageOR2.getHeight())); 
		points.add(new Point(xOr, yOr + imageOR2.getHeight())); 
		lines.add(points);
		schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get("branch2")));
		schema.addTextLabel(new GuiTextLabel( xOr - 65, yOr + imageOR2.getHeight() + 5, "branch2"));
		//
		lines = new LinkedList<List<Point>>();
		
		points = new LinkedList<Point>();
		points.add(new Point(xOr + imageOR2.getWidth(), yOr + imageOR2.getHeight()/2)); 
		points.add(new Point(xOr + imageOR2.getWidth() + 10, yOr + imageOR2.getHeight()/2)); 
		lines.add(points);
		schema.addLine(new GuiSignalLine(lines, StaticInitializer.allSignals.get("branch")));
		schema.addTextLabel(new GuiTextLabel(xOr + imageOR2.getWidth() + 17, yOr + imageOR2.getHeight()/2, "branch"));
	}
	
 
	public void setFetch2Drawables(ArrayList<DecoderE> decs, GuiSchema schema) {
		
		BufferedImage image = null;
		int x = 100, y = 100;
		int spaceX = 180; 	//razmaci izmedju dekodera
		int maxY = 0;		//maksimalna visina slike u tom redu
		String filename = null;	//naziv fajla
		
		int spanIn = 0;		//velicina dela gde se crtaju ulazni signali
		int spanOut = 0;	//velicina dela gde se crtaju izlazni signali
		int skipIn = 0;		//velicina dela gde se ne crtaju ulazni signali odozgo
		int skipOut = 0;	//velicina dela gde se ne crtaju ulazni signali odozgo
		
		for (int i = 0; i < decs.size(); i++) {
			
			//u zavisnosti od toga koliko ima ulaza postavlja se filename slike
			switch(decs.get(i).getPorts().size()) {
			
			//sa 1 ulazom
			case 4:	filename = "slike/DC1.png"; skipOut = 21; skipIn = 38;  spanOut = 35; spanIn = 1;  break;
			
			//sa 2 ulaza
			case 7:	filename = "slike/DC2.png"; skipOut = 24; skipIn = 45;  spanOut = 67; spanIn = 20; break;
			
			//sa 3 ulaza
			case 12:filename = "slike/DC3.png"; skipOut = 20; skipIn = 73;  spanOut = 164; spanIn = 57; break;
			
			//sa 4 ulaza
			case 21:filename = "slike/DC4.png"; skipOut = 23; skipIn = 142; spanOut = 356; spanIn = 72; break;
			
			//sa 5 ulaza
			case 38:filename = "slike/DC5.png"; skipOut = 11; skipIn = 319; spanOut = 711; spanIn = 132; break;
			
			//sa 6 ulaza
			case 71:filename = "slike/DC6.png"; skipOut = 18; skipIn = 641; spanOut = 1435; spanIn = 172; break;
		
			}
			
			
			//ovde se ucitava slika za komponentu
			File imagefile = new File(filename);
			try {
				image = ImageIO.read(imagefile); 
			} catch (IOException e) {
				//nista za sad
			}
			
			//pravljenje i dodavanje graficke reprezentacije komponente i njene labele
			GuiDynamicComp d = new GuiDynamicComp(image, x, y);
			schema.addDynComp(d);
			if (image.getHeight() > maxY) maxY = image.getHeight();
			
			schema.addTextLabel(new GuiTextLabel(x + image.getWidth()/2 - 15, y + image.getHeight()/2, decs.get(i).getName()));
			
			//ovde se ubacuju signali
			for (HashMap.Entry<String, Signal> j: StaticInitializer.allSignals.entrySet()) {
				
				//za ulaze
				for (int k = 0; k < decs.get(i).getNumInputs(); k++) {
					if (j.getValue().getConnectedSignals()!=null)
					for (int s = 0; s < j.getValue().getConnectedSignals().size(); s++) {
						if (j.getValue().getConnectedSignals().get(s) == decs.get(i).getPortIN(k)) {
							List<List<Point>> lines = new LinkedList<List<Point>>();
							List<Point> points;
							points = new LinkedList<Point>();
							points.add(new Point(x, y + skipIn + spanIn - spanIn / (decs.get(i).getNumInputs() - 1) * k));
							points.add(new Point(x - 10, y + spanIn + skipIn - spanIn / (decs.get(i).getNumInputs() - 1) * k));
							lines.add(points);
							GuiSignalLine l = new GuiSignalLine(lines, j.getValue());
							schema.addLine(l);
							
							schema.addTextLabel(new GuiTextLabel(x - 40,  y + skipIn + spanIn - spanIn / (decs.get(i).getNumInputs() - 1) * k + 5, j.getKey()));
						
						
					}
					}
				}
				
				//za izlaze
				for (int k = 0; k < decs.get(i).getNumOutputs(); k++) {
						if (j.getValue() == decs.get(i).getPortOUT(k)) {	
							List<List<Point>> lines = new LinkedList<List<Point>>();
							List<Point> points;
							points = new LinkedList<Point>();
							points.add(new Point(x + image.getWidth(), y + skipOut + spanOut*( decs.get(i).getNumOutputs() - k - 1) / (decs.get(i).getNumOutputs() - 1)  ));
							points.add(new Point(x + image.getWidth() + 10, y + skipOut + spanOut* ( decs.get(i).getNumOutputs() - k - 1) / (decs.get(i).getNumOutputs() - 1) ));
							lines.add(points);
							GuiSignalLine l = new GuiSignalLine(lines,j.getValue());
							schema.addLine(l);
							
							schema.addTextLabel(new GuiTextLabel(x + image.getWidth() + 16,  y + skipOut + spanOut* ( decs.get(i).getNumOutputs() - k - 1) / (decs.get(i).getNumOutputs() - 1)  + 5, j.getKey()));
						}
				}
				
				//za E
				
				if (j.getValue().getConnectedSignals() != null)
					if (j.getValue().getConnectedSignals().contains(decs.get(i).getPortE())) {
						List<List<Point>> lines = new LinkedList<List<Point>>();
						List<Point> points;
						points = new LinkedList<Point>();
						points.add(new Point(x + image.getWidth()/2, y + image.getHeight()));
						points.add(new Point(x + image.getWidth()/2, y + image.getHeight() + 10));
						lines.add(points);
						GuiSignalLine l = new GuiSignalLine(lines, j.getValue());
						schema.addLine(l);
						
						schema.addTextLabel(new GuiTextLabel(x + image.getWidth()/2 - 5, y + image.getHeight() + 25, j.getKey()));
					}
				
				
			}
			
			
			//ako je stavljeno 5 dekodera u red preci u sledeci, inace pomeranje desno
			if ((i+1)%5 != 0) x+=image.getWidth() + spaceX;
			else {
				x = 100; 
				y += maxY + 100; 
				maxY = 0;
			}
			
		}
	}
	
	
	//cvor stabla komponenti
	class CompTreeNode {
		private ArrayList<CompTreeNode> children;
		private CompTreeNode parent;
		private OneOutputGate comp;
		private int level;	//dubina cvora u stablu; koreni imaju dubinu 0
		private int x, y, h, w;
		
		public CompTreeNode(ArrayList<CompTreeNode> c, CompTreeNode p, OneOutputGate cs, int l) {
			children = c;
			if (children == null) children = new ArrayList<CompTreeNode>();
			parent = p;
			comp = cs;
			level = l;
			x = 0; y = 0; h = 0;
		}
		
		public OneOutputGate getComp () {
			return comp;
		}
		
		public int getLevel() {
			return level;
		}
		
		public void setXYHW(int xx, int yy, int hh, int ww) {
			x = xx;
			y = yy;
			h = hh;
			w = ww;
		}
		
		public int getX() {return x;}
		public int getY() {return y;}
		public int getH() {return h;}
		public int getW() {return w;}
		
		public int getMaxLevel() {
			int res = level;
			for (int i = 0; i < children.size(); i++) {
				int c = children.get(i).getMaxLevel();
				if (c > res) res = c;
			}
			return res;
			
		}
		
		public ArrayList<CompTreeNode> getAllOfLevel(int lvl) {
			ArrayList<CompTreeNode> result = new ArrayList<CompTreeNode>();
			if (lvl == level) {
				result.add(this);
				return result;
			}
			for (int i = 0; i < children.size(); i++) {
				ArrayList<CompTreeNode> c = children.get(i).getAllOfLevel(lvl);
				for (int j = 0; j < c.size(); j++) {
					result.add(c.get(j));
				}
			}
			return result;
			
		}
		
		public ArrayList<CompTreeNode> getChildren() {
			return children;
		}
		
		public CompTreeNode getNodeFromOneTree(OneOutputGate c) {
			if (comp == c ) return this;
			for (int i = 0; i < children.size(); i++) {
				CompTreeNode tRes = children.get(i).getNodeFromOneTree(c);
				if (tRes != null) return tRes;
			}
			return null;
		}	
		
		public void addChild(CompTreeNode c) {
			children.add(c);
		}
		
	}
	
	//postavlja Point od p na vrednost koordinata za komponentu c u stablima odredjenim korenima roots
	public CompTreeNode getNode(ArrayList<CompTreeNode> roots, OneOutputGate c) {
		for (int i = 0; i < roots.size(); i++) {
			CompTreeNode res = roots.get(i).getNodeFromOneTree(c);
			if (res!=null) return res;
		}
		return null;
	}

	//pravljanje liste portova koji dolaze spolja
	public ArrayList<Signal> getInputSigs(ArrayList<OneOutputGate> comps) {
		//pravljanje liste portova koji su na ulazima sema
		ArrayList<Signal> inputPorts = new ArrayList<Signal>();
		for (int i = 0; i < comps.size(); i++) {					// za svaku komponentu u listi
			for (int j = 0; j < comps.get(i).getNumInputs(); j++) {	//za svaki ulaz te komponente
				int numOfMatches = 0;
				for (int k = 0; k < comps.size(); k++) {			//se proverava da li je i izlaz neke od komponenti
					if (comps.get(k).getPortOUT().getConnectedSignals() != null)
						for (int l = 0; l < comps.get(k).getPortOUT().getConnectedSignals().size(); l++) {
							if (comps.get(k).getPortOUT().getConnectedSignals().get(l) == comps.get(i).getPortIN(j)) {
							numOfMatches++;
							break;
							}
						}
					
				}
				if (numOfMatches == 0) inputPorts.add(comps.get(i).getPortIN(j)); 
			}
		}
		//pravljenje liste signala koji su na ulazima tih portova
		ArrayList<Signal> inputs = new ArrayList<Signal>();
		for (HashMap.Entry<String, Signal> i: StaticInitializer.allSignals.entrySet()) {
			if ( i.getValue().getConnectedSignals() != null)
				for (int j = 0; j < i.getValue().getConnectedSignals().size(); j++) {
					for (int k = 0; k < inputPorts.size(); k++) {
						if ( i.getValue().getConnectedSignals().get(j) == inputPorts.get(k)) {
							inputs.add(i.getValue());
						}
					}
					
				}
		}
		
		return inputs;
		
	}
	
	//pravljanje liste signala koji su na izlazima sema
	public ArrayList<Signal> getOutputSigs(ArrayList<OneOutputGate> comps) {
		ArrayList<Signal> outputs = new ArrayList<Signal>();
		for (int i = 0; i < comps.size(); i++) {					// za svaku komponentu u listi
			int numOfMatches = 0;
			for (int j = 0; j < comps.size(); j++) {				//se proverava da li je njen izlaz ulaz neke druge komponente
				for (int k = 0; k < comps.get(j).getNumInputs(); k++) {
					if (comps.get(i).getPortOUT().getConnectedSignals() != null)
						for (int l = 0; l < comps.get(i).getPortOUT().getConnectedSignals().size(); l++) {
							if (comps.get(j).getPortIN(k) == comps.get(i).getPortOUT().getConnectedSignals().get(0)) {
								numOfMatches++;
								break;
							}
						}
					
				}
			}
			if (numOfMatches == 0) if (!outputs.contains( comps.get(i).getPortOUT() )) outputs.add(comps.get(i).getPortOUT());
		}
		
		return outputs;
	}
	
	public void setTreeBasedDrawables(ArrayList<OneOutputGate> comps, GuiSchema schema) {	
		
		ArrayList<Signal> inputs = getInputSigs(comps);
		ArrayList<Signal> outputs = getOutputSigs(comps);
		
		int x = 100, y = 100;
		int spaceX = 50, spaceY = 160; 		//razmaci izmedju grupacija
		String filename = null;	//naziv fajla
		int localX = 0;
		
		//razvrstavanje komponenti na stabla metodama za tu schemu
		ArrayList<CompTreeNode> roots = null;
		roots = makeTrees(comps, outputs);
		
		for (int i = 0; i < roots.size(); i++) {	//za svako stablo
			
			int maxX = 0, maxY = 0;	//maksimalne koordinate, ovi podaci sluze da se izbegne preklapanje delova
			
			int m = roots.get(i).getMaxLevel();
			
			for (int j = m; j >= 0; j--) {	//za svaki nivo
				localX = x + 200*(m-j);	//horizontalni razmak izmedju nivoa
				int localY = y + 50* (m-j);
				ArrayList<CompTreeNode> currentLevel = roots.get(i).getAllOfLevel(j);
				for (int k = 0; k < currentLevel.size(); k++) {
					
							switch(currentLevel.get(k).getComp().getClass().getName()) {
								
							case "simulator2.digitalcomponents.Or":	
							
							switch (currentLevel.get(k).getComp().getNumInputs()) {
							case 2: filename = "slike/OR2.png"; break;
							case 3: filename = "slike/OR3.png"; break;
							case 4: filename = "slike/OR4.png"; break;
							case 5: filename = "slike/OR5.png"; break;
							case 6: filename = "slike/OR6.png"; break;
							case 7: filename = "slike/OR7.png"; break;
							case 8: filename = "slike/OR8.png"; break;
							case 9: filename = "slike/OR9.png"; break;
							case 10: filename = "slike/OR10.png"; break;
							case 11: filename = "slike/OR11.png"; break;
							case 12: filename = "slike/OR12.png"; break;
							case 13: filename = "slike/OR13.png"; break;
							case 14: filename = "slike/OR14.png"; break;
							case 15: filename = "slike/OR15.png"; break;
							default: filename = "slike/OR16.png"; break;
								
								
								
							}
							
							break;

							case "simulator2.digitalcomponents.And":	
							
							switch (currentLevel.get(k).getComp().getNumInputs()) {
							case 2: filename = "slike/AND2.png"; break;
							case 3: filename = "slike/AND3.png"; break;
							case 4: filename = "slike/AND4.png"; break;
							case 5: filename = "slike/AND5.png"; break;
							case 6: filename = "slike/AND6.png"; break;
							case 7: filename = "slike/AND7.png"; break;
							case 8: filename = "slike/AND8.png"; break;
							case 9: filename = "slike/AND9.png"; break;
							case 10: filename = "slike/AND10.png"; break;
							case 11: filename = "slike/AND11.png"; break;
							case 12: filename = "slike/AND12.png"; break;
							case 13: filename = "slike/AND13.png"; break;
							case 14: filename = "slike/AND14.png"; break;
							case 15: filename = "slike/AND15.png"; break;
							default: filename = "slike/AND16.png"; break;
						
							}
							
							break;
							
							case "simulator2.digitalcomponents.Nand":
							
							switch (currentLevel.get(k).getComp().getNumInputs()) {
							case 2: filename = "slike/NAND2.png"; break;
							case 3: filename = "slike/NAND3.png"; break;
							case 4: filename = "slike/NAND4.png"; break;
							case 5: filename = "slike/NAND5.png"; break;
							case 6: filename = "slike/NAND6.png"; break;
							case 7: filename = "slike/NAND7.png"; break;
							case 8: filename = "slike/NAND8.png"; break;
							case 9: filename = "slike/NAND9.png"; break;
							case 10: filename = "slike/NAND10.png"; break;
							case 11: filename = "slike/NAND11.png"; break;
							case 12: filename = "slike/NAND12.png"; break;
							case 13: filename = "slike/NAND13.png"; break;
							case 14: filename = "slike/NAND14.png"; break;
							case 15: filename = "slike/NAND15.png"; break;
							default: filename = "slike/NAND16.png"; break;
							
							}
							
							break;
							
							case "simulator2.digitalcomponents.Nor":
							
							switch (currentLevel.get(k).getComp().getNumInputs()) {
							case 2: filename = "slike/NOR2.png"; break;
							case 3: filename = "slike/NOR3.png"; break;
							case 4: filename = "slike/NOR4.png"; break;
							case 5: filename = "slike/NOR5.png"; break;
							case 6: filename = "slike/NOR6.png"; break;
							case 7: filename = "slike/NOR7.png"; break;
							case 8: filename = "slike/NOR8.png"; break;
							case 9: filename = "slike/NOR9.png"; break;
							case 10: filename = "slike/NOR10.png"; break;
							case 11: filename = "slike/NOR11.png"; break;
							case 12: filename = "slike/NOR12.png"; break;
							case 13: filename = "slike/NOR13.png"; break;
							case 14: filename = "slike/NOR14.png"; break;
							case 15: filename = "slike/NOR15.png"; break;
							default: filename = "slike/NOR16.png"; break;
							
							}
							
							break;
							
							case "simulator2.digitalcomponents.Not":
							filename = "slike/NOT.png";
							break;
							
							case "simulator2.digitalcomponents.Xor":
							filename = "slike/XOR.png";
							break;
							
							case "simulator2.digitalcomponents.Nxor":
							filename = "slike/NXOR.png";
							break;
							
							}
							
			
							File imagefile = new File(filename);
							BufferedImage image = null;
							try {
								image = ImageIO.read(imagefile); 
							} catch (IOException e) {
								
							}
							
							GuiDynamicComp d = new GuiDynamicComp(image, localX, localY);
							currentLevel.get(k).setXYHW(localX, localY, image.getHeight(), image.getWidth());
							schema.addDynComp(d);
							
							
							localY += image.getHeight() + 50;
							if (maxY < localY) maxY = localY;
				}
				
			}
			//ovo je promena dela na kom se crta
			maxX = localX;
			if (i == 0 || i == 2) x += maxX + spaceX;
			else {
				x = 110;
				y += maxY + spaceY; 
				maxY = 0;
			}
			
			//kreiranje niza dela signala do pregiba - za upotrebu u setSignalLines
			double[] bendPoints = new double[roots.get(i).getMaxLevel() + 1];
			for (int k = 0; k < bendPoints.length; k++) {
				bendPoints[k] = 0.25;
			}
			
			//dodavanje signala na ulazima
			for (int q = 0; q < inputs.size(); q++) {	//za sve signale koji su ulazni za mreze se traze komponente u koje ulaze
				for (int j = 0; j < comps.size(); j++) {	//proveravaju se sve komponente
					for (int k = 0; k < comps.get(j).getNumInputs(); k++) {	//proveravaju se svi ulazi komponenti
						//provera da li ima povezanosti:
						if (inputs.get(q).getConnectedSignals()!=null)
							for (int s = 0; s < inputs.get(q).getConnectedSignals().size(); s++) {
								if (inputs.get(q).getConnectedSignals().get(s) == comps.get(j).getPortIN(k)) {
									//ima, treba crtati
									
									//treba naci koordinate koje su dodeljene komponenti, pa staviti na osnovu njih liniju i labelu
									CompTreeNode c = roots.get(i).getNodeFromOneTree(comps.get(j));
									if (c == null) break;
									Point p1 = new Point(c.getX(), c.getY());
									p1.y += k* c.getH() /(comps.get(j).getNumInputs() - 1) ;
									Point p2 = new Point(p1.x-10, p1.y);
									
									
									List<List<Point>> lines = new LinkedList<List<Point>>();
									List<Point> points;
									points = new LinkedList<Point>();
									points.add(p1);
									points.add(p2);
									lines.add(points);
									GuiSignalLine l = new GuiSignalLine(lines, inputs.get(q));
									schema.addLine(l);
									
									
									String text = "";
									for (HashMap.Entry<String, Signal> mapEntry: StaticInitializer.allSignals.entrySet()) {
										if (inputs.get(q) == mapEntry.getValue()) text = mapEntry.getKey();
									}
									
									if (text != null)
										schema.addTextLabel(new GuiTextLabel(p2.x - 20  - 4*text.length(), p2.y + 5, text));
									
								}
							}
					}
				}
			}
			
			//postavljanje signalnih linija centralnog dela stabla
			setSignalLines(roots.get(i).getChildren(), schema, bendPoints);
			
		
			
			
		}
		
		
		//dodavanje signala na izlazima
		for (int i = 0; i < outputs.size(); i++) {	//za sve signale koji su izlazni za mreze se traze komponente iz kojih izlaze
			for (int j = 0; j < comps.size(); j++) {	//proveravaju se sve komponente
				if (outputs.get(i) == comps.get(j).getPortOUT()) {
					//nadjen je signal:
					
					
					//treba naci koordinate koje su dodeljene komponenti, pa staviti na osnovu njih liniju i labelu
					CompTreeNode c = getNode(roots, comps.get(j));
					Point p1 = new Point(c.getX() + c.getW(), c.getY() + c.getH()/2);
					Point p2 = new Point(p1.x+10, p1.y);
					
					
					List<List<Point>> lines = new LinkedList<List<Point>>();
					List<Point> points;
					points = new LinkedList<Point>();
					points.add(p1);
					points.add(p2);
					lines.add(points);
					GuiSignalLine l = new GuiSignalLine(lines, outputs.get(i));
					schema.addLine(l);
					
					String text = null;
					for (HashMap.Entry<String, Signal> mapEntry: StaticInitializer.allSignals.entrySet()) {
						if (outputs.get(i) == mapEntry.getValue()) 	
							text = mapEntry.getKey();
					}
					
					if (text!= null)
						schema.addTextLabel(new GuiTextLabel(p2.x + 10, p2.y + 5, text));

				}
			}
		}
		

		
	}

	//metoda za postavljanje signalnih linija centralnog dela stabla
	public void setSignalLines(ArrayList<CompTreeNode> t, GuiSchema schema, double[] bends) {
		
		if (t == null) return;
		for (int j =0; j < t.size(); j++) {
			//uzme se mesto prevoja
			double bendPoint = bends[t.get(j).getLevel()];
			if (bendPoint > 0.85) bends[t.get(j).getLevel()] = 0.25;	//resetuje pomeraj na 0.25 ako je stiglo skoro do kraja
			else bends[t.get(j).getLevel()] += 0.12;  
			
			//izracunaju se krajnje koordinate
			int startX, startY, endX, endY = 0;
			int num = 0;	//broj ulaznog porta "odredisne" komponente signala; sluzi za pozicioniranje odredisne tacke
			List<Port> ins = t.get(j).parent.getComp().getInPorts();
			for (int k = 0; k < ins.size(); k++) {
				if (t.get(j).getComp().getPortOUT().getConnectedSignals().contains(ins.get(k))) 		//ako je taj port povezan na izlazni port komponente
				{
					num = k;
					break;
				}
			}
			
			
			startX = t.get(j).getX() + t.get(j).getW();		//od desne strane
			startY = t.get(j).getY() + t.get(j).getH()/2;	//i od pola visine
			endX = t.get(j).parent.getX();	//do naredne komponente
			endY = t.get(j).parent.getY() + t.get(j).parent.getH() / (t.get(j).parent.getComp().getNumInputs() - 1)  * num;	// visina/br. portova * broj porta odozgo
			
			List<List<Point>> lines;
			List<Point> points;
			
			lines = new LinkedList<List<Point>>();
			
			points = new LinkedList<Point>();
			points.add(new Point(startX, startY));
			points.add(new Point((int)(startX + ((double)((endX - startX)) * bendPoint)), startY));
			points.add(new Point((int)(startX + ((double)((endX - startX)) * bendPoint)), endY));
			points.add(new Point(endX, endY));
			lines.add(points);
			
			GuiSignalLine l = new GuiSignalLine(lines, t.get(j).getComp().getPortOUT());
			schema.addLine(l);
			setSignalLines(t.get(j).getChildren(), schema, bends);
		}
	}

	
	
	public ArrayList<CompTreeNode> makeTrees(ArrayList<OneOutputGate> comps, ArrayList<Signal> outputs) {
		//levels je lista listi cvorova, 
		//nulta lista sadrzi listu korena, 1. lista sadrzi listu cvorova 1. nivoa itd.
		ArrayList<ArrayList<CompTreeNode>> levels = new ArrayList<ArrayList<CompTreeNode>>();
		levels.add(new ArrayList<CompTreeNode>());
		
		//ubacivanje svih korena u levels o
		for (int i = 0; i < comps.size(); i++) {
			for (int j = 0; j < outputs.size(); j++) {
				if (comps.get(i).getPortOUT() == outputs.get(j)) {
					CompTreeNode c = new CompTreeNode(null, null, comps.get(i), 0);
					levels.get(0).add(c);
				}
				
			}
			
		}
		
		//sada kada imamo sve korene cvorove mozemo da gradimo stabla nad njima
		int level = 1;
		int numOfAdds = 1; //broj dodatih u ovoj iteraciji, kraj je kad nema vise dodavanja
		 while(numOfAdds > 0) {
			 numOfAdds = 0;
			 for (int i = 0; i < comps.size(); i++) {	//za sve komponente
				 if (levels.get(level-1) != null)
				 for (int j = 0; j < levels.get(level-1).size(); j++) {		//za sve komponente u nivou iznad
					 for (int k = 0; k < levels.get(level-1).get(j).getComp().getNumInputs(); k++)	//za sve ulazne portove komponenti u nivou iznad
						 if (comps.get(i).getPortOUT().getConnectedSignals() != null)
						 for (int l = 0; l < comps.get(i).getPortOUT().getConnectedSignals().size(); l++) {
							 if (comps.get(i).getPortOUT().getConnectedSignals().get(l) == levels.get(level-1).get(j).getComp().getPortIN(k)) {	//ako je izlazni port prvog povezan na ulazni port drugog;
								//dodati ga na sledeci nivo
								CompTreeNode ctn = new CompTreeNode(null, levels.get(level-1).get(j), comps.get(i), level);
								if (levels.size() < level + 1) 
									levels.add(new ArrayList<CompTreeNode>());
								levels.get(level).add(ctn);
								levels.get(level-1).get(j).addChild(ctn);
								numOfAdds++;
							}
						 }
						
				 }
			 }
			 level++;
		 }
		
		 //vracaju se koreni
		 return levels.get(0);
		
	}
	
	//pozicioniranje opcionih bafera za addr1
	public void set3StateBuffers(ArrayList<TriStateBuffer> comps, GuiSchema schema) {

		if (comps==null || comps.size() == 0) return;
		
		BufferedImage image = null;
		File imagefile = new File("slike/3statbuff.png");
		try {
			image = ImageIO.read(imagefile); 
		} catch (IOException e) {
			//nista za sada
		}
		
		//koordinate komponenti
		int x = 110, y = 670;	
		int spaceY = 80;
		
		//x koordinate magistrala
		int bus1 = 368;
		int bus2 = 424;
		int bus3 = 481;
		
		for (int i = 0; i < comps.size(); i++) {
			
			GuiDynamicComp d = new GuiDynamicComp(image, x, y);
			schema.addDynComp(d);
			
			//ovde se ubacuju signali
			for (HashMap.Entry<String, Signal> j: StaticInitializer.allSignals.entrySet()) {
				if (j.getValue().getConnectedSignals()!=null)
				for (int s = 0; s < j.getValue().getConnectedSignals().size(); s++) {
					if (j.getValue().getConnectedSignals().get(s) == comps.get(i).getPortC()) {
						//kontrolni
						
						List<List<Point>> lines = new LinkedList<List<Point>>();
						List<Point> points;
						points = new LinkedList<Point>();
						points.add(new Point(x + image.getWidth()/2, y + image.getHeight()*4/5));
						points.add(new Point(x + image.getWidth()/2,  y + image.getHeight() + 10));
						lines.add(points);
						GuiSignalLine l = new GuiSignalLine(lines, j.getValue());
						schema.addLine(l);
						
						String text = "";
						for (HashMap.Entry<String, Signal> mapEntry: StaticInitializer.allSignals.entrySet()) {
							if (j.getValue() == mapEntry.getValue()) text = mapEntry.getKey();
						}
						
						schema.addTextLabel(new GuiTextLabel(x + image.getWidth()/2 - 3, y + image.getHeight() + 30, text));

					}
					if (j.getValue().getConnectedSignals().get(s) == comps.get(i).getPortIN()) {
						//ulazni
						
						List<List<Point>> lines = new LinkedList<List<Point>>();
						List<Point> points;
						points = new LinkedList<Point>();
						points.add(new Point(x, y + image.getHeight()/2));
						points.add(new Point(x - 20, y + image.getHeight()/2));
						lines.add(points);
						GuiSignalLine l = new GuiSignalLine(lines, j.getValue());
						schema.addLine(l);
						
						String text = "";
						for (HashMap.Entry<String, Signal> mapEntry: StaticInitializer.allSignals.entrySet()) {
							if (j.getValue() == mapEntry.getValue()) text = mapEntry.getKey();
						}
						
						schema.addTextLabel(new GuiTextLabel(x - 55 - 4*(text.length()) , y + image.getHeight()/2 + 5, text));
						schema.addValLabel(new GuiSignalLabel(x - 25, y + 6, j.getValue().getConnectedSignals().get(s)));

					}
					if (j.getValue() == comps.get(i).getPortOUT()) {
						//izlazni
						
						List<List<Point>> lines = new LinkedList<List<Point>>();
						List<Point> points;
						points = new LinkedList<Point>();
						points.add(new Point(x + image.getWidth(), y + image.getHeight()/2));


						String text = "";
						for (HashMap.Entry<String, Signal> mapEntry: StaticInitializer.allSignals.entrySet()) {
							if (comps.get(i).getPortOUT().getConnectedSignals().get(s) == mapEntry.getValue()) text = mapEntry.getKey();
						}
						
						if ("IBUS1".equals(text)) 
							points.add(new Point(bus1,  y + image.getHeight()/2));
						if ("IBUS2".equals(text)) 
							points.add(new Point(bus2,  y + image.getHeight()/2));
						if ("IBUS3".equals(text)) 
							points.add(new Point(bus3,  y + image.getHeight()/2));
						lines.add(points);
						GuiSignalLine l = new GuiSignalLine(lines, j.getValue());
						schema.addLine(l);
						

					}
					
				}
				
				
				
			}
			y+=spaceY;	
		}
	
	}
	
	

}
