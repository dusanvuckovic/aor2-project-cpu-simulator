package simulator2.gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import simulator2.Signal;

public class SelectSignalWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextArea signalName;
	JTextArea number;
	JButton clear;
	JButton set;
	JButton draw;
	static Graph[] graph = new Graph[5];
	static boolean[] select = new boolean[5];
	static JPanel mPanel;
	static String[] sigName = new String[5];
	

	public static Signal[] selectedSignals = new Signal[5];

	public SelectSignalWindow(JFrame window) {

		this.setName("select signal");
		this.setSize(1500, 300);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setBackground(Color.WHITE);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.setLayout(new GridLayout(0, 1, 5, 5));
		signalName = new JTextArea("Signal name");
		number = new JTextArea("number");
		clear = new JButton("CLEAR");
		set = new JButton("SET");
		draw = new JButton ("DRAW");
		mPanel = new JPanel();
		mPanel.setLayout(new GridLayout(0, 5, 5, 5));
		mPanel.add(set);
		mPanel.add(signalName);
		mPanel.add(draw);
		mPanel.add(number);
		mPanel.add(clear);
		this.add(mPanel);

		for (int i = 0; i < 5; i++) {
	
			graph[i] = new Graph();
			graph[i].setSig(null, null);
			graph[i].repaint();
			this.add(graph[i]);
		}

		for (int i = 0; i < 5; i++) {
			select[i] = false;
		}

		set.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// System.out.println("set action");
				String name = signalName.getText();
				if (simulator2.staticinit.StaticInitializer.allSignals
						.containsKey(name)) {
					int value = Integer.parseInt(number.getText());
					if ((value > 5 || value < 0)) {
						signalName.setText("Nevalidan broj");
					} else {
						Signal s = simulator2.staticinit.StaticInitializer.allSignals
								.get(name);
						selectedSignals[value] = s;
						select[value] = true;
						sigName[value] = name;
					}

				} else {
					signalName.setText("Signal ne postoji");
				}
				// some repainting
			}

		});
		
		draw.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SelectSignalWindow.draw();
				
			}
			
		});

		clear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// System.out.println("clear action");
				for (int i = 0; i < 5; i++) {
					selectedSignals[i] = null;
					graph[i].setSig(null, null);
					graph[i].repaint();
					select[i] = false;
					sigName[i] = null;
					

				}

			}

		});
	}
	
	public void paintComponent(){}

	public static void draw() {

		for (int i = 0; i < 5; i++) {
			if (select[i] == true) {
				graph[i].setSig(selectedSignals[i], sigName[i]);
				graph[i].repaint();
				Window.signalWindow.repaint();
				Window.signalWindow.revalidate();
				Window.signalWindow.repaint();
				
			}

		}
	}

}
