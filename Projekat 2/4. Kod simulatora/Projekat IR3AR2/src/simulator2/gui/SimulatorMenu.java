package simulator2.gui;

import java.awt.event.ActionEvent;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class SimulatorMenu extends JMenuBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JMenu file = new javax.swing.JMenu();
	private final JMenu help = new javax.swing.JMenu();
	private final JMenuItem item1 = new JMenuItem();
	private final JMenuItem item2 = new JMenuItem();

	public SimulatorMenu() {
		file.setText("File");
		item1.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
		item1.setText("Load");
		item1.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				item1ActionPerformed(evt);
			}
		});
		file.add(item1);
		this.add(file);
		help.setText("Help");

		item2.setAccelerator(
				javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
		item2.setText("About");
		item2.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				item2ActionPerformed(evt);
			}
		});
		help.add(item2);
		this.add(help);
	}

	@SuppressWarnings("unused")
	private void item1ActionPerformed(ActionEvent evt) {
		JFileChooser jc = new JFileChooser(".//");

		int fStatus = jc.showOpenDialog(this);
		if (fStatus == JFileChooser.APPROVE_OPTION) {
			String path = jc.getSelectedFile().getPath();
		}
	}

	private void item2ActionPerformed(ActionEvent evt) {
		throw new UnsupportedOperationException("Not supported yet."); // To
																		// change
																		// body
																		// of
																		// generated
																		// methods,
																		// choose
																		// Tools
																		// |
																		// Templates.
	}
}
