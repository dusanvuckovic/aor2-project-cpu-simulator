package simulator2.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import simulator2.digitalcomponents.MemoryModule;

public class MemoryWindow extends JDialog{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	JPanel panelTop;
	JPanel panelBottom;
	JButton read;
	JButton writeByte;
	JButton writeWord;
	JTextArea setValue;
	JTextArea setAddress;
	JPanel memoryView;
	JPanel lowerPanel;
	JLabel []lab = new JLabel [160];
	JPanel addressPanel;
	JLabel[] addressLab =new JLabel [10];
	
	
	public MemoryWindow(JFrame window){
		
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.setModal(true);
		this.setLayout(new BorderLayout());
		this.setSize(1000, 350);
		this.setResizable(false);
		
		addressPanel = new JPanel(new GridLayout(0,1));
		addressPanel.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		for (int i = 0; i <10; i++){
			int j = i*16;
			addressLab[i] = new JLabel(Integer.toHexString(j)+"�"+Integer.toHexString(j+15));
			addressLab[i].setBorder(new EtchedBorder(Color.GRAY, Color.GRAY));
			addressPanel.add(addressLab[i]);
			
		}
		this.add(addressPanel, BorderLayout.WEST);
		
		lowerPanel = new JPanel(new GridLayout(0,1));
		memoryView = new JPanel(new GridLayout(10, 16));
		memoryView.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		for (int i = 0; i < 10*16; i++){
			lab[i] = new JLabel("0");
			lab[i].setSize(10, 50);
			lab[i].setBorder(new EtchedBorder(Color.GRAY, Color.GRAY));
			memoryView.add(lab[i]);
		}
		this.add(memoryView, BorderLayout.CENTER);
		
		writeByte = new JButton("WRITE BYTE");
		writeByte.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		setValue = new JTextArea("0000");
		setValue.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		read = new JButton("READ");
		read.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		setAddress = new JTextArea("0000");
		setAddress.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		writeWord = new JButton("WRITE WORD");
		writeWord.setBorder(new EtchedBorder(Color.BLACK,Color.BLACK));
		
		
		
		panelTop = new JPanel(new GridLayout(0,4));
		JLabel addrLab = new JLabel("ADDRESS:");
		addrLab.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		panelTop.add(addrLab);
		panelTop.add(setAddress);
		JLabel valueLab = new JLabel("VALUE:");
		valueLab.setBorder(new EtchedBorder(Color.BLACK, Color.BLACK));
		panelTop.add(valueLab);
		panelTop.add(setValue);
		lowerPanel.add(panelTop);
		
		
		
		panelBottom = new JPanel(new GridLayout(0,3));
		panelBottom.add(writeByte);
		panelBottom.add(writeWord);
		panelBottom.add(read);
		lowerPanel.add(panelBottom);
		this.add(lowerPanel, BorderLayout.SOUTH);
	
		
		
		
		writeByte.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int value = Integer.decode("0x"+setValue.getText());
				int addr = Integer.decode("0x"+setAddress.getText());
				int moduleNum = addr % 4;
				int moduleAddr = addr / 4;
				MemoryModule mod = new MemoryModule();
				
				switch (moduleNum) {
				
				case 0: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M0");
						break;
				case 1:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M1");
						break;
				case 2: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M2");
						break;
				case 3:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M3");
						break;
				
				}
				mod.setMemValue(moduleAddr, value);
				
				for(int i = 0; i<16*10; i++){
					int cur = addr + i;
					int currModNum = cur % 4;
					int currModAddr = cur / 4;
					switch (currModNum) {
					
					case 0: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M0");
							break;
					case 1:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M1");
							break;
					case 2: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M2");
							break;
					case 3:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M3");
							break;
					
					}
					int c = mod.getMemValue(currModAddr);
					lab[i].setText(Integer.toHexString(c));
				}
				for (int i = 0; i <10; i++){
					addressLab[i].setText(Integer.toHexString(addr+i*16)+"�"+Integer.toHexString(addr+16*(i+1)-1));					
				}
				
			}
			
		});
		
		
		read.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				int addr = Integer.decode("0x"+ setAddress.getText());
				int moduleNum = addr % 4;
				int moduleAddr = addr / 4;
				MemoryModule mod = new MemoryModule();
				
				switch (moduleNum) {
				
				case 0: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M0");
						break;
				case 1:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M1");
						break;
				case 2: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M2");
						break;
				case 3:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M3");
						break;
				
				}
				
				int value = mod.getMemValue(moduleAddr);
				setValue.setText(Integer.toHexString (value));
				
				for(int i = 0; i<16*10; i++){
					int cur = addr + i;
					int currModNum = cur % 4;
					int currModAddr = cur / 4;
					switch (currModNum) {
					
					case 0: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M0");
							break;
					case 1:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M1");
							break;
					case 2: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M2");
							break;
					case 3:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M3");
							break;
					
					}
					int c = mod.getMemValue(currModAddr);
					lab[i].setText(Integer.toHexString(c));
				}
				for (int i = 0; i <10; i++){
					addressLab[i].setText(Integer.toHexString(addr+i*16)+" � "+Integer.toHexString(addr+16*(i+1)-1));					
				}
				
				
			}
			
		});
		
		
		
		
		writeWord.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int value = Integer.decode("0x"+setValue.getText());
				int addr = Integer.decode("0x"+setAddress.getText());
				ByteBuffer buf = ByteBuffer.allocate(4);
				buf.order(ByteOrder.LITTLE_ENDIAN);
				buf.putInt(value);
				byte[] values = buf.array();
				for(int i = 0; i<4; i++){
					int moduleNum = (addr+i) % 4;
					int moduleAddr = (addr+i) / 4;
					MemoryModule mod = new MemoryModule();
					switch (moduleNum) {
					
					case 0: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M0");
							break;
					case 1:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M1");
							break;
					case 2: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M2");
							break;
					case 3:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M3");
							break;
					
					}
					mod.setMemValue(moduleAddr, values[i]);
					for(int j = 0; j<16*10; j++){
						int cur = addr + j;
						int currModNum = cur % 4;
						int currModAddr = cur / 4;
						switch (currModNum) {
						
						case 0: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M0");
								break;
						case 1:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M1");
								break;
						case 2: mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M2");
								break;
						case 3:mod =(MemoryModule) simulator2.staticinit.StaticInitializer.allComponents.get("M3");
								break;
						
						}
						int c = mod.getMemValue(currModAddr);
						lab[j].setText(Integer.toHexString(c));
					}
					
				}
				for (int i = 0; i <10; i++){
					addressLab[i].setText(Integer.toHexString(addr+i*16)+"�"+Integer.toHexString(addr+16*(i+1)-1));					
				}
				
			}
			
		});
		
		
		
		
		
		

		
	}

	
}
