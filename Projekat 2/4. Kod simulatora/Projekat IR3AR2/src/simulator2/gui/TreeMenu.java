package simulator2.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;

public final class TreeMenu extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final ButtonGroup listOfSchemes;
	private final HashMap<JRadioButton, GuiSchema> myPictures;
	private final JPanel panel;
	private final ListOfSchemesItemListener listener;
	private JPanel bottomPanel;

	public TreeMenu(String[] properNames, String[] names, GuiSchema[] schemas) {
		this.setLayout(new BorderLayout());

		bottomPanel = new JPanel(new GridLayout(3, 1));

		JButton selectSignal = new JButton("SELECT SIGNALS");
		selectSignal.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Window.signalWindow.setVisible(true);

			}

		});

		JButton memory = new JButton("MEMORY");
		memory.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MemoryWindow memW = new MemoryWindow(Window.mainW);
				memW.setVisible(true);

			}

		});

		JButton registers = new JButton("REGISTERS");
		registers.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				RegisterWindow regW = new RegisterWindow(Window.mainW);
				regW.setVisible(true);

			}

		});

		bottomPanel.add(selectSignal);
		bottomPanel.add(memory);
		bottomPanel.add(registers);
		this.add(bottomPanel, BorderLayout.SOUTH);

		listOfSchemes = new ButtonGroup();
		listener = new ListOfSchemesItemListener();
		myPictures = new HashMap<JRadioButton, GuiSchema>();
		panel = new JPanel();

		panel.setLayout(new GridLayout(names.length, 1));
		this.add(panel, BorderLayout.CENTER);

		for (int i = 0; i < names.length; i++) {
			JRadioButton newButton = new JRadioButton(properNames[i]);
			newButton.setFont(new Font("Consolas", Font.BOLD, 18));
			newButton.addActionListener(listener);
			listOfSchemes.add(newButton);
			panel.add(newButton);
			myPictures.put(newButton, schemas[i]);
		}
		this.setBorder(new MatteBorder(5, 5, 5, 5, this.getBackground()));
		this.setBorder(new EtchedBorder(Color.LIGHT_GRAY, Color.LIGHT_GRAY));

	}

	private class ListOfSchemesItemListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			myPictures.get(e.getSource()).repaint();
			if (Window.mainW.centralPanel != null) {
				Window.mainW.remove(Window.mainW.centralPanel);
			}

			Window.mainW.centralPanel = new JScrollPane(myPictures.get(e.getSource()));
			Window.mainW.centralPanel.setBackground(Color.WHITE);
			Window.mainW.setBackground(Color.WHITE);
			Window.mainW.centralPanel.setMinimumSize(new Dimension(800, 750));
			Window.mainW.centralPanel.setSize(new Dimension(800, 750));
			Window.mainW.centralPanel.repaint();
			Window.mainW.add(Window.mainW.centralPanel, BorderLayout.CENTER);
			Window.mainW.repaint();
			Window.mainW.setVisible(true);

		}
	}

	public void repaintSchema() {
		if (Window.mainW.centralPanel != null) {
			Window.mainW.remove(Window.mainW.centralPanel);
		}

		Enumeration<AbstractButton> enume = listOfSchemes.getElements();
		while (enume.hasMoreElements()) {
			JRadioButton but = (JRadioButton) enume.nextElement();
			if (but.isSelected()) {
				Window.mainW.centralPanel = new JScrollPane(myPictures.get(but));
				Window.mainW.centralPanel.setBackground(Color.WHITE);
				Window.mainW.setBackground(Color.WHITE);
				Window.mainW.centralPanel.setMinimumSize(new Dimension(800, 750));
				Window.mainW.centralPanel.setSize(new Dimension(800, 750));
				Window.mainW.centralPanel.repaint();
				Window.mainW.add(Window.mainW.centralPanel, BorderLayout.CENTER);
				Window.mainW.repaint();
				Window.mainW.setVisible(true);
				break;
			}
		}

	}

}
