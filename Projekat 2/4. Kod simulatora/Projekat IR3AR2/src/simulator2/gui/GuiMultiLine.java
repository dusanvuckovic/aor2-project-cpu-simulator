package simulator2.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

//obicna linja
public class GuiMultiLine implements Drawable {
	protected int x1, x2, y1, y2;
	protected Color color = Color.BLACK;
	protected int size = 1;

	public GuiMultiLine(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	public GuiMultiLine(int x1, int y1, int x2, int y2, int size) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.size = size;
	}

	@Override
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		Stroke stroke = g2d.getStroke(); // cuva stari stroke
		g2d.setStroke(new BasicStroke(size));

		Color col = g2d.getColor(); // cuva staru boju
		g2d.setColor(color);

		g2d.drawLine(x1, y1, x2, y2);

		// vraca stare vrednosti
		g2d.setStroke(stroke);
		g2d.setColor(col);

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

}
