package simulator2.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import simulator2.Signal;
import simulator2.dynamicinit.GraphingContainer;

public class Window extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final RightPanel rightPanel;
	public static Window mainW;
	public JScrollPane centralPanel;
	public static TreeMenu tMenu;
	public static SelectSignalWindow signalWindow;

	public Window() {

		super("Event-driven simulator IR3AR2");
		this.setIconImage(new ImageIcon("src/simulator2/gui/trueJoca.jpg").getImage());
		this.setSize(2400, 2400);
		this.setLocationRelativeTo(null);
		this.setBackground(Color.BLACK);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		rightPanel = new RightPanel();
		rightPanel.setVisible(true);
		add(rightPanel, BorderLayout.EAST);
		this.setVisible(true);
		signalWindow = new SelectSignalWindow(this);
		centralPanel = new JScrollPane();
	}

	public static void execute(GraphingContainer gc) throws IOException {
		mainW = new Window();

		final String[] names = { "slike/madr mr.png", "slike/addr1.png", "slike/addr2.png", "slike/fetch1.png", "slike/fetch2.png", "slike/fetch3.png",
				"slike/exec1.png", "slike/exec2.png", "slike/exec3.png", "slike/exec4.png", "slike/int1.png", "slike/int2.png", "slike/int3.png",
				"slike/upravljacka jedinica procesora.png", "slike/signali upravljacke jedinice.png",
				"slike/signali operacione jedinice.png", "slike/mem opr.png", "slike/mem time.png", "slike/io.png", "slike/arb.png", "slike/arb mod.png",
				"slike/arb mod io.png" };
		final String properNames[] = { "MADR/MR", "ADDR1", "ADDR2", "FETCH1", "FETCH2", "FETCH3",
				"EXEC1", "EXEC2", "EXEC3", "EXEC4", "INT1", "INT2", "INT3", "UPR. JED.", "SIGNALI UPR. JED.",
				"SIGNALI OPER. JED.", "MEM_OPR", "MEM_TIME", "IO", "ARB", "ARB_MOD_CPU", "ARB_MOD_IO"};
		
		DynamicGuiUtility du = new DynamicGuiUtility();
		 
		//ako treba, crta se druga sema
		if (GraphingContainer.howManyDynamicJumps > 0) 
			names[14] = "slike/signali upravljacke jedinice dynamic.png";

		// pravljenje niza schema
		GuiSchema[] schemas = new GuiSchema[names.length];
		for (int i = 0; i < schemas.length; i++) {
			GuiSchema gs = new GuiSchema(names[i]);
			SignalDrawables sd = new SignalDrawables();
			SignalLabels sl = new SignalLabels();
			ArrayList<GuiSignalLine> sigs = null;
			switch (i) {
			case 0:
				sigs = mapToList(sd.getMadrSignals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getMADRLabels());
				break;
			case 1:
				sigs = mapToList(sd.getAddr1Signals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getADDR1Labels());
				break;
			case 2:
				sigs = mapToList(sd.getAddr2Signals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getADDR2Labels());
				break;
			case 3:
				sigs = mapToList(sd.getFetch1Signals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getFETCH1Labels());
				du.set3StateBuffers(gc.addr1Buffers, gs);
				break;
			case 4: du.setFetch2Drawables(gc.fetch2DCs, gs); break;
			case 5: du.setTreeBasedDrawables(gc.fetch3Circuits, gs); break;
			case 6:
				sigs = mapToList(sd.getExec1Signals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getEXEC1Labels());
				break;
			case 7:
				sigs = mapToList(sd.getExec2Signals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getEXEC2Labels());
				break;
			case 8: du.setTreeBasedDrawables(gc.exec3Flags, gs); break;
			case 9:
				sigs = mapToList(sd.getExec4Signals());
				gs.setLines(sigs);
				break;
			case 10:
				sigs = mapToList(sd.getInt1Signals());
				gs.setLines(sigs);
				break;
			case 11:
				sigs = mapToList(sd.getInt2Signals());
				gs.setLines(sigs);
				break;
			case 12:
				sigs = mapToList(sd.getInt3Signals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getINT3Labels());
				break;
			case 13:
				sigs = mapToList(sd.getUpravSignals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getUPRJEDLabels());
				gs.setValBigLabels(sl.getUPRJEDBigLabels());
				break;
			case 14:
				sigs = mapToList(sd.getUprSigSignals());
				gs.setLines(sigs);
				du.setDC2(gc.jumpData, gs);
				break;
			case 15:
				sigs = mapToList(sd.getOprSigSignals());
				gs.setLines(sigs);
				break;
			case 16:
				sigs = mapToList(sd.getMemOprSignals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getMEMOPRLabels());
				break;
			case 17:
				sigs = mapToList(sd.getMemTimeSignals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getMEMTIMELabels());
				break;
			case 18:
				sigs = mapToList(sd.getIOSignals());
				gs.setLines(sigs);
				gs.setValLabels(sl.getIOLabels());
				break;
			case 19:
				sigs = mapToList(sd.getArbSignals());
				gs.setLines(sigs);
				break;
			case 20:
				sigs = mapToList(sd.getArbmodSignals());
				gs.setLines(sigs);
				break;
			case 21:
				sigs = mapToList(sd.getArbmodIOSignals());
				gs.setLines(sigs);
				break;
			}
			schemas[i] = gs;

		}

		tMenu = new TreeMenu(properNames, names, schemas);
		mainW.add(tMenu, BorderLayout.WEST);

		final SimulatorMenu sm = new SimulatorMenu();
		mainW.add(sm, BorderLayout.NORTH);
		mainW.setVisible(true);

	}

	public static ArrayList<GuiSignalLine> mapToList(HashMap<String, List<List<Point>>> hm) {

		ArrayList<GuiSignalLine> result = new ArrayList<GuiSignalLine>();
		java.util.Iterator<String> iter = hm.keySet().iterator();

		while (iter.hasNext()) {
			String s = iter.next();
			Signal test = simulator2.staticinit.StaticInitializer.allSignals.get(s);
			GuiSignalLine gsl = new GuiSignalLine(hm.get(s), test);
			result.add(gsl);
		}
		return result;
	}
}
