package simulator2.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import simulator2.staticinit.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class RegisterWindow extends JDialog {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JPanel []panel;
	JLabel []label;
	JTextArea []area;
	RegisterPasser reg;
	JButton registers;

	public RegisterWindow(JFrame window){
		this.setSize(1200, 150);
		this.setResizable(false);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		this.setLayout(new GridLayout (0,7,5,5));
		RegisterPasser reg = new RegisterPasser();
		this.setModal(true);
		panel = new JPanel[28];
		area = new JTextArea[28];
		label = new JLabel[28];
		registers = new JButton("CHANGE");
		
		
		panel[0] = new JPanel(new BorderLayout());
		label[0] = new JLabel(reg.simpleReg[0].getName());
		area[0]= new JTextArea();
		area[0].setRows(1);
		area[0].setColumns(8);
		area[0].setText(Integer.toHexString(reg.simpleReg[0].getOutValue()));
		panel[0].add(label[0], BorderLayout.WEST);
		panel[0].add(area[0], BorderLayout.EAST);
		this.add(panel[0]);
		
		panel[4] = new JPanel(new BorderLayout());
		label[4] = new JLabel(reg.simpleReg[4].getName());
		area[4]= new JTextArea();
		area[4].setRows(1);
		area[4].setColumns(8);
		area[4].setText(Integer.toHexString(reg.simpleReg[4].getOutValue()));
		panel[4].add(label[4], BorderLayout.WEST);
		panel[4].add(area[4], BorderLayout.EAST);
		this.add(panel[4]);
		
		panel[6] = new JPanel(new BorderLayout());
		label[6] = new JLabel(reg.simpleReg[6].getName());
		area[6]= new JTextArea();
		area[6].setRows(1);
		area[6].setColumns(8);
		area[6].setText(Integer.toHexString(reg.simpleReg[6].getOutValue()));
		panel[6].add(label[6], BorderLayout.WEST);
		panel[6].add(area[6], BorderLayout.EAST);
		this.add(panel[6]);
		
		panel[10] = new JPanel(new BorderLayout());
		label[10] = new JLabel(reg.simpleReg[7].getName());
		area[10]= new JTextArea();
		area[10].setRows(1);
		area[10].setColumns(8);
		area[10].setText(Integer.toHexString(reg.simpleReg[7].getOutValue()));
		panel[10].add(label[10], BorderLayout.WEST);
		panel[10].add(area[10], BorderLayout.EAST);
		this.add(panel[10]);
		
		panel[14] = new JPanel(new BorderLayout());
		label[14] = new JLabel(reg.simpleReg[14].getName());
		area[14]= new JTextArea();
		area[14].setRows(1);
		area[14].setColumns(8);
		area[14].setText(Integer.toHexString(reg.simpleReg[14].getOutValue()));
		panel[14].add(label[14], BorderLayout.WEST);
		panel[14].add(area[14], BorderLayout.EAST);
		this.add(panel[14]);
		
		panel[18] = new JPanel(new BorderLayout());
		label[18] = new JLabel(reg.simpleReg[18].getName());
		area[18]= new JTextArea();
		area[18].setRows(1);
		area[18].setColumns(8);
		area[18].setText(Integer.toHexString(reg.simpleReg[18].getOutValue()));
		panel[18].add(label[18], BorderLayout.WEST);
		panel[18].add(area[18], BorderLayout.EAST);
		this.add(panel[18]);
		
		panel[21] = new JPanel(new BorderLayout());
		label[21] = new JLabel(reg.simpleReg[21].getName());
		area[21]= new JTextArea();
		area[21].setRows(1);
		area[21].setColumns(8);
		area[21].setText(Integer.toHexString(reg.simpleReg[21].getOutValue()));
		panel[21].add(label[21], BorderLayout.WEST);
		panel[21].add(area[21], BorderLayout.EAST);
		this.add(panel[21]);
		
		panel[1] = new JPanel(new BorderLayout());
		label[1] = new JLabel(reg.simpleReg[1].getName());
		area[1]= new JTextArea();
		area[1].setRows(1);
		area[1].setColumns(8);
		area[1].setText(Integer.toHexString(reg.simpleReg[1].getOutValue()));
		panel[1].add(label[1], BorderLayout.WEST);
		panel[1].add(area[1], BorderLayout.EAST);
		this.add(panel[1]);
		
		panel[24] = new JPanel(new BorderLayout());
		label[24] = new JLabel("R0/"+reg.incDecReg[0].getName());
		area[24]= new JTextArea();
		area[24].setRows(1);
		area[24].setColumns(8);
		area[24].setText(Integer.toHexString(reg.incDecReg[0].getOutValue()));
		panel[24].add(label[24], BorderLayout.WEST);
		panel[24].add(area[24], BorderLayout.EAST);
		this.add(panel[24]);
		
		panel[7] = new JPanel(new BorderLayout());
		label[7] = new JLabel(reg.simpleReg[7].getName());
		area[7]= new JTextArea();
		area[7].setRows(1);
		area[7].setColumns(8);
		area[7].setText(Integer.toHexString(reg.simpleReg[7].getOutValue()));
		panel[7].add(label[7], BorderLayout.WEST);
		panel[7].add(area[7], BorderLayout.EAST);
		this.add(panel[7]);
		
		panel[11] = new JPanel(new BorderLayout());
		label[11] = new JLabel(reg.simpleReg[11].getName());
		area[11]= new JTextArea();
		area[11].setRows(1);
		area[11].setColumns(8);
		area[11].setText(Integer.toHexString(reg.simpleReg[11].getOutValue()));
		panel[11].add(label[11], BorderLayout.WEST);
		panel[11].add(area[11], BorderLayout.EAST);
		this.add(panel[11]);
		
		panel[15] = new JPanel(new BorderLayout());
		label[15] = new JLabel(reg.simpleReg[15].getName());
		area[15]= new JTextArea();
		area[15].setRows(1);
		area[15].setColumns(8);
		area[15].setText(Integer.toHexString(reg.simpleReg[15].getOutValue()));
		panel[15].add(label[15], BorderLayout.WEST);
		panel[15].add(area[15], BorderLayout.EAST);
		this.add(panel[15]);
		
		panel[26] = new JPanel(new BorderLayout());
		label[26] = new JLabel(reg.incDecReg[2].getName());
		area[26]= new JTextArea();
		area[26].setRows(1);
		area[26].setColumns(8);
		area[26].setText(Integer.toHexString(reg.incDecReg[2].getOutValue()));
		panel[26].add(label[26], BorderLayout.WEST);
		panel[26].add(area[26], BorderLayout.EAST);
		this.add(panel[26]);
		
		panel[22] = new JPanel(new BorderLayout());
		label[22] = new JLabel(reg.simpleReg[22].getName());
		area[22]= new JTextArea();
		area[22].setRows(1);
		area[22].setColumns(8);
		area[22].setText(Integer.toHexString(reg.simpleReg[22].getOutValue()));
		panel[22].add(label[22], BorderLayout.WEST);
		panel[22].add(area[22], BorderLayout.EAST);
		this.add(panel[22]);
		
		panel[2] = new JPanel(new BorderLayout());
		label[2] = new JLabel(reg.simpleReg[2].getName());
		area[2]= new JTextArea();
		area[2].setRows(1);
		area[2].setColumns(8);
		area[2].setText(Integer.toHexString(reg.simpleReg[2].getOutValue()));
		panel[2].add(label[2], BorderLayout.WEST);
		panel[2].add(area[2], BorderLayout.EAST);
		this.add(panel[2]);
		
		panel[25] = new JPanel(new BorderLayout());
		label[25] = new JLabel("R1/"+reg.incDecReg[1].getName());
		area[25]= new JTextArea();
		area[25].setRows(1);
		area[25].setColumns(8);
		area[25].setText(Integer.toHexString(reg.incDecReg[1].getOutValue()));
		panel[25].add(label[25], BorderLayout.WEST);
		panel[25].add(area[25], BorderLayout.EAST);
		this.add(panel[25]);
		
		panel[8] = new JPanel(new BorderLayout());
		label[8] = new JLabel(reg.simpleReg[8].getName());
		area[8]= new JTextArea();
		area[8].setRows(1);
		area[8].setColumns(8);
		area[8].setText(Integer.toHexString(reg.simpleReg[8].getOutValue()));
		panel[8].add(label[8], BorderLayout.WEST);
		panel[8].add(area[8], BorderLayout.EAST);
		this.add(panel[8]);
		
		panel[12] = new JPanel(new BorderLayout());
		label[12] = new JLabel(reg.simpleReg[12].getName());
		area[12]= new JTextArea();
		area[12].setRows(1);
		area[12].setColumns(8);
		area[12].setText(Integer.toHexString(reg.simpleReg[12].getOutValue()));
		panel[12].add(label[12], BorderLayout.WEST);
		panel[12].add(area[12], BorderLayout.EAST);
		this.add(panel[12]);
		
		panel[16] = new JPanel(new BorderLayout());
		label[16] = new JLabel(reg.simpleReg[16].getName());
		area[16]= new JTextArea();
		area[16].setRows(1);
		area[16].setColumns(8);
		area[16].setText(Integer.toHexString(reg.simpleReg[16].getOutValue()));
		panel[16].add(label[16], BorderLayout.WEST);
		panel[16].add(area[16], BorderLayout.EAST);
		this.add(panel[16]);
		
		panel[19] = new JPanel(new BorderLayout());
		label[19] = new JLabel(reg.simpleReg[19].getName());
		area[19]= new JTextArea();
		area[19].setRows(1);
		area[19].setColumns(8);
		area[19].setText(Integer.toHexString(reg.simpleReg[19].getOutValue()));
		panel[19].add(label[19], BorderLayout.WEST);
		panel[19].add(area[19], BorderLayout.EAST);
		this.add(panel[19]);
		
		panel[23] = new JPanel(new BorderLayout());
		label[23] = new JLabel(reg.simpleReg[23].getName());
		area[23]= new JTextArea();
		area[23].setRows(1);
		area[23].setColumns(8);
		area[23].setText(Integer.toHexString(reg.simpleReg[23].getOutValue()));
		panel[23].add(label[23], BorderLayout.WEST);
		panel[23].add(area[23], BorderLayout.EAST);
		this.add(panel[23]);
		
		panel[3] = new JPanel(new BorderLayout());
		label[3] = new JLabel(reg.simpleReg[3].getName());
		area[3]= new JTextArea();
		area[3].setRows(1);
		area[3].setColumns(8);
		area[3].setText(Integer.toHexString(reg.simpleReg[3].getOutValue()));
		panel[3].add(label[3], BorderLayout.WEST);
		panel[3].add(area[3], BorderLayout.EAST);
		this.add(panel[3]);
		
		panel[5] = new JPanel(new BorderLayout());
		label[5] = new JLabel(reg.simpleReg[5].getName());
		area[5]= new JTextArea();
		area[5].setRows(1);
		area[5].setColumns(8);
		area[5].setText(Integer.toHexString(reg.simpleReg[5].getOutValue()));
		panel[5].add(label[5], BorderLayout.WEST);
		panel[5].add(area[5], BorderLayout.EAST);
		this.add(panel[5]);
		
		panel[9] = new JPanel(new BorderLayout());
		label[9] = new JLabel(reg.simpleReg[9].getName());
		area[9]= new JTextArea();
		area[9].setRows(1);
		area[9].setColumns(8);
		area[9].setText(Integer.toHexString(reg.simpleReg[9].getOutValue()));
		panel[9].add(label[9], BorderLayout.WEST);
		panel[9].add(area[9], BorderLayout.EAST);
		this.add(panel[9]);
		
		panel[13] = new JPanel(new BorderLayout());
		label[13] = new JLabel(reg.simpleReg[13].getName());
		area[13]= new JTextArea();
		area[13].setRows(1);
		area[13].setColumns(8);
		area[13].setText(Integer.toHexString(reg.simpleReg[13].getOutValue()));
		panel[13].add(label[13], BorderLayout.WEST);
		panel[13].add(area[13], BorderLayout.EAST);
		this.add(panel[13]);
		
		panel[17] = new JPanel(new BorderLayout());
		label[17] = new JLabel(reg.simpleReg[17].getName());
		area[17]= new JTextArea();
		area[17].setRows(1);
		area[17].setColumns(8);
		area[17].setText(Integer.toHexString(reg.simpleReg[17].getOutValue()));
		panel[17].add(label[17], BorderLayout.WEST);
		panel[17].add(area[17], BorderLayout.EAST);
		this.add(panel[17]);
		
		panel[20] = new JPanel(new BorderLayout());
		label[20] = new JLabel(reg.simpleReg[20].getName());
		area[20]= new JTextArea();
		area[20].setRows(1);
		area[20].setColumns(8);
		area[20].setText(Integer.toHexString(reg.simpleReg[20].getOutValue()));
		panel[20].add(label[20], BorderLayout.WEST);
		panel[20].add(area[20], BorderLayout.EAST);
		this.add(panel[20]);
		
		
		this.add(registers);
		
		
		registers.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (int i = 0; i < 24; i++){
					String s = new String("#"+area[i].getText());
					int value = Integer.decode(s);
					reg.simpleReg[i].setOutValue(value);
				}
				
				for (int i = 0; i<3; i++){
					int value = Integer.decode("#" + area[i+24].getText());
					reg.incDecReg[i].setOutValue(value);
				}
				
			}
			
		});
		
		
		
		

		
	}
}
