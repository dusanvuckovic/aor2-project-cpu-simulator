package simulator2.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import simulator2.Signal;
import simulator2.Value;

public class Graph extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static int SPACE = 15;
	public final static int LENGTH = 50;
	public final static int HEIGHT = 20;
	public int lastx = 0;
	public int t = 0;
	public Value lastValue = null;
	public Signal sig;
	public String name;

	public Graph() {
		super();
		this.setBorder(new EtchedBorder(Color.BLACK, Color.LIGHT_GRAY));
	}

	public void setSig(Signal s, String n) {
		sig = s;
		name = n;
	}

	public void paintComponent(Graphics g) {
		lastx = 0;
		t = 0;
		if (sig != null) {
			List<Value> list = sig.getValueList();
			Iterator<Value> i = list.iterator();
			g.drawString(name, lastx+5, SPACE+25);
			this.setFont(this.getFont().deriveFont(12.0f));
			lastx = lastx + 100;
			while (i.hasNext()) {
				
				Value v = i.next();
				if (v != null) {
					int k;
					if (v.getHighZ() == true) {
						k = 1;
					}else
					if (v.getSize() > 1) {
						k = 2;
					} else {
						if (v.getIntValue() == 1) {
							k = 3;
						} else {
							k = 4;
						}
					}
					switch (k) {
					case 1:
						g.setColor(Color.GREEN);
						g.drawRect(lastx, 5, LENGTH, HEIGHT);
						g.drawString("highZ", lastx, SPACE);
						lastValue = v;
						lastx = lastx + LENGTH;
						break;
					case 2:
						g.setColor(Color.YELLOW);
						g.drawRect(lastx, 5, LENGTH, HEIGHT);
						g.drawString("" + v.getIntValue(), lastx, SPACE);
						lastValue = v;
						lastx = lastx + LENGTH;
						break;
					case 3:
						g.setColor(Color.RED);
						g.drawLine(lastx, SPACE, lastx + LENGTH, SPACE);
						lastValue = v;
						lastx = lastx + LENGTH;
						break;

					case 4:
						g.setColor(Color.BLUE);
						g.drawLine(lastx, SPACE+HEIGHT, lastx+LENGTH, SPACE+HEIGHT);
						lastValue = v;
						lastx = lastx + LENGTH;
						break;
					}
					g.drawString(""+t, lastx-20, SPACE+20);
					g.setColor(Color.BLACK);
					g.drawLine(lastx , SPACE+HEIGHT, lastx, SPACE);
					t++;

				}
			}

		} else {
			g.drawString("NO SIGNAL", lastx+5, SPACE);
		}


		this.setSize(1200, 500);
	}

}
