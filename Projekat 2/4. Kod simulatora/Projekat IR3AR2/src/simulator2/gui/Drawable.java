package simulator2.gui;

import java.awt.*;

public interface Drawable {

	public void draw(Graphics g);

	public void update();
}
