package simulator2.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;


//ima samo sliku i koordinate slike za iscrtavanje
public class GuiDynamicComp implements Drawable {
	
	protected BufferedImage img; //slika dinamicke komponente
	protected int x, y; // koordinate slike

	public GuiDynamicComp(BufferedImage img, int x, int y) {
		this.img = img;
		this.x = x;
		this.y = y;
	}

	public void draw(Graphics g) {
		g.drawImage(img, x, y, null);
	}


	@Override
	public void update() {
		//nista ne treba da se radi
	}
}
