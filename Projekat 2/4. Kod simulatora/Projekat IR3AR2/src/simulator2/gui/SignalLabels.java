package simulator2.gui;

import java.util.ArrayList;
import java.util.List;

import simulator2.Signal;
import simulator2.staticinit.StaticInitializer;

public final class SignalLabels {

	public List<GuiSignalLabel> getADDR1Labels() {
		List<GuiSignalLabel> addr1 = new ArrayList<GuiSignalLabel>();

		addr1.add(new GuiSignalLabel(1253, 50, "IBUS3"));
		addr1.add(new GuiSignalLabel(1335, 50, "IBUS2"));
		addr1.add(new GuiSignalLabel(1402, 50, "IBUS1"));
		addr1.add(new GuiSignalLabel(185, 650, "GPRADDR1"));
		addr1.add(new GuiSignalLabel(185, 790, "GPRADDR2"));
		addr1.add(new GuiSignalLabel(185, 920, "GPRADDR3"));
		addr1.add(new GuiSignalLabel(890, 250, "BADDR1"));
		addr1.add(new GuiSignalLabel(1090, 420, "BADDR2"));
		addr1.add(new GuiSignalLabel(90, 540, "IBUS125..22"));
		addr1.add(new GuiSignalLabel(90, 680, "IBUS124..21"));
		addr1.add(new GuiSignalLabel(90, 820, "IBUS121..18"));

		return addr1;
	}

	public List<GuiSignalLabel> getADDR2Labels() {
		List<GuiSignalLabel> addr2 = new ArrayList<GuiSignalLabel>();

		addr2.add(new GuiSignalLabel(540, 50, "IBUS3"));
		addr2.add(new GuiSignalLabel(600, 50, "IBUS2"));
		addr2.add(new GuiSignalLabel(650, 50, "IBUS1"));
		addr2.add(new GuiSignalLabel(360, 310, "R0"));
		addr2.add(new GuiSignalLabel(360, 560, "R1"));
		addr2.add(new GuiSignalLabel(360, 910, "R2"));
		addr2.add(new GuiSignalLabel(360, 1120, "R3"));
		addr2.add(new GuiSignalLabel(360, 1350, "R4"));
		addr2.add(new GuiSignalLabel(360, 1570, "R5"));
		addr2.add(new GuiSignalLabel(360, 1790, "R6"));
		addr2.add(new GuiSignalLabel(360, 2020, "R7"));
		addr2.add(new GuiSignalLabel(1020, 319, "R8"));
		addr2.add(new GuiSignalLabel(1020, 536, "R9"));
		addr2.add(new GuiSignalLabel(1020, 750, "R10"));
		addr2.add(new GuiSignalLabel(1020, 980, "R11"));
		addr2.add(new GuiSignalLabel(1020, 1204, "R12"));
		addr2.add(new GuiSignalLabel(1020, 1410, "R13"));
		addr2.add(new GuiSignalLabel(1020, 1640, "R14"));
		addr2.add(new GuiSignalLabel(1020, 1860, "R15"));

		return addr2;
	}

	public List<GuiSignalLabel> getEXEC1Labels() {
		List<GuiSignalLabel> exec1 = new ArrayList<GuiSignalLabel>();

		exec1.add(new GuiSignalLabel(88, 651, "MDR17..13"));
		exec1.add(new GuiSignalLabel(90, 737, "CNTSHIFT"));
		exec1.add(new GuiSignalLabel(579, 518, "IR17..0"));
		exec1.add(new GuiSignalLabel(560, 740, "ADDComponent"));
		exec1.add(new GuiSignalLabel(520, 990, "ALU"));
		exec1.add(new GuiSignalLabel(830, 160, "IBUS3"));
		exec1.add(new GuiSignalLabel(880, 110, "IBUS2"));
		exec1.add(new GuiSignalLabel(940, 50, "IBUS1"));

		return exec1;
	}

	public List<GuiSignalLabel> getEXEC2Labels() {
		List<GuiSignalLabel> exec2 = new ArrayList<GuiSignalLabel>();

		exec2.add(new GuiSignalLabel(522, 164, "IBUS3"));
		exec2.add(new GuiSignalLabel(577, 122, "IBUS2"));
		exec2.add(new GuiSignalLabel(623, 96, "IBUS1"));
		exec2.add(new GuiSignalLabel(156, 1039, "PSW"));

		return exec2;
	}

	public List<GuiSignalLabel> getFETCH1Labels() {
		List<GuiSignalLabel> fetch1 = new ArrayList<GuiSignalLabel>();

		fetch1.add(new GuiSignalLabel(351, 50, "IBUS3"));
		fetch1.add(new GuiSignalLabel(400, 50, "IBUS2"));
		fetch1.add(new GuiSignalLabel(464, 50, "IBUS1"));
		fetch1.add(new GuiSignalLabel(91, 222, "IR"));
		fetch1.add(new GuiSignalLabel(103, 333, "IR23..0"));
		fetch1.add(new GuiSignalLabel(103, 418, "IR20..0"));
		fetch1.add(new GuiSignalLabel(103, 555, "IR25..22"));

		return fetch1;
	}

	public List<GuiSignalLabel> getINT3Labels() {
		List<GuiSignalLabel> int3 = new ArrayList<GuiSignalLabel>();

		int3.add(new GuiSignalLabel(686, 214, "IBUS3"));
		int3.add(new GuiSignalLabel(750, 137, "IBUS2"));
		int3.add(new GuiSignalLabel(833, 67, "IBUS1"));
		int3.add(new GuiSignalLabel(291, 252, "UINThelper"));
		int3.add(new GuiSignalLabel(291, 548, "UEXThelper"));
		int3.add(new GuiSignalLabel(139, 916, "BR"));
		int3.add(new GuiSignalLabel(112, 995, "BRHelper"));
		int3.add(new GuiSignalLabel(168, 1234, "IVTP"));

		return int3;
	}

	public List<GuiSignalLabel> getIOLabels() {
		List<GuiSignalLabel> io = new ArrayList<GuiSignalLabel>();

		io.add(new GuiSignalLabel(36, 56, "DBUS"));
		io.add(new GuiSignalLabel(110, 117, "ABUS"));
		io.add(new GuiSignalLabel(344, 370, "memIO"));
		io.add(new GuiSignalLabel(844, 457, "IOMEMACC"));
		io.add(new GuiSignalLabel(642, 597, "IOTIME"));

		return io;
	}

	public List<GuiSignalLabel> getMADRLabels() {
		List<GuiSignalLabel> madr = new ArrayList<GuiSignalLabel>();

		madr.add(new GuiSignalLabel(553, 50, "IBUS3"));
		madr.add(new GuiSignalLabel(607, 50, "IBUS2"));
		madr.add(new GuiSignalLabel(662, 50, "IBUS1"));
		madr.add(new GuiSignalLabel(90, 304, "MAR"));
		madr.add(new GuiSignalLabel(90, 586, "ABUS"));
		madr.add(new GuiSignalLabel(367, 127, "DBUS"));
		madr.add(new GuiSignalLabel(367, 303, "MDR"));
		madr.add(new GuiSignalLabel(361, 439, "DBUS"));

		return madr;
	}

	public List<GuiSignalLabel> getMEMOPRLabels() {
		List<GuiSignalLabel> memopr = new ArrayList<GuiSignalLabel>();

		memopr.add(new GuiSignalLabel(42, 44, "ABUS"));
		memopr.add(new GuiSignalLabel(159, 58, "INC"));
		memopr.add(new GuiSignalLabel(562, 206, "M0"));
		memopr.add(new GuiSignalLabel(562, 433, "M1"));
		memopr.add(new GuiSignalLabel(562, 673, "M2"));
		memopr.add(new GuiSignalLabel(562, 906, "M3"));
		memopr.add(new GuiSignalLabel(480, 927, "MOmemout"));
		memopr.add(new GuiSignalLabel(336, 956, "DBUS"));

		return memopr;
	}

	public List<GuiSignalLabel> getMEMTIMELabels() {
		List<GuiSignalLabel> memtime = new ArrayList<GuiSignalLabel>();

		memtime.add(new GuiSignalLabel(392, 99, "MEMACC"));
		memtime.add(new GuiSignalLabel(633, 99, "TIME"));

		return memtime;
	}

	public List<GuiSignalLabel> getUPRJEDLabels() {
		List<GuiSignalLabel> uprjed = new ArrayList<GuiSignalLabel>();

		uprjed.add(new GuiSignalLabel(110, 176, "KMOPR"));
		uprjed.add(new GuiSignalLabel(155, 317, "MP"));
		uprjed.add(new GuiSignalLabel(159, 462, "mPC"));

		uprjed.add(new GuiSignalLabel(485, 844, "CW88..95"));
		uprjed.add(new GuiSignalLabel(652, 830, "CW96..103"));

		return uprjed;
	}

	public List<GuiBigSignalLabel> getUPRJEDBigLabels() {
		List<GuiBigSignalLabel> uprjedbig = new ArrayList<GuiBigSignalLabel>();
		List<Signal> list1 = new ArrayList<Signal>();
		List<Signal> list2 = new ArrayList<Signal>();
		for (int i = 0; i < 4; i++) {
			list1.add(StaticInitializer.allSignals.get("mMEM" + i));
		}
		for (int i = 0; i < 3; i++) {
			list2.add(StaticInitializer.allSignals.get("CW0..87_" + i));
		}

		uprjedbig.add(new GuiBigSignalLabel(104, 159, 614, list1));
		uprjedbig.add(new GuiBigSignalLabel(88, 241, 837, list2));
		return uprjedbig;
	}

}
