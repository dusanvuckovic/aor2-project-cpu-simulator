package simulator2.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;

import simulator2.Main;
import simulator2.digitalcomponents.IncDecRegister;
import simulator2.dynamicinit.NetlistMicrocode;
import simulator2.staticinit.StaticInitializer;

public class RightPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel cpu, PC;
	private JButton CLK, INS, GOTO;
	private JTextField gotoTextField;
	private JLabel IP, MP;
	private JTextArea IPText, MPText;

	private static HashMap<Integer, String> whereWeAre = new HashMap<Integer, String>();

	static {
		whereWeAre.put(0, "Start check!");
		for (int i = 0x1; i <= 0x7; i++)
			whereWeAre.put(i, "Instruction fetch!");
		whereWeAre.put(0x8, "ADD!");
		whereWeAre.put(0x9, "SUB!");
		whereWeAre.put(0xA, "CMP!");
		whereWeAre.put(0xb, "AND!");
		whereWeAre.put(0xc, "OR!");
		whereWeAre.put(0xd, "NOT!");
		whereWeAre.put(0xe, "TST!");
		for (int i = 0xf; i <= 0x11; i++)
			whereWeAre.put(i, "SHR!");
		for (int i = 0x12; i <= 0x14; i++)
			whereWeAre.put(i, "SHL!");
		for (int i = 0x15; i <= 0x18; i++)
			whereWeAre.put(i, "LD!");
		for (int i = 0x19; i <= 0x1d; i++)
			whereWeAre.put(i, "ST!");
		for (int i = 0x1e; i <= 0x24; i++)
			whereWeAre.put(i, "J...!");
		for (int i = 0x25; i <= 0x29; i++)
			whereWeAre.put(i, "CALL!");
		whereWeAre.put(0x2a, "INT!");
		for (int i = 0x2b; i <= 0x2e; i++)
			whereWeAre.put(i, "IRET!");
		for (int i = 0x2f; i <= 0x32; i++)
			whereWeAre.put(i, "RET!");
		for (int i = 0x33; i <= 0x36; i++)
			whereWeAre.put(i, "JMP!");
		for (int i = 0x37; i <= 0x3c; i++)
			whereWeAre.put(i, "PUSH!");
		for (int i = 0x3d; i <= 0x42; i++)
			whereWeAre.put(i, "POP!");
		whereWeAre.put(0x43, "Interrupt!");
		for (int i = 0x44; i <= 0x4b; i++)
			whereWeAre.put(i, "Saving processor context!");
		for (int i = 0x4c; i <= 0x56; i++)
			whereWeAre.put(i, "Interrupt check!");
		for (int i = 0x57; i <= 0x5a; i++)
			whereWeAre.put(i, "Interrupt execute!");
	}

	private String getIP() {
		return whereWeAre.get(((IncDecRegister) (StaticInitializer.allComponents.get("mPC"))).getOutValue());
	}
	
	private void updateState() {
		cpu.setText("CPU = " + Main.simulator.getCurrentTime());
		PC.setText("PC = " + Integer.toHexString((((IncDecRegister) (StaticInitializer.allComponents.get("PC"))).getOutValue())) + "h");

		MP.setText("Microprogram");

		IPText.setText(getIP());
		MPText.setText(NetlistMicrocode.stringMicrowords.get(((IncDecRegister) (StaticInitializer.allComponents.get("mPC"))).getOutValue()));
	}


	public RightPanel() {

		cpu = new JLabel();
		PC = new JLabel();
		IP = new JLabel();
		IPText = new JTextArea();
		MP = new JLabel();
		MPText = new JTextArea(); // make it bold
		gotoTextField = new JTextField("T to jump to");

		cpu.setText("CPU = 0");
		PC.setText("PC = 0");

		IP.setText("Instruction phase:");
		MP.setText("Microprogram:");
		
		cpu.setFont(new Font("Consolas", Font.BOLD, 30));
		PC.setFont(new Font("Consolas", Font.BOLD, 30));
		IP.setFont(new Font("Times New Roman", Font.BOLD, 30));
		MP.setFont(new Font("Times New Roman", Font.BOLD, 30));

		IPText.setText("");
		MPText.setText("");
		IPText.setBackground(Color.LIGHT_GRAY);
		MPText.setBackground(Color.LIGHT_GRAY);
		IPText.setFont(new Font("Times New Roman", Font.BOLD, 30));
		MPText.setFont(new Font("Consolas", Font.BOLD, 16));
		IPText.setForeground(Color.BLACK);
		MPText.setForeground(Color.MAGENTA);
		IPText.setLineWrap(true);
		MPText.setLineWrap(true);
		IPText.setWrapStyleWord(true);
		MPText.setWrapStyleWord(true);
		IPText.setEditable(false);
		MPText.setEditable(false);
		
		CLK = new JButton("CLK+");
		CLK.setFont(new Font("Times New Roman", Font.BOLD, 25));
		CLK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//lock();
				Main.simulator.tickOnce();
				updateState();
				//unlock();
			}

		});

		INS = new JButton("INS+");
		INS.setFont(new Font("Times New Roman", Font.BOLD, 25));
		INS.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//lock();
				Main.simulator.tickInstruction();
				updateState();
				//unlock();
				
			}
		});

		GOTO = new JButton("JUMP TO");
		GOTO.setFont(new Font("Times New Roman", Font.BOLD, 25));
		gotoTextField = new JTextField();
		gotoTextField.setBackground(Color.LIGHT_GRAY);
		GOTO.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.simulator.tickMultiple(Integer.parseInt(gotoTextField.getText()));
				updateState();
			}
		});

		this.setLayout(new GridLayout(11, 1, 5, 5));
		this.add(cpu);
		this.add(PC);
		this.add(CLK);
		this.add(INS);
		this.add(GOTO);
		gotoTextField.setSize(10, 10);
		gotoTextField.setFont(new Font("Consolas", Font.BOLD, 50));
		this.add(gotoTextField);
		this.add(IP);
		this.add(IPText);
		this.add(MP);
		this.add(MPText);
		this.setBorder(new MatteBorder(5, 5, 5, 5, this.getBackground()));
		this.setBorder(new EtchedBorder(Color.LIGHT_GRAY, Color.LIGHT_GRAY));
	}
}
