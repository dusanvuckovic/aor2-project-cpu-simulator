package simulator2.gui;

import java.awt.*;

import simulator2.Signal;
import simulator2.staticinit.StaticInitializer;


//ovo su samo labele sa vrednostima signala, 
//labele sa nazivima se stavljaju u listu tekstualnih labela odgovarajuce 
//scheme i ne menjaju se
public class GuiSignalLabel implements Drawable {

	protected String label;		
	protected Signal signal;
	protected int x;
	protected int y;

	public GuiSignalLabel(int xx, int yy, Signal s) {
		x = xx;
		y = yy;
		signal = s;
	}
	
	public GuiSignalLabel(int xx, int yy, String s) {
        x = xx;
        y = yy;
        signal = StaticInitializer.allSignals.get(s);
    }

	public void draw(Graphics g) {
		update();

		if (signal.getHighZ()) label = "HI";
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial", Font.BOLD, 12));
		g.drawString(label, x, y);
		
	}

	
	public void update() {
		  if (!signal.getHighZ()) {
		   switch (signal.getSize()) {
		   case 4:
		    label = String.format("%01x", signal.getValue().getIntValue()).toUpperCase();
		    break;
		   case 8:
		    label = String.format("%02x", signal.getValue().getIntValue()).toUpperCase();
		    break;
		   case 12:
		    label = String.format("%03x", signal.getValue().getIntValue()).toUpperCase();
		    break;
		   case 16:
		    label = String.format("%04x", signal.getValue().getIntValue()).toUpperCase();
		    break;
		   case 24:
		    label = String.format("%06x", signal.getValue().getIntValue()).toUpperCase();
		    break;
		   default:
		    label = Integer.toHexString(signal.getValue().getIntValue());
		    break;
		   }

		  }
		 }

}
