package simulator2.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.math.BigInteger;
import java.util.List;

import simulator2.Signal;

public class GuiBigSignalLabel implements Drawable {

	protected int numOfBits;
	protected String label;		
	protected List<Signal> signals;
	protected int x;
	protected int y;

	public GuiBigSignalLabel(int n, int xx, int yy, List<Signal> s) {
		numOfBits=n;
		x = xx;
		y = yy;
		signals = s;
	}

	public void draw(Graphics g) {
		update();
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial", Font.BOLD, 12));
		g.drawString(label, x, y);
	}

	public void update() {
		
		label = "";
		for (int i = 0; i<signals.size()-1; i++) {	//nadovezuje sve signale osim poslednjeg
			label += String.format("%32s", Integer.toBinaryString(signals.get(i).getValue().getIntValue())).replace(' ', '0');
		}
		//sad treba dodati poslednji signal delimicno
		int n = numOfBits%32;	//broj preostalih bita
		label += (String.format("%32s", Integer.toBinaryString(signals.get(signals.size() - 1).getValue().getIntValue())).replace(' ', '0')).substring(32-n,32);
		BigInteger b = new BigInteger(label, 2);
		label = b.toString(16);
	}

}
