package simulator2;

import java.io.*;
import java.util.*;

public interface Value extends Serializable, Comparable<Value> {

	public void setIntValue(int value);

	public int getIntValue();

	public void setLongValue(long value);

	public long getLongValue();

	public void setBooleanValue(boolean val);

	public boolean getBooleanValue();

	public void setHighZ(boolean highZ);

	public boolean getHighZ();

	public void setStringValue(String val);

	public String getStringValue();

	public void setSize(int size);

	public int getSize();

	public List<Value> separateValues();

	public Value copy();

	public boolean load(Value value);

	public boolean isEqual(Value value);

}
