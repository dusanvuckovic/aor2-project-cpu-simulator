package simulator2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import simulator2.staticinit.ADDR1;
import simulator2.staticinit.ADDR2;
import simulator2.staticinit.ARB;
import simulator2.staticinit.ARBMOD;
import simulator2.staticinit.EXEC1;
import simulator2.staticinit.EXEC2;
import simulator2.staticinit.EXEC4;
import simulator2.staticinit.FETCH1;
import simulator2.staticinit.GLOBALINIT;
import simulator2.staticinit.INT1;
import simulator2.staticinit.INT2;
import simulator2.staticinit.INT3;
import simulator2.staticinit.IO;
import simulator2.staticinit.MADR;
import simulator2.staticinit.MEMOPR;
import simulator2.staticinit.MEMTIME;
import simulator2.staticinit.OPRSIG;
import simulator2.staticinit.StaticInitializable;
import simulator2.staticinit.StaticInitializer;
import simulator2.staticinit.UPRJED;
import simulator2.staticinit.UPRSIG;
import simulator2.digitalcomponents.MemoryModule;
import simulator2.digitalcomponents.MicroMemory;
import simulator2.dynamicinit.NetlistDynamic;
import simulator2.dynamicinit.NetlistMemory;
import simulator2.dynamicinit.NetlistMicrocode;
import simulator2.gui.Window;

public class Main {
	public static ArrayList<StaticInitializable> myStatic = new ArrayList<StaticInitializable>(
			Arrays.asList(new GLOBALINIT(), new UPRSIG(), new OPRSIG(), new ARBMOD(0), new ARBMOD(1), new ARB(),
					new ADDR1(), new ADDR2(), new EXEC1(), new EXEC2(), new EXEC4(), new FETCH1(), new INT1(),
					new INT2(), new INT3(), new IO(), new MADR(), new MEMOPR(), new MEMTIME(), new UPRJED()));

	public static EventSimulator simulator;

	public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
	    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
	        if ("Nimbus".equals(info.getName())) {
	            UIManager.setLookAndFeel(info.getClassName());
	            break;
	        }
	    }
		SignalSimple TSTART = new SignalSimple("TSTART");
		StaticInitializer.allSignals.put("TSTART", TSTART);
		for (StaticInitializable s : myStatic)
			s.initSignals();
		NetlistDynamic n = new NetlistDynamic();
		n.init();
		for (StaticInitializable s : myStatic)
			s.connectSignals();
		NetlistMicrocode nmrc = new NetlistMicrocode();
		nmrc.process();
		MicroMemory mMem = (MicroMemory) StaticInitializer.allComponents.get("mMEM");
		mMem.fillMicroMemory(nmrc.getMicrowords());
		NetlistMemory nmem = new NetlistMemory();
		nmem.process();
		MemoryModule[] myMemory = { 
				(MemoryModule) StaticInitializer.allComponents.get("M0"),
				(MemoryModule) StaticInitializer.allComponents.get("M1"),
				(MemoryModule) StaticInitializer.allComponents.get("M2"),
				(MemoryModule) StaticInitializer.allComponents.get("M3") 
				};

		simulator = new EventSimulator(StaticInitializer.allSignals.get("CLK"), StaticInitializer.allSignals,
				StaticInitializer.allBuses, StaticInitializer.allComponents);

		Window.execute(n.passGraphingContainer());

		simulator.reset();
		
		nmem.setMemory(myMemory);

	}

}
