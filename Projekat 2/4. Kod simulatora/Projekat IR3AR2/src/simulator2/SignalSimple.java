package simulator2;

import java.util.*;


public class SignalSimple implements Signal {

	private static final long serialVersionUID = 1L;
	private static long lastId = 0;
	protected long id;
	protected String name;
	protected int defaultValue = 0;

	protected Value value;
	protected List<Signal> connectedSignals;
	protected List<Value> valueList;

	protected transient List<Event> events;

	public SignalSimple() {
		this("");
	}

	public SignalSimple(String name) {
		super();

		this.id = lastId++;
		this.name = name;

		value = new DigitalValue(0, 1, false);
		connectedSignals = new LinkedList<Signal>();
		valueList = new ArrayList<Value>();
		events = new LinkedList<Event>();
	}

	public SignalSimple(String name, int size) {
		super();

		this.id = lastId++;
		this.name = name;

		value = new DigitalValue(0, size, false);
		connectedSignals = new LinkedList<Signal>();
		valueList = new ArrayList<Value>();
		events = new LinkedList<Event>();
	}

	public void initializeEvents() {
		events = new LinkedList<Event>();
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name + "Signal" + id;
	}

	@Override
	public String getSimpleName() {
		return name;
	}

	@Override
	public void setId(long id) { // ! NOT SAFE !
		this.id = id;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setValue(Value value) { // ! NOT SAFE !
		this.value.load(value);
	}

	@Override
	public Value getValue() {
		return value;
	}

	@Override
	public void setIntValue(int value) { // ! NOT SAFE !
		this.value.setIntValue(value);
	}

	@Override
	public int getIntValue() {
		return value.getIntValue();
	}

	@Override
	public int getSize() {
		return value.getSize();
	}

	@Override
	public void setHighZ(boolean highZ) { // ! NOT SAFE !
		value.setHighZ(highZ);
	}

	@Override
	public boolean getHighZ() {
		return value.getHighZ();
	}

	@Override
	public void setDefaultValue(int defValue) {
		defaultValue = defValue;
	}

	@Override
	public int getDefaultValue() {
		return defaultValue;
	}

	@Override
	public List<Value> getValueList() {
		return valueList;
	}

	@Override
	public List<Signal> getConnectedSignals() {
		return connectedSignals;
	}

	@Override
	public void connect(Signal signal) {
		if (connectedSignals == null) {
			connectedSignals = new LinkedList<Signal>();
		}
		if (!connectedSignals.contains(signal)) {
			connectedSignals.add(signal);
		}
	}

	@Override
	public void connectBoth(Signal signal) {
		this.connect(signal);
		signal.connect(this);
	}

	@Override
	public void disconnect(Signal signal) {
		connectedSignals.remove(signal);
	}

	@Override
	public void disconnectBoth(Signal signal) {
		this.disconnect(signal);
		signal.disconnect(this);
	}

	@Override
	public boolean isConnected() {
		return connectedSignals.size() > 0;
	}

	@Override
	public boolean canConduct() {
		return true;
	}

	@Override
	public boolean canGenerate() {
		return false;
	}

	@Override
	public void init(String[] data) {
		name = data[0];
		value.setSize(Integer.parseInt(data[1]));
	}

	@Override
	public String[] state() {
		String[] state = new String[] { this.getClass().getName(), "" + this.id, "" + this.value.getSize() };
		return state;
	}

	@Override
	public List<Event> preCalculateEvents() {
		List<Event> result = events;
		result.clear();

		return result;
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value newVal = new DigitalValue(DigitalValue.ZERO, value.getSize());
		newVal.setIntValue(event.getValue().getIntValue());
		newVal.setHighZ(event.getValue().getHighZ());

		if (!value.isEqual(newVal)) {
			value.load(newVal);
			EventExecutable src = event.getSrc();
			
			for (Signal signal : connectedSignals) { // Sends the value to all
														// connected signals.
				if (signal != src) {
					Event tempEvent = new Event(event.getTime(), value, signal, this);
					result.add(tempEvent);
				}
			}
		}

		return result;
	}

	@Override
	public String toString() {
		String result = "";
		result += this.getClass().getSimpleName() + " " + name + " " + id + " " + value;
		return result;
	}

	@Override
	public void resetState() {
		value.setHighZ(false);
		value.setIntValue(defaultValue);
		valueList = new ArrayList<Value>();
	}

	@Override
	public void saveValue() {
		valueList.add(new DigitalValue(value));
	}
	
	public void saveValueLast() {
		valueList.set(valueList.size() - 1, new DigitalValue(value));
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDescription(String description) {
		// TODO Auto-generated method stub

	}

}
