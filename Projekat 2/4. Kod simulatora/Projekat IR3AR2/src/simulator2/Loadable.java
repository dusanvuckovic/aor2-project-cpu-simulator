package simulator2;

public interface Loadable {

	public void init(String[] data); // Initializes the component from loaded
										// data.

	public String[] state(); // Returns a string array equal to the
								// initialization data.
}
