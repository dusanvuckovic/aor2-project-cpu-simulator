package simulator2.util;

import java.util.HashMap;

public class SparseVector {

	private final HashMap<Integer, Byte> myVector;

	public SparseVector() {
		myVector = new HashMap<>();
	}

	public int size() {
		return myVector.size();
	}

	public void put(int i, byte x) {
		myVector.put(i, x);
	}

	public byte get(int i) {
		if (!myVector.containsKey(i)) {
			return 0;
		} else {
			return myVector.get(i);
		}
	}

	public void reset() {
		myVector.clear();
	}

}
