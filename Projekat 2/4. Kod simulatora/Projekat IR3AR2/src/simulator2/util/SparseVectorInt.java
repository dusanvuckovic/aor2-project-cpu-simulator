package simulator2.util;

import java.util.HashMap;

public class SparseVectorInt {

	private final HashMap<Integer, Integer> myVector;

	public SparseVectorInt() {
		myVector = new HashMap<>();
	}

	public int size() {
		return myVector.size();
	}

	public void put(int i, int x) {
		myVector.put(i, x);
	}

	public int get(int i) {
		if (!myVector.containsKey(i)) {
			return 0;
		} else {
			return myVector.get(i);
		}
	}

	public void reset() {
		myVector.clear();
	}

}
