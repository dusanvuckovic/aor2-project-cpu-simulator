package simulator2.staticinit;

import java.util.ArrayList;

import simulator2.SignalSimple;
import simulator2.digitalcomponents.Bus;
import simulator2.digitalcomponents.IncDecRegister;
import simulator2.digitalcomponents.MicroMemory;
import simulator2.digitalcomponents.Multiplexer;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.SignalSplitter;
import simulator2.digitalcomponents.TriStateBuffer;

public final class UPRJED implements StaticInitializable {

	private int[] opPositions = { 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0x15, 0x19, 0x1e, 0x1e, 0x1e, 0x1e, 0x1e, 0x1e,
			0x1e, 0x1e, 0x1e, 0x1e, 0x25, 0x2f, 0x2b, 0x33, 0x37, 0x3d, 0x43, 0x43, 0x0f, 0x12, 0x2a };
	private String[] opNames = { "ADD", "SUB", "CMP", "AND", "OR", "NOT", "TST", "LD", "ST", "JE", "JNE", "JGE", "JG",
			"JLE", "JL", "JP", "JNP", "JO", "JNO", "CALL", "RET", "IRET", "JMP", "PUSH", "POP", "IN", "OUT", "SHR",
			"SHL", "INT" };
	Bus KMOPRexit = new Bus("KMOPR", 8);
	ArrayList<TriStateBuffer> valueBuffers = new ArrayList<TriStateBuffer>(opPositions.length);
	ArrayList<SignalSimple> valueSignals = new ArrayList<SignalSimple>(opPositions.length);
	Or shouldLoad = new Or();
	Not shouldNotLoad = new Not();
	Multiplexer MP = new Multiplexer();
	IncDecRegister mPC = new IncDecRegister();
	MicroMemory mMEM = new MicroMemory();
	SignalSplitter CWHelper[] = new SignalSplitter[4];
	SignalMerger CW087[] = new SignalMerger[3];
	SignalMerger CW8895 = new SignalMerger(), CW96103 = new SignalMerger();

	@Override
	public void initSignals() {

		String myBufData[] = { "", "8", "1" };
		for (int i = 0; i < opNames.length; i++) {
			TriStateBuffer helperBuffer = new TriStateBuffer();
			myBufData[0] = "KMOPRHelper" + i;
			helperBuffer.init(myBufData);
			valueBuffers.add(helperBuffer);
			StaticInitializer.allComponents.put(helperBuffer.getName(), helperBuffer);
		}

		String myLogicComponentData[] = { "shouldLoad", "3", "1" };
		shouldLoad.init(myLogicComponentData);
		myLogicComponentData[0] = "shouldNotLoad";
		myLogicComponentData[1] = "1";
		shouldNotLoad.init(myLogicComponentData);

		String myData[] = { "", "", "" };

		myData[0] = "MP";
		myData[1] = "8";
		myData[2] = "2";
		MP.init(myData);

		myData[0] = "mPC";
		myData[1] = "8";
		myData[2] = "1";
		mPC.init(myData);

		myData[0] = "mMEM";
		myData[1] = "104";
		myData[2] = "8";
		mMEM.init(myData);

		for (int i = 0; i < 4; i++) {
			CWHelper[i] = new SignalSplitter();
			myData[0] = "CWHelper" + i;
			myData[1] = "1";
			myData[2] = "32";
			CWHelper[i].init(myData);
		}

		for (int i = 0; i < 3; i++) {
			CW087[i] = new SignalMerger();
			myData[0] = "CW0..87_" + i;
			myData[1] = "1";
			if (i != 2)
				myData[2] = "32";
			else
				myData[2] = "24";
			CW087[i].init(myData);
		}

		myData[0] = "CW88..95";
		myData[1] = "1";
		myData[2] = "8";
		CW8895.init(myData);

		myData[0] = "CW96..103";
		myData[1] = "1";
		myData[2] = "8";
		CW96103.init(myData);
		StaticInitializer.allBuses.put("KMOPR", KMOPRexit);
		StaticInitializer.allComponents.put(shouldLoad.getName(), shouldLoad);
		StaticInitializer.allComponents.put(shouldNotLoad.getName(), shouldNotLoad);
		StaticInitializer.allComponents.put(MP.getName(), MP);
		StaticInitializer.allComponents.put(mPC.getName(), mPC);
		StaticInitializer.allComponents.put(mMEM.getName(), mMEM);
		for (int i = 0; i < 3; i++)
			StaticInitializer.allComponents.put(CW087[i].getName(), CW087[i]);
		StaticInitializer.allComponents.put(CW8895.getName(), CW8895);
		StaticInitializer.allComponents.put(CW96103.getName(), CW96103);
		for (int i = 0; i < 4; i++) {
			StaticInitializer.allComponents.put(CWHelper[i].getName(), CWHelper[i]);
		}

		for (int i = 0; i < 4; i++)
			StaticInitializer.allSignals.put("mMEM" + i, mMEM.getPortOUT(i));

		for (int i = 0; i < OPRSIG.equivalentNames.size(); i++)
			StaticInitializer.allSignals.put(OPRSIG.equivalentNames.get(i), CWHelper[i / 32].getPortOUT(i % 32));
		StaticInitializer.allSignals.put("KMOPR", KMOPRexit);
		StaticInitializer.allSignals.put("MP", MP.getPortOUT());
		StaticInitializer.allSignals.put("mPC", mPC.getPortOUT());
		StaticInitializer.allSignals.put("LD_OR", shouldLoad.getPortOUT());
		StaticInitializer.allSignals.put("LD_ORinv", shouldNotLoad.getPortOUT());
		for (int i = 0; i < 3; i++)
			StaticInitializer.allSignals.put(CW087[i].getName(), CW087[i].getPortOUT());
		StaticInitializer.allSignals.put(CW96103.getName(), CW96103.getPortOUT());
		StaticInitializer.allSignals.put(CW8895.getName(), CW8895.getPortOUT());
		for (int i = 0; i < 103; i++)
			StaticInitializer.allSignals.put("CW" + i, CWHelper[i / 32].getPortOUT(i % 32));

	}

	@Override
	public void connectSignals() {
		for (int i = 0; i < opNames.length; i++) {
			TriStateBuffer myBuffer = valueBuffers.get(i);
			myBuffer.getPortIN().setDefaultValue(opPositions[i]);
			StaticInitializer.allSignals.get(opNames[i]).connect(myBuffer.getPortC());
			myBuffer.getPortOUT().connect(KMOPRexit);
		}
		KMOPRexit.connect(MP.getPortIN(1));

		StaticInitializer.allSignals.get("bropr").connect(shouldLoad.getPortIN(0));
		StaticInitializer.allSignals.get("branch1").connect(shouldLoad.getPortIN(1));
		StaticInitializer.allSignals.get("br").connect(shouldLoad.getPortIN(2));
		shouldLoad.getPortOUT().connect(shouldNotLoad.getPortIN());

		MP.getPortOUT().connect(mPC.getPortIN());
		StaticInitializer.allSignals.get("bropr").connect(MP.getPortS(0));

		StaticInitializer.allSignals.get("CLK").connect(mPC.getPortCLK());
		shouldLoad.getPortOUT().connect(mPC.getPortLD());
		shouldNotLoad.getPortOUT().connect(mPC.getPortINC());
		mPC.getPortOUT().connect(mMEM.getPortADR());
		StaticInitializer.allSignals.get("0").connect(mMEM.getPortWR());
		StaticInitializer.allSignals.get("1").connect(mMEM.getPortRD());

		for (int i = 0; i < 4; i++)
			mMEM.getPortOUT(i).connect(CWHelper[i].getPortIN());

		for (int i = 0; i < 88; i++)
			StaticInitializer.allSignals.get("CW" + i).connect(CW087[i / 32].getPortIN(i % 32));
		for (int i = 88; i < 96; i++)
			StaticInitializer.allSignals.get("CW" + i).connect(CW8895.getPortIN(i - 88));
		for (int i = 96; i < 103; i++)
			StaticInitializer.allSignals.get("CW" + i).connect(CW96103.getPortIN(i - 96));
		CW96103.getPortOUT().connect(MP.getPortIN(0));
	}

}
