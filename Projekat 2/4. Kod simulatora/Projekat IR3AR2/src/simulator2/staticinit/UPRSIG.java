package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.DecoderE;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;

public final class UPRSIG implements StaticInitializable {

	DecoderE DC4 = new DecoderE();
	And UPRAnds[] = new And[13];
	Not UPRNots[] = new Not[13];
	Or UPRbrnch = new Or();
	Not inv92 = new Not();
	static public DecoderE DC4Dynamic = new DecoderE();
	static public Or UPRbrnchDynamic = new Or();
	Or UPRbrnchFINAL = new Or();
	private String[] helperString = { "next", "br", "bropr", "START", "hack0", "fcCPU", "gropr", "noSHIFT", "noSHIFT",
			"brjmp", "JMPREGIND", "prekid", "PRINS", "PRCOD", "PRADR", "printr" };
	private String[] helperOUTString = { "next", "br", "bropr", "START", "hack0", "fcCPU", "gropr", "!noSHIFT",
			"noSHIFT", "brjmp", "JMPREGIND", "prekid", "PRINS", "PRCOD", "PRADR", "printr" };

	@Override
	public void initSignals() {
		String[] notData = { "inv92", "1" };
		inv92.init(notData);

		String[] myData = { "DC4", "4", "1" };
		DC4.init(myData);
		myData[0] = "DC4Dynamic";
		DC4Dynamic.init(myData);

		myData[0] = "UPRbrnch";
		myData[1] = "13";
		UPRbrnch.init(myData);

		myData[0] = "UPRbrnchFINAL";
		myData[1] = "2";
		UPRbrnchFINAL.init(myData);
		UPRbrnch.getPortOUT().connect(UPRbrnchFINAL.getPortIN(0));
		for (int i = 0; i < 13; i++) {
			UPRAnds[i] = new And();
			myData[0] = "UPRAnds" + i;
			myData[1] = "2";
			UPRAnds[i].init(myData);
			if (i != 8 || i != 10) {
				UPRNots[i] = new Not();
				myData[0] = "UPRNots" + i;
				myData[1] = "1";
				UPRNots[i].init(myData);
			}
		}

		StaticInitializer.allComponents.put(inv92.getName(), inv92);
		StaticInitializer.allSignals.put("!CW92", inv92.getPortOUT());
		StaticInitializer.allComponents.put(DC4.getName(), DC4);
		StaticInitializer.allComponents.put(UPRbrnch.getName(), UPRbrnch);
		StaticInitializer.allComponents.put(DC4Dynamic.getName(), DC4Dynamic);
		StaticInitializer.allComponents.put(UPRbrnchDynamic.getName(), UPRbrnchDynamic);
		StaticInitializer.allComponents.put(UPRbrnchFINAL.getName(), UPRbrnchFINAL);

		for (int i = 0; i < 13; i++) {
			StaticInitializer.allComponents.put(UPRAnds[i].getName(), UPRAnds[i]);
			StaticInitializer.allComponents.put(UPRNots[i].getName(), UPRNots[i]);
		}
		for (int i = 0; i < 3; i++)
			StaticInitializer.allSignals.put(helperString[i], DC4.getPortOUT(i));

		for (int i = 3; i < 16; i++)
			StaticInitializer.allSignals.put("DC_" + helperOUTString[i], DC4.getPortOUT(i));

		for (int i = 3; i < 16; i++)
			StaticInitializer.allSignals.put("AND_" + helperOUTString[i], UPRAnds[i - 3].getPortOUT());

		StaticInitializer.allSignals.put("branch1", UPRbrnch.getPortOUT());
		StaticInitializer.allSignals.put("branch", UPRbrnchFINAL.getPortOUT());
		for (int i = 0; i < 3; i++)
			StaticInitializer.allSignals.put(helperString[i], DC4.getPortOUT(i));
	}

	@Override
	public void connectSignals() {
		StaticInitializer.allSignals.get("branch1").connect(UPRbrnchFINAL.getPortIN(0));
		if (StaticInitializer.allSignals.containsKey("branch2"))
			StaticInitializer.allSignals.get("branch2").connect(UPRbrnchFINAL.getPortIN(1));

		StaticInitializer.allSignals.get("CW92").connect(inv92.getPortIN());
		for (int i = 0; i < 4; i++)
			StaticInitializer.allSignals.get("CW" + (i + 88)).connect(DC4.getPortIN(i));
		// DC4.invertPort(DC4.getNumInputs() + DC4.getNumOutputs()); //invert E
		// porta
		StaticInitializer.allSignals.get("1").connect(DC4.getPortE());
		for (int i = 3; i < 16; i++) {
			DC4.getPortOUT(i).connect(UPRAnds[i - 3].getPort(0));
			if (i != 8 && i != 10) {
				StaticInitializer.allSignals.get(helperString[i]).connect(UPRNots[i - 3].getPortIN());
				UPRNots[i - 3].getPortOUT().connect(UPRAnds[i - 3].getPortIN(1));
			} else
				StaticInitializer.allSignals.get(helperString[i]).connect(UPRAnds[i - 3].getPort(1));
			UPRAnds[i - 3].getPortOUT().connect(UPRbrnch.getPortIN(i - 3));
		}
	}
}
