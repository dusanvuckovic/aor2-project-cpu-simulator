package simulator2.staticinit;

public interface StaticInitializable {

	public void initSignals();

	public void connectSignals();

}
