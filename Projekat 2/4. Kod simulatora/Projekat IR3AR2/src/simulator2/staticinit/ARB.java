package simulator2.staticinit;

import simulator2.digitalcomponents.CoderEW;
import simulator2.digitalcomponents.DecoderE;

public final class ARB implements StaticInitializable {

	CoderEW CD0 = new CoderEW();
	DecoderE DC2 = new DecoderE();

	@Override
	public void initSignals() {
		String[] myData = { "DC2", "1" };
		DC2.init(myData);

		myData[0] = "CD0";
		CD0.init(myData);

		StaticInitializer.allComponents.put(CD0.getName(), CD0);
		StaticInitializer.allComponents.put(DC2.getName(), DC2);

		StaticInitializer.allSignals.put("CPU_BG_IN", DC2.getPortOUT(0));
		StaticInitializer.allSignals.put("IO_BG_IN", DC2.getPortOUT(1));
		StaticInitializer.allSignals.put(CD0.getName(), CD0.getPortOUT(0));
		StaticInitializer.allSignals.put("WtoE", CD0.getPortW());

	}

	@Override
	public void connectSignals() {
		// CD0
		StaticInitializer.allSignals.get("CPU_BR").connect(CD0.getPortIN(0));
		StaticInitializer.allSignals.get("IO_BR").connect(CD0.getPortIN(1));
		StaticInitializer.allSignals.get("1").connect(CD0.getPortE());
		CD0.getPortOUT(0).connect(DC2.getPortIN(0));
		DC2.getPortE().connectBoth(CD0.getPortW());
		// CD0

		// DC2
		StaticInitializer.allSignals.get("CPU_BG_IN").connectBoth(DC2.getPortOUT(0));
		StaticInitializer.allSignals.get("IO_BG_IN").connectBoth(DC2.getPortOUT(1));
		// DC2
	}
}
