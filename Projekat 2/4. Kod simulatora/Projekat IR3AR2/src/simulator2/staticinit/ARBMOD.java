package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.RSFlipFlop;
import simulator2.digitalcomponents.TriStateBufferNegated;

public final class ARBMOD implements StaticInitializable {

	Not AMN1 = new Not(), AMN2 = new Not(),
			AMN3 = new Not();
	And AMA1 = new And(), AMA2 = new And(), AMA3 = new And();
	TriStateBufferNegated AMbusy = new TriStateBufferNegated();
	RSFlipFlop FF0 = new RSFlipFlop(), FF1 = new RSFlipFlop();
	Integer instanceNumber;
	private String myString;

	public ARBMOD(Integer instanceNumber) {
		this.instanceNumber = instanceNumber;
		if (instanceNumber == 0)
			myString = "CPU";
		else
			myString = "IO";
	}

	@Override
	public void initSignals() {

		String[] myData = { "AMN1_" + instanceNumber, "1", "1" };
		AMN1.init(myData);

		myData[0] = "AMN2_" + instanceNumber;
		AMN2.init(myData);

		myData[0] = "AMN3_" + instanceNumber;
		AMN3.init(myData);
		
		myData[1] = "2";
		myData[0] = "AMA1_" + instanceNumber;
		AMA1.init(myData);

		myData[0] = "AMA2_" + instanceNumber;
		AMA2.init(myData);

		myData[0] = "AMA3_" + instanceNumber;
		AMA3.init(myData);

		myData[0] = "AMBusy_" + instanceNumber;
		myData[1] = "1";
		AMbusy.init(myData);

		myData[0] = "FF0_" + instanceNumber;
		FF0.init(myData);

		myData[0] = "FF1_" + instanceNumber;
		FF1.init(myData);

		StaticInitializer.allComponents.put(AMN1.getName(), AMN1);
		StaticInitializer.allComponents.put(AMN2.getName(), AMN2);
		StaticInitializer.allComponents.put(AMN3.getName(), AMN3);
		StaticInitializer.allComponents.put(AMA1.getName(), AMA1);
		StaticInitializer.allComponents.put(AMA2.getName(), AMA2);
		StaticInitializer.allComponents.put(AMA3.getName(), AMA3);
		StaticInitializer.allComponents.put(AMbusy.getName(), AMbusy);
		StaticInitializer.allComponents.put(FF0.getName(), FF0);
		StaticInitializer.allComponents.put(FF1.getName(), FF1);

		StaticInitializer.allSignals.put(myString + "_BG_OUT", AMA2.getPortOUT());
		StaticInitializer.allSignals.put(myString + "_BR", FF0.getPortQ());

		StaticInitializer.allSignals.put(myString + "BUSYBUSinv", AMN2.getPortOUT());
		StaticInitializer.allSignals.put(myString + "BUSYBUSinvinv", AMN3.getPortOUT());

		StaticInitializer.allSignals.put(myString + "BRNeg", AMN1.getPortOUT());
		StaticInitializer.allSignals.put(myString + "BRandBG_IN", AMA1.getPortOUT());

		StaticInitializer.allSignals.put(myString + "FF1S", AMA2.getPortOUT());
		StaticInitializer.allSignals.put(myString + "FF1Q", FF1.getPortQ());
		StaticInitializer.allSignals.put("hack" + instanceNumber, FF1.getPortQ());
	}

	@Override
	public void connectSignals() {
		// FF0
		StaticInitializer.allSignals.get("CLK").connect(FF0.getPortCLK());
		StaticInitializer.allSignals.get("hreq" + instanceNumber).connect(FF0.getPortS());
		StaticInitializer.allSignals.get("hclr" + instanceNumber).connect(FF0.getPortR());

		// AMN1
		FF0.getPortQ().connect(AMN1.getPortIN());
		// AMN1

		// AMA1
		FF0.getPortQ().connect(AMA1.getPortIN(0));
		StaticInitializer.allSignals.get(myString + "_BG_IN").connect(AMA1.getPortIN(1));
		// AMA1

		// AMA2
		AMN1.getPortOUT().connect(AMA2.getPortIN(0));
		StaticInitializer.allSignals.get(myString + "_BG_IN").connect(AMA2.getPortIN(1));
		// AMA2

		// AMA3
		AMA1.getPortOUT().connect(AMA3.getPortIN(0));
		AMN3.getPortOUT().connect(AMA3.getPortIN(1));
		// AMA3

		// FF1
		StaticInitializer.allSignals.get("CLK").connect(FF1.getPortCLK());
		AMA3.getPortOUT().connect(FF1.getPortS());
		StaticInitializer.allSignals.get("hclr" + instanceNumber).connect(FF1.getPortR());
		// FF1

		// AMbusy
		FF1.getPortQ().connect(AMbusy.getPortC());
		StaticInitializer.allSignals.get("1").connect(AMbusy.getPortIN());
		AMbusy.getPortOUT().connect(StaticInitializer.allSignals.get("!BUSYBUS")); // mi propagiramo na magistralu
		// AMbusy

		// AMN2
		StaticInitializer.allSignals.get("!BUSYBUS").connect(AMN2.getPortIN());
		// AMN2

		// AMN3
		AMN2.getPortOUT().connect(AMN3.getPortIN());
		// AMN3
	}

}
