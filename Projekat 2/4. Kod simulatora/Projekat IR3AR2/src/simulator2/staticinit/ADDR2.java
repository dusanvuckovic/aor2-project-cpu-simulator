package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.IncDecRegister;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SimpleRegister;
import simulator2.digitalcomponents.TriStateBuffer;

public final class ADDR2 implements StaticInitializable {

	IncDecRegister PC = new IncDecRegister(), SP = new IncDecRegister();
	And PCand1 = new And(), PCand2 = new And(), PCand3 = new And(), SPand1 = new And(), SPand2 = new And(),
			SPand3 = new And();
	SimpleRegister sysRegisters[] = new SimpleRegister[14]; // R2 - R15
	And first[] = new And[14];
	And second[] = new And[14];
	And third[] = new And[14];
	Or pcspout[] = new Or[2];
	Or pcload = new Or();
	Or pcout2 = new Or();
	TriStateBuffer[] IBUS1exit = new TriStateBuffer[14];
	TriStateBuffer[] IBUS2exit = new TriStateBuffer[14];
	TriStateBuffer PCexit1 = new TriStateBuffer(), PCexit2 = new TriStateBuffer(), SPexit1 = new TriStateBuffer(),
			SPexit2 = new TriStateBuffer();

	private void initPC() {
		String mySuperRegisterString[] = { "PC", "32", "4" };
		PC.init(mySuperRegisterString);
		PC.setDefaultValue(0x100);

		String myData[] = { "PCand1", "2", "1" };
		PCand1.init(myData);

		myData[0] = "PCand2";
		PCand2.init(myData);

		myData[0] = "PCand3";
		PCand3.init(myData);

		pcspout[0] = new Or();
		myData[0] = "pcspout0";
		pcspout[0].init(myData);

		myData[0] = "pcload";
		pcload.init(myData);

		myData[0] = "pcout2";
		pcout2.init(myData);

		myData[1] = "32";

		myData[0] = "PCexit1";
		PCexit1.init(myData);

		myData[0] = "PCexit2";
		PCexit2.init(myData);
	}

	private void initSP() {
		String mySuperRegisterString[] = { "SP", "32", "4" };
		SP.init(mySuperRegisterString);
		SP.setDefaultValue(0x3000);

		String myData[] = { "SPand1", "2", "1" };
		SPand1.init(myData);

		myData[0] = "SPand2";
		SPand2.init(myData);

		myData[0] = "SPand3";
		SPand3.init(myData);

		pcspout[1] = new Or();
		myData[0] = "pcspout1";
		pcspout[1].init(myData);

		myData[1] = "32";

		myData[0] = "SPexit1";
		SPexit1.init(myData);

		myData[0] = "SPexit2";
		SPexit2.init(myData);
	}

	private void initSingleRegister(int i) {
		if (i == 0)
			initPC();
		else if (i == 1)
			initSP();
		else { // ina�?e već inicijalizovano
			int iname = i;
			i -= 2;

			sysRegisters[i] = new SimpleRegister();
			String mySuperRegisterString[] = { "", "32", "4" };

			mySuperRegisterString[0] = "R" + iname;
			sysRegisters[i].init(mySuperRegisterString);

			String myData[] = { "first" + iname, "2", "1" };
			first[i] = new And();
			first[i].init(myData);

			second[i] = new And();
			myData[0] = "second" + iname;
			second[i].init(myData);

			third[i] = new And();
			myData[0] = "third" + iname;
			third[i].init(myData);

			IBUS1exit[i] = new TriStateBuffer();
			myData[0] = "IBUS1exit" + iname;
			myData[1] = "32";
			IBUS1exit[i].init(myData);

			IBUS2exit[i] = new TriStateBuffer();
			myData[0] = "IBUS2exit" + iname;
			IBUS2exit[i].init(myData);
		}

	}

	private void connectSingleRegister(int i) {

		if (i == 0)
			connectPC();
		else if (i == 1)
			connectSP();
		else {
			int iname = i;
			i -= 2;
			// firstAND
			StaticInitializer.allSignals.get("wrGPR").connect(first[i].getPortIN(0)); //
			StaticInitializer.allSignals.get("GPRADDR1_" + iname).connect(first[i].getPortIN(1));
			// firstAND

			// secondAND
			StaticInitializer.allSignals.get("GPRADDR1_" + iname).connect(second[i].getPortIN(0));
			StaticInitializer.allSignals.get("rdGPR1").connect(second[i].getPortIN(1));
			// secondAND

			// thirdAND
			StaticInitializer.allSignals.get("GPRADDR2_" + iname).connect(third[i].getPortIN(0));
			StaticInitializer.allSignals.get("rdGPR2").connect(third[i].getPortIN(1));
			// thirdAND

			// sysRegisters
			StaticInitializer.allSignals.get("CLK").connect(sysRegisters[i].getPortCLK());
			first[i].getPortOUT().connect(sysRegisters[i].getPortLD());
			StaticInitializer.allSignals.get("IBUS3").connect(sysRegisters[i].getPortIN());
			// sysRegisters

			// IBUS1
			second[i].getPortOUT().connect(IBUS1exit[i].getPortC());
			sysRegisters[i].getPortOUT().connect(IBUS1exit[i].getPortIN());
			IBUS1exit[i].getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));
			// IBUS1

			// IBUS2
			third[i].getPortOUT().connect(IBUS2exit[i].getPortC());
			sysRegisters[i].getPortOUT().connect(IBUS2exit[i].getPortIN());
			IBUS2exit[i].getPortOUT().connect(StaticInitializer.allSignals.get("IBUS2"));
			// IBUS2
		}
	}

	private void connectPC() {
		StaticInitializer.allSignals.get("wrGPR").connect(PCand1.getPortIN(0));
		StaticInitializer.allSignals.get("GPRADDR1_0").connect(PCand1.getPortIN(1));

		StaticInitializer.allSignals.get("GPRADDR1_0").connect(PCand2.getPortIN(0));
		StaticInitializer.allSignals.get("rdGPR1").connect(PCand2.getPortIN(1));
		
		StaticInitializer.allSignals.get("GPRADDR2_0").connect(PCand3.getPortIN(0));
		StaticInitializer.allSignals.get("rdGPR2").connect(PCand3.getPortIN(1));
		
		PCand1.getPortOUT().connect(pcload.getPortIN(0));
		StaticInitializer.allSignals.get("ldPC").connect(pcload.getPortIN(1));
		
		PCand2.getPortOUT().connect(pcspout[0].getPortIN(0));
		StaticInitializer.allSignals.get("PCout1").connect(pcspout[0].getPortIN(1));
		
		PCand3.getPortOUT().connect(pcout2.getPortIN(0));
		StaticInitializer.allSignals.get("PCout2").connect(pcout2.getPortIN(1));
		
		pcout2.getPortOUT().connect(PCexit2.getPortC());
		PC.getPortOUT().connect(PCexit2.getPortIN());
		PCexit2.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS2"));

		pcspout[0].getPortOUT().connect(PCexit1.getPortC());
		PC.getPortOUT().connect(PCexit1.getPortIN());
		PCexit1.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));

		StaticInitializer.allSignals.get("CLK").connect(PC.getPortCLK());
		StaticInitializer.allSignals.get("incPC").connect(PC.getPortINC());
		StaticInitializer.allSignals.get("IBUS3").connect(PC.getPortIN());
		pcload.getPortOUT().connect(PC.getPortLD());
	}

	private void connectSP() {
		StaticInitializer.allSignals.get("wrGPR").connect(SPand1.getPortIN(0));
		StaticInitializer.allSignals.get("GPRADDR1_1").connect(SPand1.getPortIN(1));

		StaticInitializer.allSignals.get("GPRADDR1_1").connect(SPand2.getPortIN(0));
		StaticInitializer.allSignals.get("rdGPR1").connect(SPand2.getPortIN(1));
		
		StaticInitializer.allSignals.get("GPRADDR2_1").connect(SPand3.getPortIN(0));
		StaticInitializer.allSignals.get("rdGPR2").connect(SPand3.getPortIN(1));
		
		SPand2.getPortOUT().connect(pcspout[1].getPortIN(0));
		StaticInitializer.allSignals.get("SPout1").connect(pcspout[1].getPortIN(1));
		
		SPand3.getPortOUT().connect(SPexit2.getPortC());
		pcspout[1].getPortOUT().connect(SPexit1.getPortC());
		
		SP.getPortOUT().connect(SPexit2.getPortIN());
		SP.getPortOUT().connect(SPexit1.getPortIN());

		SPexit2.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS2"));
		SPexit1.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));

		StaticInitializer.allSignals.get("CLK").connect(SP.getPortCLK());
		StaticInitializer.allSignals.get("incSP").connect(SP.getPortINC());
		StaticInitializer.allSignals.get("decSP").connect(SP.getPortDEC());
		StaticInitializer.allSignals.get("IBUS3").connect(SP.getPortIN());
		StaticInitializer.allSignals.get("CLK").connect(SP.getPortCLK());
		SPand1.getPortOUT().connect(SP.getPortLD());
	}

	@Override
	public void initSignals() {
		for (int i = 0; i < 16; i++) {
			initSingleRegister(i);
			if (i > 1)
				StaticInitializer.allComponents.put("R" + i, sysRegisters[i - 2]);
		}
		StaticInitializer.allComponents.put("PC", PC);
		StaticInitializer.allComponents.put("SP", SP);
		StaticInitializer.allComponents.put("R0", PC);
		StaticInitializer.allComponents.put("R1", SP);
		StaticInitializer.allComponents.put(PCand1.getName(), PCand1);
		StaticInitializer.allComponents.put(PCand2.getName(), PCand2);
		StaticInitializer.allComponents.put(PCand3.getName(), PCand3);
		StaticInitializer.allComponents.put(SPand1.getName(), SPand1);
		StaticInitializer.allComponents.put(SPand2.getName(), SPand2);
		StaticInitializer.allComponents.put(SPand3.getName(), SPand3);
		StaticInitializer.allComponents.put(PCexit1.getName(), PCexit1);
		StaticInitializer.allComponents.put(PCexit2.getName(), PCexit2);
		StaticInitializer.allComponents.put(SPexit1.getName(), SPexit1);
		StaticInitializer.allComponents.put(SPexit2.getName(), SPexit2);
		for (int i = 0; i < 14; i++) {
			StaticInitializer.allComponents.put(first[i].getName(), first[i]);
			StaticInitializer.allComponents.put(second[i].getName(), second[i]);
			StaticInitializer.allComponents.put(third[i].getName(), third[i]);
			StaticInitializer.allComponents.put(IBUS1exit[i].getName(), IBUS1exit[i]);
			StaticInitializer.allComponents.put(IBUS2exit[i].getName(), IBUS2exit[i]);
		}
		for (int i = 0; i < 1; i++)
			StaticInitializer.allComponents.put(pcspout[i].getName(), pcspout[i]);

		StaticInitializer.allSignals.put("R" + 0, PC.getPortOUT());
		StaticInitializer.allSignals.put("R" + 1, SP.getPortOUT());
		for (int i = 0; i < 14; i++)
			StaticInitializer.allSignals.put("R" + (i + 2), sysRegisters[i].getPortOUT());
		StaticInitializer.allSignals.put("PC", PC.getPortOUT());
		StaticInitializer.allSignals.put("SP", SP.getPortOUT());
		for (int i = 0; i < 14; i++) {
			StaticInitializer.allSignals.put(first[i].getName(), first[i].getPortOUT());
			StaticInitializer.allSignals.put(second[i].getName(), second[i].getPortOUT());
			StaticInitializer.allSignals.put(third[i].getName(), third[i].getPortOUT());
		}
		StaticInitializer.allSignals.put(pcload.getName(), pcload.getPortOUT());
		StaticInitializer.allSignals.put(pcspout[0].getName(), pcspout[0].getPortOUT());
		StaticInitializer.allSignals.put(pcout2.getName(), pcout2.getPortOUT());
		StaticInitializer.allSignals.put(pcspout[1].getName(), pcspout[1].getPortOUT());
		StaticInitializer.allSignals.put(PCand1.getName(), PCand1.getPortOUT());
		StaticInitializer.allSignals.put(PCand2.getName(), PCand2.getPortOUT());
		StaticInitializer.allSignals.put(PCand3.getName(), PCand3.getPortOUT());
		StaticInitializer.allSignals.put(SPand1.getName(), SPand1.getPortOUT());
		StaticInitializer.allSignals.put(SPand2.getName(), SPand2.getPortOUT());
		StaticInitializer.allSignals.put(SPand3.getName(), SPand3.getPortOUT());

	}

	@Override
	public void connectSignals() {
		for (int i = 0; i < 16; i++)
			connectSingleRegister(i);
	}
}
