package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.Coder;
import simulator2.digitalcomponents.Comparator;

public final class INT2 implements StaticInitializable {

	// a izlaz?

	And INT2And = new And();
	Coder CD1 = new Coder();
	Comparator CMP1 = new Comparator();

	@Override
	public void initSignals() {
		String[] myData = { "INT2And", "2", "1" };
		INT2And.init(myData);

		myData[0] = "CD1";
		myData[1] = "3"; // trebalo bi valjda numOutputs, ne numInputs (logika
							// OK, leksika ne)
		CD1.init(myData); // sad mi pada na pamet da mo�da mo�e ime refleksijom
							// iz imena fajla?

		myData[0] = "CMP1";
		myData[1] = "3";
		CMP1.init(myData);

		StaticInitializer.allComponents.put(CD1.getName(), CD1);
		StaticInitializer.allComponents.put(CMP1.getName(), CMP1);
		StaticInitializer.allComponents.put(INT2And.getName(), INT2And);

		for (int i = 0; i < 3; i++)
			StaticInitializer.allSignals.put("prl" + i, CD1.getPortOUT(i));
		StaticInitializer.allSignals.put("CMP1", CMP1.getPortGR());
		StaticInitializer.allSignals.put("int", INT2And.getPortOUT());
	}

	@Override
	public void connectSignals() {
		StaticInitializer.allSignals.get("0").connect(CD1.getPortIN(0));
		for (int i = 1; i < 8; i++)
			StaticInitializer.allSignals.get("PRINTR" + i).connect(CD1.getPortIN(i));
		for (int i = 0; i < 3; i++)
			CD1.getPortOUT(i).connect(CMP1.getPortA(i));
		for (int i = 0; i < 3; i++)
			StaticInitializer.allSignals.get("PSWL" + i).connect(CMP1.getPortB(i));

		StaticInitializer.allSignals.get("PSWI").connect(INT2And.getPortIN(0));
		CMP1.getPortGR().connect(INT2And.getPortIN(1));
	}
}
