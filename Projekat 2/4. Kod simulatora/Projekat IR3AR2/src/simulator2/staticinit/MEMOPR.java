package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.Incrementer;
import simulator2.digitalcomponents.IncrementerSimple;
import simulator2.digitalcomponents.MemoryModule;
import simulator2.digitalcomponents.Multiplexer;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.digitalcomponents.TriStateBufferNegated;

public final class MEMOPR implements StaticInitializable {

	Incrementer inc = new IncrementerSimple();
	Multiplexer MX6 = new Multiplexer(), MX7 = new Multiplexer(), MX8 = new Multiplexer();
	Not MONrd = new Not(), MONwr = new Not();
	And MOArd = new And(), MOAwr = new And();
	And MOAmx = new And();
	Or MOOmx = new Or();
	MemoryModule M0 = new MemoryModule(), M1 = new MemoryModule(), M2 = new MemoryModule(), M3 = new MemoryModule();
	TriStateBuffer MO3buf = new TriStateBuffer();
	TriStateBufferNegated MOfc3buf = new TriStateBufferNegated();
	SignalMerger MOmemout = new SignalMerger();

	@Override
	public void initSignals() {
		String myData[] = { "M0", "30", "1"};
		M0.init(myData);
		myData[0] = "M1";
		M1.init(myData);
		myData[0] = "M2";
		M2.init(myData);
		myData[0] = "M3";
		M3.init(myData);

		myData[0] = "INC";
		myData[1] = "30";
		myData[2] = "1";
		inc.init(myData);

		myData[1] = "30";
		myData[2] = "1";
		myData[0] = "MX6";
		MX6.init(myData);
		myData[0] = "MX7";
		MX7.init(myData);
		myData[0] = "MX8";
		MX8.init(myData);

		myData[0] = "MO3buf";
		myData[1] = "32";
		MO3buf.init(myData);
		myData[0] = "MOfc3buf";
		myData[1] = "1";
		MOfc3buf.init(myData);

		myData[1] = "1";
		myData[0] = "MONrd";
		MONrd.init(myData);
		myData[0] = "MONwr";
		MONwr.init(myData);
		
		myData[1] = "2";
		myData[2] = "1";
		
		myData[0] = "MOArd";
		MOArd.init(myData);
		myData[0] = "MOAwr";
		MOAwr.init(myData);
		myData[0] = "MOAmx";
		MOAmx.init(myData);
		myData[0] = "MOOmx";
		MOOmx.init(myData);

		myData[0] = "MOmemout";
		myData[1] = "8";
		myData[2] = "4";
		MOmemout.init(myData);

		StaticInitializer.allComponents.put(inc.getName(), inc);
		StaticInitializer.allComponents.put(MX6.getName(), MX6);
		StaticInitializer.allComponents.put(MX7.getName(), MX7);
		StaticInitializer.allComponents.put(MX8.getName(), MX8);
		StaticInitializer.allComponents.put(M0.getName(), M0);
		StaticInitializer.allComponents.put(M1.getName(), M1);
		StaticInitializer.allComponents.put(M2.getName(), M2);
		StaticInitializer.allComponents.put(M3.getName(), M3);
		StaticInitializer.allComponents.put(MONrd.getName(), MONrd);
		StaticInitializer.allComponents.put(MONwr.getName(), MONwr);
		StaticInitializer.allComponents.put(MOArd.getName(), MOArd);
		StaticInitializer.allComponents.put(MOAwr.getName(), MOAwr);
		StaticInitializer.allComponents.put(MOAmx.getName(), MOAmx);
		StaticInitializer.allComponents.put(MOOmx.getName(), MOOmx);
		StaticInitializer.allComponents.put(MO3buf.getName(), MO3buf);
		StaticInitializer.allComponents.put(MOfc3buf.getName(), MOfc3buf);
		StaticInitializer.allComponents.put(MOmemout.getName(), MOmemout);

		StaticInitializer.allSignals.put("RD", MOArd.getPortOUT());
		StaticInitializer.allSignals.put("WR", MOAwr.getPortOUT());
		StaticInitializer.allSignals.put(MOmemout.getName(), MOmemout.getPortOUT());
		StaticInitializer.allSignals.put("MX6", MX6.getPortOUT());
		StaticInitializer.allSignals.put("MX7", MX7.getPortOUT());
		StaticInitializer.allSignals.put("MX8", MX8.getPortOUT());
		StaticInitializer.allSignals.put("M0", M0.getPortDO());
		StaticInitializer.allSignals.put("M1", M1.getPortDO());
		StaticInitializer.allSignals.put("M2", M2.getPortDO());
		StaticInitializer.allSignals.put("M3", M3.getPortDO());
		StaticInitializer.allSignals.put("INC", inc.getPortOUT());

		// za iscrtavanje
		StaticInitializer.allSignals.put("DO0", M0.getPortDO());
		StaticInitializer.allSignals.put("DO1", M1.getPortDO());
		StaticInitializer.allSignals.put("DO2", M2.getPortDO());
		StaticInitializer.allSignals.put("DO3", M3.getPortDO());
		StaticInitializer.allSignals.put("DO", MOmemout.getPortOUT());
		StaticInitializer.allSignals.put("MEM3bufout", MO3buf.getPortOUT());
		StaticInitializer.allSignals.put("ABUSinc", inc.getPortOUT());
		StaticInitializer.allSignals.put("MI0OR", MOOmx.getPortOUT());
		StaticInitializer.allSignals.put("MI2AND", MOAmx.getPortOUT());
		StaticInitializer.allSignals.put("RDBUSINV", MONrd.getPortOUT());
		StaticInitializer.allSignals.put("WRBUSINV", MONwr.getPortOUT());
		
		MOArd.invertPort(1);
		MOAwr.invertPort(1);
	}

	@Override
	public void connectSignals() {
		
		StaticInitializer.allSignals.get("ABUS31..2").connect(inc.getPortIN());

		StaticInitializer.allSignals.get("ABUS1").connect(MOOmx.getPortIN(0));
		StaticInitializer.allSignals.get("ABUS0").connect(MOOmx.getPortIN(1));
		MOOmx.getPortOUT().connect(MX6.getPortS(0));

		StaticInitializer.allSignals.get("ABUS1").connect(MOAmx.getPortIN(0));
		StaticInitializer.allSignals.get("ABUS0").connect(MOAmx.getPortIN(1));
		MOAmx.getPortOUT().connect(MX8.getPortS(0));

		StaticInitializer.allSignals.get("ABUS31..2").connect(MX6.getPortIN(0));
		inc.getPortOUT().connect(MX6.getPortIN(1));
		MX6.getPortOUT().connect(M0.getPortADDR());

		StaticInitializer.allSignals.get("ABUS1").connect(MX7.getPortS(0));
		StaticInitializer.allSignals.get("ABUS31..2").connect(MX7.getPortIN(0));
		inc.getPortOUT().connect(MX7.getPortIN(1));
		MX7.getPortOUT().connect(M1.getPortADDR());

		StaticInitializer.allSignals.get("ABUS31..2").connect(MX8.getPortIN(0));
		inc.getPortOUT().connect(MX8.getPortIN(1));
		MX8.getPortOUT().connect(M2.getPortADDR());

		StaticInitializer.allSignals.get("DBUS31..24").connect(M0.getPortDI());
		M0.getPortDO().connect(MOmemout.getPortIN(3));
		MX6.getPortOUT().connect(M0.getPortADDR());
		StaticInitializer.allSignals.get("RD").connect(M0.getPortRD());
		StaticInitializer.allSignals.get("WR").connect(M0.getPortWR());

		StaticInitializer.allSignals.get("DBUS23..16").connect(M1.getPortDI());
		M1.getPortDO().connect(MOmemout.getPortIN(2));
		MX7.getPortOUT().connect(M1.getPortADDR());
		StaticInitializer.allSignals.get("RD").connect(M1.getPortRD());
		StaticInitializer.allSignals.get("WR").connect(M1.getPortWR());

		StaticInitializer.allSignals.get("DBUS15..8").connect(M2.getPortDI());
		M2.getPortDO().connect(MOmemout.getPortIN(1));
		MX8.getPortOUT().connect(M2.getPortADDR());
		StaticInitializer.allSignals.get("RD").connect(M2.getPortRD());
		StaticInitializer.allSignals.get("WR").connect(M2.getPortWR());

		StaticInitializer.allSignals.get("DBUS7..0").connect(M3.getPortDI());
		M3.getPortDO().connect(MOmemout.getPortIN(0));
		StaticInitializer.allSignals.get("ABUS31..2").connect(M3.getPortADDR());
		StaticInitializer.allSignals.get("RD").connect(M3.getPortRD());
		StaticInitializer.allSignals.get("WR").connect(M3.getPortWR());

		MOmemout.getPortOUT().connect(MO3buf.getPortIN());
		StaticInitializer.allSignals.get("MEMout").connect(MO3buf.getPortC());
		MO3buf.getPortOUT().connect(StaticInitializer.allSignals.get("DBUS"));

		StaticInitializer.allSignals.get("!RDBUS").connect(MONrd.getPortIN());
		MONrd.getPortOUT().connect(MOArd.getPortIN(0));
		StaticInitializer.allSignals.get("!WRBUS").connect(MONwr.getPortIN());
		MONwr.getPortOUT().connect(MOAwr.getPortIN(0));
		StaticInitializer.allSignals.get("M/IO").connect(MOArd.getPort(1));
		StaticInitializer.allSignals.get("M/IO").connect(MOAwr.getPort(1));

		StaticInitializer.allSignals.get("fcMEM").connect(MOfc3buf.getPortC());
		StaticInitializer.allSignals.get("1").connect(MOfc3buf.getPortIN());
		MOfc3buf.getPortOUT().connect(StaticInitializer.allSignals.get("!FCBUS"));
		
	}
}
