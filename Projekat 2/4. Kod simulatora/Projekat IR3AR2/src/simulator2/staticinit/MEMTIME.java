package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.ComparatorWideE;
import simulator2.digitalcomponents.IncDecRegister;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SimpleRegister;

public final class MEMTIME implements StaticInitializable {

	Or MTrdwr = new Or();
	And MTinc = new And(), MTmout = new And();
	SimpleRegister TIME = new SimpleRegister();
	IncDecRegister MEMACC = new IncDecRegister();
	ComparatorWideE CMP = new ComparatorWideE();
	Not tmpNot = new Not();

	@Override
	public void initSignals() {
		String notData[] = {"tmpNot", "1"};
		tmpNot.init(notData);
		
		String myData[] = { "MTrdwr", "2", "1" };
		MTrdwr.init(myData);

		myData[0] = "MTinc";
		MTinc.init(myData);

		myData[0] = "MTmout";
		MTmout.init(myData);

		myData[0] = "CMP";
		myData[1] = "8";
		CMP.init(myData);

		myData[0] = "TIME";
		myData[1] = "8";
		TIME.init(myData);

		myData[0] = "MEMACC";
		MEMACC.init(myData);

		StaticInitializer.allSignals.put("opr", MTrdwr.getPortOUT());
		StaticInitializer.allSignals.put("MEMout", MTmout.getPortOUT());
		StaticInitializer.allSignals.put("fcMEM", CMP.getPortEQ());
		StaticInitializer.allSignals.put("MEMACC_INC_AND", MTinc.getPortOUT());

		StaticInitializer.allSignals.put("MEMACC", MEMACC.getPortOUT());
		StaticInitializer.allSignals.put("TIME", TIME.getPortOUT());
		StaticInitializer.allSignals.put("CMP", CMP.getPortEQ());

		StaticInitializer.allComponents.put("MEMACC", MEMACC);
		StaticInitializer.allComponents.put("TIME", TIME);
		StaticInitializer.allComponents.put("CMP", CMP);
		StaticInitializer.allComponents.put(tmpNot.getName(), tmpNot);
		StaticInitializer.allComponents.put(MTrdwr.getName(), MTrdwr);
		StaticInitializer.allComponents.put(MTinc.getName(), MTinc);
		StaticInitializer.allComponents.put(MTmout.getName(), MTmout);

	}

	@Override
	public void connectSignals() {
		
		StaticInitializer.allSignals.get("fcMEM").connect(tmpNot.getPortIN());
		
		StaticInitializer.allSignals.get("RD").connect(MTrdwr.getPortIN(0));
		StaticInitializer.allSignals.get("WR").connect(MTrdwr.getPortIN(1));
		MTrdwr.getPortOUT().connect(MTinc.getPortIN(0));

		tmpNot.getPortOUT().connect(MTinc.getPortIN(1));
		MTinc.getPortOUT().connect(MEMACC.getPortINC());

		StaticInitializer.allSignals.get("CLK").connect(MEMACC.getPortCLK());
		StaticInitializer.allSignals.get("fcMEM").connect(MEMACC.getPortCL());
		MEMACC.getPortOUT().connect(CMP.getPortA());

		StaticInitializer.allSignals.get("CLK").connect(TIME.getPortCLK());
		TIME.getPortOUT().connect(CMP.getPortB());

		StaticInitializer.allSignals.get("RD").connect(MTmout.getPortIN(0));
		StaticInitializer.allSignals.get("fcMEM").connect(MTmout.getPortIN(1));
		StaticInitializer.allSignals.get("opr").connect(CMP.getPortE());
	}
}
