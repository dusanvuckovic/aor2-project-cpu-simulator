package simulator2.staticinit;

import simulator2.Signal;
import simulator2.SignalSimple;
import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.Multiplexer;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.digitalcomponents.Xor;

public final class EXEC4 implements StaticInitializable {

	Signal jl = new SignalSimple(), jge = new SignalSimple(), jle = new SignalSimple(), jg = new SignalSimple(),
			jo = new SignalSimple(), jno = new SignalSimple(), je = new SignalSimple(), jne = new SignalSimple(),
			jp = new SignalSimple(), jnp = new SignalSimple(), brjmp = new SignalSimple();
	Xor nv = new Xor();
	And jump = new And();
	Not n1 = new Not(), n2 = new Not(), n3 = new Not(), n4 = new Not(), n5 = new Not();
	Or z = new Or(), anyJumpCondition = new Or();
	Multiplexer MX4 = new Multiplexer();
	TriStateBuffer jmpregind = new TriStateBuffer();

	private String[] bigNames = { "JE", "JNE", "JGE", "JG", "JLE", "JL", "JP", "JNP", "JO", "JNO" };

	@Override
	public void initSignals() {

		String myData[] = { "nv", "2", "1" };
		nv.init(myData);

		myData[0] = "z";
		z.init(myData);
		myData[0] = "n1";
		myData[1] = "1";
		n1.init(myData);
		myData[0] = "n2";
		n2.init(myData);
		myData[0] = "n3";
		n3.init(myData);
		myData[0] = "n4";
		n4.init(myData);
		myData[0] = "n5";
		n5.init(myData);
		
		myData[1] = "2";

		myData[0] = "jump";
		jump.init(myData);
		myData[0] = "anyJumpCondition";
		myData[1] = "10";
		anyJumpCondition.init(myData);

		myData[0] = "MX4";
		myData[1] = "1";
		myData[2] = "4";
		MX4.init(myData);

		myData[0] = "JMPREGINDbuf";
		myData[1] = "1";
		jmpregind.init(myData);

		StaticInitializer.allComponents.put(nv.getName(), nv);
		StaticInitializer.allComponents.put(jump.getName(), jump);
		StaticInitializer.allComponents.put(n1.getName(), n1);
		StaticInitializer.allComponents.put(n2.getName(), n2);
		StaticInitializer.allComponents.put(n3.getName(), n3);
		StaticInitializer.allComponents.put(n4.getName(), n4);
		StaticInitializer.allComponents.put(n5.getName(), n5);
		StaticInitializer.allComponents.put(z.getName(), z);
		StaticInitializer.allComponents.put(anyJumpCondition.getName(), anyJumpCondition);
		StaticInitializer.allComponents.put(MX4.getName(), MX4);
		StaticInitializer.allComponents.put(jmpregind.getName(), jmpregind);

		StaticInitializer.allSignals.put("jmpOR", anyJumpCondition.getPortOUT());
		StaticInitializer.allSignals.put("JMPREGIND", jmpregind.getPortOUT());
		StaticInitializer.allSignals.put("brjmp", jump.getPortOUT());
		StaticInitializer.allSignals.put("MX4", MX4.getPortOUT());

		StaticInitializer.allSignals.put("jl", jl);
		StaticInitializer.allSignals.put("jge", jge);
		StaticInitializer.allSignals.put("jle", jle);
		StaticInitializer.allSignals.put("jg", jg);
		StaticInitializer.allSignals.put("jo", jo);
		StaticInitializer.allSignals.put("jno", jno);
		StaticInitializer.allSignals.put("je", je);
		StaticInitializer.allSignals.put("jne", jne);
		StaticInitializer.allSignals.put("jp", jp);
		StaticInitializer.allSignals.put("jnp", jnp);
	}

	@Override
	public void connectSignals() {

		StaticInitializer.allSignals.get("IR25").connect(jmpregind.getPortIN());
		StaticInitializer.allSignals.get("1").connect(jmpregind.getPortC());

		StaticInitializer.allSignals.get("PSWN").connect(nv.getPortIN(0));
		StaticInitializer.allSignals.get("PSWV").connect(nv.getPortIN(1));

		nv.getPortOUT().connect(jl);

		jl.connect(n1.getPortIN());
		n1.getPortOUT().connect(jge);

		jl.connect(z.getPortIN(0));
		StaticInitializer.allSignals.get("PSWZ").connect(z.getPortIN(1));
		z.getPortOUT().connect(jle);

		jle.connect(n2.getPortIN());
		n2.getPortOUT().connect(jg);

		jo = StaticInitializer.allSignals.get("PSWV");
		jo.connect(n3.getPortIN(0));
		n3.getPortOUT().connect(jno);

		je = StaticInitializer.allSignals.get("PSWZ");
		je.connect(n4.getPortIN(0));
		n4.getPortOUT().connect(jne);

		jp = StaticInitializer.allSignals.get("PSWN");
		jp.connect(n5.getPortIN(0));
		n5.getPortOUT().connect(jnp);

		StaticInitializer.allSignals.get("IR26").connect(MX4.getPortS(0));
		StaticInitializer.allSignals.get("IR27").connect(MX4.getPortS(1));
		StaticInitializer.allSignals.get("IR28").connect(MX4.getPortS(2));
		StaticInitializer.allSignals.get("IR29").connect(MX4.getPortS(3));
		jl.connect(MX4.getPortIN(0));
		jp.connect(MX4.getPortIN(1));
		jnp.connect(MX4.getPortIN(2));
		jo.connect(MX4.getPortIN(3));
		jno.connect(MX4.getPortIN(4));
		je.connect(MX4.getPortIN(11));
		jne.connect(MX4.getPortIN(12));
		jge.connect(MX4.getPortIN(13));
		jg.connect(MX4.getPortIN(14));
		jl.connect(MX4.getPortIN(15));

		for (int i = 0; i < bigNames.length; i++) {
			StaticInitializer.allSignals.get(bigNames[i]).connect(anyJumpCondition.getPortIN(i));
		}

		anyJumpCondition.getPortOUT().connect(jump.getPortIN(0));
		MX4.getPortOUT().connect(jump.getPortIN(1));
	}
}