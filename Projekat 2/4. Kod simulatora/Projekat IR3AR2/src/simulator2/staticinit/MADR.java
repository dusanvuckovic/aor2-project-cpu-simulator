package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.Multiplexer;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.SimpleRegister;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.digitalcomponents.TriStateBufferNegated;

public class MADR implements StaticInitializable {
	SimpleRegister MAR = new SimpleRegister(), MDR = new SimpleRegister();
	Multiplexer MX5 = new Multiplexer();
	TriStateBuffer MARA3buf = new TriStateBuffer(), MAR3buf = new TriStateBuffer(), MDRD3buf = new TriStateBuffer(),
			MDR3buf = new TriStateBuffer();
	TriStateBufferNegated RD3buf = new TriStateBufferNegated(), WR3buf = new TriStateBufferNegated();
	SignalMerger MDRexec1helper = new SignalMerger();
	Not MADRNot = new Not(), fcNot = new Not();
	And MADRAnd = new And();
	Or MADROr = new Or();
	TriStateBuffer BRG1_2 = new TriStateBuffer(), BRG2_3 = new TriStateBuffer(), NLLBUS1 = new TriStateBuffer(),
			BRG1_3 = new TriStateBuffer();

	@Override
	public void initSignals() {
		String[] myData = { "MAR", "32", "2" };
		MAR.init(myData);

		myData[0] = "MDR";
		MDR.init(myData);

		myData[0] = "MX5";
		MX5.init(myData);

		myData[2] = "1";

		myData[0] = "MARA3buf";
		MARA3buf.init(myData);

		myData[0] = "MAR3buf";
		MAR3buf.init(myData);

		myData[0] = "MDRD3buf";
		MDRD3buf.init(myData);

		myData[0] = "MDR3buf";
		MDR3buf.init(myData);

		myData[1] = "32";

		myData[0] = "NLLBUS1";
		NLLBUS1.init(myData);
		myData[0] = "BRG1_2";
		BRG1_2.init(myData);
		myData[0] = "BRG2_3";
		BRG2_3.init(myData);
		myData[0] = "BRG1_3";
		BRG1_3.init(myData);

		MAR.getPortOUT().connect(GLOBALINIT.MARsplit.getPortIN());
		MDR.getPortOUT().connect(GLOBALINIT.MDRsplit.getPortIN());


		myData[0] = "RD3buf";
		RD3buf.init(myData);

		myData[0] = "WR3buf";
		WR3buf.init(myData);

		myData[0] = "MADRNot";
		myData[1] = "1";
		MADRNot.init(myData);

		myData[0] = "fcNot";
		fcNot.init(myData);

		myData[0] = "MADRAnd";
		myData[1] = "2";
		MADRAnd.init(myData);

		myData[0] = "MADROr";
		MADROr.init(myData);

		myData[0] = "MDRexec1helper";
		myData[1] = "1";
		myData[2] = "5";
		MDRexec1helper.init(myData);

		GLOBALINIT.MARsplit.getPortIN().connect(MAR.getPortOUT());
		GLOBALINIT.MDRsplit.getPortIN().connect(MDR.getPortOUT());

		StaticInitializer.allComponents.put(MAR.getName(), MAR);
		StaticInitializer.allComponents.put(MDR.getName(), MDR);
		StaticInitializer.allComponents.put(MX5.getName(), MX5);
		StaticInitializer.allComponents.put(MARA3buf.getName(), MARA3buf);
		StaticInitializer.allComponents.put(MAR3buf.getName(), MAR3buf);
		StaticInitializer.allComponents.put(MDRD3buf.getName(), MDRD3buf);
		StaticInitializer.allComponents.put(MDR3buf.getName(), MDR3buf);
		StaticInitializer.allComponents.put(RD3buf.getName(), RD3buf);
		StaticInitializer.allComponents.put(WR3buf.getName(), WR3buf);
		StaticInitializer.allComponents.put(MDRexec1helper.getName(), MDRexec1helper);
		StaticInitializer.allComponents.put(MADRNot.getName(), MADRNot);
		StaticInitializer.allComponents.put(fcNot.getName(), fcNot);
		StaticInitializer.allComponents.put(MADRAnd.getName(), MADRAnd);
		StaticInitializer.allComponents.put(MADROr.getName(), MADROr);
		StaticInitializer.allComponents.put(BRG1_2.getName(), BRG1_2);
		StaticInitializer.allComponents.put(BRG2_3.getName(), BRG2_3);
		StaticInitializer.allComponents.put(NLLBUS1.getName(), NLLBUS1);
		StaticInitializer.allComponents.put(BRG1_3.getName(), BRG1_3);

		StaticInitializer.allSignals.put("fcCPU", fcNot.getPortOUT());
		StaticInitializer.allSignals.put("MAR", MAR.getPortOUT());
		StaticInitializer.allSignals.put("MDR", MDR.getPortOUT());
		StaticInitializer.allSignals.put("MX5", MX5.getPortOUT());

		StaticInitializer.allSignals.put("ldMDR_AND", MADRAnd.getPortOUT());
		StaticInitializer.allSignals.put("ldMDR_OR", MADROr.getPortOUT());
		StaticInitializer.allSignals.put("MAR3bufout", MAR3buf.getPortOUT());
		StaticInitializer.allSignals.put("MARA3bufout", MARA3buf.getPortOUT());
		StaticInitializer.allSignals.put("MDR3bufout", MDR3buf.getPortOUT());
		StaticInitializer.allSignals.put("MDRD3bufout", MDRD3buf.getPortOUT());

		StaticInitializer.allSignals.put("BRG1_2", BRG1_2.getPortOUT());
		StaticInitializer.allSignals.put("BRG2_3", BRG2_3.getPortOUT());
		StaticInitializer.allSignals.put("NLLBUS1", NLLBUS1.getPortOUT());

		StaticInitializer.allSignals.put("MDR17..13", MDRexec1helper.getPortOUT());
		StaticInitializer.allSignals.put("MDRoutput1713", MDRexec1helper.getPortOUT());

	}

	@Override
	public void connectSignals() {

		GLOBALINIT.MDRsplit.getPortOUT(21).connect(MDRexec1helper.getPortIN(0));
		GLOBALINIT.MDRsplit.getPortOUT(22).connect(MDRexec1helper.getPortIN(1));
		GLOBALINIT.MDRsplit.getPortOUT(23).connect(MDRexec1helper.getPortIN(2));
		GLOBALINIT.MDRsplit.getPortOUT(8).connect(MDRexec1helper.getPortIN(3));
		GLOBALINIT.MDRsplit.getPortOUT(9).connect(MDRexec1helper.getPortIN(4));

		StaticInitializer.allSignals.get("CLK").connect(MAR.getPortCLK());
		StaticInitializer.allSignals.get("ldMAR").connect(MAR.getPortLD());
		StaticInitializer.allSignals.get("IBUS3").connect(MAR.getPortIN());
		StaticInitializer.allSignals.get("MARout").connect(MAR3buf.getPortC());
		StaticInitializer.allSignals.get("MARAout").connect(MARA3buf.getPortC());
		MAR.getPortOUT().connect(MAR3buf.getPortIN());
		MAR.getPortOUT().connect(MARA3buf.getPortIN());
		MAR3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));
		MARA3buf.getPortOUT().connect(StaticInitializer.allSignals.get("ABUS"));

		StaticInitializer.allSignals.get("mxMDR").connect(MX5.getPortS(0));
		StaticInitializer.allSignals.get("DBUS").connect(MX5.getPortIN(0));
		StaticInitializer.allSignals.get("IBUS2").connect(MX5.getPortIN(1));
		StaticInitializer.allSignals.get("rdCPU").connect(MADRAnd.getPortIN(0));
		StaticInitializer.allSignals.get("fcCPU").connect(MADRAnd.getPortIN(1));
		StaticInitializer.allSignals.get("ldMDR").connect(MADROr.getPortIN(0));
		MADRAnd.getPortOUT().connect(MADROr.getPortIN(1));
		StaticInitializer.allSignals.get("CLK").connect(MDR.getPortCLK());
		MADROr.getPortOUT().connect(MDR.getPortLD());
		MX5.getPortOUT().connect(MDR.getPortIN());

		StaticInitializer.allSignals.get("rdCPU").connect(RD3buf.getPortC());
		StaticInitializer.allSignals.get("wrCPU").connect(WR3buf.getPortC());
		StaticInitializer.allSignals.get("1").connect(RD3buf.getPortIN());
		StaticInitializer.allSignals.get("1").connect(WR3buf.getPortIN());
		RD3buf.getPortOUT().connect(StaticInitializer.allSignals.get("!RDBUS"));
		WR3buf.getPortOUT().connect(StaticInitializer.allSignals.get("!WRBUS"));
		StaticInitializer.allSignals.get("!FCBUS").connect(fcNot.getPortIN());

		StaticInitializer.allSignals.get("MDRout").connect(MDR3buf.getPortC());
		StaticInitializer.allSignals.get("MDRDout").connect(MDRD3buf.getPortC());
		MDR.getPortOUT().connect(MDR3buf.getPortIN());
		MDR.getPortOUT().connect(MDRD3buf.getPortIN());
		MDR3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));
		MDRD3buf.getPortOUT().connect(StaticInitializer.allSignals.get("DBUS"));

		StaticInitializer.allSignals.get("NULLBUS1").connect(NLLBUS1.getPortC());
		StaticInitializer.allSignals.get("0").connect(NLLBUS1.getPortIN());
		NLLBUS1.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));

		StaticInitializer.allSignals.get("BRG1_2").connect(BRG1_2.getPortC());
		StaticInitializer.allSignals.get("IBUS1").connect(BRG1_2.getPortIN());
		BRG1_2.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS2"));

		StaticInitializer.allSignals.get("BRG2_3").connect(BRG2_3.getPortC());
		StaticInitializer.allSignals.get("IBUS2").connect(BRG2_3.getPortIN());
		BRG2_3.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3"));

		StaticInitializer.allSignals.get("BRG1_3").connect(BRG1_3.getPortC());
		StaticInitializer.allSignals.get("IBUS1").connect(BRG1_3.getPortIN());
		BRG1_3.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3"));


	}
}
