package simulator2.staticinit;

import java.util.HashMap;

import simulator2.LogicComponent;
import simulator2.Signal;
import simulator2.digitalcomponents.Bus;

public final class StaticInitializer {
	public static HashMap<String, Signal> allSignals = new HashMap<String, Signal>();
	public static HashMap<String, Bus> allBuses = new HashMap<String, Bus>();
	public static HashMap<String, LogicComponent> allComponents = new HashMap<String, LogicComponent>();
}
