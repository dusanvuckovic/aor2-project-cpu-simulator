package simulator2.staticinit;

import simulator2.digitalcomponents.Decoder;
import simulator2.digitalcomponents.Multiplexer;
import simulator2.digitalcomponents.SignalSplitter;
import simulator2.digitalcomponents.SimpleRegister;

public final class ADDR1 implements StaticInitializable {

	Decoder DC0 = new Decoder(), DC1 = new Decoder();
	SimpleRegister GPRADDR1 = new SimpleRegister(), GPRADDR2 = new SimpleRegister(), GPRADDR3 = new SimpleRegister(),
			BADDR1 = new SimpleRegister(), BADDR2 = new SimpleRegister();
	Multiplexer MX0 = new Multiplexer(), MX1 = new Multiplexer(), MX2 = new Multiplexer(), MX3 = new Multiplexer();
	SignalSplitter ADDR1Splitter = new SignalSplitter(), ADDR2Splitter = new SignalSplitter();

	@Override
	public void initSignals() {

		String[] myData = { "DC0", "4" };
		DC0.init(myData);

		myData[0] = "DC1";
		DC1.init(myData);

		myData[0] = "GPRADDR1";
		GPRADDR1.init(myData);

		myData[0] = "GPRADDR2";
		GPRADDR2.init(myData);

		myData[0] = "GPRADDR3";
		GPRADDR3.init(myData);

		myData[0] = "BADDR1";
		BADDR1.init(myData);

		myData[0] = "BADDR2";
		BADDR2.init(myData);

		String myMPData[] = { "MX0", "4", "1" };
		MX0.init(myMPData);

		myMPData[0] = "MX1";
		MX1.init(myMPData);

		myMPData[0] = "MX2";
		MX2.init(myMPData);

		myMPData[0] = "MX3";
		MX3.init(myMPData);

		myMPData[0] = "ADDR1Splitter";
		myMPData[1] = "1";
		myMPData[2] = "4";
		ADDR1Splitter.init(myMPData);
		myMPData[0] = "ADDR2Splitter";
		ADDR2Splitter.init(myMPData);

		StaticInitializer.allComponents.put(DC0.getName(), DC0);
		StaticInitializer.allComponents.put(DC1.getName(), DC1);
		StaticInitializer.allComponents.put(GPRADDR1.getName(), GPRADDR1);
		StaticInitializer.allComponents.put(GPRADDR2.getName(), GPRADDR2);
		StaticInitializer.allComponents.put(GPRADDR3.getName(), GPRADDR3);
		StaticInitializer.allComponents.put(BADDR1.getName(), BADDR1);
		StaticInitializer.allComponents.put(BADDR2.getName(), BADDR2);
		StaticInitializer.allComponents.put(MX0.getName(), MX0);
		StaticInitializer.allComponents.put(MX1.getName(), MX1);
		StaticInitializer.allComponents.put(MX2.getName(), MX2);
		StaticInitializer.allComponents.put(MX3.getName(), MX3);
		StaticInitializer.allComponents.put(ADDR1Splitter.getName(), ADDR1Splitter);
		StaticInitializer.allComponents.put(ADDR2Splitter.getName(), ADDR2Splitter);

		StaticInitializer.allSignals.put("GPRADDR1", GPRADDR1.getPortOUT());
		StaticInitializer.allSignals.put("GPRADDR2", GPRADDR2.getPortOUT());
		StaticInitializer.allSignals.put("GPRADDR3", GPRADDR3.getPortOUT());
		for (int i = 0; i < 16; i++) {
			StaticInitializer.allSignals.put("GPRADDR1_" + i, DC0.getPortOUT(i));
			StaticInitializer.allSignals.put("GPRADDR2_" + i, DC1.getPortOUT(i));
		}
		for (int i = 0; i < 4; i++) {
			StaticInitializer.allSignals.put("ADDR1_" + i, ADDR1Splitter.getPortOUT(i));
			StaticInitializer.allSignals.put("ADDR2_" + i, ADDR2Splitter.getPortOUT(i));
		}

		StaticInitializer.allSignals.put("GPRADDR1", GPRADDR1.getPortOUT());
		StaticInitializer.allSignals.put("GPRADDR2", GPRADDR2.getPortOUT());
		StaticInitializer.allSignals.put("GPRADDR3", GPRADDR3.getPortOUT());
		StaticInitializer.allSignals.put("BADDR1", BADDR1.getPortOUT());
		StaticInitializer.allSignals.put("BADDR2", BADDR2.getPortOUT());
		StaticInitializer.allSignals.put("MX0", MX0.getPortOUT());
		StaticInitializer.allSignals.put("MX1", MX1.getPortOUT());
		StaticInitializer.allSignals.put("MX2", MX2.getPortOUT());
		StaticInitializer.allSignals.put("MX3", MX3.getPortOUT());
		StaticInitializer.allSignals.put("ADDR1", MX0.getPortOUT());
		StaticInitializer.allSignals.put("ADDR2", MX2.getPortOUT());

	}

	@Override
	public void connectSignals() {
		MX0.getPortOUT().connect(ADDR1Splitter.getPortIN()); // splits ADDRi
																	// to ADDRi0
																	// ...
																	// ADDRi3
		MX2.getPortOUT().connect(ADDR2Splitter.getPortIN());

		for (int i = 0; i < 4; i++) {
			ADDR1Splitter.getPortOUT(i).connect(DC0.getPortIN(i)); // connects ADDRij to DCs 0 and 1
			ADDR2Splitter.getPortOUT(i).connect(DC1.getPortIN(i));
		}

		// GPRADDR1
		StaticInitializer.allSignals.get("CLK").connect(GPRADDR1.getPortCLK());
		StaticInitializer.allSignals.get("ldGPRADDR1").connect(GPRADDR1.getPortLD());
		StaticInitializer.allSignals.get("IBUS125..22").connect(GPRADDR1.getPortIN());
		// GPRADDR1

		// GPRADDR2
		StaticInitializer.allSignals.get("CLK").connect(GPRADDR2.getPortCLK());
		StaticInitializer.allSignals.get("ldGPRADDR2").connect(GPRADDR2.getPortLD());
		StaticInitializer.allSignals.get("IBUS124..21").connect(GPRADDR2.getPortIN());
		// GPRADDR2

		// GPRADDR3
		StaticInitializer.allSignals.get("CLK").connect(GPRADDR3.getPortCLK());
		StaticInitializer.allSignals.get("ldGPRADDR3").connect(GPRADDR3.getPortLD());
		StaticInitializer.allSignals.get("IBUS121..18").connect(GPRADDR3.getPortIN());
		// GPRADDR3

		// BADDR1
		StaticInitializer.allSignals.get("CLK").connect(BADDR1.getPortCLK());
		StaticInitializer.allSignals.get("ldBADDR1").connect(BADDR1.getPortLD());
		StaticInitializer.allSignals.get("IBUS3").connect(BADDR1.getPortIN());
		// BADDR1

		// BADDR2
		StaticInitializer.allSignals.get("CLK").connect(BADDR2.getPortCLK());
		StaticInitializer.allSignals.get("ldBADDR2").connect(BADDR2.getPortLD());
		StaticInitializer.allSignals.get("IBUS2").connect(BADDR2.getPortIN());
		// BADDR2

		// MX3
		StaticInitializer.allSignals.get("mxADDR0").connect(MX3.getPortS(0));
		GPRADDR1.getPortOUT().connect(MX3.getPortIN(0));
		GPRADDR2.getPortOUT().connect(MX3.getPortIN(1));
		MX3.getPortOUT().connect(MX0.getPortIN(0));
		// MX3

		// MX0
		StaticInitializer.allSignals.get("mxADDR2").connect(MX0.getPortS(0));
		MX3.getPortOUT().connect(MX0.getPortIN(0));
		BADDR1.getPortOUT().connect(MX0.getPortIN(1)); // MX0[1] <= BADDR1output
		// MX0

		// MX1
		StaticInitializer.allSignals.get("mxADDR1").connect(MX1.getPortS(0));
		GPRADDR3.getPortOUT().connect(MX1.getPortIN(0)); // MX1[0] <=
															// GPRADDR3output
		GPRADDR2.getPortOUT().connect(MX1.getPortIN(1)); // MX1[1] <=
															// GPRADDR2output
		// MX1

		// MX2
		StaticInitializer.allSignals.get("mxADDR3").connect(MX2.getPortS(0));
		MX1.getPortOUT().connect(MX2.getPortIN(0)); // MX2[0] <= MX1output
		BADDR2.getPortOUT().connect(MX2.getPortIN(1)); // MX2[1] <= BADDR2output
		// MX2
	}
}
