package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.RSFlipFlop;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.TriStateBuffer;

public final class EXEC2 implements StaticInitializable {

	RSFlipFlop start = new RSFlipFlop();

	RSFlipFlop nzcvprl[] = new RSFlipFlop[7];
	And[] firstA = new And[7], secondA = new And[7], thirdA = new And[7], fourthA = new And[7];
	Or firstO[] = new Or[7], secondO[] = new Or[7];

	RSFlipFlop I = new RSFlipFlop(), T = new RSFlipFlop();
	And firstAI = new And(), secondAI = new And();
	Or firstOI = new Or(), secondOI = new Or();
	And firstAT = new And(), secondAT = new And();
	Or firstOT = new Or(), secondOT = new Or();
	Not nIBUS1_0 = new Not(), nIBUS1_1 = new Not(), nIBUS1_2 = new Not(), nIBUS1_3 = new Not(), nIBUS1_4 = new Not(), nIBUS1_5 = new Not(), nIBUS1_6 = new Not(), 
			nIBUS1_14 = new Not(), nIBUS1_15 = new Not(), nZ = new Not(), nC = new Not(), nN = new Not(), nV = new Not(),
			nprl0 = new Not(), nprl1 = new Not(), nprl2 = new Not();   

	SignalMerger PSW3bufhelper = new SignalMerger();
	TriStateBuffer PSW3buf = new TriStateBuffer();

	private String helperString[] = { "N", "Z", "C", "V", "prl0", "prl1", "prl2" };
	private String helperLDString[] = { "ldN", "ldZ", "ldC", "ldV", "ldL", "ldL", "ldL" };

	private String helperStringFF[] = { "N", "Z", "C", "V", "L0", "L1", "L2" };

	private String drawBUSString[] = { "BUSN", "BUSZ", "BUSC", "BUSV", "BUSprl0", "BUSprl1", "BUSprl2" };
	private String drawLDString[] = { "ldN", "ldZ", "ldC", "ldV", "ldprl0", "ldprl1", "ldprl2" };
	private String drawString[] = { "N", "Z", "C", "V", "prl0", "prl1", "prl2" };

	@Override
	public void initSignals() {
		String myNotData[] = {"", "1"};
		
		myNotData[0] = "!IBUS1_0";
		nIBUS1_0.init(myNotData);
		myNotData[0] = "!IBUS1_1";
		nIBUS1_1.init(myNotData);
		myNotData[0] = "!IBUS1_2";
		nIBUS1_2.init(myNotData);
		myNotData[0] = "!IBUS1_3";
		nIBUS1_3.init(myNotData);
		myNotData[0] = "!IBUS1_4";
		nIBUS1_4.init(myNotData);
		myNotData[0] = "!IBUS1_5";
		nIBUS1_5.init(myNotData);
		myNotData[0] = "!IBUS1_6";
		nIBUS1_6.init(myNotData);
		myNotData[0] = "!IBUS1_14";
		nIBUS1_14.init(myNotData);
		myNotData[0] = "!IBUS1_15";
		nIBUS1_15.init(myNotData);
		myNotData[0] = "!Z";
		nZ.init(myNotData);
		myNotData[0] = "!C";
		nC.init(myNotData);
		myNotData[0] = "!V";
		nV.init(myNotData);
		myNotData[0] = "!N";
		nN.init(myNotData);
		myNotData[0] = "!prl0";
		nprl0.init(myNotData);
		myNotData[0] = "!prl1";
		nprl1.init(myNotData);
		myNotData[0] = "!prl2";
		nprl2.init(myNotData);
		
		String myData[] = { "FF2", "2", "1" };
		start.init(myData);
		for (int i = 3; i < 10; i++) {
			nzcvprl[i - 3] = new RSFlipFlop();
			myData[0] = "FF" + i;
			nzcvprl[i - 3].init(myData);

			firstA[i - 3] = new And();
			myData[0] = "exec2FirstA" + i;
			firstA[i - 3].init(myData);

			secondA[i - 3] = new And();
			myData[0] = "exec2SecondA" + i;
			secondA[i - 3].init(myData);

			thirdA[i - 3] = new And();
			myData[0] = "exec2ThirdA" + i;
			thirdA[i - 3].init(myData);

			fourthA[i - 3] = new And();
			myData[0] = "exec2FourthA" + i;
			fourthA[i - 3].init(myData);

			firstO[i - 3] = new Or();
			myData[0] = "exec2FirstO" + i;
			firstO[i - 3].init(myData);

			secondO[i - 3] = new Or();
			myData[0] = "exec2SecondO" + i;
			secondO[i - 3].init(myData);
		}

		myData[0] = "FF10";
		I.init(myData);
		myData[0] = "exec2FirstAI";
		firstAI.init(myData);
		myData[0] = "exec2SecondAI";
		secondAI.init(myData);
		myData[0] = "exec2FirstOI";
		firstOI.init(myData);
		myData[0] = "exec2SecondOI";
		secondOI.init(myData);

		myData[0] = "FF11";
		T.init(myData);
		myData[0] = "exec2FirstAT";
		firstAT.init(myData);
		myData[0] = "exec2SecondAT";
		secondAT.init(myData);
		myData[0] = "exec2FirstOT";
		firstOT.init(myData);
		myData[0] = "exec2SecondOT";
		secondOT.init(myData);

		myData[0] = "PSW3bufhelper";
		myData[1] = "1";
		myData[2] = "32";
		PSW3bufhelper.init(myData);

		myData[0] = "PSW3buf";
		myData[1] = "32";
		PSW3buf.init(myData);

		StaticInitializer.allComponents.put("FF2", start);
		for (int i = 0; i < 7; i++)
			StaticInitializer.allComponents.put("FF" + (i + 3), nzcvprl[i]);
		StaticInitializer.allComponents.put("FF10", I);
		StaticInitializer.allComponents.put("FF11", T);

		for (int i = 0; i < 7; i++) {
			StaticInitializer.allComponents.put(nzcvprl[i].getName(), nzcvprl[i]);
			StaticInitializer.allComponents.put(firstA[i].getName(), firstA[i]);
			StaticInitializer.allComponents.put(secondA[i].getName(), secondA[i]);
			StaticInitializer.allComponents.put(thirdA[i].getName(), thirdA[i]);
			StaticInitializer.allComponents.put(fourthA[i].getName(), fourthA[i]);
			StaticInitializer.allComponents.put(firstO[i].getName(), firstO[i]);
			StaticInitializer.allComponents.put(secondO[i].getName(), secondO[i]);
		}
		StaticInitializer.allComponents.put(firstAI.getName(), firstAI);
		StaticInitializer.allComponents.put(secondAI.getName(), secondAI);
		StaticInitializer.allComponents.put(firstOI.getName(), firstOI);
		StaticInitializer.allComponents.put(secondOI.getName(), secondOI);
		StaticInitializer.allComponents.put(firstAT.getName(), firstAT);
		StaticInitializer.allComponents.put(secondAT.getName(), secondAT);
		StaticInitializer.allComponents.put(firstOT.getName(), firstOT);
		StaticInitializer.allComponents.put(secondOT.getName(), secondOT);
		
		StaticInitializer.allComponents.put(nIBUS1_0.getName(), nIBUS1_0);
		StaticInitializer.allComponents.put(nIBUS1_1.getName(), nIBUS1_1);
		StaticInitializer.allComponents.put(nIBUS1_2.getName(), nIBUS1_2);
		StaticInitializer.allComponents.put(nIBUS1_3.getName(), nIBUS1_3);
		StaticInitializer.allComponents.put(nIBUS1_4.getName(), nIBUS1_4);
		StaticInitializer.allComponents.put(nIBUS1_5.getName(), nIBUS1_5);
		StaticInitializer.allComponents.put(nIBUS1_6.getName(), nIBUS1_6);
		StaticInitializer.allComponents.put(nIBUS1_14.getName(), nIBUS1_14);
		StaticInitializer.allComponents.put(nIBUS1_15.getName(), nIBUS1_15);
		StaticInitializer.allComponents.put(nN.getName(), nN);
		StaticInitializer.allComponents.put(nZ.getName(), nZ);
		StaticInitializer.allComponents.put(nC.getName(), nC);
		StaticInitializer.allComponents.put(nV.getName(), nV);
		StaticInitializer.allComponents.put(nprl0.getName(), nprl0);
		StaticInitializer.allComponents.put(nprl1.getName(), nprl1);
		StaticInitializer.allComponents.put(nprl2.getName(), nprl2);
		
		
		StaticInitializer.allSignals.put(nIBUS1_0.getName(), nIBUS1_0.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_1.getName(), nIBUS1_1.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_2.getName(), nIBUS1_2.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_3.getName(), nIBUS1_3.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_4.getName(), nIBUS1_4.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_5.getName(), nIBUS1_5.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_6.getName(), nIBUS1_6.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_14.getName(), nIBUS1_14.getPortOUT());
		StaticInitializer.allSignals.put(nIBUS1_15.getName(), nIBUS1_15.getPortOUT());
		StaticInitializer.allSignals.put(nN.getName(), nN.getPortOUT());
		StaticInitializer.allSignals.put(nZ.getName(), nZ.getPortOUT());
		StaticInitializer.allSignals.put(nC.getName(), nC.getPortOUT());
		StaticInitializer.allSignals.put(nV.getName(), nV.getPortOUT());
		StaticInitializer.allSignals.put(nprl0.getName(), nprl0.getPortOUT());
		StaticInitializer.allSignals.put(nprl1.getName(), nprl1.getPortOUT());
		StaticInitializer.allSignals.put(nprl2.getName(), nprl2.getPortOUT());

		StaticInitializer.allSignals.put("START", start.getPortQ());
		for (int i = 0; i < 7; i++)
			StaticInitializer.allSignals.put("PSW" + helperStringFF[i], nzcvprl[i].getPortQ());
		StaticInitializer.allSignals.put("PSWI", I.getPortQ());
		StaticInitializer.allSignals.put("PSWT", T.getPortQ());
		StaticInitializer.allSignals.put("PSW", PSW3bufhelper.getPortOUT());
		StaticInitializer.allSignals.put("PSW3bufout", PSW3buf.getPortOUT());

		for (int i = 0; i < 7; i++) {
			StaticInitializer.allSignals.put(drawBUSString[i] + "_upperAND", firstA[i].getPortOUT());
			StaticInitializer.allSignals.put(drawBUSString[i] + "_lowerAND", thirdA[i].getPortOUT());
			StaticInitializer.allSignals.put(drawLDString[i] + "_upperAND", secondA[i].getPortOUT());
			StaticInitializer.allSignals.put(drawLDString[i] + "_lowerAND", fourthA[i].getPortOUT());
			StaticInitializer.allSignals.put(drawString[i] + "_upperOR", firstO[i].getPortOUT());
			StaticInitializer.allSignals.put(drawString[i] + "_lowerOR", secondO[i].getPortOUT());
		}
		StaticInitializer.allSignals.put("BUSI_upperAND", firstAI.getPortOUT());
		StaticInitializer.allSignals.put("I_upperOR", firstOI.getPortOUT());
		StaticInitializer.allSignals.put("BUSI_lowerAND", secondAI.getPortOUT());
		StaticInitializer.allSignals.put("I_lowerOR", secondOI.getPortOUT());
		StaticInitializer.allSignals.put("BUST_upperAND", firstAT.getPortOUT());
		StaticInitializer.allSignals.put("T_upperOR", firstOT.getPortOUT());
		StaticInitializer.allSignals.put("BUST_lowerAND", secondAT.getPortOUT());
		StaticInitializer.allSignals.put("T_lowerOR", secondOT.getPortOUT());
	}

	@Override
	public void connectSignals() {
		
		StaticInitializer.allSignals.get("IBUS1_0").connect(nIBUS1_0.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_1").connect(nIBUS1_1.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_2").connect(nIBUS1_2.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_3").connect(nIBUS1_3.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_4").connect(nIBUS1_4.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_5").connect(nIBUS1_5.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_6").connect(nIBUS1_6.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_14").connect(nIBUS1_14.getPortIN());
		StaticInitializer.allSignals.get("IBUS1_15").connect(nIBUS1_15.getPortIN());
		StaticInitializer.allSignals.get("N").connect(nN.getPortIN());
		StaticInitializer.allSignals.get("Z").connect(nZ.getPortIN());
		StaticInitializer.allSignals.get("C").connect(nC.getPortIN());
		StaticInitializer.allSignals.get("V").connect(nV.getPortIN());
		StaticInitializer.allSignals.get("prl0").connect(nprl0.getPortIN());
		StaticInitializer.allSignals.get("prl1").connect(nprl1.getPortIN());
		StaticInitializer.allSignals.get("prl2").connect(nprl2.getPortIN());

		// PSW3bufhelper
		StaticInitializer.allSignals.get("PSWN").connect(PSW3bufhelper.getPortIN(0));
		StaticInitializer.allSignals.get("PSWZ").connect(PSW3bufhelper.getPortIN(1));
		StaticInitializer.allSignals.get("PSWC").connect(PSW3bufhelper.getPortIN(2));
		StaticInitializer.allSignals.get("PSWV").connect(PSW3bufhelper.getPortIN(3));
		StaticInitializer.allSignals.get("PSWL0").connect(PSW3bufhelper.getPortIN(4));
		StaticInitializer.allSignals.get("PSWL1").connect(PSW3bufhelper.getPortIN(5));
		StaticInitializer.allSignals.get("PSWL2").connect(PSW3bufhelper.getPortIN(6));
		StaticInitializer.allSignals.get("PSWI").connect(PSW3bufhelper.getPortIN(14));
		StaticInitializer.allSignals.get("PSWT").connect(PSW3bufhelper.getPortIN(15));
		// PSW3bufhelper

		// PSW3buf
		StaticInitializer.allSignals.get("PSWout").connect(PSW3buf.getPortC());
		PSW3bufhelper.getPortOUT().connect(PSW3buf.getPortIN());
		PSW3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS2")); // mi
																					// propagiramo
																					// na
																					// magistralu
		// PSW3buf

		// start
		StaticInitializer.allSignals.get("CLK").connect(start.getPortCLK());
		StaticInitializer.allSignals.get("TSTART").connect(start.getPortS());
		StaticInitializer.allSignals.get("clSTART").connect(start.getPortR());
		// start

		for (int i = 3; i < 10; i++) {
			connectSingleFF(i);
		}
		connectI();
		connectT();
	}

	private void connectSingleFF(int i) {
		i -= 3;
		StaticInitializer.allSignals.get("CLK").connect(nzcvprl[i].getPortCLK());

		// firstA
		StaticInitializer.allSignals.get("ldPSW").connect(firstA[i].getPortIN(0));
		StaticInitializer.allSignals.get("IBUS1_" + i).connect(firstA[i].getPortIN(1));
		// firstA

		// secondA
		StaticInitializer.allSignals.get(helperLDString[i]).connect(secondA[i].getPortIN(0));
		StaticInitializer.allSignals.get(helperString[i]).connect(secondA[i].getPortIN(1));
		// secondA

		// firstO
		firstA[i].getPortOUT().connect(firstO[i].getPortIN(0));
		secondA[i].getPortOUT().connect(firstO[i].getPortIN(1));
		// firstO

		// thirdA
		StaticInitializer.allSignals.get("ldPSW").connect(thirdA[i].getPortIN(0));
		StaticInitializer.allSignals.get("!IBUS1_" + i).connect(thirdA[i].getPort(1));
		// thirdA

		// fourthA
		StaticInitializer.allSignals.get(helperLDString[i]).connect(fourthA[i].getPort(0));
		StaticInitializer.allSignals.get("!" + helperString[i]).connect(fourthA[i].getPort(1));
		// fourthA

		// secondO
		thirdA[i].getPortOUT().connect(secondO[i].getPortIN(0));
		fourthA[i].getPortOUT().connect(secondO[i].getPortIN(1));
		// secondO

		// nzcvprl
		firstO[i].getPortOUT().connect(nzcvprl[i].getPortS());
		secondO[i].getPortOUT().connect(nzcvprl[i].getPortR());
		// nzcvprl
	}

	private void connectI() {
		// firstAI
		StaticInitializer.allSignals.get("ldPSW").connect(firstAI.getPortIN(0));
		StaticInitializer.allSignals.get("IBUS1_" + 14).connect(firstAI.getPortIN(1));
		// firstAI

		// secondAI
		StaticInitializer.allSignals.get("ldPSW").connect(secondAI.getPortIN(0));
		StaticInitializer.allSignals.get("!IBUS1_" + 14).connect(secondAI.getPort(1));
		// secondAI

		// firstOI
		firstAI.getPortOUT().connect(firstOI.getPortIN(0));
		StaticInitializer.allSignals.get("stPSWI").connect(firstOI.getPortIN(1));
		// firstOI

		// secondOI
		secondAI.getPortOUT().connect(secondOI.getPortIN(0));
		StaticInitializer.allSignals.get("clPSWI").connect(secondOI.getPortIN(1));
		// secondOI

		// I
		StaticInitializer.allSignals.get("CLK").connect(I.getPortCLK());
		firstOI.getPortOUT().connect(I.getPortS());
		secondOI.getPortOUT().connect(I.getPortR());
		// I
	}

	private void connectT() {
		// firstAT
		StaticInitializer.allSignals.get("ldPSW").connect(firstAT.getPortIN(0));
		StaticInitializer.allSignals.get("IBUS1_" + 15).connect(firstAT.getPortIN(1));
		// firstAT

		// secondAT
		StaticInitializer.allSignals.get("ldPSW").connect(secondAT.getPortIN(0));
		StaticInitializer.allSignals.get("!IBUS1_" + 15).connect(secondAT.getPort(1));
		// secondAT

		// firstOT
		firstAT.getPortOUT().connect(firstOT.getPortIN(0));
		StaticInitializer.allSignals.get("stPSWT").connect(firstOT.getPortIN(1));
		// firstOT

		// secondOT
		secondAT.getPortOUT().connect(secondOT.getPortIN(0));
		StaticInitializer.allSignals.get("clPSWT").connect(secondOT.getPortIN(1));
		// secondOT

		// T
		StaticInitializer.allSignals.get("CLK").connect(T.getPortCLK());
		firstOT.getPortOUT().connect(T.getPortS());
		secondOT.getPortOUT().connect(T.getPortR());
		// T
	}

}
