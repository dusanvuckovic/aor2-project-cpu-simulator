package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.ComparatorWide;
import simulator2.digitalcomponents.IncDecRegister;
import simulator2.digitalcomponents.MemoryModuleIO;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SimpleRegister;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.digitalcomponents.TriStateBufferNegated;

public final class IO implements StaticInitializable {

	MemoryModuleIO memIO = new MemoryModuleIO();
	SimpleRegister TIMEIO = new SimpleRegister();
	IncDecRegister MEMACCIO = new IncDecRegister();
	ComparatorWide CMP2 = new ComparatorWide();
	TriStateBuffer memIO3buf = new TriStateBuffer();
	Not rdIO3buf = new Not(), wrIO3buf = new Not();
	TriStateBufferNegated mioIO3buf = new TriStateBufferNegated();
	And rdandIO = new And(), wrandIO = new And(), exitandIO = new And();
	Or exitorIO = new Or();

	@Override
	public void initSignals() {
		String myData[] = { "MEM I/O", "32", "32" };
		memIO.init(myData);

		myData[0] = "TIMEIO";
		myData[1] = "8";
		TIMEIO.init(myData);

		myData[0] = "MEMACCIO";
		MEMACCIO.init(myData);

		myData[0] = "CMP2";
		CMP2.init(myData);

		myData[0] = "memIO3buf";
		myData[1] = "32";
		memIO3buf.init(myData);
		myData[0] = "rdIO3";
		rdIO3buf.init(myData);
		myData[0] = "wrIO3";
		wrIO3buf.init(myData);
		myData[0] = "mioIO3buf";
		mioIO3buf.init(myData);

		myData[0] = "rdandIO";
		myData[1] = "2";
		myData[2] = "8";
		rdandIO.init(myData);
		myData[0] = "wrandIO";
		wrandIO.init(myData);
		myData[0] = "exitandIO";
		exitandIO.init(myData);
		myData[0] = "exitorIO";
		exitorIO.init(myData);

		StaticInitializer.allComponents.put("MEMACCIO", MEMACCIO);
		StaticInitializer.allComponents.put("TIMEIO", TIMEIO);
		StaticInitializer.allComponents.put("memIO", memIO);
		StaticInitializer.allComponents.put("CMP2", CMP2);
		StaticInitializer.allComponents.put(memIO3buf.getName(), memIO3buf);
		StaticInitializer.allComponents.put(rdIO3buf.getName(), rdIO3buf);
		StaticInitializer.allComponents.put(wrIO3buf.getName(), wrIO3buf);
		StaticInitializer.allComponents.put(mioIO3buf.getName(), mioIO3buf);
		StaticInitializer.allComponents.put(rdandIO.getName(), rdandIO);
		StaticInitializer.allComponents.put(wrandIO.getName(), wrandIO);
		StaticInitializer.allComponents.put(exitandIO.getName(), exitandIO);
		StaticInitializer.allComponents.put(exitorIO.getName(), exitorIO);

		StaticInitializer.allSignals.put("RDBUSinv", rdIO3buf.getPortOUT());
		StaticInitializer.allSignals.put("WRBUSinv", wrIO3buf.getPortOUT());
		StaticInitializer.allSignals.put("MIOinv", mioIO3buf.getPortOUT());
		StaticInitializer.allSignals.put("WRBUSinv", wrIO3buf.getPortOUT());
		StaticInitializer.allSignals.put("IOMEMACCin", MEMACCIO.getPortIN());
		StaticInitializer.allSignals.put("IOTIMEin", TIMEIO.getPortIN());
		StaticInitializer.allSignals.put("IOMEMACC", MEMACCIO.getPortOUT());
		StaticInitializer.allSignals.put("IOTIME", TIMEIO.getPortOUT());
		StaticInitializer.allSignals.put("memIO", memIO.getPortDO());
		StaticInitializer.allSignals.put("MEMI/O", memIO.getPortDO()); // za
																		// iscrtavanje
		StaticInitializer.allSignals.put("MEMI/Oout", memIO3buf.getPortOUT()); // za
																				// iscrtavanje
		StaticInitializer.allSignals.put("INCOR", exitorIO.getPortOUT()); // za
																			// iscrtavanje
		StaticInitializer.allSignals.put("INCAND", exitandIO.getPortOUT()); // za
																			// iscrtavanje
		StaticInitializer.allSignals.put("CMP2", CMP2.getPortEQ()); // za
																	// iscrtavanje
		StaticInitializer.allSignals.put("IOfc", CMP2.getPortEQ()); // interno
	}

	@Override
	public void connectSignals() {

		// memIO
		StaticInitializer.allSignals.get("ABUS").connect(memIO.getPortADDR());
		rdandIO.getPortOUT().connect(memIO.getPortRD());
		wrandIO.getPortOUT().connect(memIO.getPortWR());
		memIO.getPortDO().connect(memIO3buf.getPortIN());

		StaticInitializer.allSignals.get("MEMI/Oout").connect(memIO3buf.getPortC());
		memIO3buf.getPortOUT().connect(StaticInitializer.allSignals.get("DBUS"));
		// memIO

		// TIME
		TIMEIO.getPortCLK().connect(StaticInitializer.allSignals.get("CLK"));
		TIMEIO.getPortLD().connect(StaticInitializer.allSignals.get("START"));
		TIMEIO.getPortOUT().connect(CMP2.getPortB());
		// TIME

		// bufs
		StaticInitializer.allSignals.get("1").connect(mioIO3buf.getPortC());
		StaticInitializer.allSignals.get("!RDBUS").connect(rdIO3buf.getPortIN());
		StaticInitializer.allSignals.get("!WRBUS").connect(wrIO3buf.getPortIN());
		StaticInitializer.allSignals.get("M/IO").connect(mioIO3buf.getPortIN());
		// bufs

		rdIO3buf.getPortOUT().connect(rdandIO.getPortIN(0));
		mioIO3buf.getPortOUT().connect(rdandIO.getPortIN(1));

		wrIO3buf.getPortOUT().connect(wrandIO.getPortIN(0));
		mioIO3buf.getPortOUT().connect(wrandIO.getPortIN(1));

		rdandIO.getPortOUT().connect(exitorIO.getPortIN(0));
		wrandIO.getPortOUT().connect(exitorIO.getPortIN(1));

		exitandIO.getPortIN(0).connect(exitorIO.getPortOUT());
		StaticInitializer.allSignals.get("IOfc").connect(exitandIO.invertPort(1).getPort(1));

		// memacc
		StaticInitializer.allSignals.get("CLK").connect(MEMACCIO.getPortCLK());
		StaticInitializer.allSignals.get("IOfc").connect(MEMACCIO.getPortCL());
		exitandIO.getPortOUT().connect(MEMACCIO.getPortINC());
		MEMACCIO.getPortOUT().connect(CMP2.getPortA());
		// memacc

	}
}
