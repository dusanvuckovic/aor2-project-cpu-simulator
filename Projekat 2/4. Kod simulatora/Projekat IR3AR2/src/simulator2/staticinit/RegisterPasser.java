package simulator2.staticinit;

import simulator2.digitalcomponents.IncDecRegister;
import simulator2.digitalcomponents.SimpleRegister;

public class RegisterPasser {
	public SimpleRegister[] simpleReg = new SimpleRegister[24];
	public IncDecRegister[] incDecReg = new IncDecRegister[4];

	public RegisterPasser() {

		simpleReg[0] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("GPRADDR1");
		simpleReg[1] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("GPRADDR2");
		simpleReg[2] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("GPRADDR3");
		simpleReg[3] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("BADDR1");
		simpleReg[4] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("BADDR2");
		simpleReg[5] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R2");
		simpleReg[6] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R3");
		simpleReg[7] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R4");
		simpleReg[8] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R5");
		simpleReg[9] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R6");
		simpleReg[10] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R7");
		simpleReg[11] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R8");
		simpleReg[12] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R9");
		simpleReg[13] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R10");
		simpleReg[14] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R11");
		simpleReg[15] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R12");
		simpleReg[16] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R13");
		simpleReg[17] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R14");
		simpleReg[18] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("R15");
		simpleReg[19] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("IR");
		simpleReg[20] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("BR");
		simpleReg[21] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("IVTP");
		simpleReg[22] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("MAR");
		simpleReg[23] = (SimpleRegister) simulator2.staticinit.StaticInitializer.allComponents.get("MDR");
		incDecReg[0] = (IncDecRegister) simulator2.staticinit.StaticInitializer.allComponents.get("PC");
		incDecReg[1] = (IncDecRegister) simulator2.staticinit.StaticInitializer.allComponents.get("SP");
		incDecReg[2] = (IncDecRegister) simulator2.staticinit.StaticInitializer.allComponents.get("CNTSHIFT");
	}

}
