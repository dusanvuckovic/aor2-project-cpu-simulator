package simulator2.staticinit;

import simulator2.digitalcomponents.ALU;
import simulator2.digitalcomponents.Add;
import simulator2.digitalcomponents.ComparatorWide;
import simulator2.digitalcomponents.IncDecRegister;
import simulator2.digitalcomponents.Multiplexer;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.SignalExtender;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.SignalSplitter;
import simulator2.digitalcomponents.TriStateBuffer;

public final class EXEC1 implements StaticInitializable {

	IncDecRegister CNTSHIFT = new IncDecRegister();
	Multiplexer MX3 = new Multiplexer();
	SignalMerger IR17 = new SignalMerger();
	SignalExtender IRext = new SignalExtender();
	SignalSplitter ALUsplitter = new SignalSplitter();
	Add ADDcomponents = new Add();
	ALU alu = new ALU();
	ComparatorWide CMP0 = new ComparatorWide();
	TriStateBuffer ADD3buff = new TriStateBuffer(), ALU3buff = new TriStateBuffer();
	Not C32Neg = new Not();

	@Override
	public void initSignals() {
		String myDataExt[] = {"IRext", "18", "32"};
		IRext.init(myDataExt);
		
		String myDataS[] = { "ALUsplitter", "1", "32" };
		ALUsplitter.init(myDataS);

		myDataS[0] = "C32Neg";
		myDataS[1] = "1";
		C32Neg.init(myDataS);

		String myData[] = { "CNTSHIFT", "5", "1" };
		CNTSHIFT.init(myData);

		myData[0] = "MX3";
		myData[1] = "18";
		myData[2] = "1";
		MX3.init(myData);

		myData[0] = "ALU";
		myData[1] = "32";
		alu.init(myData);

		myData[0] = "IR17";
		myData[1] = "1";
		myData[2] = "18";
		IR17.init(myData);

		myData[0] = "ADDComponent";
		myData[1] = "32";
		ADDcomponents.init(myData);

		myData[0] = "CMP0";
		myData[1] = "5";
		CMP0.init(myData);

		myData[0] = "ADD3buff";
		myData[1] = "32";
		ADD3buff.init(myData);

		myData[0] = "ALU3buff";
		myData[1] = "32";
		ALU3buff.init(myData);

		StaticInitializer.allComponents.put(CNTSHIFT.getName(), CNTSHIFT);
		StaticInitializer.allComponents.put(CMP0.getName(), CMP0);
		StaticInitializer.allComponents.put(MX3.getName(), MX3);
		StaticInitializer.allComponents.put(ADDcomponents.getName(), ADDcomponents);
		StaticInitializer.allComponents.put(alu.getName(), alu);
		StaticInitializer.allComponents.put(IR17.getName(), IR17);
		StaticInitializer.allComponents.put(ALUsplitter.getName(), ALUsplitter);
		StaticInitializer.allComponents.put(ADD3buff.getName(), ADD3buff);
		StaticInitializer.allComponents.put(ALU3buff.getName(), ALU3buff);
		StaticInitializer.allComponents.put(C32Neg.getName(), C32Neg);

		StaticInitializer.allSignals.put("noSHIFT", CMP0.getPortEQ());
		StaticInitializer.allSignals.put("CNTSHIFT", CNTSHIFT.getPortOUT());
		StaticInitializer.allSignals.put("CMP0", CMP0.getPortEQ());
		StaticInitializer.allSignals.put("MX3", MX3.getPortOUT());
		StaticInitializer.allSignals.put("ADDComponent", ADDcomponents.getPortOUT());
		StaticInitializer.allSignals.put("ALU", alu.getPortOUT());
		StaticInitializer.allSignals.put("IR17..0", IR17.getPortOUT());
		StaticInitializer.allSignals.put("ADD3bufout", ADD3buff.getPortOUT());
		StaticInitializer.allSignals.put("ALU3bufout", ALU3buff.getPortOUT());

		for (int i = 0; i < 32; i++)
			StaticInitializer.allSignals.put("ALU" + i, ALUsplitter.getPortOUT(i));

		StaticInitializer.allSignals.put("C0", alu.getPortC0());
		StaticInitializer.allSignals.put("C32", alu.getPortCMAX());
		StaticInitializer.allSignals.put("CMAX", alu.getPortCMAX());
		StaticInitializer.allSignals.put("!C32", C32Neg.getPortOUT());
	}

	@Override
	public void connectSignals() {

		// C32Neg
		alu.getPortCMAX().connect(C32Neg.getPortIN());
		// C32Neg

		// ALUsplitter
		alu.getPortOUT().connect(ALUsplitter.getPortIN());
		// ALUsplitter

		// IR17
		for (int i = 0; i < 18; i++)
			StaticInitializer.allSignals.get("IR" + i).connect(IR17.getPortIN(i));
		// IR17

		// CNTSHIFT
		StaticInitializer.allSignals.get("CLK").connect(CNTSHIFT.getPortCLK());
		StaticInitializer.allSignals.get("ldCNTSHIFT").connect(CNTSHIFT.getPortLD());
		StaticInitializer.allSignals.get("decCNTSHIFT").connect(CNTSHIFT.getPortDEC());
		StaticInitializer.allSignals.get("MDRoutput1713").connect(CNTSHIFT.getPortIN());
		// CNTSHIFT

		// CMP0
		CNTSHIFT.getPortOUT().connect(CMP0.getPortA());
		// B je 0
		// CMP0

		// MX3
		StaticInitializer.allSignals.get("mxSIG").connect(MX3.getPortS(0));
		// ulaz 0 je 0
		IR17.getPortOUT().connect(MX3.getPortIN(1));
		// MX3

		// ADD
		StaticInitializer.allSignals.get("IBUS2").connect(ADDcomponents.getPortA());
		MX3.getPortOUT().connect(IRext.getPortIN());
		IRext.getPortOUT().connect(ADDcomponents.getPortB());
		// ADD

		// ADD3buff
		StaticInitializer.allSignals.get("ADDout").connect(ADD3buff.getPortC());
		ADDcomponents.getPortOUT().connect(ADD3buff.getPortIN());
		ADD3buff.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3")); // mi
																					// stavljamo
																					// na
																					// magistralu
		// ADD3buff

		// ALU
		StaticInitializer.allSignals.get("IBUS1").connect(alu.getPortA());
		ADDcomponents.getPortOUT().connect(alu.getPortB());
		StaticInitializer.allSignals.get("add").connect(alu.getPortADD());
		StaticInitializer.allSignals.get("sub").connect(alu.getPortSUB());
		StaticInitializer.allSignals.get("not").connect(alu.getPortNOT());
		StaticInitializer.allSignals.get("or").connect(alu.getPortOR());
		StaticInitializer.allSignals.get("and").connect(alu.getPortAND());
		StaticInitializer.allSignals.get("shl").connect(alu.getPortSHL());
		StaticInitializer.allSignals.get("shr").connect(alu.getPortSHR());
		// ALU

		// ALU3buff
		StaticInitializer.allSignals.get("ALUout").connect(ALU3buff.getPortC());
		alu.getPortOUT().connect(ALU3buff.getPortIN());
		ALU3buff.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3")); // mi
																					// guramo
																					// na
																					// magistralu
		// ALU3buff
	}

}
