package simulator2.staticinit;

import simulator2.digitalcomponents.Nor;
import simulator2.digitalcomponents.SignalExtender;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.SignalSplitter;
import simulator2.digitalcomponents.SimpleRegister;
import simulator2.digitalcomponents.TriStateBuffer;

public final class FETCH1 implements StaticInitializable {

	public static Nor ERRNOR = null;
	SimpleRegister IR = new SimpleRegister();
	SignalSplitter IRSplitter = new SignalSplitter();
	SignalMerger IRMEMMerger = new SignalMerger();
	SignalMerger IR240 = new SignalMerger(), IR200 = new SignalMerger(), IR2522 = new SignalMerger();
	TriStateBuffer JMPPOM3buf = new TriStateBuffer(), JPOM3buf = new TriStateBuffer(), IRPOM3buf = new TriStateBuffer();
	SignalExtender extJ1 = new SignalExtender(), extJ2 = new SignalExtender();

	@Override
	public void initSignals() {
		
		String extendData[] = {"extJ1", "24", "32"};
		extJ1.init(extendData);
		extendData[0] = "extJ2";
		extendData[1] = "21";
		extJ2.init(extendData);
		
		String[] myData = { "IR", "32", "32" };
		IR.init(myData);

		myData[0] = "IRSplitter";
		myData[1] = "1";
		IRSplitter.init(myData);

		myData[0] = "JMPPOM3buf";
		myData[1] = "24";
		JMPPOM3buf.init(myData);

		myData[0] = "JPOM3buf";
		myData[1] = "21";
		JPOM3buf.init(myData);

		myData[0] = "IRPOM3buf";
		myData[1] = "4";
		IRPOM3buf.init(myData);

		myData[0] = "IR23..0";
		myData[1] = "1";
		myData[2] = "25";
		IR240.init(myData);

		myData[0] = "IR20..0";
		myData[2] = "21";
		IR200.init(myData);

		myData[0] = "IR25..22";
		myData[2] = "4";
		IR2522.init(myData);
		
		myData[0] = "IRMEMMerger";
		myData[1] = "1";
		myData[2] = "32";
		IRMEMMerger.init(myData);

		for (int i = 0; i < 32; i++)
			StaticInitializer.allSignals.put("IR" + i, IRSplitter.getPortOUT(i));

		StaticInitializer.allComponents.put(extJ1.getName(), extJ1);
		StaticInitializer.allComponents.put(extJ2.getName(), extJ2);
		StaticInitializer.allComponents.put("IR", IR);
		StaticInitializer.allComponents.put(IRSplitter.getName(), IRSplitter);
		StaticInitializer.allComponents.put(IR240.getName(), IR240);
		StaticInitializer.allComponents.put(IR200.getName(), IR200);
		StaticInitializer.allComponents.put(IR2522.getName(), IR2522);
		StaticInitializer.allComponents.put(JMPPOM3buf.getName(), JMPPOM3buf);
		StaticInitializer.allComponents.put(JPOM3buf.getName(), JPOM3buf);
		StaticInitializer.allComponents.put(IRPOM3buf.getName(), IRPOM3buf);
		StaticInitializer.allComponents.put(IRMEMMerger.getName(), IRMEMMerger);

		StaticInitializer.allSignals.put("IR", IR.getPortOUT());
		StaticInitializer.allSignals.put("IR31..0", IR.getPortOUT());
		StaticInitializer.allSignals.put("JMPPOM3bufout", JMPPOM3buf.getPortOUT());
		StaticInitializer.allSignals.put("JPOM3bufout", JPOM3buf.getPortOUT());
		StaticInitializer.allSignals.put("IRPOM3bufout", IRPOM3buf.getPortOUT());
		StaticInitializer.allSignals.put(IR240.getName(), IR240.getPortOUT());
		StaticInitializer.allSignals.put(IR200.getName(), IR200.getPortOUT());
		StaticInitializer.allSignals.put(IR2522.getName(), IR2522.getPortOUT());
		StaticInitializer.allSignals.put(IRMEMMerger.getName(), IRMEMMerger.getPortOUT());
	}

	@Override
	public void connectSignals() {

		//IRMEMMerger
		for (int i = 0; i < 8; i++)
			StaticInitializer.allSignals.get("IBUS1_" + (24+i)).connect(IRMEMMerger.getPortIN(i));
		for (int i = 8; i < 16; i++)
			StaticInitializer.allSignals.get("IBUS1_" + (i+8)).connect(IRMEMMerger.getPortIN(i));
		for (int i = 16; i < 24; i++)
			StaticInitializer.allSignals.get("IBUS1_" + (i-8)).connect(IRMEMMerger.getPortIN(i));
		for (int i = 24; i < 32; i++)
			StaticInitializer.allSignals.get("IBUS1_" + (i-24)).connect(IRMEMMerger.getPortIN(i));
		//IRMEMMerger
		
		// IR
		StaticInitializer.allSignals.get("CLK").connect(IR.getPortCLK());
		StaticInitializer.allSignals.get("ldIR").connect(IR.getPortLD());
		IRMEMMerger.getPortOUT().connect(IR.getPortIN());
		// IR

		// IRsplitter && IRmergers
		IR.getPortOUT().connect(IRSplitter.getPortIN());
		for (int i = 0; i <= 24; i++)
			IRSplitter.getPortOUT(i).connect(IR240.getPortIN(i));
		for (int i = 0; i <= 20; i++)
			IRSplitter.getPortOUT(i).connect(IR200.getPortIN(i));
		for (int i = 22; i <= 25; i++)
			IRSplitter.getPortOUT(i).connect(IR2522.getPortIN(i-22));
		
		// IRsplitter && IRmergers

		// BUFFERS
		StaticInitializer.allSignals.get("JMPPOMout").connect(JMPPOM3buf.getPortC());
		IR240.getPortOUT().connect(JMPPOM3buf.getPortIN());
		JMPPOM3buf.getPortOUT().connect(extJ1.getPortIN());
		extJ1.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));

		StaticInitializer.allSignals.get("JPOMout").connect(JPOM3buf.getPortC());
		IR200.getPortOUT().connect(JPOM3buf.getPortIN());
		JPOM3buf.getPortOUT().connect(extJ2.getPortIN());
		extJ2.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS1"));

		StaticInitializer.allSignals.get("IRPOMout").connect(IRPOM3buf.getPortC());
		IR2522.getPortOUT().connect(IRPOM3buf.getPortIN());
		IRPOM3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3"));
		// BUFFERS
	}
}
