package simulator2.staticinit;

import simulator2.digitalcomponents.Coder;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.SignalSplitter;
import simulator2.digitalcomponents.SimpleRegister;
import simulator2.digitalcomponents.TriStateBuffer;

public final class INT3 implements StaticInitializable {
	Coder CD2 = new Coder(), CD3 = new Coder();
	SimpleRegister BR = new SimpleRegister(), IVTP = new SimpleRegister();
	TriStateBuffer UINT3buf = new TriStateBuffer(), UEXT3buf = new TriStateBuffer(), BR3buf = new TriStateBuffer(),
			IVTP3buf = new TriStateBuffer();
	private SignalMerger UINThelper = new SignalMerger(), UEXThelper = new SignalMerger(),
			BRHelper = new SignalMerger();
	private SignalSplitter BRsplitter = new SignalSplitter();

	@Override
	public void initSignals() {
		String[] myData = { "CD2", "2", "8" };
		CD2.init(myData);

		myData[0] = "CD3";
		myData[1] = "3";
		CD3.init(myData);

		myData[0] = "BR";
		myData[1] = "8";
		BR.init(myData);

		myData[0] = "IVTP";
		myData[1] = "32";
		IVTP.init(myData);

		myData[0] = "UINT3buf";
		myData[1] = "8";
		UINT3buf.init(myData);

		myData[0] = "UEXT3buf";
		UEXT3buf.init(myData);

		myData[0] = "BRsplitter";
		myData[1] = "1";
		myData[2] = "8";
		BRsplitter.init(myData);

		myData[0] = "BRHelper";
		myData[1] = "1";
		myData[2] = "16";
		BRHelper.init(myData);

		myData[0] = "BR3buf";
		myData[1] = "16";
		BR3buf.init(myData);

		myData[0] = "IVTP3buf";
		myData[1] = "32";
		IVTP3buf.init(myData);

		myData[1] = "1";
		myData[2] = "8";
		myData[0] = "UINThelper";
		UINThelper.init(myData);

		myData[0] = "UEXThelper";
		UEXThelper.init(myData);

		StaticInitializer.allComponents.put(CD2.getName(), CD2);
		StaticInitializer.allComponents.put(CD3.getName(), CD3);
		StaticInitializer.allComponents.put(BR.getName(), BR);
		StaticInitializer.allComponents.put(IVTP.getName(), IVTP);
		StaticInitializer.allComponents.put(UINT3buf.getName(), UINT3buf);
		StaticInitializer.allComponents.put(UEXT3buf.getName(), UEXT3buf);
		StaticInitializer.allComponents.put(BR3buf.getName(), BR3buf);
		StaticInitializer.allComponents.put(IVTP3buf.getName(), IVTP3buf);
		StaticInitializer.allComponents.put(UINThelper.getName(), UINThelper);
		StaticInitializer.allComponents.put(UEXThelper.getName(), UEXThelper);
		StaticInitializer.allComponents.put(BRHelper.getName(), BRHelper);
		StaticInitializer.allComponents.put(BRsplitter.getName(), BRsplitter);

		StaticInitializer.allSignals.put("BR", BR.getPortOUT());
		StaticInitializer.allSignals.put("BRHelper", BRHelper.getPortOUT());
		StaticInitializer.allSignals.put(UINThelper.getName(), UINThelper.getPortOUT());
		StaticInitializer.allSignals.put(UEXThelper.getName(), UEXThelper.getPortOUT());
		StaticInitializer.allSignals.put("IVTP", IVTP.getPortOUT());

		StaticInitializer.allSignals.put("IVTP3bufout", IVTP3buf.getPortOUT());
		StaticInitializer.allSignals.put("BR3bufout", BR3buf.getPortOUT());

		StaticInitializer.allSignals.put("CD2_0", CD2.getPortOUT(0)); // za
																		// iscrtavanje
		StaticInitializer.allSignals.put("CD2_1", CD2.getPortOUT(1));

		StaticInitializer.allSignals.put("CD3_0", CD3.getPortOUT(0)); // za
																		// iscrtavanje
		StaticInitializer.allSignals.put("CD3_1", CD3.getPortOUT(1));
		StaticInitializer.allSignals.put("CD3_2", CD3.getPortOUT(2));

		StaticInitializer.allSignals.put("UINT", UINThelper.getPortOUT()); // za
																			// iscrtavanje
		StaticInitializer.allSignals.put("UINT3bufout", UINT3buf.getPortOUT());

		StaticInitializer.allSignals.put("UEXT", UEXThelper.getPortOUT()); // za
																			// iscrtavanje
		StaticInitializer.allSignals.put("UEXT3bufout", UEXT3buf.getPortOUT());

	}

	@Override
	public void connectSignals() {
		// CD2
		StaticInitializer.allSignals.get("PSWT").connect(CD2.getPortIN(0));
		// prazno
		StaticInitializer.allSignals.get("PRINM").connect(CD2.getPortIN(1));
		StaticInitializer.allSignals.get("PRCOD").connect(CD2.getPortIN(3));
		// CD2

		// uint
		CD2.getPortOUT(0).connect(UINThelper.getPortIN(0));
		CD2.getPortOUT(1).connect(UINThelper.getPortIN(1));
		StaticInitializer.allSignals.get("UINTout").connect(UINT3buf.getPortC());
		UINThelper.getPortOUT().connect(UINT3buf.getPortIN());
		UINT3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3"));
		// uint

		for (int i = 1; i < 8; i++)
			StaticInitializer.allSignals.get("PRINTR" + i).connect(CD3.getPortIN(i));

		// uext
		CD3.getPortOUT(0).connect(UEXThelper.getPortIN(0));
		CD3.getPortOUT(1).connect(UEXThelper.getPortIN(1));
		CD3.getPortOUT(2).connect(UEXThelper.getPortIN(2));
		StaticInitializer.allSignals.get("1").connect(UEXThelper.getPortIN(3));
		StaticInitializer.allSignals.get("UEXTout").connect(UEXT3buf.getPortC());
		UEXThelper.getPortOUT().connect(UEXT3buf.getPortIN());
		UEXT3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3"));
		// uext

		// BR
		StaticInitializer.allSignals.get("CLK").connect(BR.getPortCLK());
		StaticInitializer.allSignals.get("ldBR").connect(BR.getPortLD());
		StaticInitializer.allSignals.get("IBUS3").connect(BR.getPortIN());
		BR.getPortOUT().connect(BRsplitter.getPortIN());
		for (int i = 0; i < BRsplitter.getNumber(); i++)
			BRsplitter.getPortOUT(i).connect(BRHelper.getPortIN(i + 2));
		BRHelper.getPortOUT().connect(BR3buf.getPortIN());

		StaticInitializer.allSignals.get("BRout").connect(BR3buf.getPortC());
		StaticInitializer.allSignals.get("IBUS2").connect(BR3buf.getPortOUT());
		BRHelper.getPortOUT().connect(BR3buf.getPortIN());
		// BR

		// IVTP

		StaticInitializer.allSignals.get("CLK").connect(IVTP.getPortCLK());
		StaticInitializer.allSignals.get("ldIVTP").connect(IVTP.getPortLD());
		StaticInitializer.allSignals.get("IBUS3").connect(IVTP.getPortIN());

		StaticInitializer.allSignals.get("IVTPout").connect(IVTP3buf.getPortC());
		IVTP.getPortOUT().connect(IVTP3buf.getPortIN());
		IVTP3buf.getPortOUT().connect(StaticInitializer.allSignals.get("IBUS3"));
		// IVTP
	}
}
