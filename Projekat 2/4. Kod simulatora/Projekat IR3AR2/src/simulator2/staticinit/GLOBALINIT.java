package simulator2.staticinit;

import simulator2.Signal;
import simulator2.SignalSimple;
import simulator2.digitalcomponents.Bus;
import simulator2.digitalcomponents.SignalMerger;
import simulator2.digitalcomponents.SignalSplitter;
import simulator2.digitalcomponents.TriStateBufferNegated;

public class GLOBALINIT implements StaticInitializable {

	Signal CLK, One, Zero;
	Bus IBUS1, IBUS2, IBUS3, FCBUS, ABUS, DBUS, RDBUS, WRBUS, BUSYBUS;
	static SignalSplitter firstBUS = new SignalSplitter(), secondBUS = new SignalSplitter(),
			thirdBUS = new SignalSplitter(), MARsplit = new SignalSplitter(), MDRsplit = new SignalSplitter(),
			Asplit = new SignalSplitter(), Dsplit = new SignalSplitter();
	static TriStateBufferNegated RDBUSN = new TriStateBufferNegated(), WRBUSN = new TriStateBufferNegated(),
			FCBUSN = new TriStateBufferNegated(), BUSYBUSN = new TriStateBufferNegated();
	SignalMerger IBUS12522 = new SignalMerger(), IBUS12421 = new SignalMerger(), IBUS12118 = new SignalMerger(),
			ABUS312 = new SignalMerger(), DBUS3124 = new SignalMerger(), DBUS2316 = new SignalMerger(),
			DBUS158 = new SignalMerger(), DBUS70 = new SignalMerger();
	SignalSimple intr1 = new SignalSimple("intr1"), intr2 = new SignalSimple("intr2"),
			intr3 = new SignalSimple("intr3"), intr4 = new SignalSimple("intr4"), intr5 = new SignalSimple("intr5"),
			intr6 = new SignalSimple("intr6"), intr7 = new SignalSimple("intr7");

	@Override
	public void initSignals() {
		String myDataSM[] = { "IBUS125..22", "1", "4" };
		IBUS12522.init(myDataSM);
		myDataSM[0] = "IBUS124..21";
		IBUS12421.init(myDataSM);
		myDataSM[0] = "IBUS121..18";
		IBUS12118.init(myDataSM);

		myDataSM[2] = "8";

		myDataSM[0] = "DBUS31..24";
		DBUS3124.init(myDataSM);
		myDataSM[0] = "DBUS23..16";
		DBUS2316.init(myDataSM);
		myDataSM[0] = "DBUS15..8";
		DBUS158.init(myDataSM);
		myDataSM[0] = "DBUS7..0";
		DBUS70.init(myDataSM);

		myDataSM[0] = "ABUS31..2";
		myDataSM[2] = "30";
		ABUS312.init(myDataSM);

		String myDataN[] = { "RDBUS", "32" };
		RDBUSN.init(myDataN);
		myDataN[0] = "WRBUS";
		WRBUSN.init(myDataN);
		myDataN[0] = "FCBUS";
		FCBUSN.init(myDataN);
		myDataN[0] = "BUSYBUS";
		BUSYBUSN.init(myDataN);

		IBUS1 = new Bus("IBUS1");
		IBUS2 = new Bus("IBUS2");
		IBUS3 = new Bus("IBUS3");
		ABUS = new Bus("ABUS");
		DBUS = new Bus("DBUS");
		RDBUS = new Bus("!RDBUS", 1);
		WRBUS = new Bus("!WRBUS", 1);
		FCBUS = new Bus("!FCBUS", 1);
		BUSYBUS = new Bus("!BUSYBUS", 1);

		String[] myData = { "firstBUS", "1", "32" };
		firstBUS.init(myData);
		myData[0] = "secondBUS";
		secondBUS.init(myData);
		myData[0] = "thirdBUS";
		thirdBUS.init(myData);
		myData[0] = "MARsplit";
		MARsplit.init(myData);
		myData[0] = "MDRsplit";
		MDRsplit.init(myData);
		myData[0] = "Asplit";
		Asplit.init(myData);
		myData[0] = "Dsplit";
		Dsplit.init(myData);
		StaticInitializer.allBuses.put(IBUS1.getName(), IBUS1);
		StaticInitializer.allBuses.put(IBUS2.getName(), IBUS2);
		StaticInitializer.allBuses.put(IBUS3.getName(), IBUS3);
		StaticInitializer.allBuses.put(FCBUS.getName(), FCBUS);
		StaticInitializer.allBuses.put(ABUS.getName(), ABUS);
		StaticInitializer.allBuses.put(DBUS.getName(), DBUS);
		StaticInitializer.allBuses.put(RDBUS.getName(), RDBUS);
		StaticInitializer.allBuses.put(WRBUS.getName(), WRBUS);
		StaticInitializer.allBuses.put(BUSYBUS.getName(), BUSYBUS);

		StaticInitializer.allComponents.put(firstBUS.getName(), firstBUS);
		StaticInitializer.allComponents.put(secondBUS.getName(), secondBUS);
		StaticInitializer.allComponents.put(thirdBUS.getName(), thirdBUS);
		StaticInitializer.allComponents.put(MARsplit.getName(), MARsplit);
		StaticInitializer.allComponents.put(MDRsplit.getName(), MDRsplit);
		StaticInitializer.allComponents.put(Asplit.getName(), Asplit);
		StaticInitializer.allComponents.put(RDBUSN.getName(), RDBUSN);
		StaticInitializer.allComponents.put(WRBUSN.getName(), WRBUSN);
		StaticInitializer.allComponents.put(FCBUSN.getName(), FCBUSN);
		StaticInitializer.allComponents.put(BUSYBUSN.getName(), BUSYBUSN);
		StaticInitializer.allComponents.put(IBUS12522.getName(), IBUS12522);
		StaticInitializer.allComponents.put(IBUS12421.getName(), IBUS12421);
		StaticInitializer.allComponents.put(IBUS12118.getName(), IBUS12118);
		StaticInitializer.allComponents.put(ABUS312.getName(), ABUS312);
		StaticInitializer.allComponents.put(DBUS3124.getName(), DBUS3124);
		StaticInitializer.allComponents.put(DBUS2316.getName(), DBUS2316);
		StaticInitializer.allComponents.put(DBUS158.getName(), DBUS158);
		StaticInitializer.allComponents.put(DBUS70.getName(), DBUS70);

		StaticInitializer.allSignals.put(intr1.getName(), intr1);
		StaticInitializer.allSignals.put(intr2.getName(), intr2);
		StaticInitializer.allSignals.put(intr3.getName(), intr3);
		StaticInitializer.allSignals.put(intr4.getName(), intr4);
		StaticInitializer.allSignals.put(intr5.getName(), intr5);
		StaticInitializer.allSignals.put(intr6.getName(), intr6);
		StaticInitializer.allSignals.put(intr7.getName(), intr7);
		StaticInitializer.allSignals.put("IBUS1", IBUS1);
		StaticInitializer.allSignals.put("IBUS2", IBUS2);
		StaticInitializer.allSignals.put("IBUS3", IBUS3);
		StaticInitializer.allSignals.put("ABUS", ABUS);
		StaticInitializer.allSignals.put("DBUS", DBUS);
		StaticInitializer.allSignals.put("!RDBUS", RDBUS);
		StaticInitializer.allSignals.put("!WRBUS", WRBUS);
		StaticInitializer.allSignals.put("!FCBUS", FCBUS);
		StaticInitializer.allSignals.put("!BUSYBUS", BUSYBUS);
		StaticInitializer.allSignals.put("RDBUS", RDBUSN.getPortOUT());
		StaticInitializer.allSignals.put("WRBUS", WRBUSN.getPortOUT());
		StaticInitializer.allSignals.put("FCBUS", FCBUSN.getPortOUT());
		StaticInitializer.allSignals.put("BUSYBUS", BUSYBUSN.getPortOUT());
		StaticInitializer.allSignals.put(IBUS12522.getName(), IBUS12522.getPortOUT());
		StaticInitializer.allSignals.put(IBUS12421.getName(), IBUS12421.getPortOUT());
		StaticInitializer.allSignals.put(IBUS12118.getName(), IBUS12118.getPortOUT());
		StaticInitializer.allSignals.put(DBUS3124.getName(), DBUS3124.getPortOUT());
		StaticInitializer.allSignals.put(DBUS2316.getName(), DBUS2316.getPortOUT());
		StaticInitializer.allSignals.put(DBUS158.getName(), DBUS158.getPortOUT());
		StaticInitializer.allSignals.put(DBUS70.getName(), DBUS70.getPortOUT());
		StaticInitializer.allSignals.put(ABUS312.getName(), ABUS312.getPortOUT());

		CLK = new SignalSimple("CLK");
		StaticInitializer.allSignals.put("CLK", CLK);
		One = new SignalSimple("1");
		StaticInitializer.allSignals.put("1", One);
		Zero = new SignalSimple("0");
		StaticInitializer.allSignals.put("0", Zero);
		StaticInitializer.allSignals.put("hreq1", Zero);
		StaticInitializer.allSignals.put("hclr1", Zero);

		ABUS.connect(Asplit.getPortIN());
		IBUS1.connect(firstBUS.getPortIN());
		IBUS2.connect(secondBUS.getPortIN());
		IBUS3.connect(thirdBUS.getPortIN());
		DBUS.connect(Dsplit.getPortIN());

		for (int i = 0; i < 32; i++) {
			StaticInitializer.allSignals.put("ABUS" + i, Asplit.getPortOUT(i));
			StaticInitializer.allSignals.put("DBUS" + i, Dsplit.getPortOUT(i));
			StaticInitializer.allSignals.put("IBUS1_" + i, firstBUS.getPortOUT(i));
			StaticInitializer.allSignals.put("IBUS2_" + i, secondBUS.getPortOUT(i));
			StaticInitializer.allSignals.put("IBUS3_" + i, thirdBUS.getPortOUT(i));
			StaticInitializer.allSignals.put("MAR" + i, MARsplit.getPortOUT(i));
			StaticInitializer.allSignals.put("MDR" + i, MDRsplit.getPortOUT(i));
		}
		for (int i = 18; i <= 21; i++) {
			firstBUS.getPortOUT(getInvertedEndian(i)).connect(IBUS12118.getPortIN(i - 18));
			firstBUS.getPortOUT(getInvertedEndian(i + 3)).connect(IBUS12421.getPortIN(i - 18));
			firstBUS.getPortOUT(getInvertedEndian(i + 4)).connect(IBUS12522.getPortIN(i - 18));
		}
		for (int i = 2; i < 31; i++)
			Asplit.getPortOUT(i).connect(ABUS312.getPortIN(i-2));
		for (int i = 0; i < 8; i++)
			Dsplit.getPortOUT(i).connect(DBUS70.getPortIN(i));
		for (int i = 8; i < 16; i++)
			Dsplit.getPortOUT(i).connect(DBUS158.getPortIN(i-8));
		for (int i = 16; i < 24; i++)
			Dsplit.getPortOUT(i).connect(DBUS2316.getPortIN(i-16));
		for (int i = 24; i < 32; i++)
			Dsplit.getPortOUT(i).connect(DBUS3124.getPortIN(i-24));
		
			
	}

	@Override
	public void connectSignals() {

	}
	
	private int getInvertedEndian(int i) {
		if (i <= 7)
			return i + 24;
		else if (i >= 8 && i <= 15)
			return i + 8;
		else if (i >= 16 && i <= 23)
			return i - 8;
		else
			return i-24;
	}

}
