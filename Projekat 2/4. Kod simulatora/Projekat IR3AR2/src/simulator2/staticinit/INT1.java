package simulator2.staticinit;

import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.DecoderE;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.RSFlipFlop;

public final class INT1 implements StaticInitializable {

	RSFlipFlop[] FFs = new RSFlipFlop[10];
	private RSFlipFlop PRADR = new RSFlipFlop();
	DecoderE DC3 = new DecoderE();
	And INT1And = new And();
	Or INT1Or = new Or();
	Or INT1printrOr = new Or();
	private String[] helperString = { "FF12", "FF13", "FF14", "FF15", "FF16", "FF17", "FF18", "FF19", "FF20", "FF21" };
	private String[] helperS = { "stPRINS", "stPRCOD", "inm", "intr1", "intr2", "intr3", "intr4", "intr5", "intr6",
			"intr7" };
	private String[] helperR = { "clPRINS", "clPRCOD", "clPRINM", "clINTR1", "clINTR2", "clINTR3", "clINTR4", "clINTR5",
			"clINTR6", "clINTR7" };
	private String[] Q = { "PRINS", "PRCOD", "PRINM", "PRINTR1", "PRINTR2", "PRINTR3", "PRINTR4", "PRINTR5", "PRINTR6",
			"PRINTR7" };

	@Override
	public void initSignals() {
		String[] myData = { "", "3", "1" };
		for (int i = 0; i < 10; i++) {
			FFs[i] = new RSFlipFlop();
			myData[0] = helperString[i];
			FFs[i].init(myData);
		}
		myData[0] = "PRADR";
		PRADR.init(myData);

		myData[0] = "DC3";
		DC3.init(myData);

		myData[0] = "INT1And";
		myData[1] = "2";
		INT1And.init(myData);

		myData[0] = "INT1Or";
		myData[1] = "5";
		INT1Or.init(myData);

		myData[0] = "INT1printrOr";
		myData[1] = "8";
		INT1printrOr.init(myData);

		for (int i = 12; i < 22; i++)
			StaticInitializer.allComponents.put("FF" + i, FFs[i - 12]);
		StaticInitializer.allComponents.put("DC3", DC3);

		StaticInitializer.allComponents.put(PRADR.getName(), PRADR);
		StaticInitializer.allComponents.put(INT1And.getName(), INT1And);
		StaticInitializer.allComponents.put(INT1Or.getName(), INT1Or);
		StaticInitializer.allComponents.put(INT1printrOr.getName(), INT1printrOr);

		for (int i = 1; i < 8; i++)
			StaticInitializer.allSignals.put("clINTR" + i, DC3.getPortOUT(i));
		for (int i = 0; i < 8; i++)
			StaticInitializer.allSignals.put("intr" + i, StaticInitializer.allSignals.get("0"));

		for (int i = 0; i < 10; i++)
			StaticInitializer.allSignals.put(Q[i], FFs[i].getPortQ());

		StaticInitializer.allSignals.put("PrekidAND", INT1And.getPortOUT());
		StaticInitializer.allSignals.put("prekid", INT1Or.getPortOUT()); // isto
																			// kao
																			// gore
		StaticInitializer.allSignals.put("printr", INT1printrOr.getPortOUT());
		StaticInitializer.allSignals.put("PRADR", PRADR.getPortQ());
	}

	@Override
	public void connectSignals() {
		// FFs
		for (int i = 0; i < 10; i++) {
			StaticInitializer.allSignals.get("CLK").connect(FFs[i].getPortCLK());
			StaticInitializer.allSignals.get(helperS[i]).connect(FFs[i].getPortS());
			StaticInitializer.allSignals.get(helperR[i]).connect(FFs[i].getPortR());
		}
		// FFs

		// DC3
		StaticInitializer.allSignals.get("clINTR").connect(DC3.getPortE());
		StaticInitializer.allSignals.get("prl0").connect(DC3.getPortIN(0));
		StaticInitializer.allSignals.get("prl0").connect(DC3.getPortIN(1));
		StaticInitializer.allSignals.get("prl0").connect(DC3.getPortIN(2));
		// DC3

		for (int i = 1; i < 8; i++)
			StaticInitializer.allSignals.get("PRINTR" + i).connect(INT1printrOr.getPortIN(i));

		// int1AND
		StaticInitializer.allSignals.get("PSWT").connect(INT1And.getPortIN(0));
		StaticInitializer.allSignals.get("RET").connect(INT1And.invertPort(1).getPort(1));
		//// int1AND

		// int1OR
		StaticInitializer.allSignals.get("PRINS").connect(INT1Or.getPortIN(0));
		StaticInitializer.allSignals.get("PRCOD").connect(INT1Or.getPortIN(1));
		StaticInitializer.allSignals.get("PRINM").connect(INT1Or.getPortIN(2));
		StaticInitializer.allSignals.get("printr").connect(INT1Or.getPortIN(3));
		StaticInitializer.allSignals.get("PRINS").connect(INT1Or.getPortIN(0));
		INT1And.getPortOUT().connect(INT1Or.getPortIN(4));
		// INT1OR
	}
}