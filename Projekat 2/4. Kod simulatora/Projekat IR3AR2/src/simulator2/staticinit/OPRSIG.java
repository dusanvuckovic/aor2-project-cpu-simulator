package simulator2.staticinit;

import java.util.ArrayList;
import java.util.Arrays;

public final class OPRSIG implements StaticInitializable { // prvo ide ova klasa
															// pri
															// inicijalizaciji,
															// posle nje
															// globalinit
	static public final ArrayList<String> conditionalNames;
	static public final ArrayList<String> equivalentNames;

	static {
		equivalentNames = new ArrayList<String>(Arrays.asList("ldBADDR1", "ldBADDR2", "mxADDR1", "mxADDR2", "mxADDR3",
				"ldGPRADDR1", "ldGPRADDR2", "ldGPRADDR3", "ldPC", "incPC", "PCout1", "PCout2", "incSP", "decSP",
				"SPout1", "wrGPR", "rdGPR1", "rdGPR2", "mxSIG", "ADDout", "decCNTSHIFT", "not", "or", "and", "add",
				"sub", "shl", "shr", "ALUout", "clSTART", "ldN", "ldZ", "ldC", "ldV", "ldL", "PSWout", "stPSWI",
				"clPSWI", "stPSWT", "clPSWT", "JMPPOMout", "JPOMout", "IRPOMout", "stPRINS", "clPRINS", "stPRCOD",
				"clPRCOD", "clPRINM", "clINTR", "UINTout", "UEXTout", "BRout", "IVTPout", "MEMI/Oout", "hreq0",
				"SPout2", "ldMAR", "MARout", "MARAout", "mxMDR", "ldMDR", "MDRout", "rdCPU", "wrCPU", "NULLBUS1",
				"BRG2_3", "BRG1_2", "BRG1_3", "ldCNTSHIFT", "inm", "ldIR", "stERROR", "ldBR", "ldPSW", "M/IO",
				"mxADDR0", "hclr0", "hreq1", "hclr1", "ldIVTP", "MDRDout"));
		conditionalNames = new ArrayList<String>(
				Arrays.asList("next", "br", "bropr", "!START", "!hack0", "!fcCPU", "!gropr", "!noSHIFT", "noSHIFT",
						"!brjmp", "JMPREGIND", "!prekid", "!PRINS", "!PRCOD", "!PRADR", "!PRINM", "!printr"));
	}

	@Override
	public void initSignals() {
	}

	@Override
	public void connectSignals() {
		// intentionally empty
	}
}