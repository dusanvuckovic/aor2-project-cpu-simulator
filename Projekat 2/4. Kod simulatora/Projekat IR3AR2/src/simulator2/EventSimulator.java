package simulator2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

import simulator2.digitalcomponents.Bus;
import simulator2.gui.Window;
import simulator2.staticinit.StaticInitializer;

public class EventSimulator {
	protected long currentTime = 0;

	protected Signal CLK;
	protected HashMap<String, Signal> signalsMap = null;
	protected HashMap<String, Bus> busesMap = null;
	protected HashMap<String, LogicComponent> componentsMap = null;

	protected List<Event> currentEventsList = null;

	Signal mPC;

	public EventSimulator() {
	}

	public EventSimulator(Signal CLK, HashMap<String, Signal> signalsMap, HashMap<String, Bus> busesMap,
			HashMap<String, LogicComponent> componentsMap) {
		this.CLK = CLK;
		this.signalsMap = signalsMap;
		this.busesMap = busesMap;
		this.componentsMap = componentsMap;
		mPC = StaticInitializer.allSignals.get("mPC");
	}

	public void setCLK(Signal CLK) {
		this.CLK = CLK;
	}

	public void setSignalsList(HashMap<String, Signal> signalsList) {
		this.signalsMap = signalsList;
	}

	public void setBusesList(HashMap<String, Bus> busesMap) {
		this.busesMap = busesMap;
	}

	public void setComponentsList(HashMap<String, LogicComponent> componentsMap) {
		this.componentsMap = componentsMap;
	}

	public long getCurrentTime() {
		return currentTime;
	}

	public void addEvent(Event event) {
		if (currentEventsList != null)
			currentEventsList.add(event);
	}

	public void reset() { // resets the system
		Event tempEvent;

		currentTime = 0;

		currentEventsList = new LinkedList<Event>();

		Iterator<Signal> signalIterator = signalsMap.values().iterator(); // reset
																			// signals
		while (signalIterator.hasNext()) {
			signalIterator.next().resetState();
		}

		Iterator<Bus> busIterator = busesMap.values().iterator(); // reset buses
																	// (just in
																	// case)
		while (busIterator.hasNext()) {
			busIterator.next().resetState();
		}

		
		Iterator<LogicComponent> componentIterator = componentsMap.values().iterator(); // reset
																						// components
		while (componentIterator.hasNext()) {
			componentIterator.next().resetState();
		}


		tempEvent = new Event(0, new DigitalValue(DigitalValue.ONE), signalsMap.get("1")); // propagate
																							// the
																							// 1
																							// value
		currentEventsList.add(tempEvent);

		tempEvent = new Event(0, new DigitalValue(DigitalValue.ONE),
				StaticInitializer.allSignals.get("TSTART")); // propagate
																			// the
																			// 1
																			// value
		currentEventsList.add(tempEvent);


		Iterator<LogicComponent> componentIterator2 = componentsMap.values().iterator(); // pre
																							// calculate
																							// components
		
		while (componentIterator2.hasNext()) {
			currentEventsList.addAll(componentIterator2.next().preCalculateEvents());
		}


		while (currentEventsList.size() > 0) { // execute the 0th tick
			
			tempEvent = currentEventsList.remove(0);
			
			currentEventsList.addAll(tempEvent.getDst().execute(tempEvent));
		}
		
		

		Iterator<LogicComponent> componentIterator3 = componentsMap.values().iterator(); // reset
																							// components
		while (componentIterator3.hasNext()) {
			componentIterator3.next().resetStateCLK();
		}
		

		HashMap<String, Signal> tempSignals = new HashMap<String, Signal>();
		Iterator<Signal> signalIterator2 = signalsMap.values().iterator(); // save signal values for display
		while (signalIterator2.hasNext()) {
			Signal sigTemp = signalIterator2.next();
			if (!tempSignals.containsValue(sigTemp)) {
				tempSignals.put(sigTemp.getName(), sigTemp);
				sigTemp.saveValue();
			}
		}

		tempEvent = new Event(0, new DigitalValue(DigitalValue.ONE), CLK); // activate
																			// CLK
		currentEventsList = CLK.execute(tempEvent);

		Window.tMenu.repaintSchema();
		Window.signalWindow.repaint();

	}

	protected void tick() { // executes one tick
		Event tempEvent;

		currentTime++;
		
		Iterator<Bus> busIterator = busesMap.values().iterator(); // reset buses
		while (busIterator.hasNext()) {
			Bus busTemp = busIterator.next();
			busTemp.resetBus();
		}

		

		while (currentEventsList.size() > 0) { // execute the tick
			tempEvent = currentEventsList.remove(0);
			
			currentEventsList.addAll(tempEvent.getDst().execute(tempEvent));
		}

		tempEvent = new Event(currentTime, new DigitalValue(DigitalValue.ZERO), CLK); // reset
																						// the
																						// CLK
		currentEventsList.addAll(tempEvent.getDst().execute(tempEvent));

		while (currentEventsList.size() > 0) { // propagate CLK reset
			tempEvent = currentEventsList.remove(0);
			currentEventsList.addAll(tempEvent.getDst().execute(tempEvent));
		}

		HashMap<String, Signal> tempSignals = new HashMap<String, Signal>();
		Iterator<Signal> signalIterator = signalsMap.values().iterator();
		while (signalIterator.hasNext()) { // save signal values for display
			Signal sigTemp = signalIterator.next();
			if (!tempSignals.containsValue(sigTemp)) {
				tempSignals.put(sigTemp.getName(), sigTemp);
				sigTemp.saveValue();
			}
		}

		Iterator<LogicComponent> componentIterator = componentsMap.values().iterator(); // reset components
		while (componentIterator.hasNext()) {
			componentIterator.next().resetStateCLK();
		}

		tempEvent = new Event(currentTime, new DigitalValue(DigitalValue.ONE), CLK); // set
																						// the
																						// CLK
		currentEventsList = CLK.execute(tempEvent);
		
	}

	public void tickOnce() { // simulates one tick
		tick();

		Window.tMenu.repaintSchema();
		Window.signalWindow.repaint();
	}

	public void tickMultiple(long endTime) { // simulates multiple ticks
		while (currentTime < endTime) {
			tick();
		}

		Window.tMenu.repaintSchema();
		Window.signalWindow.repaint();
	}

	public void tickInstruction() {
		while (mPC.getIntValue() == 0) {
			tick();
		}
		
		while (mPC.getIntValue() != 0) {
			tick();
		}

		Window.tMenu.repaintSchema();
		Window.signalWindow.repaint();

	}
}