package simulator2;

import java.io.Serializable;
import java.util.List;

public interface Signal extends EventExecutable, Serializable, Loadable {

	public static final int NOTSET = -1;
	public static final int CONNECTED = 1;

	public void setName(String name);

	public String getName();

	public String getSimpleName();

	public void setId(long id); // ! NOT SAFE !

	public long getId();

	public void setValue(Value value); // ! NOT SAFE !

	public Value getValue();

	public void setIntValue(int value); // ! NOT SAFE !

	public int getIntValue();

	public int getSize();

	public void setHighZ(boolean highZ); // ! NOT SAFE !

	public boolean getHighZ();

	public void setDefaultValue(int defValue);

	public int getDefaultValue();

	public List<Value> getValueList();

	public List<Signal> getConnectedSignals();

	public void connect(Signal signal);

	public void connectBoth(Signal signal);

	public void disconnect(Signal signal);

	public void disconnectBoth(Signal signal);

	public boolean isConnected();

	public boolean canConduct();

	public boolean canGenerate();

	public void saveValue();
	
	public void saveValueLast();

	public String getDescription();

	public void setDescription(String description);

}
