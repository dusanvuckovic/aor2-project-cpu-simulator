package simulator2.dynamicinit;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Scanner;

import simulator2.digitalcomponents.MemoryModule;

public final class NetlistMemory {

	private HashMap<Integer, Integer> memoryValues = new HashMap<Integer, Integer>(); // sadržaj memorije koji će se prekopirati u memorijske module
	private ByteBuffer b; // pomoćna Javina klasa
	boolean isBigEndian = true;

	public NetlistMemory() {
		b = ByteBuffer.allocate(4);
		b.order(ByteOrder.BIG_ENDIAN); // naš CPU je li'l endian; na po�?etku su
										// instrukcije
	}

	private void setEndian(ByteBuffer b) {
		if (isBigEndian)
			b.order(ByteOrder.BIG_ENDIAN);
		else
			b.order(ByteOrder.LITTLE_ENDIAN);
	}

	private boolean checkForEndian(String s) {
		if (!s.contains("Endian"))
			return false;
		if (s.contains("bigEndian"))
			isBigEndian = true;
		else
			isBigEndian = false;
		return true;
	}

	public void process() throws FileNotFoundException {
		Integer i1, i2; // pozicija i vrednosti nje i tri sledeće
		Scanner scanner = new Scanner(new File("src/simulator2/dynamicinit/memory.txt")); // otvori fajl
		while (scanner.hasNextLine()) { // pro�?itaj liniju (pretpostavka da nema
										// grešaka)
			String s = scanner.nextLine();
			if ("".equals(s) || s.startsWith("//"))
				continue;
			if (!checkForEndian(s)) {
				Scanner deepScanner = new Scanner(s);
				deepScanner.useRadix(16); // podesi na heksa �?itanje
				i1 = deepScanner.nextInt();
				i2 = deepScanner.nextInt();
				memoryValues.put(i1, i2);
				deepScanner.close();
			}
		}
		scanner.close();
	}

	public void setMemory(MemoryModule[] m) { // niz memorijskih modula sistema,
												// poređanih redom
		for (HashMap.Entry<Integer, Integer> entry : memoryValues.entrySet()) { // kroz svako preslikavanje u mapi
			Integer key = entry.getKey(); // uzmi poziciju
			Integer value = entry.getValue();
			b = ByteBuffer.allocate(4);
			setEndian(b);
			b.putInt(value); // ubaci u ByteBuffer

			byte[] hold = b.array(); // napravi niz bajtova od int-a
			for (int i = 0; i < 4; i++)
				m[i].setMemory((key >> 2), hold[i]); // i sredi ta�?nu memorijsku re�? (nije key+i jer svaki modul ima svoje brojanje)
		}
	}
}
