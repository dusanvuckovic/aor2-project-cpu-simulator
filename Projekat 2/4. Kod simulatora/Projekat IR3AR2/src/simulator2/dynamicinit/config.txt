// DECODER decoderName activationSignalNumber(2) IR21 IR20 ADD SUB AND OR //format generisanja dekodera
decoderINS DCD0 START 6 IR26 IR27 IR28 IR29 IR30 IR31 ADD SUB ERR2 ERR3 CMP AND OR NOT TST LD ST JE JNE JGE JG JLE JL JP JNP JO JNO CALL TRPL PUSH POP ERR25 IN OUT SHR SHL INT ERR31 ERR32 ERR33 ERR34 ERR35 ERR36 ERR37 ERR38 ERR39 ERR40 ERR41 ERR42 ERR43 ERR44 ERR45 ERR46 ERR47 ERR48 ERR49 ERR50 ERR51 ERR52 ERR53 ERR54 ERR55 ERR56 ERR57 ERR58 ERR59 ERR60 ERR61 ERR62 ERR63 //format generisanja dekodera
decoderINS DCD1 TRPL 2 IR24 IR25 RET IRET JMP ERR22

// vreme za IOTIME/TIME
timeLength 5

//flagsNZ format - broj signala, signali
//drugi deo - generise se kolo
flagSet flagsNZ 11 ADD SUB CMP AND OR NOT TST SHR SHL LD POP

//flagsCC32, flagsC!C32, flagsCALU0, flagsCALU31 format - broj signala, signali
flagSet flagsCC32 1 ADD
flagSet flagsC!C32 2 SUB CMP
flagSet flagsCALU0 1 SHR
flagSet flagsCALU31 1 SHL

//flagsV format: signal operacije, formati kola u dekadnom broju (njegov aktivan bit pozicije i je aktivan signal ALU31-IBUS231-IBUS131)
//ADD ima dve, A B !ALU i  
flagSet flagsV 3 ADD 3 4 1
flagSet flagsV 3 SUB 6 1 4
flagSet flagsV 3 CMP 6 1 4

// LOGIC componentName signalName componentType numOfEntries Entry1 (...) Entry16 //format logickih komponenti

logic checkIfOnlyOne1 isOK1 XNOR 2 ADD SUB
logic checkIfOnlyOne2 isOK2 XNOR 2 AND OR
logic checkFinal isOK XNOR 2 isOK1 isOK2

//buffersDECVAL - BUFFER bufferName, bufferActivationSignal, bufferOutBus, bufferBitLength, decValue
//buffersIRVAL - BUFFER bufferName, bufferActivationSignal, bufferOutBus, numOfBits, startPosition

//jumps - SignalCondition, jumpPosition, isInverted