package simulator2.dynamicinit;

import java.util.HashMap;

import simulator2.staticinit.OPRSIG;

public final class OrganizationParameters {

	public static HashMap<String, Integer> ControlSignalToPosition = null;
	public static HashMap<String, Integer> JumpConditionToDecoderEntry = null; // u
																				// ovo
																				// se
																				// ubacuje
																				// i
																				// kasnije
																				// prilikom
																				// iš�?itavanja
																				// dinami�?kih
																				// dekodera
																				// skoka

	static {
		ControlSignalToPosition = new HashMap<String, Integer>();
		JumpConditionToDecoderEntry = new HashMap<String, Integer>();
		for (int i = 0; i < OPRSIG.equivalentNames.size(); i++)
			ControlSignalToPosition.put(OPRSIG.equivalentNames.get(i), i);
		for (int i = 0; i < OPRSIG.conditionalNames.size(); i++)
			JumpConditionToDecoderEntry.put(OPRSIG.conditionalNames.get(i), i);
	}

}
