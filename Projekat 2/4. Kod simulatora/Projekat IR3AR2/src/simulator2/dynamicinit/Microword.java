package simulator2.dynamicinit;

import java.util.BitSet;

public final class Microword {

	// pozicije u CW bitima
	public static final int CW_SIZE_IN_BITS = 104;
	public static final int CW_Z_BEGIN = 0;
	public static final int CW_Z_END = 87;
	public static final int CW_CC_START = 88;
	public static final int CW_CC_END = 95;
	public static final int CW_BA_START = 96;
	public static final int CW_BA_END = 103;
	// pozicije u CW bitima

	private BitSet myCode; // sam kod mikrore�?i

	public Microword() {
		myCode = new BitSet(CW_SIZE_IN_BITS);
		myCode.clear();
	}

	private void set(int myBit, int candidate) { // pomoćna funkcija zbog nekih
													// osobina klase BitSet
		set(myBit, toBool(candidate));
	}

	private void set(int myBit, boolean value) { // pomoćna funkcija zbog nekih
													// osobina klase BitSet
		if (myBit > CW_SIZE_IN_BITS || myBit < 0)
			System.out.println("Los broj!");
		myCode.set(myBit, value);
	}

	private boolean toBool(int candidate) {
		if (candidate == 0)
			return false;
		return true;
	}

	public int getFromBool(boolean b) {
		if (b)
			return 1;
		else
			return 0;
	}

	public Integer[] getIntegerData() {
		Integer[] helper = new Integer[4];
		for (int i = 0; i < 4; i++)
			helper[i] = new Integer(0);
		for (int i = 0; i < CW_SIZE_IN_BITS; i++)
			helper[i / 32] |= (getFromBool(myCode.get(i)) << (i % 32));
		return helper;
	}

	public void setBA(String s) {
		Integer myAddress = Integer.parseInt(s.substring(4), 16); // podstring od madr
		for (int i = 0; i <= CW_BA_END - CW_BA_START; i++) // zbog autoinkrementa bitseta
			set(i + CW_BA_START, myAddress & (1 << i)); // od prvog do poslednjeg, setujemo bit po bit
	}

	public void setCC(String s) {
		Integer myValue = OrganizationParameters.JumpConditionToDecoderEntry.get(s); // iz
																						// mape
		for (int i = 0; i <= CW_CC_END - CW_CC_START; i++) // prođi kroz
			set(i + CW_CC_START, myValue & (1 << i));
	}

	public void setZ(String s) { // prosto setuj odgovarajući bit na 0
		set(OrganizationParameters.ControlSignalToPosition.get(s), true);
	}

}
