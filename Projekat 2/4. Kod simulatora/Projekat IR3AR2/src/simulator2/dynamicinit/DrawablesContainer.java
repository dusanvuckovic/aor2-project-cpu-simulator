package simulator2.dynamicinit;

import java.util.ArrayList;
import java.util.HashMap;

import simulator2.SignalSimple;
import simulator2.digitalcomponents.And;
import simulator2.digitalcomponents.DecoderE;
import simulator2.digitalcomponents.Nand;
import simulator2.digitalcomponents.Nor;
import simulator2.digitalcomponents.Not;
import simulator2.digitalcomponents.Nxor;
import simulator2.digitalcomponents.OneOutputGate;
import simulator2.digitalcomponents.Or;
import simulator2.digitalcomponents.SignalMergerSimple;
import simulator2.digitalcomponents.SimpleRegister;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.digitalcomponents.Xor;
import simulator2.dynamicinit.NetlistDynamic.BufferDynamic;
import simulator2.dynamicinit.NetlistDynamic.DecoderDynamic;
import simulator2.dynamicinit.NetlistDynamic.FlagDynamic;
import simulator2.dynamicinit.NetlistDynamic.JumpDynamic;
import simulator2.dynamicinit.NetlistDynamic.LogicCircuitDynamic;
import simulator2.staticinit.FETCH1;
import simulator2.staticinit.StaticInitializer;
import simulator2.staticinit.UPRSIG;

public final class DrawablesContainer {

	public final int timeDelay;
	public final ArrayList<FlagDynamic> allFlags;
	public final ArrayList<DecoderDynamic> allDecoders; // decoderName ||
														// activationSignal,
														// numOfEntriesN,
														// Entry1, ..., Entry N + 2^N
	public final ArrayList<LogicCircuitDynamic> allLogicCircuits; // componentName
																	// ||
																	// signalName,
																	// componentType,
																	// numOfEntries,
																	// Entry1,
																	// ...,
																	// EntryN
	public final ArrayList<BufferDynamic> allBuffers; // bufferName ||
														// activationSignal,
														// outBUS, bitLength,
														// Bit1, ..., BitN
	public final ArrayList<JumpDynamic> allJumps;
	GraphingContainer gc = null;

	public DrawablesContainer(int timeDelay, ArrayList<FlagDynamic> allFlags, ArrayList<DecoderDynamic> allDecoders,
			ArrayList<LogicCircuitDynamic> allLogicCircuits, ArrayList<BufferDynamic> allBuffers,
			ArrayList<JumpDynamic> allJumps) {
		this.timeDelay = timeDelay;
		this.allFlags = allFlags;
		this.allDecoders = allDecoders;
		this.allLogicCircuits = allLogicCircuits;
		this.allBuffers = allBuffers;
		this.allJumps = allJumps;
		gc = new GraphingContainer();
	}

	public void init() {
		initTimeDelay();
		initDecoders();
		initFlags();
		initLogicCircuits();
		initBuffers();
		initJumps();
	}

	private void initTimeDelay() {
		String[] myData = { "TIMEvalue", "5", "" };
		SignalSimple loadTime = new SignalSimple();
		loadTime.init(myData);
		loadTime.setIntValue(timeDelay);
		SimpleRegister TIMEhelper = (SimpleRegister) StaticInitializer.allComponents.get("TIME");
		SimpleRegister TIMEIOhelper = (SimpleRegister) StaticInitializer.allComponents.get("TIMEIO");
		loadTime.connect(TIMEhelper.getPortIN());
		loadTime.connect(TIMEIOhelper.getPortIN());
		TIMEhelper.setDefaultValue(timeDelay);
		TIMEIOhelper.setDefaultValue(timeDelay);
	}

	private void initFlags() {
		// pravimo kola
		And N = new And();
		And Z = new And();
		Or C = new Or();
		Or V = new Or();
		// pravimo kola

		// pravimo pomoÄ‡ne promenljive
		Or helperOps = new Or();
		Nor helperALU = new Nor();

		int helperNo = 0;
		String[] myCTempData = { "", "", "1" };
		String[] myVTempData = { "", "", "1" };
		
		Not snailInvertIBUS1_31 = new Not();
		Not snailInvertIBUS2_31 = new Not();
		Not snailInvertALU31 = new Not();
		
		String mySnailData[] = {"snailInvertIBUS1_31", "1"};
		snailInvertIBUS1_31.init(mySnailData);
		mySnailData[0] = "snailInvertIBUS2_31";
		snailInvertIBUS2_31.init(mySnailData);
		mySnailData[0] = "snailInvertALU31";
		snailInvertALU31.init(mySnailData);
		
		StaticInitializer.allSignals.get("IBUS1_31").connect(snailInvertIBUS1_31.getPortIN());
		StaticInitializer.allSignals.get("IBUS2_31").connect(snailInvertIBUS2_31.getPortIN());
		StaticInitializer.allSignals.get("ALU31").connect(snailInvertALU31.getPortIN());
		
		StaticInitializer.allSignals.put("!IBUS1_31", snailInvertIBUS1_31.getPortOUT());
		StaticInitializer.allSignals.put("!IBUS2_31", snailInvertIBUS2_31.getPortOUT());
		StaticInitializer.allSignals.put("!ALU31", snailInvertALU31.getPortOUT());
		
		StaticInitializer.allComponents.put(snailInvertIBUS1_31.getName(), snailInvertIBUS1_31);
		StaticInitializer.allComponents.put(snailInvertIBUS2_31.getName(), snailInvertIBUS2_31);
		StaticInitializer.allComponents.put(snailInvertALU31.getName(), snailInvertALU31);
		
		ArrayList<And> cHolder = new ArrayList<And>(); // drÅ¾i sva kola koja treba da uÄ‘u u veliki C OR
		ArrayList<And> vHolder = new ArrayList<And>(); // drÅ¾i sva kola koja treba da uÄ‘u u veliki V OR
		HashMap<String, String> cSignals = new HashMap<String, String>();
		cSignals.put("flagsCC32", "C32");
		cSignals.put("flagsC!C32", "!C32");
		cSignals.put("flagsCALU0", "ALU0");
		cSignals.put("flagsCALU32", "ALU31");
		// pravimo pomoÄ‡ne promenljive

		for (FlagDynamic f : allFlags) {
			switch (f.typeOfFlag) {
			// flegovi N ili Z
			case "flagsNZ": // ovo moÅ¾e samo jednom da se pozove
				// init N i Z
				String myZTempData[] = { "N", "2", "1" };
				N.init(myZTempData);

				myZTempData[0] = "Z";
				Z.init(myZTempData);
				// init N i Z

				// kreiranje pomocnih kola - operacije i ALU bitovi
				myZTempData[0] = "helperOps";
				myZTempData[1] = Integer.toString(f.listOfStrings.size());
				helperOps.init(myZTempData);
				myZTempData[0] = "helperALU";
				myZTempData[1] = "32";
				helperALU.init(myZTempData);
				// kreiranje pomocnih kola - operacije i ALU bitovi

				// inicijalizacija pomoÄ‡nih kola i njihovo povezivanje sa N i Z
				// (vidi sliku)
				for (int i = 0; i < f.listOfStrings.size(); i++)
					StaticInitializer.allSignals.get(f.listOfStrings.get(i)).connect(helperOps.getPortIN(i)); // OR kolo, bilo koja od operacija
				for (int i = 0; i < 32; i++)
					StaticInitializer.allSignals.get("ALU" + i).connect(helperALU.getPortIN(i));
				helperOps.getPortOUT().connect(N.getPortIN(0));
				helperOps.getPortOUT().connect(Z.getPortIN(0));
				StaticInitializer.allSignals.get("ALU31").connect(N.getPortIN(1));
				helperALU.getPortOUT().connect(Z.getPortIN(1));
				// inicijalizacija pomoÄ‡nih kola i njihovo povezivanje sa N i Z
				// (vidi sliku)

				// postavljanje N i Z u allSignals
				StaticInitializer.allSignals.put("N", N.getPortOUT());
				StaticInitializer.allSignals.put("Z", Z.getPortOUT());
				StaticInitializer.allComponents.put("N", N);
				StaticInitializer.allComponents.put("Z", Z);
				StaticInitializer.allSignals.put(helperOps.getName(), helperOps.getPortOUT());
				StaticInitializer.allComponents.put(helperOps.getName(), helperOps);
				StaticInitializer.allSignals.put(helperALU.getName(), helperALU.getPortOUT());

				gc.exec3Flags.add(N);
				gc.exec3Flags.add(Z);
				gc.exec3Flags.add(helperOps);
				gc.exec3Flags.add(helperALU);
				// postavljanje N i Z u allSignals
				break;
			// flegovi N ili Z

			// flegovi za C kolo
			case "flagsCC32":
			case "flagsC!C32":
			case "flagsCALU0":
			case "flagsCALU32": // ovo prolazi vie puta
				And lvl2CHelper = new And(); // and koji se povezuje na veliki C or nakon Å¡to se izaÄ‘e iz switch-a, u meÄ‘uvremenu u cHolder ovde pravimo i kolo i povezujemo ga
				myCTempData[0] = "lvl2CHelper" + helperNo++;
				myCTempData[1] = "2"; // uvek ima dva ulazna porta

				lvl2CHelper.init(myCTempData);
				StaticInitializer.allComponents.put(lvl2CHelper.getName(), lvl2CHelper);
				StaticInitializer.allSignals.get(cSignals.get(f.typeOfFlag)).connect(lvl2CHelper.getPort(0)); // na jednom je C/ALU signal (vidi cSignals mapu gore)
				gc.exec3Flags.add(lvl2CHelper); // dodaje se za iscrtavanje
				if (f.numberOfSignals > 1) { // ako je vise od jednog signala inicijalizujemo pomoÄ‡no kolo koje skuplja sve signale operacija
					Or lvl3CHelper = new Or();
					myCTempData[0] = "lvl3CHelper" + helperNo++;
					myCTempData[1] = Integer.toString(f.listOfStrings.size());
					lvl3CHelper.init(myCTempData);
					gc.exec3Flags.add(lvl3CHelper); // dodajemo za crtanje
					StaticInitializer.allComponents.put(lvl3CHelper.getName(), lvl3CHelper);
					for (int i = 0; i < f.numberOfSignals; i++)
						StaticInitializer.allSignals.get(f.listOfStrings.get(i)).connect(lvl3CHelper.getPortIN(i));
					// inicijalizujemo pomocno kolo koje skuplja sve signale operacija
					lvl3CHelper.getPortOUT().connect(lvl2CHelper.getPortIN(1)); // poveÅ¾i sa prethodnim nivoom
				} else
					StaticInitializer.allSignals.get(f.listOfStrings.get(0)).connect(lvl2CHelper.getPortIN(1)); // inaÄ?e prosto dodamo signal
				cHolder.add(lvl2CHelper);
				break;
			// flegovi za V kolo
			case "flagsV": // ovo takoÄ‘e ide viÅ¡e puta
				// pravimo novo kolo, uvek sa dva ulaza
				And lvl2VHelper = new And();
				myVTempData[0] = "lvl2VHelper" + helperNo++;
				myVTempData[1] = "2";
				lvl2VHelper.init(myVTempData);
				StaticInitializer.allSignals.get(f.listOfStrings.get(0)).connect(lvl2VHelper.getPort(0));
				gc.exec3Flags.add(lvl2VHelper);
				StaticInitializer.allComponents.put(lvl2VHelper.getName(), lvl2VHelper);
				// pravimo novo kolo, uvek sa dva ulaza

				// uzimamo pomoÄ‡ne promenljive - broj potrebnih kola
				f.listOfStrings.remove(0); // vadimo kod operacije
				int numberOfCircuitsNeeded = f.listOfStrings.size();
				// uzimamo pomoÄ‡ne promenljive - broj potrebnih kola

				// pravimo OR kolo
				Or lvl3VHelper = new Or();
				myVTempData[0] = "lvl3VHelper" + helperNo++;
				myVTempData[1] = Integer.toString(numberOfCircuitsNeeded); // koje ima onoliko ulaza koliko ima razliÄ?itih IBUS-IBUS-ALUOUT konfiguracija
				lvl3VHelper.init(myVTempData);
				gc.exec3Flags.add(lvl3VHelper);
				StaticInitializer.allComponents.put(lvl3VHelper.getName(), lvl3VHelper);
				// pravimo OR kolo
				for (int i = 0; i < numberOfCircuitsNeeded; i++) { // onoliko puta koliko treba razliÄ?itih konfiguracija
					// pravimo and kolo sa uvek 3 ulaza
					And lvl4VHelper = new And();
					myVTempData[0] = "lvl4VHelper" + helperNo++;
					myVTempData[1] = "3";
					lvl4VHelper.init(myVTempData);
					StaticInitializer.allComponents.put(lvl4VHelper.getName(), lvl4VHelper);
					gc.exec3Flags.add(lvl4VHelper);
					// pravimo and kolo sa uvek 3 ulaza
					int helperInt = Integer.parseInt(f.listOfStrings.get(i)); // uzimamo decimalnu vrednost i gledamo bite 0, 1 i 2
					if ((helperInt & 1) > 0)
						StaticInitializer.allSignals.get("IBUS1_31").connect(lvl4VHelper.getPortIN(2)); // A
					else
						StaticInitializer.allSignals.get("!IBUS1_31").connect(lvl4VHelper.getPortIN(2)); // A
					if ((helperInt & 2) > 0)
						StaticInitializer.allSignals.get("IBUS2_31").connect(lvl4VHelper.getPortIN(1)); // B
					else
						StaticInitializer.allSignals.get("!IBUS2_31").connect(lvl4VHelper.getPortIN(1)); // B
					if ((helperInt & 4) > 0)
						StaticInitializer.allSignals.get("ALU31").connect(lvl4VHelper.getPortIN(0)); // ALU
					else
						StaticInitializer.allSignals.get("!ALU31").connect(lvl4VHelper.getPortIN(0)); // ALU
					lvl4VHelper.getPortOUT().connect(lvl3VHelper.getPortIN(i)); // povezujemo prethodna dva kola
				}
				lvl3VHelper.getPortOUT().connect(lvl2VHelper.getPortIN(1)); // povezujemo prethodna dva kola
				vHolder.add(lvl2VHelper); // i dodajemo glavno na vHolder
				break;
			// flegovi za V kolo
			} // kraj switch-a
		}

		// ubaci c i v kola na kraju petlje
		String[] myTempData = new String[3];
		myTempData[2] = "1"; // debljina signala
		myTempData[0] = "C";
		myTempData[1] = Integer.toString(cHolder.size());
		C.init(myTempData); // init C
		myTempData[0] = "V";
		myTempData[1] = Integer.toString(vHolder.size());
		V.init(myTempData); // i V
		// ubaci prethodno napravljena c i v pomoÄ‡na kola (ranije nepoznata
		// veliÄ?ina)
		for (int i = 0; i < cHolder.size(); i++)
			cHolder.get(i).getPortOUT().connect(C.getPort(i));
		for (int i = 0; i < vHolder.size(); i++)
			vHolder.get(i).getPortOUT().connect(V.getPort(i));
		// ubaci prethodno napravljena c i v pomoÄ‡na kola (ranije nepoznata
		// veliÄ?ina)
		StaticInitializer.allComponents.put("C", C);
		StaticInitializer.allComponents.put("V", V);
		StaticInitializer.allSignals.put("C", C.getPortOUT());
		StaticInitializer.allSignals.put("V", V.getPortOUT());
		gc.exec3Flags.add(C);
		gc.exec3Flags.add(V);
		// ubaci c i v kola na kraju petlje
	}

	private void initDecoders() {
		String myData[] = { "", "" };
		for (DecoderDynamic d : allDecoders) { // podaci u svim dekoderima
			DecoderE dcdr = new DecoderE();
			gc.fetch2DCs.add(dcdr); // za crtanje
			myData[0] = d.name;
			myData[1] = Integer.toString(d.inputSignals.size());
			dcdr.init(myData);
			for (int i = 0; i < d.inputSignals.size(); i++)
				StaticInitializer.allSignals.get(d.inputSignals.get(i)).connect(dcdr.getPortIN(i)); // poveÅ¾i portove
			StaticInitializer.allSignals.get(d.enableSignal).connect(dcdr.getPortE());
			for (int i = 0; i < d.outputSignals.size(); i++)
				StaticInitializer.allSignals.put(d.outputSignals.get(i), dcdr.getPortOUT(i)); // ubaci izlaze dekodera
			StaticInitializer.allComponents.put(dcdr.getName(), dcdr); // i sam dekoder
		}
		
		ArrayList<String> instructions = new ArrayList<String>();
		for (DecoderDynamic d : allDecoders)
			if (d.isInstruction)
				for (String s : d.outputSignals)
					if (!s.contains("ERR"))
						instructions.add(s);
		if (!instructions.isEmpty()) {
			FETCH1.ERRNOR = new Nor();
			String[] myDCData = { "ERRDCDR", Integer.toString(instructions.size()), "1" };
			FETCH1.ERRNOR.init(myDCData);
			gc.fetch3Circuits.add(FETCH1.ERRNOR);
			StaticInitializer.allComponents.put(FETCH1.ERRNOR.getName(), FETCH1.ERRNOR);
			StaticInitializer.allSignals.put("gropr", FETCH1.ERRNOR.getPortOUT());
			for (int i = 0; i < instructions.size(); i++)
				StaticInitializer.allSignals.get(instructions.get(i)).connect(FETCH1.ERRNOR.getPortIN(i));
		}
		else
			StaticInitializer.allSignals.put("gropr", StaticInitializer.allSignals.get("0"));
		
		Not negateIRET = new Not();
		String[] myRData = { "!IRET", "1" };
		negateIRET.init(myRData);
		StaticInitializer.allSignals.get("IRET").connect(negateIRET.getPortIN());
		StaticInitializer.allSignals.put("!IRET", negateIRET.getPortOUT());
		StaticInitializer.allComponents.put(negateIRET.getName(), negateIRET);
	}

	private void initLogicCircuits() { // obiÄ?na ne-flegovska logiÄ?ka kola
		String myData[] = { "", "", "1" };
		for (LogicCircuitDynamic lc : allLogicCircuits) {
			OneOutputGate circ = null;
			myData[0] = lc.logicComponentName;
			myData[1] = Integer.toString(lc.inputSignals.size());
			switch (lc.logicComponentType) { // odaberi logiÄ?ki tip
			case "AND":
				circ = new And(); break;
			case "OR":
				circ = new Or(); break;
			case "XOR":
				circ = new Xor(); break;
			case "NAND":
				circ = new Nand(); break;
			case "NOR":
				circ = new Nor(); break;
			case "XNOR":
				circ = new Nxor(); break;
			case "NOT":
				circ = new Not(); break;
			}
			circ.init(myData);
			gc.fetch3Circuits.add(circ); // dodaj za iscrtavanje
			StaticInitializer.allComponents.put(circ.getName(), circ); // i ubaci izlazni port
			for (int i = 0; i < lc.inputSignals.size(); i++) {
				StaticInitializer.allSignals.get(lc.inputSignals.get(i)).connect(circ.getPort(i)); // ubaci traÅ¾ene signale
			}
			StaticInitializer.allSignals.put(lc.outputSignalName, circ.getPort(circ.getPorts().size() - 1)); // i ubaci izlazni port
		}
	}

	private void initBuffers() {
		String myData[] = { "", "" };
		for (BufferDynamic myBuffer : allBuffers) {
			TriStateBuffer myBuf = new TriStateBuffer();
			gc.addr1Buffers.add(myBuf);
			myData[0] = myBuffer.bufferName;
			myData[1] = "32";
			myBuf.init(myData);
			if (myBuffer.isIR) {
				// create helper signal merger
				SignalMergerSimple helper = new SignalMergerSimple(); // ovo treba da spaja signale bafera kako bi uÅ¡ao u ulaz
				myData[0] = "bufferHelper" + allBuffers.indexOf(myBuffer); // ime preko indeksa iteriranja
				myData[1] = "32";
				helper.init(myData);
				for (int i = 0; i < 32; i++)
					if (i >= myBuffer.bufferBitLength && i < myBuffer.bufferBitLength + myBuffer.value)
						StaticInitializer.allSignals.get("IR" + i).connect(helper.getPort(i)); // poveÅ¾i se sa IRBUS bitovima
					else
						StaticInitializer.allSignals.get("0").connect(helper.getPort(i)); // poveÅ¾i se sa IRBUS bitovima
				//connect buffer
				helper.getPortOUT().connect(myBuf.getPortIN());
				StaticInitializer.allComponents.put(helper.getName(), helper);
				StaticInitializer.allSignals.put(helper.getName(), helper.getPortOUT());
			}
			else {
				myBuf.getPortIN().setDefaultValue(myBuffer.value);
			}
			StaticInitializer.allSignals.get(myBuffer.bufferActivationSignal).connect(myBuf.getPortC());
			myBuf.getPortOUT().connect(StaticInitializer.allSignals.get(myBuffer.bufferOutBus));
			StaticInitializer.allSignals.put(myBuf.getName() + allBuffers.indexOf(myBuffer), myBuf.getPortOUT());
			StaticInitializer.allSignals.put(myBuf.getName(), myBuf.getPortOUT());
		}
	}

	private void initJumps() {
		for (JumpDynamic jd : allJumps) {
			if (OrganizationParameters.JumpConditionToDecoderEntry.size() > 32)
				return; // pun dekoder
			++GraphingContainer.howManyDynamicJumps;
			if (!jd.isInverted) // ako nije inverted
				OrganizationParameters.JumpConditionToDecoderEntry.put(jd.signalName, jd.jumpValue);
			else if (StaticInitializer.allSignals.containsKey("!" + jd.signalName)) // jeste inv, i definisan
				OrganizationParameters.JumpConditionToDecoderEntry.put("!" + jd.signalName, jd.jumpValue);
			else { // ako nema invertovanog signala
				Not helperNot = new Not();
				String[] myData = { "helperNotJump" + jd.signalName,
						Integer.toString(StaticInitializer.allSignals.get(jd.signalName).getSize()) };
				helperNot.init(myData);
				StaticInitializer.allSignals.get(jd.signalName).connect(helperNot.getPortIN());
				StaticInitializer.allSignals.put("!" + jd.signalName, helperNot.getPortOUT());
				StaticInitializer.allComponents.put(helperNot.getName(), helperNot);
				OrganizationParameters.JumpConditionToDecoderEntry.put("!" + jd.signalName, jd.jumpValue);
			}
		}
		// init DC4 i AND
		String myData[] = { "ANDDynamicJMP", Integer.toString(allJumps.size()), "1" };
		if ("0".equals(myData[1]))
			myData[1] = "1";
		UPRSIG.UPRbrnchDynamic.init(myData);
		myData[0] = "DC4Dynamic";
		myData[1] = "4";
		UPRSIG.DC4Dynamic.init(myData);
		for (int i = 0; i < 4; i++)
			StaticInitializer.allSignals.get("CW" + (i + 88)).connect(UPRSIG.DC4Dynamic.getPortIN(i));
		StaticInitializer.allSignals.get("CW92").connect(UPRSIG.DC4Dynamic.getPortE());
		StaticInitializer.allSignals.put("branch2", UPRSIG.UPRbrnchDynamic.getPortOUT());

		for (JumpDynamic jd : allJumps) {
			And helperAnd = new And();
			String myJDData[] = { "helperAndDynamicJump" + GraphingContainer.howManyDynamicJumps, "2", "1" };
			helperAnd.init(myJDData);
			StaticInitializer.allComponents.put(helperAnd.getName(), helperAnd);
			UPRSIG.DC4Dynamic.getPort(GraphingContainer.howManyDynamicJumps).connect(helperAnd.getPort(0));
			if (jd.isInverted)
				StaticInitializer.allSignals.get("!" + jd.signalName).connect(helperAnd.getPort(1));
			else
				StaticInitializer.allSignals.get(jd.signalName).connect(helperAnd.getPort(1));
			helperAnd.getPortOUT().connect(UPRSIG.UPRbrnchDynamic.getPortIN(GraphingContainer.howManyDynamicJumps));
			gc.jumpData.add(gc.new JumpData(jd, UPRSIG.DC4Dynamic.getPort(2), helperAnd.getPortOUT()));
			GraphingContainer.howManyDynamicJumps++;
		}
	}
};