package simulator2.dynamicinit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public final class NetlistMicrocode {

	private Scanner scanner;
	private ArrayList<Microword> microwords = new ArrayList<Microword>();
	public static ArrayList<String> stringMicrowords = new ArrayList<String>();

	public NetlistMicrocode() throws FileNotFoundException {
		microwords = new ArrayList<Microword>();
		scanner = new Scanner(new File("src/simulator2/dynamicinit/microcode.txt"));
	}

	public ArrayList<Microword> getMicrowords() {
		return microwords;
	}

	public ArrayList<String> getStringMicrowords() {
		return stringMicrowords;
	}

	private void simpleBranch(Microword mw, String process) {
		Scanner temp = new Scanner(process);
		String s = null;
		s = temp.next(); // br, odbacuje se
		s = temp.next();
		mw.setBA(s); // podesi BA
		mw.setCC("br"); // i default skok za bezuslovni
		temp.close();
	}

	private void conditionBranch(Microword mw, String process) {
		Scanner temp = new Scanner(process);
		String s = null;
		s = temp.next(); // br, odbacuje se
		s = temp.next(); // (if, odbacuje se
		s = temp.next();
		mw.setCC(s); // condition, postavi
		s = temp.next(); // then, odbaci
		String hold = temp.next(); // sama adresa );
		hold = hold.substring(0, hold.indexOf(')')); // isecimo produžetak
		mw.setBA(hold); // i postavimo
		temp.close();
	}

	private void operationBranch(Microword mw) {
		// mw.setBA(s); automatski load
		mw.setCC("bropr");
	}

	public void process() {
		while (scanner.hasNextLine()) {
			String toBeTrimmed = scanner.nextLine();
			if ("".equals(toBeTrimmed) || toBeTrimmed.startsWith("!"))
				continue; // ako prazan string ili ime operacije sledeća linija
			stringMicrowords.add(new String(toBeTrimmed));
			toBeTrimmed = toBeTrimmed.substring(0, toBeTrimmed.indexOf(";")); // iseci
																				// komentare
			toBeTrimmed.trim();
			toBeTrimmed = toBeTrimmed.substring(7); // izbaci madr00
			Scanner deepScanner = new Scanner(toBeTrimmed);
			deepScanner.useDelimiter(", "); // ovo pod navodnicima nam uzima re�?
			String singleSignal; // naš jedan signal (ili naredba)
			Microword singleMicroword = new Microword(); // prazna mikrore�?
			while (deepScanner.hasNext()) {
				singleSignal = deepScanner.next(); // pro�?itaj signal ili
													// naredbu
				if (singleSignal.contains("br madr") || singleSignal.contains("if ") || "bropr".equals(singleSignal)) { // ako
																														// naredba
					if ("bropr".equals(singleSignal))
						operationBranch(singleMicroword);
					else if (singleSignal.contains("if ")) // vidi vrstu
						conditionBranch(singleMicroword, singleSignal);
					else
						simpleBranch(singleMicroword, singleSignal);
					break; // za svaki slucaj
				} else
					singleMicroword.setZ(singleSignal); // postavi bit tog
														// signala
			}
			deepScanner.close();
			microwords.add(singleMicroword); // i dodaj u vektor mikrore�?i
												// (poredak o�?uvan redosledom
												// dodavanja)
		}
		scanner.close();
	}

}