package simulator2.dynamicinit;

import java.util.ArrayList;

import simulator2.Signal;
import simulator2.digitalcomponents.DecoderE;
import simulator2.digitalcomponents.OneOutputGate;
import simulator2.digitalcomponents.TriStateBuffer;
import simulator2.dynamicinit.NetlistDynamic.JumpDynamic;

public final class GraphingContainer {

	public class JumpData {
		public JumpDynamic jd;
		public Signal dcSignal;
		public Signal andOut;

		public JumpData(JumpDynamic jd, Signal dcSignal, Signal andOut) {
			this.jd = jd;
			this.dcSignal = dcSignal;
			this.andOut = andOut;
		}
	}

	public static Integer howManyDynamicJumps;

	static {
		howManyDynamicJumps = 0;
	}

	public final ArrayList<JumpData> jumpData;
	public final ArrayList<DecoderE> fetch2DCs;
	public final ArrayList<OneOutputGate> fetch3Circuits;
	public final ArrayList<OneOutputGate> exec3Flags;
	public final ArrayList<TriStateBuffer> addr1Buffers;

	public GraphingContainer() {
		jumpData = new ArrayList<JumpData>();
		fetch3Circuits = new ArrayList<OneOutputGate>();
		fetch2DCs = new ArrayList<DecoderE>();
		exec3Flags = new ArrayList<OneOutputGate>();
		addr1Buffers = new ArrayList<TriStateBuffer>();
	}
};