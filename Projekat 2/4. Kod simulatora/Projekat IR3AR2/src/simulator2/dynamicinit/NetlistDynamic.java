package simulator2.dynamicinit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public final class NetlistDynamic {

	// data structures
	public final class FlagDynamic {
		public String typeOfFlag; // flagCC16, flagC!C16, flagCAB0, flagCAB16,
									// flagV!ALU!A!B, flagV!ALU!AB,
									// flagV!ALUA!B, flagV!ALUAB, flagVALU!A!B,
									// flagVALU!AB, flagVALUA!B, flagVALUAB
		public int numberOfSignals;
		public ArrayList<String> listOfStrings; // for
												// numberOfSignals(listOfStrings.size())
												// >= 2 we generate a new OR
												// circuit to be connected to
												// the flag generator

		public FlagDynamic(String typeOfFlag, int numberOfSignals, ArrayList<String> listOfStrings) {
			this.typeOfFlag = typeOfFlag;
			this.numberOfSignals = numberOfSignals;
			this.listOfStrings = listOfStrings;
		}
	}

	public final class DecoderDynamic {
		public String name;
		public String enableSignal;
		public ArrayList<String> inputSignals; // inputSignals.size ==
												// numberOfPorts
		public ArrayList<String> outputSignals;
		public Boolean isInstruction;

		public DecoderDynamic(Boolean isTrue, String name, String enableSignal, ArrayList<String> inputSignals,
				ArrayList<String> outputSignals) {
			this.isInstruction = isTrue;
			this.name = name;
			this.enableSignal = enableSignal;
			this.inputSignals = inputSignals;
			this.outputSignals = outputSignals;
		}
	}

	public final class LogicCircuitDynamic {
		String logicComponentName; // name of the component
		String logicComponentType; // and, or, xnor, ...
		public ArrayList<String> inputSignals; // its input signals
		String outputSignalName; // name of output signal that will be generated

		public LogicCircuitDynamic(String logicComponentName, String logicComponentType, ArrayList<String> inputSignals,
				String outputSignalName) {
			super();
			this.logicComponentName = logicComponentName;
			this.logicComponentType = logicComponentType;
			this.inputSignals = inputSignals;
			this.outputSignalName = outputSignalName;
		}
	}

	public final class BufferDynamic {
		public boolean isIR = false;
		public String bufferName;
		public String bufferActivationSignal; // C signal
		public String bufferOutBus; // where to go
		public int bufferBitLength; // how much is it
		public int value;

		public BufferDynamic(boolean isIR, String bufferName, String bufferActivationSignal, String bufferOutBus, int bufferBitLength,
				int value) {
			super();
			this.isIR = isIR; 
			this.bufferName = bufferName;
			this.bufferActivationSignal = bufferActivationSignal;
			this.bufferOutBus = bufferOutBus;
			this.bufferBitLength = bufferBitLength;
			this.value = value;
		}
	}

	public final class JumpDynamic {
		public String signalName;
		public Integer jumpValue; // where to jump to
		public Boolean isInverted;

		public JumpDynamic(String signalName, Integer jumpValue, Boolean isInverted) {
			this.signalName = signalName;
			this.jumpValue = jumpValue;
			this.isInverted = isInverted;
		}
	}
	// data structures

	private Scanner scanner = null, deepScanner = null;
	private DrawablesContainer dc = null; // dc contains gc

	private int timeDelay = 0;

	ArrayList<FlagDynamic> allFlags = null;
	ArrayList<DecoderDynamic> allDecoders = null;
	ArrayList<LogicCircuitDynamic> allLogicCircuits = null;
	ArrayList<BufferDynamic> allBuffers = null;
	ArrayList<JumpDynamic> allJumps = null;

	public NetlistDynamic() {
		allFlags = new ArrayList<FlagDynamic>();
		allDecoders = new ArrayList<DecoderDynamic>();
		allLogicCircuits = new ArrayList<LogicCircuitDynamic>();
		allBuffers = new ArrayList<BufferDynamic>();
		allJumps = new ArrayList<JumpDynamic>();
	}

	public void init() throws FileNotFoundException {
		scanner = new Scanner(new File("src/simulator2/dynamicinit/config.txt")); // start
																					// scanning

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine(); // get next line
			if ("".equals(line) || line.startsWith("//")) // if empty line or
															// comment
				continue; // next line (if there is one)
			deepScanner = new Scanner(line); // or start reading word by word
			String word = deepScanner.next();
			switch (word) {
			case "timeLength": // decode word and jump to appropriate function
				timeDelay = Integer.parseInt(deepScanner.next());
				break;
			case "flagSet":
				doFlagSet(allFlags);
				break;
			case "decoder":
				doDecoders(false, allDecoders);
				break;
			case "decoderINS":
				doDecoders(true, allDecoders);
				break;
			case "logic":
				doLogicCircuits(allLogicCircuits);
				break;
			case "bufferDECVAL":
				doAllBuffers(false, allBuffers);
				break;
			case "bufferIRVAL":
				doAllBuffers(true, allBuffers);
				break;
			case "jump":
				doAllJumps(allJumps);
				break;
			default:
				break; // error
			}
		}
		dc = new DrawablesContainer(timeDelay, allFlags, allDecoders, allLogicCircuits, allBuffers, allJumps);
		dc.init();

	}

	public final DrawablesContainer passDrawablesContainer() {
		return dc;
	}

	public final GraphingContainer passGraphingContainer() {
		return dc.gc;
	}

	public final ArrayList<JumpDynamic> passJumps() {
		return allJumps;
	}

	private void doFlagSet(ArrayList<FlagDynamic> allFlags) {
		String typeOfFlag = deepScanner.next(); // e.g. flagsNZ, flagsC, flagsV...
		int numberOfSignals = Integer.parseInt(deepScanner.next()); // dependent on context
		ArrayList<String> listOfSignals = new ArrayList<String>();
		for (int i = 0; i < numberOfSignals; i++) // for the following number of signals
			listOfSignals.add(deepScanner.next()); // read those signals
		allFlags.add(new FlagDynamic(typeOfFlag, numberOfSignals, listOfSignals));
	}

	private void doDecoders(Boolean isTrue, ArrayList<DecoderDynamic> allDecoders) {
		// String name, String enableSignal, ArrayList<String> inputSignals,
		// ArrayList<String> outputSignals
		String name = deepScanner.next();
		String enableSignal = deepScanner.next();
		int numberOfSignals = Integer.parseInt(deepScanner.next());
		ArrayList<String> inputSignals = new ArrayList<String>();
		for (int i = 0; i < numberOfSignals; i++)
			inputSignals.add(deepScanner.next()); // these signals should
													// already exist
		numberOfSignals = (int) Math.pow(2, numberOfSignals); // 2^numberOfSignals
		ArrayList<String> outputSignals = new ArrayList<String>();
		for (int i = 0; i < numberOfSignals; i++)
			outputSignals.add(deepScanner.next()); // these signals will be
													// created under the
													// specified names
		allDecoders.add(new DecoderDynamic(isTrue, name, enableSignal, inputSignals, outputSignals));
	}

	private void doLogicCircuits(ArrayList<LogicCircuitDynamic> allLogicCircuits) {
		// simple logical components: and, xnor, ...
		String logicComponentName = deepScanner.next();
		String outputSignalName = deepScanner.next();
		String componentType = deepScanner.next(); // caps e.g XNOR
		ArrayList<String> inputSignals = new ArrayList<String>();
		int numberOfSignals = Integer.parseInt(deepScanner.next());
		for (int i = 0; i < numberOfSignals; i++)
			inputSignals.add(deepScanner.next());
		allLogicCircuits
				.add(new LogicCircuitDynamic(logicComponentName, componentType, inputSignals, outputSignalName));
	}

	private void doAllBuffers(boolean isIR, ArrayList<BufferDynamic> allBuffers) {
		String bufferName = deepScanner.next();
		String bufferActivationSignal = deepScanner.next();
		String bufferOutBUS = deepScanner.next();
		int bufferBitLength = Integer.parseInt(deepScanner.next());
		int value = Integer.parseInt(deepScanner.next());
		allBuffers.add(new BufferDynamic(isIR, bufferName, bufferActivationSignal, bufferOutBUS, bufferBitLength, value));
	}

	private void doAllJumps(ArrayList<JumpDynamic> allJumps) {
		String name = deepScanner.next();
		Integer position = deepScanner.nextInt();
		Boolean isTrue = deepScanner.nextBoolean();
		allJumps.add(new JumpDynamic(name, position, isTrue));
	}
}
