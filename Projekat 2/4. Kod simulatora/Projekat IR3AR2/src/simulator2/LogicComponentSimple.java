
package simulator2;

import java.util.*;

public abstract class LogicComponentSimple implements LogicComponent {
	private static final long serialVersionUID = 1L;
	protected static long lastId = 0;
	protected long id;
	protected String name;
	protected List<Port> ports;


	protected transient List<Event> events;

	public LogicComponentSimple() {
		events = new LinkedList<Event>();
		id = lastId++;
		ports = new LinkedList<Port>();
	}

	public void initializeEvents() {
		events = new LinkedList<Event>();
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public List<Port> getPorts() {
		return ports;
	}

	@Override
	public void setPorts(List<Port> ports) {
		this.ports = new LinkedList<Port>(ports);
	}

	@Override
	public Port getPort(int index) {
		return ports.get(index);
	}

	@Override
	public void setPort(int index, Port port) {
		ports.set(index, port);
	}

	@Override
	public LogicComponentSimple invertPort(int index) {
		if (ports.size() <= index)
			return null;

		Port port = ports.get(index);
		int portType = port.getType();
		int size = port.getSize();
		Value value = port.getValue();
		String name = port.getName();

		Port newPort = new PortSimpleNegated(portType, size, value, name, index, this);
		ports.set(index, newPort);
		newPort.setDefaultValue(0xffffffff);
		return this;
	}

	@Override
	public Port findPort(String name) {
		for (Port port : ports) {
			if (port.getName().equals(name)) {
				return port;
			}
		}
		return null;
	}

	@Override
	public List<Port> findPorts(int portType) {
		List<Port> result = new LinkedList<Port>();
		for (Port port : ports) {
			if (port.getType() == portType) {
				result.add(port);
			}
		}
		return result;
	}

	@Override
	public List<Port> findUnconnectedPorts(int type) {
		List<Port> result = new LinkedList<Port>();
		for (Port port : ports) {
			if ((port.getType() == type) && (!port.isConnected())) {
				result.add(port);
			}
		}
		return result;
	}

	@Override
	public void setSignals(List<Signal> signals) {
		int min = (signals.size() < ports.size()) ? signals.size() : ports.size();

		for (int i = 0; i < min; i++) {
			ports.get(i).connectBoth(signals.get(i));
		}
	}

	@Override
	public void setSignal(Signal signal, int index) {
		ports.get(index).connect(signal);
		signal.connect(ports.get(index));
	}

	@Override
	public int getOutValue() {
		return 0;
	}

	@Override
	public void setOutValue(int val) {
	}

	@Override
	public int getMemValue(int i) {
		return 0;
	}

	@Override
	public void setMemValue(int i, int x) {
	}

	@Override
	public void resetStateCLK() {
	}

	@Override
	public String toString() {
		String result = "";
		result += this.getClass().getName() + " " + name + " " + id;
		return result;
	}

	@Override
	public List<Event> preCalculateEvents() {
		for (Port port : ports) {
			port.preCalculateEvents();
		}

		Event tempEvent;
		for (Port port : ports) {
			if (port.canGenerate()) {
				tempEvent = new Event(0, new DigitalValue(DigitalValue.ZERO, port.getSize()), port, this);
				port.execute(tempEvent);
			}
		}

		return execute(new Event(0, new DigitalValue(), this, this));
	}

	@Override
	public void resetState() {
		for (Port port : ports)
			port.resetState();
	}

}
