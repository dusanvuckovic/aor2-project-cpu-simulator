package simulator2;

import java.util.List;

public class PortSimpleNegated extends PortSimple {

	private static final long serialVersionUID = 1L;

	public PortSimpleNegated(int portType, Value value, String name, int portNumber, LogicComponent component) {
		super(portType, value, name, portNumber, component);
		this.value.setIntValue(~this.value.getIntValue());
	}

	public PortSimpleNegated(int portType, int size, Value value, String name, int portNumber, LogicComponent component) {
		super(portType, size, value, name, portNumber, component);
		this.value.setIntValue(~this.value.getIntValue());
	}

	@Override
	public List<Event> execute(Event event) {
		List<Event> result = events;
		result.clear();

		Value newVal = new DigitalValue(DigitalValue.ZERO, value.getSize());
		newVal.setHighZ(event.getValue().getHighZ());
		newVal.setIntValue(event.getValue().getIntValue());

		Value newValNeg = new DigitalValue(DigitalValue.ZERO, value.getSize());
		newVal.setHighZ(event.getValue().getHighZ()); // TODO: see what to do in case of highZ
		newVal.setIntValue(~event.getValue().getIntValue());

		if (!value.isEqual(newValNeg)) {
			value.load(newValNeg);

			if (canConduct()) { // Sends the value to its component.
				Event tempEvent = new Event(event.getTime(), value, component, this);
				result.add(tempEvent);
			}
		}

		EventExecutable src = event.getSrc();
		for (Signal signal : connectedSignals) { // Sends the value to all
													// connected signals.
			if (signal != src) {
				Event tempEvent = new Event(event.getTime(), newVal, signal, this);
				result.add(tempEvent);
			}
		}

		return result;
	}

}
